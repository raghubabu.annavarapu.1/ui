FROM node:14.16.1
WORKDIR /app
COPY package.json .
RUN node -v && npm -v
RUN apt update && apt upgrade -y
#RUN apt install build-essential -y
RUN npm install 
#RUN npm install pm2@latest -g
COPY . .
EXPOSE 4000
#RUN npm run build:dev
#RUN npm install -g serve
CMD ["node","server.js"]

