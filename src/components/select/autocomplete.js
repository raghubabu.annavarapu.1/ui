import * as React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

export default function AutoSelect(props) {
  return (
    <Autocomplete
        id="select-on-focus"
        disableClearable
        options={props.items}
        selectOnFocus
        style={{width: 320}}
        renderInput={(params) => (
          <TextField {...params} label={props.selectLabel} variant="standard" 
          style={{ fontSize: 14, color: "#366E93", fontWeight: 400, backgroundColor: '#FFFFFF'}}/>
        )}
        renderOption={(props, option, { selected }) => 
      {
      return (
        <li {...props}
        style={{ fontSize: 14, color: "#366E93", fontWeight: 'bold', padding: 5}}>
          {option}
        </li>
      )
      }}
      onChange={(event, value) => props.onSelect(value)}
      defaultValue={props.items[props.index]}
      />
      
  );
}
