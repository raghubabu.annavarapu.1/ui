import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { useHistory } from 'react-router-dom';
export default function SimpleMenu() {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (path) => {
        setAnchorEl(null);

    };
    const handleMenuItem = (path) => {
        // setAnchorEl(null);
        let history = useHistory();
        history.push(path)
    };
    return (
        <div>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} style={{ color: '#fff' }}>
                Open Menu
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={handleMenuItem('./Reports')}>Profile</MenuItem>
                <MenuItem onClick={handleMenuItem('./Reports')}>My account</MenuItem>
                <MenuItem onClick={handleMenuItem('./Reports')}>Logout</MenuItem>
            </Menu>
        </div>
    );
}