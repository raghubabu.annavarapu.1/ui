import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 250,
    },
}));
export default function DateAndTimePicker() {
    const classes = useStyles();
    return (
        <form className={classes.container} noValidate>
            <TextField
                id="datetime-local"
                label=""
                type="datetime-local"
                defaultValue={"2017-05-24T07:30"}
                className={classes.textField}
                InputLabelProps={{
                    shrink: true,
                }}
            />
        </form>
    );
}
