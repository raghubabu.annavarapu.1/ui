import { Toolbar, AppBar, Typography, IconButton, Paper, TextField, Divider, Button, CssBaseline } from '@material-ui/core';
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import suvega from '../assets/images/suvega.png';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import man from '../assets/images/man.png';
import truck from '../assets/images/truck.png';
import SearchIcon from '@material-ui/icons/Search';
import GoogleMapReact from 'google-map-react';
import { useHistory } from 'react-router-dom';
import SimpleMenu from './Menu';
const useStyles = makeStyles((theme) => ({
    appBar: {
        backgroundColor: '#366E93',
    },
    grow: {
        flexGrow: '1',
        marginLeft: theme.spacing(2),
    },
    image: {
        width: '140px',
        marginRight: theme.spacing(8),
    },
    name: {
        marginRight: theme.spacing(1),
        fontSize: '0.9rem',
    },
    content: {
        display: 'flex'
    },
    list: {
        color: '#366E93',
        fontWeight: 'bold',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '10px',
    },
    middle: {
        display: 'flex',
        flexGrow: '1'
    },
    menu: {
        display: 'flex',
        marginRight: theme.spacing(5),
        color: 'white'
    }
}));
const AnyReactComponent = ({ text }) => <div>{text}</div>;
export default function Map() {
    const classes = useStyles();
    let history = useHistory();
    const [auth, setAuth] = React.useState(true);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [userName, setUserName] = React.useState(sessionStorage.getItem('profile') ? sessionStorage.getItem('profile').name : 'John Doe');
    const open = Boolean(anchorEl);
    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = (event) => {
        setAnchorEl(null);
    };
    const handleLogout = (event) => {
        sessionStorage.removeItem("accessToken");
        sessionStorage.removeItem("authId");
        sessionStorage.removeItem("profile");
        window.location = '/';
    }
    const handleOpen = (event) => {
    }
    const menuOpen = (event) => { }
    const vehicles =
        ["AP07TG7439", "AP16TC5778", "AP07TB6588", "AP07TE6378", "AP07TC1289", "AP16TG2835", "AP16TH1467", "mh43y0445", "GJ03AT0668", "mho4gc606", "AP07TB2898", "AP07TW1899", "AP27TY1519", "AP16TG2835", "AP16TH1467", "mh43y0445", "GJ03AT0668", "mho4gc606", "AP07TB2898", "AP07TW1899", "AP27TY1519"]
    const [list, setSearch] = useState(vehicles)
    const onSearch = (event) => {
        // console.log(event)
        let filteredSearch = vehicles.filter(x => {
            return x.toLowerCase().includes(event.target.value)
        })
        setSearch(filteredSearch);
    };
    const [color, setColor] = useState('#366E93')
    function customMe() {
        setColor('black')
    }
    const defaultProps = {
        center: {
            lat: 1.99835602,
            lng: 7.01502627
        },
        zoom: 7
    };
    const winWidth = window.innerWidth;
    const winHeight = window.innerHeight;
    console.log("useWindowDimensions", winWidth, winHeight);
    return (
        <div>
            <div>
                <CssBaseline />
                <AppBar position="static" className={classes.appBar}>
                    <Toolbar>
                        <img src={suvega} className={classes.image} />
                        <SimpleMenu />
                        <div className={classes.grow} />
                        <Typography className={classes.name}>{userName}</Typography>
                        {auth && (
                            <div>
                                <IconButton
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={handleMenu}
                                    color="inherit"
                                ><img src={man} onClick={handleOpen} />
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={open}
                                    onClose={handleClose}
                                >
                                    <MenuItem onClick={handleLogout} >Logout</MenuItem>
                                </Menu>
                            </div>
                        )}
                    </Toolbar>
                </AppBar>
            </div >
            <div className={classes.content}>
                <div>
                    <Paper style={{ width: '100%', height: winHeight, overflow: 'auto' }}>
                        <div style={{ borderColor: 'red' }}>
                            <TextField style={{ padding: '10px', width: '200px' }} variant="outlined" placeholder="Search by Vehicle Number"
                                inputProps={{ style: { fontSize: 11, color: '#174A84' } }}
                                onChange={onSearch}
                                InputProps={{
                                    endAdornment: (
                                        <SearchIcon style={{ color: '#174A84', width: '17px' }} onChange={onSearch} />
                                    )
                                }} />
                        </div>
                        <Divider style={{ margin: '10px' }} />
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <img src={truck} style={{ width: '20px', height: '20px', marginTop: '2px' }} />
                            <Typography style={{ color: '#366E93', fontWeight: 'bold', marginLeft: '10px' }}>All vehicles</Typography>
                        </div>
                        <div>
                            {list.map(x =>
                                <li className={classes.list}>
                                    <Button style={{ color: '#366E93', fontWeight: 'bold', borderLeft: '5px', borderColor: 'red' }} color={color} onClick={() => customMe()} fullWidth>
                                        {x}
                                    </Button>
                                </li>
                            )}
                        </div>
                    </Paper>
                </div>
                <div style={{ flexGrow: '1', backgroundColor: '#F4F7F9' }}>
                    <div className={classes.middle}>
                        <div style={{ color: '#366E93', fontWeight: 'bold', flexGrow: '1', padding: '20px' }}>
                            Owner Name, Lorem Ipsum is a dummy content
                        </div>
                        <div>
                            <Paper style={{ padding: '10px', margin: '10px' }}>
                                <form>
                                    <TextField style={{ borderBottom: 'none' }}
                                        id="datetime-local"
                                        type="datetime-local"
                                        className={classes.textField}
                                        KeyboardButtonProps={{ disabled: true }}
                                        InputProps={{ disableUnderline: true }} />
                                </form>
                                <form>
                                    <TextField style={{ disableUnderline: true, borderColor: 'red', fontWeight: 'bold' }}
                                        id="datetime-local"
                                        type="datetime-local"
                                        className={classes.textField}
                                        KeyboardButtonProps={{ disabled: true, style: { display: 'none' } }}
                                        InputProps={{ disableUnderline: true }}
                                    />
                                </form>
                            </Paper>
                        </div>
                    </div>
                    <div style={{ height: '65vh', width: '100%', marginLeft: '10px' }}>
                        <GoogleMapReact
                            bootstrapURLKeys={{ key: "" }}
                            defaultCenter={defaultProps.center}
                            defaultZoom={defaultProps.zoom}
                        >
                            <AnyReactComponent
                                lat={59.955413}
                                lng={30.337844}
                                text="My Marker"
                            />
                        </GoogleMapReact>
                    </div>
                </div>
            </div>
        </div>
    );
}


