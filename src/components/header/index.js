import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Toolbar, AppBar, Typography, IconButton, Menu, MenuItem } from "@material-ui/core";
import { suvegaImage as suvega, manImage as man, truckImage as truck } from "../../assets/index";
import AppMenu from "./menu";

const useStyles = makeStyles((theme) => ({
  appBar: {
    backgroundColor: "#366E93",
  },
  grow: {
    flexGrow: '1',
    marginLeft: theme.spacing(2),
  },
  image: {
    width: "120px",
    marginRight: theme.spacing(6),
  },
  name: {
    marginRight: theme.spacing(1),
    fontSize: "0.9rem",
  },
}));

export default function Header(props) {
  const classes = useStyles();
  const [auth, setAuth] = React.useState(true);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [userName, setUserName] = React.useState(null);
  const [profilepicBase64, setProfilepicBase64] = React.useState(null);
  const open = Boolean(anchorEl);
  useEffect(() => {
    if (global.session.get('profile')) {
      let userProfile = JSON.parse(global.session.get('profile'));
      if (userProfile) {
        setUserName(userProfile.name);
        if (userProfile.profile_pic) {
          getProfileImage(userProfile.profile_pic);
        }
      }
    }
  },[props.ownerId]);

  const getProfileImage = (imagePath) => {
    global.service.getFleetOwnerImage({ imagePath: imagePath })
      .then((response) => {
        // console.log("Response: ", response);
        if (response.status) {
          let base64string = response.result;
          setProfilepicBase64(base64string);
        } else {
          // console.log("unable to get profile pic base64 string");
        }
      })
      .catch((err) => {
        // console.log(err);
      });
  }

  const [menuPosition, setMenuPosition] = useState(null);
  const handleItemsClick = (event) => {
    if (menuPosition) {
      setAnchorEl(event.currentTarget);
      return;
    }
    event.preventDefault();
    setMenuPosition({
      top: event.pageY + 20,
      left: event.pageX - 40,
    });
  };
  const handleItemClick = (event) => {
    setMenuPosition(null);
  };
  const handleClose = (event) => {
    setAnchorEl(null);
  };
  const handleLogout = (event) => {
    global.session.remove("BearerToken");
    global.session.remove("profile");
    global.session.remove('hideHeader');
    window.location = "/";
  };
  const handleOpen = (event) => { };
  return (
    <div key={props.ownerId}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <img src={suvega} className={classes.image} />
          <AppMenu />
          <div className={classes.grow} />
          {auth && (
            <div style={{display: 'flex', alignItems: 'center'}}>
              {(window.innerWidth > 450) ?
                <Typography className={classes.name}>{userName}</Typography>
                : null
              }
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleItemsClick}
                color="inherit"
              >
                <img src={profilepicBase64 ? ("data:image/png;base64," + profilepicBase64) : man} onClick={handleOpen} style={{ width: 40, height: 40 }} />
              </IconButton>
              <Menu
                open={!!menuPosition}
                onClose={() => setMenuPosition(null)}
                anchorReference="anchorPosition"
                anchorPosition={menuPosition}
              >
                <MenuItem>
                  <Typography className={classes.name}>{userName}</Typography>
                </MenuItem>
                <MenuItem className={classes.logout} onClick={handleLogout}>Logout</MenuItem>
              </Menu>
            </div>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
}
