import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { useHistory } from "react-router-dom";
import MenuIcon from "@material-ui/icons/Menu";
import NestedMenuItem from "material-ui-nested-menu-item";
function AppMenu() {
  const [anchorEl, setAnchorEl] = useState(null);
  const [menuPosition, setMenuPosition] = useState(null);
  const handleItemsClick = (event) => {
    if (menuPosition) {
      return;
    }
    event.preventDefault();
    setMenuPosition({
      top: 45,
      left: 207,
    });
  };
  const handleItemClick = (event) => {
    setMenuPosition(null);
  };
  let history = useHistory();
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  return (
    <div onContextMenu={handleClick}>
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        variant="contained"
        onClick={handleItemsClick}
        style={{ textTransform: "none" }}
        startIcon={<MenuIcon />}
      >
        Menu
      </Button>
      <Menu
        open={!!menuPosition}
        onClose={() => setMenuPosition(null)}
        anchorReference="anchorPosition"
        anchorPosition={menuPosition}
      >
        <MenuItem onClick={() => history.push("./dashboard")}>
          Dashboard
        </MenuItem>
        {/* <NestedMenuItem
          style={{ width: "15vw" }}
          label="Accounts"
          parentMenuOpen={!!menuPosition}
          onClick={handleItemClick}
        >
          <MenuItem onClick={() => history.push("./TripSheet")}>
            Trip_SheetList
          </MenuItem>
          <MenuItem
            style={{ width: "15vw" }}
            onClick={() => history.push("./CreateTripSheet")}
          >
            Create_TripSheet
          </MenuItem>
        </NestedMenuItem> */}
        <MenuItem onClick={() => history.push("./clients")}>Clients</MenuItem>
        <MenuItem onClick={() => history.push("/Trip")}>Trips</MenuItem>
        <MenuItem onClick={() => history.push("./Indent")}>Indents</MenuItem>
        {/* <MenuItem onClick={() => history.push('./Vendor')}>Vendor</MenuItem> */}
        {/* <MenuItem onClick={() => history.push('./drivers')}>Drivers</MenuItem> */}
        {/* <MenuItem onClick={() => history.push('./Users')}>myUsers</MenuItem> */}
        <NestedMenuItem
          style={{ width: "15vw" }}
          label="Users"
          parentMenuOpen={!!menuPosition}
          onClick={handleItemClick}
        >
          <MenuItem onClick={() => history.push("./CompanyUsers")}>
            Company Users
          </MenuItem>
          <MenuItem
            style={{ width: "15vw" }}
            onClick={() => history.push("./FleetUsers")}
          >
            Fleet Users
          </MenuItem>
        </NestedMenuItem>
        <MenuItem onClick={() => history.push("./Groups")}>Groups</MenuItem>
        <NestedMenuItem
          style={{ width: "15vw" }}
          label="Vendors"
          parentMenuOpen={!!menuPosition}
          onClick={handleItemClick}
        >
          {/* <MenuItem onClick={() => history.push('./FleetVendor')}>Fleet Vendors</MenuItem> */}
          <MenuItem
            style={{ width: "15vw" }}
            onClick={() => history.push("./ServiceVendor")}
          >
            Service Vendors
          </MenuItem>
        </NestedMenuItem>
      </Menu>
    </div>
  );
}
export default AppMenu;