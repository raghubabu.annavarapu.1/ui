
module.exports = Object.freeze({
  ALERT_SEVERITY: {
    error: "error",
    warning: "warning",
    info: "info",
    success: "success",
  },
  PAGINATION: {
    current: 0,
    pageSize: 25,
  },

  CLIENT_MORE_OPTIONS: [
    // { label: "Bookings", value: "./clientBookings" },
    { label: "Edit Client", value: "./updateClient" },
    { label: "View Details", value: "./clientDetails" },
    { label: "Contracts", value: "./clientContracts" }
  ],
  CLIENT_STATUS: [
    { label: "Deactive", value: "0", color: "#F24B4B" },
    { label: "Active", value: "1", color: "#008000" },
  ],
  CONTRACT_MORE_OPTIONS: [
    { label: "Routes", value: "./contractRoutes" },
    { label: "View Details", value: "./contractDetails" }
  ],
  DRIVER_MORE_OPTIONS: [{ label: "Payments", value: "./driverPayments" }],
  VENDOR_MORE_OPTIONS: [{ label: "Vendor amounts", value: "./vendorDetails" }],
  CLIENT_TYPE_OPTIONS: [
    { label: "Company", value: "Company" },
    { label: "Agent", value: "Agent" },
    { label: "Individual", value: "Individual" },
  ],
  VEHICLE_TYPES: [
    { label: "Own Vehicle", value: "own_vehicle" },
    { label: "Market Vehicle", value: "market_vehicle" },
  ],
  TRIP_TYPES: [
    { label: "TBB", value: "TBB" },
    { label: "To-Pay", value: "TOPAY" },
    { label: "Return", value: "RETURN" },
  ],
  PAN: "pan",
  GST: "gst",
  FREIGHT_TYPES: [
    { label: "Freight", value: "Freight" },
    { label: "Ton", value: "Ton" }
  ],
  ROUTE_MORE_OPTIONS: [{ label: "View details", value: "./routeDetails" }],
  TRIP_MORE_OPTIONS: [{ label: "View details", value: "./tripDetails" }],
  TRIP_STATUS: [
    // { label: "Pending", value: "1", color: "#F57302" },
    { label: "To be Started ", value: "2", color: "#F2AC29" },
    { label: "On Trip", value: "3", color: "#6AAED9" },
    { label: "Completed", value: "4", color: "#008300" },
    // { label: "Empty Trip", value: "5", color: "#BBE8F2" },
    // { label: "Loading", value: "6", color: "#6ef9f5" },
    // { label: "Started", value: "7", color: "#e5d9f2" },
    // { label: "Arrived", value: "8", color: "#ffbfb7" },
    // { label: "Pilot Arriving", value: "9", color: "#f9dec9" },
    { label: "At Dropoff Location", value: "10", color: "#63BF94" },
    { label: "Cancelled", value: "11", color: "#F24B4B" }
  ],
  INDENT_STATUS: [
    { label: "Draft", value: "0", color: "#6AAED9" },
    { label: "Review", value: "1", color: "#f9dec9" },
    { label: "Approved", value: "2", color: "#ffbfb7" },
    { label: "Rejected", value: "3", color: "#F24B4B" },
    { label: "Completed", value: "4", color: "#008300" },
  ],
  USER_MORE_OPTIONS: [
    { label: "Service", value: "./updateClient" },
    { label: "Admin", value: "./clientDetails" },
    { label: "maintenance", value: "./clientContracts" }
  ],
  GROUPS_MORE_OPTIONS: [
    { label: "Edit", value: "./updateGroup" },
    { label: "Delete", value: "", type: "button" },
    { label: "View Group Members", value: "./groupView" }
  ],
  USER_ROLES: [
    { label: "Fleet Owner", value: "0" },
    { label: "Fleet Supervisor", value: "1" },
    { label: "Maintenance Supervisor", value: "2" },
    { label: "Accountant supervisor", value: "3" },
    { label: "Accountant", value: "4" },
    { label: "Fleet Head", value: "5" },
    { label: "Inventory Head", value: "6" },
    { label: "Vehicle Helper", value: "7" },
    { label: "Fleet Observer", value: "8" },
    { label: "Pilot", value: "9" },
    { label: "Emt", value: "10" }
  ],
  VENDORS_MORE_OPTIONS: [
    { label: "Edit", value: "./addService" },
    { label: "View Details", value: "./serviceDetails" },
    { label: "Contracts", value: "./servicecontractList" },
    { label: "Indents", value: "./serviceindentList" }
  ],
  GST_TYPES: [
    { label: "CGST", value: "CGST" },
    { label: "IGST", value: "IGST" },
    { label: "SGST", value: "SGST" }
  ],
  CUSTOM_CLIENT_DETAILS: [
    { label: "VAT", value: "vat", re: /^[A-Z0-9]{15}$/ },
    { label: "TAX", value: "tax", re: /^[A-Z0-9]{9}$/ },
    { label: "PAN", value: "pan", re: /^[A-Z0-9]{10}$/ },
    { label: "TAN", value: "tan", re: /^[A-Z0-9]{10}$/ },
    { label: "TIN", value: "tin", re: /^[A-Z0-9]{9}$/ },
    { label: "CIN", value: "cin", re: /^[A-Z0-9]{21}$/ }
  ],
  STATUSES: [
    { label: "Active", value: "1", color: "#008000" },
    { label: "Inactive", value: "0", color: "#F24B4B" }
  ],
  SERVICECONTRACT_MORE_OPTIONS: [
    { label: "View Details", value: "./servicecontractDetails" },
    { label: "Edit", value: "./addserviceContract" }
  ]
});
