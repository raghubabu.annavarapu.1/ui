import HttpService from "./httpService";
import Session from "../session/index";
import util from "./util";
import accountSupervisorPath from "./path/account-supervisor";
import vehicleTrackingPath from "./path/vehicle-tracking";
import ownerPath from "./path/owner";
import allModulesPath from "./path/all-modules";
import Authentication from "./apis/authentication";
import Booking from "./apis/booking";
import Driver from "./apis/driver";
import Tripsheet from "./apis/tripsheet";
import Truck from "./apis/truck";
import Tracking from "./apis/tracking";
import Owner from "./apis/owner";
import Expenses from "./apis/expenses";
import Clients from "./apis/clients";
import Contracts from "./apis/contracts";
import Routes from "./apis/routes";
import Commodities from "./apis/commodities";
import Vehicles from "./apis/vehicles";
import Trips from "./apis/trips";
import Roles from "./apis/roles";
import Indent from "./apis/indents";
import Groups from "./apis/groups";
import Users from "./apis/users";
import Vendors from "./apis/vendors";
class Services {
  constructor() {
    const session = new Session();
    const Service = new HttpService(session);
    const Util = new util();
    // const accountSupervisorPath = accountSupervisorPath;
    // const ownerPath = ownerPath;
    this.authentication = new Authentication(Service, accountSupervisorPath, ownerPath, Util);
    this.booking = new Booking(Service, accountSupervisorPath, ownerPath, Util);
    this.driver = new Driver(Service, accountSupervisorPath, ownerPath, Util);
    this.tripsheet = new Tripsheet(Service, accountSupervisorPath, ownerPath, Util);
    this.truck = new Truck(Service, accountSupervisorPath, ownerPath, Util);
    this.tracking = new Tracking(Service, ownerPath);
    this.owner = new Owner(Service, ownerPath);
    this.expenses = new Expenses(Service, accountSupervisorPath, ownerPath, Util)
    this.tracking = new Tracking(Service, vehicleTrackingPath);
    this.clients = new Clients(Service, allModulesPath, Util);
    this.contracts = new Contracts(Service, allModulesPath, Util);
    this.routes = new Routes(Service, allModulesPath, Util);
    this.commodities = new Commodities(Service, allModulesPath, Util);
    this.vehicles = new Vehicles(Service, allModulesPath, Util);
    this.trips = new Trips(Service, allModulesPath, ownerPath, Util);
    this.roles = new Roles(Service, allModulesPath, Util);
    this.indents = new Indent(Service, allModulesPath, Util);
    this.groups = new Groups(Service, allModulesPath, Util);
    this.users = new Users(Service, allModulesPath, Util);
    this.vendors = new Vendors(Service, allModulesPath, Util);
  }

  // Authentication
  userLogin = (data) => {
    return this.authentication.userLogin(data);
  };
  userLoginByToken = () => {
    return this.authentication.userLoginByToken();
  };
  getUserDetails = () => {
    return this.authentication.getUserDetails();
  }

  // Tripsheet
  getTripSheetList = (data) => {
    return this.tripsheet.getTripSheets(data);
  };
  createTripSheet = (data) => {
    return this.tripsheet.createTripSheet(data);
  };
  updateTripSheet = (data) => {
    return this.tripsheet.updateTripSheet(data);
  };
  getTripsheetSummary = (data) => {
    return this.tripsheet.getTripsheetSummary(data);
  };
  closeTripSheet = (data) => {
    return this.tripsheet.closeTripSheet(data);
  }

  // Driver
  getDrivers = (data) => {
    return this.driver.getDriverList(data);
  };

  // Truck
  getTrucks = (data) => {
    return this.truck.getTruckList(data);
  };
  getDashboardTrucks = (data) => {
    return this.truck.getDashboardTrucks(data);
  };

  // Booking
  getSearchedBookings = (data) => {
    return this.booking.getSearchedBookings(data);
  };
  addAdvance = (data) => {
    return this.booking.addAdvance(data);
  };

  // Expenses
  CreateExpenses = (data) => {
    return this.expenses.CreateExpenses(data);
  };
  getExpenseTypes = () => {
    return this.expenses.getExpenseTypes();
  };
  getBookingExpenses = (data) => {
    return this.expenses.getBookingExpenses(data);
  };
  getExpenseAttachments = (data) => {
    return this.expenses.getExpenseAttachments(data);
  };

  //Tracking
  getTrackingMessages = (data) => {
    return this.tracking.getMessages(data);
  };
  getOverSpeedingEvents = (data) => {
    return this.tracking.getOverSpeedingEvents(data);
  };
  getCrashEvents = (data) => {
    return this.tracking.getCrashEvents(data);
  };
  getHarshAccelerationEvents = (data) => {
    return this.tracking.getHarshAccelerationEvents(data);
  };
  getHarshBrakingEvents = (data) => {
    return this.tracking.getHarshBrakingEvents(data);
  };
  getHarshCorneringEvents = (data) => {
    return this.tracking.getHarshCorneringEvents(data);
  };
  getRunningHours = (data) => {
    return this.tracking.getRunningHours(data);
  };
  getTraveledDistance = (data) => {
    return this.tracking.getTraveledDistance(data);
  };
  getFuelHistory = (data) => {
    return this.tracking.getFuelHistory(data);
  }
  //owner-profile-image
  getFleetOwnerImage = (data) => {
    return this.owner.getFleetOwnerImage(data);
  }
  getFleetOwnerProfile = () => {
    return this.owner.getFleetOwnerProfile();
  }
  UpdateExpenses = (data) => {
    return this.expenses.UpdateExpenses(data);
  };

  //Clients
  createClient = (data, addAddress) => {
    return this.clients.createClient(data, addAddress);
  };
  getClients = (data) => {
    return this.clients.getClients(data);
  };
  getClient = (data) => {
    return this.clients.getClient(data);
  };
  updateClient = (data, clientData, addAddress) => {
    return this.clients.updateClient(data, clientData, addAddress);
  };
  getClientDocuments = (clientData) => {
    return this.clients.getClientDocuments(clientData);
  };
  uploadClientDocument = (file, clientData, config) => {
    return this.clients.uploadClientDocument(file, clientData, config);
  };

  //Contracts
  createContract = (data) => {
    return this.contracts.createContract(data);
  };
  createVendorContract = (data) => {
    return this.contracts.createVendorContract(data);
  };
  updateVendorContract = (data) => {
    return this.contracts.updateVendorContract(data);
  }
  getContracts = (data) => {
    return this.contracts.getContracts(data);
  };
  getContract = (data) => {
    return this.contracts.getContract(data);
  };
  updateContract = (data, contractData) => {
    return this.contracts.updateContract(data, contractData);
  };
  getContractDocuments = (contractData) => {
    return this.contracts.getContractDocuments(contractData)
  }
  uploadContractDocument = (file, contractData, config) => {
    return this.contracts.uploadContractDocument(file, contractData, config);
  };

  //Routes
  createRoute = (data) => {
    return this.routes.createRoute(data);
  };
  getRoutes = (data) => {
    return this.routes.getRoutes(data);
  };
  getRoute = (data) => {
    return this.routes.getRoute(data);
  };
  updateRoute = (data, id) => {
    return this.groups.updateRoute(data, id);
  };

  //Commodities
  getCommodities = () => {
    return this.commodities.getCommodities();
  };

  //vehicles
  getVehicles = (dataObj) => {
    return this.vehicles.getVehicles(dataObj);
  };
  getLatestTrip = (dataObj) => {
    return this.vehicles.getLatestTrip(dataObj);
  };

  //Trips
  getTrips = (data) => {
    return this.trips.getTrips(data);
  };
  getTrip = (data) => {
    return this.trips.getTrip(data);
  };
  createTrip = (data) => {
    return this.trips.createTrip(data);
  };
  cancelTrip = (data, tripData) => {
    return this.trips.cancelTrip(data, tripData);
  };
  uploadTripDocument = (file, tripData) => {
    return this.trips.uploadTripDocument(file, tripData);
  };
  getTripsDocuments = (data) => {
    return this.trips.getTripsDocuments(data);
  };
  getTripUnits = () => {
    return this.trips.getTripUnits();
  };
  updateTripLocation = (reqData) => {
    return this.trips.updateTripLocation(reqData);
  };
  addTripLocation = (data) => {
    return this.trips.addTripLocation(data);
  };
  updateRouteDetails = (data) => {
    return this.trips.updateRouteDetails(data);
  };
  getElocks = (data) => {
    return this.trips.getElocks(data);
  }

  //Roles
  getRoles = () => {
    return this.roles.getRoles();
  };
  getRole = (data) => {
    return this.roles.getRole(data);
  };

  //Indents
  getIndentTypes = () => {
    return this.indents.getIndentTypes();
  };
  createIndent = (data, indents) => {
    return this.indents.createIndent(data, indents);
  };
  getIndents = (data) => {
    return this.indents.getIndents(data);
  };
  getIndent = (data) => {
    return this.indents.getIndent(data);
  };

  //Users
  getCompanyUsers = (data) => {
    return this.users.getCompanyUsers(data);
  };
  getFleetUsers = (data) => {
    return this.users.getFleetUsers(data);
  };

  //Groups
  getGroups = (data) => {
    return this.groups.getGroups(data);
  };
  getMembers = (data) => {
    return this.groups.getMembers(data);
  };
  createGroup = (data) => {
    return this.groups.createGroup(data);
  };
  getGroupMembers = (data) => {
    return this.groups.getGroupMembers(data);
  };
  getGroup = (data) => {
    return this.groups.getGroup(data);
  };
  updateGroup = (data, id) => {
    return this.groups.updateGroup(data, id);
  };
  deleteGroup = (data) => {
    return this.groups.deleteGroup(data);
  }
  //Vendors
  getVendors = (data) => {
    return this.vendors.getVendors(data);
  };
  getVendor = (data) => {
    return this.vendors.getVendor(data);
  };
  createVendor = (data) => {
    return this.vendors.createVendor(data);
  };
  getServiceAreas = () => {
    return this.vendors.getServiceAreas();
  };
  updateServiceVendor = (data, vendorData) => {
    return this.vendors.updateServiceVendor(data, vendorData)
  };
}
export default Services;
