class VendorsRequestBuilders {
  constructor(util) {
    this.util = util;
  }
  CreateVendor = (reqObj) => {
    return {
        firstName: reqObj.firstName.value,
        lastName: reqObj.lastName.value,
        mobileNumber: reqObj.mobileNumber.value,
        emailAddress: reqObj.emailAddress.value,
        ...(reqObj.companyName.value ? { companyName: reqObj.companyName.value } : {}),
        ...(reqObj.gstNumber.value ? { gstNumber: reqObj.gstNumber.value } : {}),
        ...(reqObj.businessId.value ? { businessId: reqObj.businessId.value } : {}),
        ...(reqObj.openingBalance.value ? {openingBalance: reqObj.openingBalance.value} : {}),
        areaServiceId: Number(reqObj.areaServiceId.value),
        status: reqObj.status.value,
        clientAddress: {
            addressLine1: reqObj.addressLine1.value,
            ...(reqObj.addressLine2.value ? { addressLine2 : reqObj.addressLine2.value } : {}),
            ...(reqObj.addressLine3.value ? { addressLine3 : reqObj.addressLine3.value } : {}),
            city: reqObj.city.value,
            state: reqObj.state.value,
            country: reqObj.country.value,
            postalCode: reqObj.postalCode.value
        }
    };
  };
  UpdateVendor = (reqObj) => {
    return {
      firstName: reqObj.firstName.value,
      lastName: reqObj.lastName.value,
      emailAddress: reqObj.emailAddress.value,
      gstNumber: reqObj.gstNumber.value,
      status: reqObj.status.value,
      clientAddress: {
        addressLine1: reqObj.addressLine1.value,
        addressLine2 : reqObj.addressLine2.value,
        addressLine3 : reqObj.addressLine3.value,
        city: reqObj.city.value,
        state: reqObj.state.value,
        country: reqObj.country.value,
        postalCode: reqObj.postalCode.value
    }
    }
  };
}

export default VendorsRequestBuilders;
