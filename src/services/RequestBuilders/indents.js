class IndentRequestBuilders {
  constructor(util) {
    this.util = util;
  }
  CreateIndent = (reqObj, indents) => {
    return {
      truckId: reqObj.truckId.value,
      vendorId: Number(reqObj.vendorId.value),
      ...(reqObj.contractId.value ? { contractId: Number(reqObj.contractId.value) } : {}),
      ...(reqObj.tripId.value ? { tripId: parseInt(reqObj.tripId.value) } : {}),
      ...(reqObj.vendorMobile.value ? { vendorMobile: reqObj.vendorMobile.value } : {}),
      indentAlias: reqObj.indentAlias.value,
      indentDate: this.util.formatDate(reqObj.indentDate.value),
      requestedUserId: reqObj.requestedUserId.value,
      requestedUserRoleId: reqObj.requestedUserRoleId.value,
      ...(reqObj.comments.value ? { comments: reqObj.comments.value } : {}),
      items: indents.map((ind) => {
        return {
          indentTypeId: ind.indentTypeId.value.value,
          quantity: parseFloat(ind.quantity.value),
          unitQuantity: parseFloat(ind.unitQuantity.value),
          total: ind.total.value,
          ...(ind.indentRemarks.value ? { indentRemarks: ind.indentRemarks.value } : {})
        }
      })
    };
  };
}
export default IndentRequestBuilders;
