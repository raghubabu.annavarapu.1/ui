class ContractRequestBuilders {
  constructor(util) {
    this.util = util;
  }
  CreateContract = (reqObj) => {
    return {
      clientId: reqObj.clientId,
      payload: {
        customerContractReferenceNumber: reqObj.customerContractReferenceNumber.value,
        clientId: reqObj.clientId,
        fromDate: reqObj.fromDate,
        toDate: reqObj.toDate,
        onReceipt: reqObj.onReceipt,
        ...(!reqObj.onReceipt && {
          paymentTermsInDays: parseInt(reqObj.paymentTermsInDays.value),
        }),
        notes: reqObj.notes.value,
      },
    };
  };
  CreateVendorContract = (reqObj) => {
    return {
      vendorId: reqObj.vendorId,
      payload: {
        customerContractReferenceNumber: reqObj.customerContractReferenceNumber.value,
        clientId: reqObj.clientId,
        fromDate: reqObj.fromDate,
        toDate: reqObj.toDate,
        onReceipt: reqObj.onReceipt,
        ...(!reqObj.onReceipt && {
          paymentTermsInDays: parseInt(reqObj.paymentTermsInDays.value),
        }),
        notes: reqObj.notes.value,
      },
    };
  };
  UpdateVendorContract = (reqObj) => {
    return {
      vendorId: reqObj.vendorId,
      contractId: reqObj.contractId,
      fromDate: reqObj.fromDate,
      toDate: reqObj.toDate,
      ...(!reqObj.onReceipt ? {
        paymentTermsInDays: parseInt(reqObj.paymentTermsInDays.value),
      } : {paymentTermsInDays: 0}),
    }
  };
  UpdateContract = (reqObj) => {
    return {};
  };
}
export default ContractRequestBuilders;
