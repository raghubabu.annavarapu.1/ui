module.exports = Object.freeze({
    CLIENTS: "/clients",
    CONTRACTS: "/contracts",
    ROUTES: "/routes",
    COMMODITIES: "/commodities",
    VEHICLES: "/vehicles",
    TRIPS: "/trips",
    AUTHUSER: "/authuser",
    ROLES: "/roles",
    INDENT: "/indent",
    GROUPS: "/groups",
    VENDORS: "/vendors",
    TRIP_SECURITY_TAGS: "/trip-security-tags"
})