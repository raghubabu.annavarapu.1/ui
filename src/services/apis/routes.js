import RouteRequestBuilders from "../RequestBuilders/routes";
class Routes {
  constructor(Service, path, Util) {
    this.httpService = Service;
    this.path = path;
    this.Util = Util;
    this.routeRequestBuilder = new RouteRequestBuilders(Util);
  }

  getRoutes = (data) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.Util.queryParamsFromObj(this.path.ROUTES, data);
    return new Promise((resolve, reject) => {
      this.httpService
        .get(url)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  getRoute = (data) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.ROUTES +
      "/" +
      data.routeId;
    return new Promise((resolve, reject) => {
      this.httpService
        .get(url)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  createRoute = (data) => {
    let reqData = this.routeRequestBuilder.CreateRoute(data);
    const url = process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 + this.path.ROUTES;
    return new Promise((resolve, reject) => {
      this.httpService
        .post(url, reqData)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  updateRoute = (data, id) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.ROUTES +
      `/updateroute/${id}`;
    let dataObj = this.routeRequestBuilders.createRoute(data);
    return new Promise((resolve, reject) => {
      this.httpService
        .patch(url, dataObj)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };
}

export default Routes;
