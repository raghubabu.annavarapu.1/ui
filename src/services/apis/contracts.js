import ContractRequestBuilders from "../RequestBuilders/contracts";

class Clients {
  constructor(Service, path, Util) {
    this.httpService = Service;
    this.path = path;
    this.Util = Util;
    this.contractRequestBuilder = new ContractRequestBuilders(Util);
  }

  getContracts = (data) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.Util.queryParamsFromObj(this.path.CONTRACTS, data);
    return new Promise((resolve, reject) => {
      this.httpService
        .get(url)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };
  getContract = (data) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.CONTRACTS +
      "/" +
      data.contractId;
    return new Promise((resolve, reject) => {
      this.httpService
        .get(url)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };
  createContract = (data) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 + this.path.CONTRACTS;
    let reqData = this.contractRequestBuilder.CreateContract(data);
    return new Promise((resolve, reject) => {
      this.httpService
        .post(url, reqData)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  createVendorContract = (data) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 + this.path.CONTRACTS;
    let reqData = this.contractRequestBuilder.CreateVendorContract(data);
    return new Promise((resolve, reject) => {
      this.httpService
        .post(url, reqData)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  updateVendorContract = (data) => { 
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 + this.path.CONTRACTS + "/updatevenodrcontract";
    let reqData = this.contractRequestBuilder.UpdateVendorContract(data);
    return new Promise((resolve, reject) => {
      this.httpService
        .patch(url, reqData)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  getContractDocuments = (contractData) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.CONTRACTS +
      `/${contractData.id}` +
      "/attachments";
    return new Promise((resolve, reject) => {
      this.httpService
        .get(url)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  uploadContractDocument = (file, contractData, config) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.CONTRACTS +
      "/" +
      contractData.id +
      "/attachments";
    return new Promise((resolve, reject) => {
      this.httpService
        .post(url, file, config)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };
}

export default Clients;
