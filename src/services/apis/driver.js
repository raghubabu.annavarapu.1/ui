class Driver {
  constructor(Service, path, path2, Util) {
    this.httpService = Service;
    this.path = path;
    this.path2 = path2;
    this.Util = Util;
  }

  getDriverList = (data) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_2 +
      this.Util.queryParamsFromObj(this.path.GET_DRIVER_LIST, data);
    return new Promise((resolve, reject) => {
      this.httpService
        .get(url)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };
}

export default Driver;
