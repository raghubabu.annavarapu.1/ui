
class Owner {
    constructor(Service, ownerPath) {
        this.httpService = Service;
        this.ownerPath = ownerPath;
    }

    getFleetOwnerImage = (data) => {
        return new Promise((resolve, reject) => {
            const url = process.env.REACT_APP_SUVEGA_API_ENDPOINT_1 + this.ownerPath.OWNER_PROFILE_IMAGE;
            // console.log(url);
            return this.httpService.post(url, data)
                .then((res) => {
                    resolve(res);
                })
                .catch((errorObj) => {
                    reject(errorObj);
                });
        });
    };

    getFleetOwnerProfile = () => {
        return new Promise((resolve, reject) => {
            const url = process.env.REACT_APP_SUVEGA_API_ENDPOINT_1 + this.ownerPath.OWNER_GET_PROFILE;
            // console.log(url);
            return this.httpService.post(url)
                .then((res) => {
                    resolve(res);
                })
                .catch((errorObj) => {
                    reject(errorObj);
                });
        });
    };
}

export default Owner;