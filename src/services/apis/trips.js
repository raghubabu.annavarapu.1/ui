import TripRequestBuilders from "../RequestBuilders/trips";
class Trips {
  constructor(Service, path, path2, Util) {
    this.httpService = Service;
    this.path = path;
    this.path2 = path2;
    this.Util = Util;
    this.tripRequestBuilder = new TripRequestBuilders(Util);
  }

  getTrips = (data) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.Util.queryParamsFromObj(this.path.TRIPS, data);
    return new Promise((resolve, reject) => {
      this.httpService
        .get(url)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  getTrip = (data) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.TRIPS +
      "/" +
      data.tripId;
    return new Promise((resolve, reject) => {
      this.httpService
        .get(url)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  createTrip = (data) => {
    let reqData = this.tripRequestBuilder.CreateTrip(data);
    const url = process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 + this.path.TRIPS;
    return new Promise((resolve, reject) => {
      this.httpService
        .post(url, reqData)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  updateTripLocation = (data) => {
    let reqData = this.tripRequestBuilder.UpdateTrip(data);
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.ROUTES +
      "/updateRoute";
    return new Promise((resolve, reject) => {
      this.httpService
        .patch(url, reqData)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  cancelTrip = (data, tripData) => {
    let reqData = this.tripRequestBuilder.CancelTrip(data);
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.TRIPS +
      `/${tripData.id}` +
      "/cancelTrip";
    return new Promise((resolve, reject) => {
      this.httpService
        .post(url, reqData)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  uploadTripDocument = (file, tripData) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.TRIPS +
      "/" +
      tripData.id +
      "/documents";
    return new Promise((resolve, reject) => {
      this.httpService
        .post(url, file)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  getTripsDocuments = (tripData) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.TRIPS +
      "/" +
      tripData.tripId +
      "/documents";
    return new Promise((resolve, reject) => {
      this.httpService
        .get(url)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  getTripUnits = () => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 + this.path.TRIPS + "/units";
    return new Promise((resolve, reject) => {
      this.httpService
        .get(url)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  addTripLocation = (data) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.TRIPS +
      `/${data.tripId}/addWaypoint`;
    let reqData = this.tripRequestBuilder.AddTripLocation(data);
    return new Promise((resolve, reject) => {
      this.httpService
        .post(url, reqData)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  updateRouteDetails = (data) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.TRIPS +
      `/${data.tripId.id}/updateFixedDetails`;
    let reqData = this.tripRequestBuilder.UpdateRouteDetails(data);
    return new Promise((resolve, reject) => {
      this.httpService
        .post(url, reqData)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  getElocks = (data) => {
    const url =
      process.env.REACT_APP_SUVEGA_API_ENDPOINT_3 +
      this.path.TRIP_SECURITY_TAGS +
      `/${data.tripId}`;
    return new Promise((resolve, reject) => {
      this.httpService
        .get(url)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };
}

export default Trips;
