

function PolyLineCoordinates() {
    var data = require('./source_less.json');
    var polylinePoints = data.features[0].geometry.coordinates;
    let coordinates = polylinePoints.map(x=>[x[1],x[0]])
    // console.log(coordinates);
    return coordinates;
}

export default PolyLineCoordinates;