import React, { Component, Suspense } from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import { Signin } from "./pages/index";
import { routes } from "./navigation/index";
import { Loader } from "./components/index";
import Services from "./services/index";
import Session from "./session/index";
import "./App.css";
const Service = new Services();
global.service = Service;
const session = new Session();
global.session = session;

class App extends Component {
  render() {
    const menu = routes.map((route, index) => {
      return route.component && global.session.get("BearerToken") ? (
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          name={route.name}
          render={(props) => <route.component {...props} />}
        />
      ) : null;
    });
    return (
      <BrowserRouter>
        <Suspense fallback={<Loader />}>
          <Switch>
            {menu}
            <Route path="/" component={Signin} />
          </Switch>
        </Suspense>
      </BrowserRouter>
    );
  }
}

export default App;
