import React, { Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TablePagination from "@mui/material/TablePagination";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import PropTypes from "prop-types";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import Divider from "@mui/material/Divider";
import Checkbox from "@mui/material/Checkbox";
const useStyles = makeStyles((theme) => ({
  table_head: {
    padding: "10px 24px 15px 24px",
  },
  selected_text: {
    color: "#929DAF",
    fontSize: "14px",
    letterSpacing: "0.5px",
    alignSelf: "center",
  },
  row_paper: {
    background: "#FFFFFF",
    boxShadow: "rgba(0, 0, 0, 0.05) 0px 0px 0px 1px",
    borderRadius: "4px",
  },
  divider: {
    height: "12px",
  },
}));

function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, numSelected, rowCount, headRows } = props;
  return (
    <TableHead>
      <TableRow>
        {/* {props.checkbox ? (
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{ "aria-label": "select all desserts" }}
              className={classes.whiteColor}
            />
          </TableCell>
        ) : null} */}
        {headRows.map((row) => {
          return (
            <TableCell
              key={row.id}
              align={row.alignLeft ? "left" : "center"}
              padding={row.disablePadding ? "none" : "default"}
              className={classes.whiteColor}
              style={{
                padding: row.padding ? "20px 30px" : "",
                width: row.width,
              }}
            >
              {row.label}
            </TableCell>
          );
        })}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  rowCount: PropTypes.number.isRequired,
  headRows: PropTypes.array.isRequired,
};

const DataTable = (props) => {
  const classes = useStyles();
  const handlePageChange = (e, page) => {
    props.pageChangeHandler(page);
  };
  const handleChangeRowsPerPage = (e) => {
    props.rowsPerPageChangeHandler(e.target.value);
  };
  return (
    <>
      <div className={classes.table_head}>
        <span className={classes.selected_text}></span>
        <div className="custom_pagination">
          {props.pagination ? <TablePagination
            component="div"
            count={props.total}
            page={props.pagination.current}
            onPageChange={handlePageChange}
            rowsPerPage={props.pagination.pageSize}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />: null }
        </div>
      </div>
      <div className="custom_table">
        <Table>
          <EnhancedTableHead
            classes={classes}
            numSelected={0}
            rowCount={20}
            onSelectAllClick={() => {}}
            headRows={props.headRows}
          />
          <TableBody>
            {props.tableData.map((rec, i) => {
              return (
                <Fragment key={i}>
                  <TableRow className={classes.row_paper}>
                    {/* {props.checkbox ? (
                      <TableCell padding="checkbox">
                        <Checkbox />
                      </TableCell>
                    ) : null} */}
                    {Object.keys(rec).map((key, j) => {
                      return (
                        <TableCell key={j} align="center">
                          {rec[`${key}`]}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                  <Divider className={classes.divider} />
                </Fragment>
              );
            })}
          </TableBody>
        </Table>
      </div>
      {props.tableData.length === 0 ? (
        <div
          style={{ textAlign: "center", height: "50vh", position: "relative" }}
        >
          <span
            style={{
              position: "absolute",
              top: "60%",
              left: "46%",
              fontSize: "20px",
            }}
          >
            No Data Found!
          </span>
        </div>
      ) : null}
    </>
  );
};

export default DataTable;
