import React from "react";
import moment from "moment";
import { FormLabel, FormHelperText } from "@material-ui/core";
import { DatePicker } from "antd";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  datePickerField: {
    marginTop: "8px",
    width: "100%",
    padding: "9px",
    borderRadius: "4px",
    border: "1px solid #91b0c4",
  },
  helperText: { color: "red", marginRight: "0px", marginLeft: "0px" },
  headLabel: {
    fontWeight: "normal",
    color: "#333333",
    fontSize: "14px",
  },
}));

const DatePickerField = (props) => {
  const classes = useStyles();
  return (
    <div>
      <FormLabel
        className={classes.headLabel}
        required={props.fieldData.validationRequired}
      >
        {props.fieldData.label}
      </FormLabel>
      <DatePicker
        className={classes.datePickerField}
        value={props.fieldData.value}
        format="YYYY-MM-DD HH:mm"
        showTime={{ defaultValue: moment("00:00:00", "HH:mm:ss") }}
        placeholder=""
        onChange={(value) => {
            props.timeStampChangeHandler(value, props.fieldData.name);
        }}
        disabledDate={props.fieldData.disabledDate}
      />
      {props.fieldData.isValid ? null : (
        <FormHelperText className={classes.helperText}>
          {props.fieldData.errorMsg}
        </FormHelperText>
      )}
    </div>
  );
};

export default DatePickerField;
