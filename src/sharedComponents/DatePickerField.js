import React from "react";
import { TextField, FormLabel, FormHelperText } from "@material-ui/core";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  datePickerField: { width: "100%" },
  helperText: { color: "red", marginRight: "0px", marginLeft: "0px" },
  mtop: {
    marginTop: "8px",
    border: "0.5px solid #91b0c4",
    borderRadius: "4px",
  },
  headLabel: {
    // textTransform: "uppercase",
    fontWeight: "normal",
    color: "#333333",
    fontSize: "14px",
  },
}));

const DatePickerField = (props) => {
  const classes = useStyles();
  return (
    <div>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        {props.fieldData.topLabel ? (
          <FormLabel
            className={classes.headLabel}
            required={props.fieldData.validationRequired}
          >
            {props.fieldData.label}
          </FormLabel>
        ) : null}
        <div className="date_picker_field">
          <DatePicker
            label={props.fieldData.topLabel ? null : props.fieldData.label}
            value={props.fieldData.value}
            onChange={(value) => {
              props.timeStampChangeHandler(value, props.fieldData.name);
            }}
            minDate={props.fieldData.minDate}
            maxDate={props.fieldData.maxDate}
            renderInput={(params) => (
              <TextField
                {...params}
                variant={props.variant}
                required={props.fieldData.validationRequired}
                className={
                  props.fieldData.topLabel
                    ? `${classes.mtop} ${classes.datePickerField}`
                    : classes.datePickerField
                }
              />
            )}
          />
          {props.fieldData.isValid ? null : (
            <FormHelperText className={classes.helperText}>
              {props.fieldData.errorMsg}
            </FormHelperText>
          )}
        </div>
      </LocalizationProvider>
    </div>
  );
};

export default DatePickerField;
