import React from "react";
import {
  withGoogleMap,
  GoogleMap,
  withScriptjs,
  Marker,
} from "react-google-maps";

const Map = (props) => {
  let { pickupLocation, dropoffLocation, zoom } = props;
  const AsyncMap = withScriptjs(
    withGoogleMap((props) => (
      <div>
        {pickupLocation.lat && pickupLocation.lng ? (
          <GoogleMap
            defaultZoom={zoom}
            defaultCenter={{ lat: pickupLocation.lat, lng: pickupLocation.lng }}
          >
            <Marker
              position={{ lat: pickupLocation.lat, lng: pickupLocation.lng }}
            />
            {dropoffLocation.lat && dropoffLocation.lng ? (
              <Marker
                
                position={{
                  lat: dropoffLocation.lat,
                  lng: dropoffLocation.lng,
                }}
              />
            ) : null}
          </GoogleMap>
        ) : null}
      </div>
    ))
  );

  return pickupLocation.lat && pickupLocation.lng ? (
    <div>
      <AsyncMap
        googleMapURL={
          "https://maps.googleapis.com/maps/api/js?key=AIzaSyB98LIxlG4-VX7GMVGp5DEZD0ZDIgTN07E&libraries=places"
        }
        loadingElement={<div style={{ height: "calc(100vh - 80px)" }}></div>}
        containerElement={
          <div
            style={{
              height: "calc(100vh - 80px)",
              margin: "0px 0px 80px 0px",
            }}
          ></div>
        }
        mapElement={<div style={{ height: "calc(100vh - 80px)" }}></div>}
      />
    </div>
  ) : null;
};

export default Map;
