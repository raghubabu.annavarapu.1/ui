import React from 'react';
import $ from 'jquery';
window.jQuery = $;
window.$ = $;
global.jQuery = $;
const Dashboard = React.lazy(() => import('../../pages/dashboard/dashboard'));
// const Signin = React.lazy(() => import('../../pages/signin/signin'));
const TripsheetList = React.lazy(() => import('../../pages/tripsheet/TripsheetList'));
const CreateTripsheet = React.lazy(() => import('../../pages/tripsheet/CreateTripsheet'));
const TripsheetSummary = React.lazy(() => import("../../pages/tripsheet/TripsheetSummary/index"));
const Trip = React.lazy(() => import("../../pages/trips/Trip"));
const AddTrip = React.lazy(() => import("../../pages/trips/createTrip/AddTrip"));
const EditTrip = React.lazy(() => import("../../pages/trips/EditTrip"));
const CloseTrip = React.lazy(() => import("../../pages/trips/CloseTrip"))
const UpdateTrip = React.lazy(() => import("../../pages/trips/createTrip/UpdateTrip"));
const TripDetails = React.lazy(() => import("../../pages/trips/TripDetails"));
const ClientList = React.lazy(() => import("../../pages/clients/ClientList"));
const Vendor = React.lazy(() => import('../../pages/vendor/vendor'));
const AddVendor = React.lazy(() => import('../../pages/vendor/AddVendor'));
const AddClient = React.lazy(() => import('../../pages/clients/AddClient'));
const ClientBookings = React.lazy(() => import('../../pages/clients/ClientBookings'));
const DriverList = React.lazy(() => import('../../pages/drivers/DriverList'));
const DriverPayments = React.lazy(() => import('../../pages/drivers/DriverPayments'));
const MakePayment = React.lazy(() => import('../../pages/drivers/MakePayment'));
const AddDriver = React.lazy(() => import("../../pages/drivers/AddDriver"));
const VendorDetails = React.lazy(() => import("../../pages/vendor/VendorDetails"));
const AdvanceList = React.lazy(() => import('../../pages/vendor/AdvanceList'));
const DueList = React.lazy(() => import('../../pages/vendor/DueList'))
const LedgerList = React.lazy(() => import('../../pages/vendor/LedgerList'));
const RouteList = React.lazy(() => import('../../pages/clients/contracts/routes/RouteList'));
const ContractList = React.lazy(() => import('../../pages/clients/contracts/ContractList'));
const AddRoute = React.lazy(() => import('../../pages/clients/contracts/routes/AddRoute'));
const addContract = React.lazy(() => import('../../pages/clients/contracts/AddContract'));
const ClientDetails = React.lazy(() => import('../../pages/clients/ClientDetails'));
const ContractDetails = React.lazy(() => import('../../pages/clients/contracts/ContractDetails'));
const RouteDetails = React.lazy(() => import('../../pages/clients/contracts/routes/RouteDetails'));
const Indent = React.lazy(() => import('../../pages/indent/Indent'));
const AddIndent = React.lazy(() => import('../../pages/indent/AddIndent'));
const CompanyUsers = React.lazy(() => import("../../pages/users/CompanyUsers"));
const FleetUsers = React.lazy(() => import("../../pages/users/FleetUsers"));
const AddUser = React.lazy(() => import("../../pages/users/AddUser"));
const Groups = React.lazy(() => import("../../pages/groups/Groups"));
const GroupView = React.lazy(() => import("../../pages/groups/GroupView"));
const AddGroup = React.lazy(() => import("../../pages/groups/AddGroup"));
const FleetVendor = React.lazy(() => import("../../pages/vendors/FleetVendor/FleetVendor"));
const ServiceVendor = React.lazy(() => import("../../pages/vendors/ServiceVendor/ServiceVendor"));
const AddFleet = React.lazy(() => import("../../pages/vendors/FleetVendor/AddFleet"));
const AddService = React.lazy(() => import("../../pages/vendors/ServiceVendor/AddService"));
const ServiceContractList = React.lazy(() => import("../../pages/vendors/ServiceVendor/servicecontracts/ServiceContractList"));
const AddServiceContract = React.lazy(() => import("../../pages/vendors/ServiceVendor/servicecontracts/AddServiceContract"));
const ServiceIndentList = React.lazy(() => import("../../pages/vendors/ServiceVendor/serviceindent/ServiceIndentList"));
const ServiceDetails = React.lazy(() => import("../../pages/vendors/ServiceVendor/ServiceDetails"));
const ServiceContractDetails = React.lazy(() => import("../../pages/vendors/ServiceVendor/servicecontracts/ServiceContractDetails"))
const routes = [
    { path: ["/", "/dashboard"], exact: true, name: 'dashboard', component: Dashboard },
    { path: '/tripsheet', exact: true, name: 'tripsheet', component: TripsheetList },
    { path: '/createTripsheet', exact: true, name: 'createTripsheet', component: CreateTripsheet },
    { path: "/tripsheetSummary", exact: true, name: 'tripsheetSummary', component: TripsheetSummary },
    { path: '/Trip', exact: true, name: 'trip', component: Trip },
    { path: '/addTrip', exact: true, name: 'addTrip', component: AddTrip },
    { path: '/editTrip', exact: true, name: 'editTrip', component: EditTrip },
    { path: '/closeTrip', exact: true, name: 'closeTrip', component: CloseTrip },
    { path: '/updateTrip', exact: true, name: 'updateTrip', component: UpdateTrip },
    { path: '/tripDetails', exact: true, name: 'tripDetails', component: TripDetails },
    { path: '/clients', exact: true, name: 'clients', component: ClientList },
    { path: ["/", "/vendor"], exact: true, name: 'vendor', component: Vendor },
    { path: '/addVendor', exact: true, name: 'addVendor', component: AddVendor },
    { path: ['/addClient', '/updateClient'], exact: true, name: 'addClient', component: AddClient },
    { path: '/clientBookings', exact: true, name: 'clientBookings', component: ClientBookings },
    { path: '/drivers', exact: true, name: 'drivers', component: DriverList },
    { path: "/driverPayments", exact: true, name: 'driverPayments', component: DriverPayments },
    { path: "/makeDriverPayment", exact: true, name: 'makeDriverPayment', component: MakePayment },
    { path: "/addDriver", exact: true, name: 'addDriver', component: AddDriver },
    { path: '/vendorDetails', exact: true, name: 'vendorDetails', component: VendorDetails },
    { path: './advanceList', exact: true, name: 'advanceList', component: AdvanceList },
    { path: './dueList', exact: true, name: 'dueList', component: DueList },
    { path: './ledgerList', exact: true, name: 'ledgerList', component: LedgerList },
    { path: '/contractRoutes', exact: true, name: 'contractRoutes', component: RouteList },
    { path: '/clientContracts', exact: true, name: 'clientContracts', component: ContractList },
    { path: '/addRoute', exact: true, name: 'addRoute', component: AddRoute },
    { path: '/addContract', exact: true, name: 'addContract', component: addContract },
    { path: '/clientDetails', exact: true, name: 'clientDetails', component: ClientDetails },
    { path: '/contractDetails', exact: true, name: 'contractDetails', component: ContractDetails },
    { path: '/routeDetails', exact: true, name: 'routeDetails', component: RouteDetails },
    { path: '/indent', exact: true, name: 'indent', component: Indent },
    { path: '/addIndent', exact: true, name: 'addIndent', component: AddIndent },
    { path: '/companyUsers', exact: true, name: 'companyUsers', component: CompanyUsers },
    { path: '/fleetUsers', exact: true, name: 'fleetUsers', component: FleetUsers },
    { path: '/addUser', exact: true, name: 'addUser', component: AddUser },
    { path: '/groups', exact: true, name: 'groups', component: Groups },
    { path: '/groupView', exact: true, name: 'groupView', component: GroupView },
    { path: ['/addGroup', '/updateGroup'], exact: true, name: 'addGroup', component: AddGroup },
    { path: '/fleetVendor', exact: true, name: 'fleetVendor', component: FleetVendor },
    { path: '/addFleet', exact: true, name: 'addFleet', component: AddFleet },
    { path: '/servicevendor', exact: true, name: 'servicevendor', component: ServiceVendor },
    { path: '/addService', exact: true, name: 'addService', component: AddService },
    { path: '/servicecontractList', exact: true, name: 'servicecontractList', component: ServiceContractList },
    { path: '/addserviceContract', exact: true, name: 'addserviceContract', component: AddServiceContract },
    { path: '/serviceindentList', exact: true, name: 'servicecontractList', component: ServiceIndentList },
    { path: '/serviceDetails', exact: true, name: 'serviceDetails', component: ServiceDetails },
    { path: '/servicecontractDetails', exact: true, name: 'servicecontractDetails', component: ServiceContractDetails }
];
export default routes;
