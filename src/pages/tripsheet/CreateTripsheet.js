import React, { useState, useEffect } from "react";
import Header from "../../components/header/index";
import { ArrowBack } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import {
  Button,
  Paper,
  Typography,
  Grid,
  Divider,
  CircularProgress,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import * as Fields from "../../sharedComponents/index";
import BookingList from "./BookingList";
import ValidateFields from "../../validations/validateFields";
import _ from "lodash";
import AlertMessage from "../../components/alertmessage/AlertMessage";
import * as CONFIG from "../../config/GlobalConstants";
var Service;

const useStyles = makeStyles((theme) => ({
  main: {
    display: "flex",
    padding: "30px",
  },
  alertBox: {
    padding: "10px 30px",
  },
  icon: {
    color: "#366E93",
    marginRight: theme.spacing(2),
  },
  arrow_back: {
    cursor: "pointer",
    color: "#366E93",
    marginRight: theme.spacing(2),
  },
  trip: {
    fontWeight: "bold",
    flexGrow: "1",
  },
  form: {
    width: "100%",
  },
  label: {
    fontSize: "2vh",
    alignSelf: "center",
  },
  textField: {
    width: "100%",
  },
  button: {
    marginRight: theme.spacing(2),
    backgroundColor: "#E15656",
    textTransform: "none",
    padding: "5px 30px",
  },
  text: {
    marginTop: "20px",
    marginRight: theme.spacing(2),
    display: "flex",
    flexWrap: "wrap",
    paddingBottom: "20px",
  },
  TextField: {
    width: 180,
    marginRight: theme.spacing(2),
    marginBottom: theme.spacing(3),
  },
  paper: {
    color: theme.palette.text.secondary,
  },
}));
export default function CreateTripSheet() {
  const formFields = {
    start_odo: {
      label: "Start ODO",
      name: "start_odo",
      value: "",
      errorMsg: "Please enter start odo",
      validPattern: "NUMERIC",
      validationRequired: true,
      isValid: true,
      type: "number",
      min: 0,
    },
    trip_sheet_number: {
      label: "Trip sheet number",
      name: "trip_sheet_number",
      value: "",
      errorMsg: "Please enter tripsheet number",
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: true,
      isValid: true,
      min: 0,
    },
    freight_received: {
      label: "Freight received",
      name: "freight_received",
      value: "",
      errorMsg: "Please enter freight received",
      validPattern: "NUMERIC",
      validationRequired: false,
      isValid: true,
      type: "number",
      min: 0,
    },
    diesel_shortage: {
      label: "Diesel Shortage",
      name: "diesel_shortage",
      value: "",
      errorMsg: "Please enter diesel shortage",
      validPattern: "NUMERIC",
      validationRequired: false,
      isValid: true,
      type: "number",
      min: 0,
    },
    material_damage: {
      label: "Material Damage",
      name: "material_damage",
      value: "",
      errorMsg: "Please enter material damage",
      validPattern: "NUMERIC",
      validationRequired: false,
      isValid: true,
      type: "number",
      min: 0,
    },
  };
  const filterFields = {
    driver_id: {
      label: "Driver name",
      name: "driver_id",
      value: { value: "", label: "" },
      options: [],
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: true,
      errorMsg: "Please select driver",
      isValid: true,
    },
    truck_id: {
      label: "Vehicle number",
      name: "truck_id",
      validPattern: "SPECIAL_CHARS_DESC",
      value: { value: "", label: "" },
      options: [],
      validationRequired: true,
      errorMsg: "Please select vehicle",
      isValid: true,
    },
    start_date: {
      label: "Start Date",
      name: "start_date",
      value: "",
      validationRequired: true,
      errorMsg: "Please select start date",
      validPattern: "DATE",
      isValid: true,
    },
  };
  const classes = useStyles();
  let history = useHistory();
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const [filters, setFilters] = useState(_.cloneDeep(filterFields));
  const [isFormValidated, setIsFormValidated] = useState(false);
  const [bookingData, setBookingData] = useState([]);
  const [searchSpinner, setSearchSpinner] = useState(false);
  const [createSpinner, setCreateSpinner] = useState(false);
  const [bookings, setBookings] = useState([]);
  const [bookList, setBookList] = useState(true);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  Service = global.service;
  let dataObj = { owner_id: 450 };
  let validateFields = new ValidateFields();
  useEffect(() => {
    let filterObj = {
      driver_id: filters.driver_id.value,
      truck_id: filters.truck_id.value,
      start_date: filters.start_date.value,
    };
    const promise1 = Service.getDrivers(dataObj);
    const promise2 = Service.getTrucks(dataObj);
    Promise.all([promise1, promise2])
      .then((res) => {
        let newFields = _.cloneDeep(fields);
        let newFilters = _.cloneDeep(filters);
        newFilters.driver_id.options = res[0].data.map((driver) => {
          return { value: driver.id, label: driver.name };
        });
        newFilters.truck_id.options = res[1].data.map((vehicle) => {
          return { value: vehicle.id, label: vehicle.regi_no };
        });
        setFilters(_.cloneDeep(newFilters));
      })
      .catch(() => {});
  }, []);
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    setFields(newFields);
  };
  const dateChangeHandler = (value, name) => {
    let newFilters = _.cloneDeep(filters);
    newFilters[name]["value"] = value;
    setFilters(newFilters);
  };
  const searchBookingsHandler = () => {
    let filterObj = {
      driver_id: filters.driver_id.value.value,
      truck_id: filters.truck_id.value.value,
      start_date: filters.start_date.value,
    };
    let dataObj = _.cloneDeep({
      ...filters,
      driver_id: {
        ...filters.driver_id,
        value:
          filters.driver_id.value === undefined
            ? filters.driver_id.value
            : filters.driver_id.value.value,
      },
      truck_id: {
        ...filters.truck_id,
        value:
          filters.truck_id.value === undefined
            ? filters.truck_id.value
            : filters.truck_id.value.value,
      },
    });
    const dataStatus = validateFields.validateFieldsData(dataObj);
    if (dataStatus.status) {
      setSearchSpinner(true);
      Service.getSearchedBookings(filterObj)
        .then((res) => {
          setSearchSpinner(false);
          let newFilters = _.cloneDeep(filters);
          newFilters = {
            ...dataStatus.data,
            driver_id: {
              ...dataStatus.data.driver_id,
              value: filters.driver_id.value,
            },
            truck_id: {
              ...dataStatus.data.truck_id,
              value: filters.truck_id.value,
            },
          };
          if (res.data.length === 0) {
            setBookList(false);
          } else {
            setBookList(true);
          }
          setBookingData(res.data);
          setFilters(newFilters);
        })
        .catch(() => {
          setSearchSpinner(false);
          let newFilters = _.cloneDeep(filters);
          newFilters = dataStatus.data;
          setFilters(newFilters);
        });
    } else {
      let newFilters = _.cloneDeep(filters);
      newFilters = {
        ...filters,
        ...dataStatus.data,
        driver_id: {
          ...dataStatus.data.driver_id,
          value: dataStatus.data.driver_id.value
            ? newFilters.driver_id.options.filter(
                (opt) => opt.value === dataStatus.data.driver_id.value
              )[0]
            : { label: "", value: "" },
        },
        truck_id: {
          ...dataStatus.data.truck_id,
          value: dataStatus.data.truck_id.value
            ? newFilters.truck_id.options.filter(
                (opt) => opt.value === dataStatus.data.truck_id.value
              )[0]
            : { label: "", value: "" },
        },
      };
      setFilters(newFilters);
    }
  };
  const setBookingIds = (ids) => {
    setBookings(ids);
  };

  const createTripsheetHandler = () => {
    let dataObj = _.cloneDeep(fields);
    dataObj = {
      ...dataObj,
      driver_id: {
        ...filters.driver_id,
        value:
          filters.driver_id.value.value === undefined
            ? filters.driver_id.value
            : filters.driver_id.value.value,
      },
      truck_id: {
        ...filters.truck_id,
        value:
          filters.truck_id.value.value === undefined
            ? filters.truck_id.value
            : filters.truck_id.value.value,
      },
    };
    const dataStatus = validateFields.validateFieldsData(dataObj);
    if (dataStatus.status) {
      setCreateSpinner(true);
      dataObj = { ...dataObj, booking_ids: bookings };
      Service.createTripSheet(dataObj)
        .then((res) => {
          let newFields = _.cloneDeep(formFields);
          let newFilters = _.cloneDeep(filterFields);
          newFilters.driver_id.options = [...filters.driver_id.options];
          newFilters.truck_id.options = [...filters.truck_id.options];
          setFields(newFields);
          setFilters(newFilters);
          setBookList(false);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.success,
            message: res.message,
          });
          setCreateSpinner(false);
          // history.push("./TripSheet");
        })
        .catch((error) => {
          setCreateSpinner(false);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
        });
    } else {
      let newFields = _.cloneDeep(fields);
      newFields = dataStatus.data;
      setIsFormValidated(dataStatus.status);
      setFields(newFields);
    }
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  const autoCompleteChangeHandler = (value, name) => {
    if (value) {
      let newFilters = _.cloneDeep(filters);
      newFilters[name]["value"] = value;
      setFilters(newFilters);
    } else {
      let newFilters = _.cloneDeep(filters);
      newFilters[name]["value"] = { value: "", label: "" };
      setFilters(newFilters);
    }
  };
  return (
    <div>
      <Header />
      {alertData.open ? (
        <div className={classes.alertBox}>
          <AlertMessage
            severity={alertData.severity}
            message={alertData.message}
            closeAlert={closeAlert}
          />
        </div>
      ) : null}
      <div className={classes.main}>
        <ArrowBack
          className={classes.arrow_back}
          onClick={() => {
            history.push("./TripSheet");
          }}
        />
        <Typography className={classes.trip}>Create Trip Sheet</Typography>
        {/* <Button
          color="primary"
          variant="contained"
          style={{ backgroundColor: "#808895", textTransform: "none" }}
        >
          Create
        </Button> */}
      </div>
      <div style={{ padding: "30px" }}>
        <Paper style={{ padding: "30px" }}>
          <div>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                <Fields.AutoCompleteField
                  fieldData={filters.driver_id}
                  variant="outlined"
                  autoCompleteChangeHandler={autoCompleteChangeHandler}
                />
              </Grid>
              <Grid item xs={2}>
                <Fields.AutoCompleteField
                  fieldData={filters.truck_id}
                  variant="outlined"
                  autoCompleteChangeHandler={autoCompleteChangeHandler}
                />
              </Grid>
              <Grid item xs={2}>
                <Fields.DateField
                  fieldData={filters.start_date}
                  variant="outlined"
                  dateChangeHandler={dateChangeHandler}
                />
              </Grid>
              <Grid item xs={3}>
                <Button
                  variant="contained"
                  color="secondary"
                  style={{ textTransform: "none" }}
                  onClick={() => {
                    searchBookingsHandler();
                  }}
                  startIcon={
                    searchSpinner ? (
                      <CircularProgress size={20} color={"#fff"} />
                    ) : null
                  }
                >
                  Search Trips
                </Button>
              </Grid>
            </Grid>
          </div>
          <Divider style={{ margin: "15px 0px 20px 0px" }} />
          <div>
            <Grid container spacing={2}>
              <Grid item xs={2}>
                <Fields.InputField
                  fieldData={fields.start_odo}
                  variant="outlined"
                  inputChangeHandler={inputChangeHandler}
                />
              </Grid>
              <Grid item xs={2}>
                <Fields.InputField
                  fieldData={fields.trip_sheet_number}
                  variant="outlined"
                  inputChangeHandler={inputChangeHandler}
                />
              </Grid>
              <Grid item xs={2}>
                <Fields.InputField
                  fieldData={fields.freight_received}
                  variant="outlined"
                  inputChangeHandler={inputChangeHandler}
                />
              </Grid>
              <Grid item xs={2}>
                <Fields.InputField
                  fieldData={fields.diesel_shortage}
                  variant="outlined"
                  inputChangeHandler={inputChangeHandler}
                />
              </Grid>
              <Grid item xs={2}>
                <Fields.InputField
                  fieldData={fields.material_damage}
                  variant="outlined"
                  inputChangeHandler={inputChangeHandler}
                />
              </Grid>
              <Grid item xs={2}>
                <Button
                  variant="contained"
                  color="primary"
                  style={{ textTransform: "none", width: "100%" }}
                  onClick={() => {
                    createTripsheetHandler();
                  }}
                  startIcon={
                    createSpinner ? (
                      <CircularProgress size={20} color={"#fff"} />
                    ) : null
                  }
                >
                  {" "}
                  Create Trip Sheet
                </Button>
              </Grid>
            </Grid>
          </div>
        </Paper>
      </div>
      {bookList ? (
        <BookingList bookingData={bookingData} setBookingIds={setBookingIds} />
      ) : (
        bookingData.length === 0 ? <div style={{ textAlign: "center" }}>
          <Typography>No Trips Found!</Typography>
        </div> : null
      )}
    </div>
  );
}
