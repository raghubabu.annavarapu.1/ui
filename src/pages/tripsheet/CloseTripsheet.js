import React, { useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Paper,
  CircularProgress,
} from "@material-ui/core";
import Draggable from "react-draggable";
import * as CONFIG from "../../config/GlobalConstants";

var Service;

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

export default function CloseTripsheet(props) {
  const [spinner, setSpinner] = useState(false);
  Service = global.service;
  const closeTripsheetHandler = () => {
    setSpinner(true);
    Service.closeTripSheet({ tripsheet_id: props.tripsheet.tripsheet_id })
      .then((res) => {
        setSpinner(false);
        props.openAlert(CONFIG.ALERT_SEVERITY.success, res.message);
        props.handleClose();
        props.renderTripsheets();
        window.scrollTo(0,0);
      })
      .catch((error) => {
        setSpinner(false);
        props.openAlert(
          CONFIG.ALERT_SEVERITY.error,
          error.response && error.response.data ? error.response.data.message : "Something went wrong!"
        );
        props.handleClose();
        window.scrollTo(0,0);
      });
  };
  return (
    <div>
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle
          style={{ cursor: "move", textAlign: "center" }}
          id="draggable-dialog-title"
        >
          {props.tripsheet.trip_sheet_no}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure want to close this tripsheet ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            autoFocus
            onClick={props.handleClose}
            color="primary"
            style={{ textTransform: "none" }}
          >
            Cancel
          </Button>
          <Button
            color="primary"
            style={{ textTransform: "none" }}
            onClick={() => {
              closeTripsheetHandler();
            }}
            startIcon={spinner ? <CircularProgress size={20} /> : null}
          >
            Close Tripsheet
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
