import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";

const headRows = [
  { id: "booking_id", disablePadding: true, label: "Booking ID" },
  { id: "lr_no", disablePadding: true, label: "LR/THC Number" },
  { id: "customer_name", disablePadding: true, label: "Customer name" },
  { id: "loading_location", disablePadding: true, label: "Loading Location" },
  { id: "loading_date", disablePadding: true, label: "Loading date" },
  {
    id: "unloading_location",
    disablePadding: true,
    label: "Unloading Location",
  },
  { id: "unloading_date", disablePadding: true, label: "Unloading date" },
  { id: "qty", disablePadding: true, label: "Qty (tons/lit/num)" },
  { id: "expenses", disablePadding: true, label: "expenses" },
  { id: "booking_status", disablePadding: true, label: "Booking Status" },
];

function EnhancedTableHead(props) {
  const {
    classes,
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount
  } = props;

  return (
    <TableHead className={classes.tableHead}>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ "aria-label": "select all desserts" }}
            className={classes.whiteColor}
          />
        </TableCell>
        {headRows.map((row) => (
          <TableCell
            key={row.id}
            align={"center"}
            padding={row.disablePadding ? "none" : "default"}
            sortDirection={orderBy === row.id ? order : false}
            className={classes.whiteColor}
          >
            {row.label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}
EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  whiteColor: {
    color: "#fff",
    padding: "10px",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  tableHead: {
    backgroundColor: "#366E93",
  },
  tableWrapper: {
    overflowX: "auto",
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

export default function EnhancedTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("calories");
  const [selected, setSelected] = React.useState([]);

  function handleRequestSort(event, property) {
    const isDesc = orderBy === property && order === "desc";
    setOrder(isDesc ? "asc" : "desc");
    setOrderBy(property);
  }

  function handleSelectAllClick(event) {
    if (event.target.checked) {
      const newSelecteds = props.bookingData.map((n) => n.id);
      props.setBookingIds(newSelecteds);
      setSelected(newSelecteds);
      return;
    }
    props.setBookingIds([]);
    setSelected([]);
  }

  function handleClick(event, name) {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    props.setBookingIds(newSelected);
    setSelected(newSelected);
  }

  const isSelected = (name) => selected.indexOf(name) !== -1;

  return (
    <div className={classes.root}>
      <Paper
        className={classes.paper}
        style={{ width: "95vw", margin: "auto" }}
      >
        <div className={classes.tableWrapper}>
          {props.bookingData.length === 0 ? null : (
            <Table className={classes.table} aria-labelledby="tableTitle">
              <EnhancedTableHead
                classes={classes}
                numSelected={selected.length}
                rowCount={props.bookingData.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                bookingData={props.bookingData}
                setBookingIds={props.setBookingIds}
              />
              <TableBody>
                {props.bookingData.map((row, index) => {
                  const isItemSelected = isSelected(row.id);
                  const labelId = `enhanced-table-checkbox-${index}`;
                  return (
                    <TableRow
                      hover
                      onClick={(event) => handleClick(event, row.id)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.id}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ "aria-labelledby": labelId }}
                        />
                      </TableCell>
                      <TableCell component="th" id={labelId} scope="row">
                        {row.id}
                      </TableCell>
                      <TableCell align="center">{row.LR}</TableCell>
                      <TableCell align="center">
                        {row.customer_name}
                      </TableCell>
                      <TableCell align="center">
                        {row.fromAddress.split(',').reverse()[2]}, {row.fromAddress.split(',').reverse()[3]}
                      </TableCell>
                      <TableCell align="center">{row.startDateTime}</TableCell>
                      <TableCell align="center">
                        {row.toAddress.split(',').reverse()[2]}, {row.toAddress.split(',').reverse()[3]}
                      </TableCell>
                      <TableCell align="center">
                        {row.unloadDateTime}
                      </TableCell>
                      <TableCell align="center">{row.unloading_weight}</TableCell>
                      <TableCell align="center">{row.expenses}</TableCell>
                      <TableCell align="center">
                        {row.status}
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          )}
        </div>
      </Paper>
      <br />
      <br />
    </div>
  );
}
