import React, { useState, useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import {
  Typography,
  Button,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
} from "@material-ui/core";
import Header from "../../components/header";
import SystemUpdateIcon from "@material-ui/icons/SystemUpdate";
import EditIcon from "@material-ui/icons/Edit";
import CircularLoading from "../../components/loader/circularLoading";
import CloseTripsheet from "./CloseTripsheet";
import { useHistory } from "react-router-dom";
import _ from "lodash";
import * as Fields from "../../sharedComponents/index";
import * as CONFIG from "../../config/GlobalConstants";
import AlertMessage from "../../components/alertmessage/AlertMessage";
var Service;

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#366E93",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);
const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);
const useStyles = makeStyles((theme) => ({
  text: {
    fontWeight: "bold",
    flexGrow: "1",
  },
  alertBox: {
    padding: "10px 30px",
  },
  main: {
    display: "flex",
    padding: "30px",
  },
  form: {
    width: "8vw",
    minWidth: 100,
    marginRight: theme.spacing(2),
  },
  label: {
    alignSelf: "center",
  },
  select: {
    width: "160px",
    marginRight: "16px",
  },
  side: {
    fontWeight: "bold",
    color: "#366E93",
    marginRight: theme.spacing(2),
    alignSelf: "center",
  },
  button: {
    marginRight: theme.spacing(2),
  },
}));
export default function TripSheetList(props) {
  const classes = useStyles();
  let history = useHistory();
  Service = global.service;
  let filterFields = {
    driver_id: {
      label: "Pilot",
      name: "driver_id",
      value: { value: "", label: "" },
      options: [],
    },
    truck_id: {
      label: "Vehicle",
      name: "truck_id",
      value: { value: "", label: "" },
      options: [],
    },
  };
  const [data, setData] = useState([]);
  const [spinner, setSpinner] = useState(false);
  const [filters, setFilters] = useState(filterFields);
  const [clear, setClear] = useState(false);
  const [open, setOpen] = useState(false);
  const [tripsheet, setTripsheet] = useState({});
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: 20,
  });
  const [total, setTotal] = useState(0);
  let dataObj = { owner_id: JSON.parse(sessionStorage.getItem("profile")).id };
  // console.log(JSON.parse(sessionStorage.getItem("profile")).id);
  useEffect(() => {
    const promise1 = Service.getDrivers(dataObj);
    const promise2 = Service.getTrucks(dataObj);
    setSpinner(true);
    Promise.all([promise1, promise2])
      .then((res) => {
        setSpinner(false);
        let newFilters = _.cloneDeep(filters);
        newFilters.driver_id.options = res[0].data.map((driver) => {
          return { value: driver.id, label: driver.name };
        });
        newFilters.truck_id.options = res[1].data.map((truck) => {
          return { value: truck.id, label: truck.regi_no };
        });
        setFilters(_.cloneDeep(newFilters));
      })
      .catch(() => {
        setSpinner(false);
      });
  }, []);
  useEffect(() => {
    let dataObj = {
      driver_id: filters.driver_id.value.value,
      truck_id: filters.truck_id.value.value,
      owner_id: JSON.parse(sessionStorage.getItem("profile")).id,
      page: pagination.current + 1,
    };
    renderTripsheets(dataObj);
  }, [clear]);
  const renderTripsheets = (dataObj) => {
    setSpinner(true);
    Service.getTripSheetList(dataObj)
      .then((res) => {
        setSpinner(false);
        setData(res.data);
        if (res.data.length > 0) {
          setTotal(res.total_records);
        }
      })
      .catch(() => {
        setSpinner(false);
      });
  };
  const clearHandler = () => {
    let newFilters = _.cloneDeep(filters);
    newFilters.driver_id.value = { value: "", label: "" };
    newFilters.truck_id.value = { value: "", label: "" };
    setFilters(newFilters);
    setClear(!clear);
  };
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const openAlert = (severity, message) => {
    setAlertData({ open: true, severity: severity, message: message });
  };
  const closeAlert = () => {
    setAlertData({ open: false, severity: "", message: "" });
  };
  const autoCompleteChangeHandler = (value, name) => {
    if (value) {
      let newFilters = _.cloneDeep(filters);
      newFilters[name]["value"] = value;
      setFilters(newFilters);
    } else {
      let newFilters = _.cloneDeep(filters);
      newFilters[name]["value"] = { value: "", label: "" };
      setFilters(newFilters);
    }
  };
  const pageChangeHandler = (event, newPage) => {
    let newPagination = _.cloneDeep(pagination);
    newPagination.current = newPage;
    setPagination(newPagination);
    let dataObj = {
      driver_id: filters.driver_id.value.value,
      truck_id: filters.truck_id.value.value,
      owner_id: JSON.parse(sessionStorage.getItem("profile")).id,
      page: newPage + 1,
    };
    renderTripsheets(dataObj);
  };
  return (
    <div>
      <Header />
      {alertData.open ? (
        <div className={classes.alertBox}>
          <AlertMessage
            severity={alertData.severity}
            message={alertData.message}
            closeAlert={closeAlert}
          />
        </div>
      ) : null}
      <CloseTripsheet
        open={open}
        handleClose={handleClose}
        tripsheet={tripsheet}
        renderTripsheets={() => {
          let dataObj = {
            driver_id: filters.driver_id.value.value,
            truck_id: filters.truck_id.value.value,
            owner_id: JSON.parse(sessionStorage.getItem("profile")).id,
            page: pagination.current + 1,
          };
          renderTripsheets(dataObj);
        }}
        openAlert={openAlert}
      />
      <div className={classes.main}>
        <Typography className={classes.text}>Trip Sheet Details</Typography>
        <Typography className={classes.side}>Filter by</Typography>
        <div className={classes.select}>
          <Fields.AutoCompleteField
            fieldData={filters.driver_id}
            variant="outlined"
            autoCompleteChangeHandler={autoCompleteChangeHandler}
          />
        </div>
        <div className={classes.select}>
          <Fields.AutoCompleteField
            fieldData={filters.truck_id}
            variant="outlined"
            autoCompleteChangeHandler={autoCompleteChangeHandler}
          />
        </div>
        <div>
          <Button
            variant="contained"
            className={classes.button}
            style={{ textTransform: "none" }}
            color="secondary"
            onClick={() => {
              let dataObj = {
                driver_id: filters.driver_id.value.value,
                truck_id: filters.truck_id.value.value,
                owner_id: JSON.parse(sessionStorage.getItem("profile")).id,
                page: pagination.current + 1,
              };
              renderTripsheets(dataObj);
            }}
          >
            Search
          </Button>
          <Button
            variant="contained"
            className={classes.button}
            style={{ textTransform: "none" }}
            onClick={() => clearHandler()}
          >
            Clear
          </Button>
          <Button
            variant="contained"
            className={classes.button}
            color="primary"
            style={{ textTransform: "none", backgroundColor: "#275786" }}
            onClick={() => history.push("./CreateTripSheet")}
          >
            Create Trip Sheet
          </Button>
          {/* <Button
            variant="contained"
            color="primary"
            style={{ textTransform: "none", backgroundColor: "#E15656" }}
            endIcon={<SystemUpdateIcon />}
          >
            Export
          </Button> */}
        </div>
      </div>
      {spinner ? (
        <CircularLoading />
      ) : (
        <TableContainer
          style={{ width: "95vw", margin: "auto", padding: "30px 0px" }}
        >
          <Table aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell align="center">TripSheet#</StyledTableCell>
                <StyledTableCell align="center">Pilot</StyledTableCell>
                <StyledTableCell align="center">Vehicle#</StyledTableCell>
                <StyledTableCell align="center">Start Date</StyledTableCell>
                <StyledTableCell align="center">Total kms</StyledTableCell>
                <StyledTableCell align="center">
                  Total Fuelfilled
                </StyledTableCell>
                <StyledTableCell align="center">Advanced Paid</StyledTableCell>
                <StyledTableCell align="center">Total Expenses</StyledTableCell>
                <StyledTableCell align="center">
                  Payment Pending (Driver)
                </StyledTableCell>
                <StyledTableCell align="center">
                  Pilot Excess amount
                </StyledTableCell>
                <StyledTableCell align="center">status</StyledTableCell>
                <StyledTableCell align="center">Actions</StyledTableCell>
              </TableRow>
            </TableHead>
            {data && data.length > 0 ? (
              data.map((rec) => (
                <TableBody key={rec.id}>
                  <StyledTableRow>
                    <StyledTableCell component="th" scope="row">
                      {rec.trip_sheet_no}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {rec.driver && rec.driver.name}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {rec.truck.regi_no}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {rec.start_date}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {rec.total_distance}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {rec.total_fuel_filled}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {rec.total_advance_paid}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {rec.total_expense}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {rec.payment_pending}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {rec.excess_amount}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      <Button
                        style={{
                          backgroundColor:
                            rec.status === "Open" ? "#3A9F22 " : "#FB5555",
                          borderRadius: "5em",
                          textTransform: "none",
                          color: "#fff",
                        }}
                        onClick={() => {
                          if (rec.status === "Open") {
                            setTripsheet({
                              tripsheet_id: rec.id,
                              trip_sheet_no: rec.trip_sheet_no,
                            });
                            handleOpen();
                          }
                        }}
                      >
                        {rec.status}
                      </Button>
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      <EditIcon
                        style={{
                          alignSelf: "center",
                          cursor: "pointer",
                          marginRight: "4px",
                        }}
                        onClick={() => {
                          sessionStorage.setItem(
                            "tripsheet",
                            JSON.stringify({
                              id: rec.id,
                              tripsheet: rec.trip_sheet_no,
                            })
                          );
                          history.push({
                            pathname: "./TripsheetSummary",
                            state: {
                              tripsheet_id: rec.id,
                              tripsheet: rec.trip_sheet_no,
                            },
                          });
                        }}
                      />
                    </StyledTableCell>
                  </StyledTableRow>
                </TableBody>
              ))
            ) : (
              <TableBody style={{ textAlign: "center", width: "100%" }}>
                <StyledTableRow>
                  <StyledTableCell
                    colSpan={12}
                    style={{
                      textAlign: "center",
                      background: "#FFF",
                      borderBottom: "0px",
                    }}
                  >
                    <Typography>No Data Found!</Typography>
                  </StyledTableCell>
                </StyledTableRow>
              </TableBody>
            )}
          </Table>
        </TableContainer>
      )}
      <TablePagination
        component="div"
        rowsPerPageOptions={[10, 20, 50]}
        count={total && total}
        rowsPerPage={pagination.pageSize}
        page={pagination.current}
        onPageChange={pageChangeHandler}
        style={{ width: "95vw", margin: "auto" }}
      />
    </div>
  );
}
