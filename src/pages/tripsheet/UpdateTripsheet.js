import React, { useState, useEffect } from "react";
import {
  Dialog,
  DialogTitle,
  IconButton,
  Button,
  Grid,
  CircularProgress,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import * as Fields from "../../sharedComponents/index";
import ValidateFields from "../../validations/validateFields";
import * as CONFIG from "../../config/GlobalConstants";
import _ from "lodash";
var Service;
const useStyles = makeStyles((theme) => ({
  root: {
    width: "20%",
    textAlignItems: "center",
  },
  form_item: {
    marginBottom: "15px",
  },
  dialog_container: {
    padding: "0px 20px",
  },
  main_dialog: {
    margin: "auto",
  },
  dialog: {
    display: "flex",
    margin: "0px 80px ",
    flexgrow: "1",
    fontWeight: "bold",
  },
  cancel_button: {
    backgroundColor: "#707070",
    textTransform: "none",
    margin: "0px 0px 10px 10px",
  },
  update_button: {
    backgroundColor: "#215483",
    textTransform: "none",
    margin: "0px 0px 10px 10px",
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
}));
function SimpleDialog(props) {
  const formFields = {
    start_odometer: {
      label: "Start Odometer",
      name: "start_odometer",
      value: "",
      errorMsg: "Please enter start odo",
      validPattern: "NUMERIC",
      validationRequired: true,
      isValid: true,
      type: "number",
      min: 0,
    },
    end_odometer: {
      label: "End Odometer",
      name: "end_odometer",
      value: "",
      errorMsg: "Please enter end odo",
      validPattern: "NUMERIC",
      validationRequired: true,
      isValid: true,
      type: "number",
      min: 0,
    },
    total_fuel_filled: {
      label: "Total fuel filled",
      name: "total_fuel_filled",
      value: "",
      errorMsg: "Please enter total fuel filled",
      validPattern: "NUMERIC",
      validationRequired: false,
      isValid: true,
      type: "number",
      min: 0,
    },
    diesel_shortage: {
      label: "Diesel Shortage",
      name: "diesel_shortage",
      value: "",
      errorMsg: "Please enter diesel shortage",
      validPattern: "NUMERIC",
      validationRequired: false,
      isValid: true,
      type: "number",
      min: 0,
    },
    material_damage: {
      label: "Material Damage",
      name: "material_damage",
      value: "",
      errorMsg: "Please enter material damage",
      validPattern: "NUMERIC",
      validationRequired: false,
      isValid: true,
      type: "number",
      min: 0,
    },
  };
  const classes = useStyles();
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const [spinner, setSpinner] = useState(false);
  const { onClose, selectedValue, open } = props;
  Service = global.service;
  let validateFields = new ValidateFields();
  useEffect(() => {
    if (props.data) {
      let newFields = _.cloneDeep(fields);
      newFields.start_odometer.value = props.data.start_odometer;
      newFields.end_odometer.value = props.data.end_odometer;
      newFields.total_fuel_filled.value = props.data.total_fuel_filled;
      newFields.material_damage.value = props.data.material_damage;
      newFields.diesel_shortage.value = props.data.diesel_shortage;
      setFields(newFields);
    }
  }, []);
  const handleClose = () => {
    onClose();
    if (props.data) {
      let newFields = _.cloneDeep(formFields);
      newFields.start_odometer.value = props.data.start_odometer;
      newFields.end_odometer.value = props.data.end_odometer;
      newFields.total_fuel_filled.value = props.data.total_fuel_filled;
      newFields.material_damage.value = props.data.material_damage;
      newFields.diesel_shortage.value = props.data.diesel_shortage;
      setFields(newFields);
    }
  };
  const validateOdometers = () => {
    var newFields = _.cloneDeep(fields);
    var status = true;
    var startOdo = newFields["start_odometer"].value;
    var endOdo = newFields["end_odometer"].value;
    if( startOdo.value !== '' && endOdo.value !== ''){
      startOdo = parseInt(startOdo);
      endOdo = parseInt(endOdo);
      if(startOdo >= endOdo){
        status = false;
        newFields["start_odometer"]["isValid"] = false;
        newFields["end_odometer"]["isValid"] = false;
        newFields["start_odometer"]["errorMsg"] = "Start Odometer should be less than End Odemeter";
        newFields["end_odometer"]["errorMsg"] = "End Odometer should be greater than Start Odometer";
      }else{
        status = true;
        newFields["start_odometer"]["isValid"] = true;
        newFields["end_odometer"]["isValid"] = true;
        newFields["start_odometer"]["errorMsg"] = "Please enter start odometer";
        newFields["end_odometer"]["errorMsg"] = "Please enter end odometer";
      }
      setFields(newFields);
    }
    return status;
  };
  const updateHandler = () => {
    let dataObj = _.cloneDeep(fields);
    const dataStatus = validateFields.validateFieldsData(dataObj);
    const validateOdo = validateOdometers();
    if (dataStatus.status && validateOdo) {
      setSpinner(true);
      dataObj = { ...dataObj, tripsheet_id: props.data.id };
      Service.updateTripSheet(dataObj)
        .then((res) => {
          setSpinner(false);
          onClose();
          let reqObj = {
            owner_id: 450,
            tripsheet_id: JSON.parse(sessionStorage.getItem("tripsheet")).id,
          };
          props.getTripsheetSummary(reqObj);
          props.openAlert(CONFIG.ALERT_SEVERITY.success, res.message);
        })
        .catch((error) => {
          setSpinner(false);
          onClose();
          props.openAlert(
            CONFIG.ALERT_SEVERITY.error,
            error.response && error.response.data ? error.response.data.message : "Something went wrong!"
          );
        });
    } else {
      if(!validateOdo){
      }else{
        let newFields = _.cloneDeep(fields);
        newFields = {...dataStatus.data};
        setFields(newFields);
      }
    }
  };
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    setFields(newFields);
  };
  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
      className={classes.main_dialog}
    >
      <div className={classes.dialog_container}>
        <div className={classes.dialog}>
          <DialogTitle id="simple-dialog-title" style={{ fontWeight: "bold" }}>
            Update Details
          </DialogTitle>
          {onClose ? (
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={() => {
                onClose();
                if (props.data) {
                  let newFields = _.cloneDeep(formFields);
                  newFields.start_odometer.value = props.data.start_odometer;
                  newFields.end_odometer.value = props.data.end_odometer;
                  newFields.total_fuel_filled.value =
                    props.data.total_fuel_filled;
                  newFields.material_damage.value = props.data.material_damage;
                  newFields.diesel_shortage.value = props.data.diesel_shortage;
                  setFields(newFields);
                }
              }}
            >
              <CloseIcon />
            </IconButton>
          ) : null}
        </div>
        <Grid container className={classes.form_item}>
          <Grid item xs={12}>
            <Fields.InputField
              fieldData={fields.start_odometer}
              variant="outlined"
              inputChangeHandler={inputChangeHandler}
            />
          </Grid>
        </Grid>
        <Grid container className={classes.form_item}>
          <Grid item xs={12}>
            <Fields.InputField
              fieldData={fields.end_odometer}
              variant="outlined"
              inputChangeHandler={inputChangeHandler}
            />
          </Grid>
        </Grid>
        <Grid container className={classes.form_item}>
          <Grid item xs={12}>
            <Fields.InputField
              fieldData={fields.total_fuel_filled}
              variant="outlined"
              inputChangeHandler={inputChangeHandler}
            />
          </Grid>
        </Grid>
        <Grid container className={classes.form_item}>
          <Grid item xs={12}>
            <Fields.InputField
              fieldData={fields.diesel_shortage}
              variant="outlined"
              inputChangeHandler={inputChangeHandler}
            />
          </Grid>
        </Grid>
        <Grid container className={classes.form_item}>
          <Grid item xs={12}>
            <Fields.InputField
              fieldData={fields.material_damage}
              variant="outlined"
              inputChangeHandler={inputChangeHandler}
            />
          </Grid>
        </Grid>
        <div style={{ textAlign: "right" }}>
          <Button
            variant="contained"
            margin="center"
            color="primary"
            className={classes.cancel_button}
            onClick={() => {
              handleClose();
              if (props.data) {
                let newFields = _.cloneDeep(formFields);
                newFields.start_odometer.value = props.data.start_odometer;
                newFields.end_odometer.value = props.data.end_odometer;
                newFields.total_fuel_filled.value =
                  props.data.total_fuel_filled;
                newFields.material_damage.value = props.data.material_damage;
                newFields.diesel_shortage.value = props.data.diesel_shortage;
                setFields(newFields);
              }
            }}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            margin="right"
            color="primary"
            className={classes.update_button}
            onClick={() => {
              updateHandler();
            }}
            startIcon={spinner ? <CircularProgress size={20} /> : null}
          >
            update
          </Button>
        </div>
      </div>
    </Dialog>
  );
}
SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  getTripsheetSummary: PropTypes.func.isRequired,
  openAlert: PropTypes.func.isRequired,
};
export default function UpdateTripsheet(props) {
  const classes = useStyles();
  return (
    <div>
      <SimpleDialog
        selectedValue={props.selectedValue}
        open={props.open}
        onClose={props.handleClose}
        data={props.data}
        getTripsheetSummary={props.getTripsheetSummary}
        openAlert={props.openAlert}
      />
    </div>
  );
}
