import React, { useEffect, useState } from "react";
import {
  Dialog,
  DialogTitle,
  makeStyles,
  IconButton,
  Grid,
  Button,
  CircularProgress,
  DialogContent,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import PropTypes from "prop-types";
import _ from "lodash";
import ValidateFields from "../../../validations/validateFields";
import * as Fields from "../../../sharedComponents/index";
import Util from "../../../utils";
import { Image } from "antd";

var Service;
const useStyles = makeStyles((theme) => ({
  dialog: {
    display: "flex",
    flexgrow: "1",
    fontWeight: "bold",
    justifyContent: "center",
    margin: "0px 70px ",
  },
  form_item: {
    marginBottom: "15px",
  },
  cancel_button: {
    backgroundColor: "#707070",
    textTransform: "none",
    margin: "0px 0px 10px 10px",
  },
  main_dialog: {
    margin: "auto",
  },
  dialog_container: {
    padding: "0px 20px",
  },
  add_button: {
    backgroundColor: "#215483",
    textTransform: "none",
    margin: "0px 0px 10px 10px",
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
}));
function SimpleDialog(props) {
  const formFields = {
    expenses_type: {
      label: "Expenses Type",
      name: "expenses_type",
      value: { value: "", label: "" },
      errorMsg: "Please enter expenses type",
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: true,
      isValid: true,
      options: props.exptypes,
    },
    price: {
      label: "Price(rs)",
      name: "price",
      value: "",
      type: "number",
      errorMsg: "Please enter price",
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: true,
      isValid: true,
      min: 0
    },
    unit: {
      label: "Quantity",
      name: "unit",
      value: "",
      type: "number",
      errorMsg: "Please enter unit",
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: false,
      isValid: true,
      min: 0,
    },
    unit_value: {
      label: "Price Per Quantity",
      name: "unit_value",
      value: "",
      type: "number",
      errorMsg: "Please enter unit value",
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: false,
      isValid: true,
      min: 0,
    },
    expense_datetime: {
      label: "Expense Date",
      name: "expense_datetime",
      value: null,
      isValid: true,
      maxDate: new Date(),
    },
    location: {
      label: "Location",
      name: "location",
      value: "",
      type: "text",
      errorMsg: "Please enter location",
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: false,
      isValid: true,
    },
    voucher_id: {
      label: "Voucher_id",
      name: "voucher_id",
      value: "",
      errorMsg: "Please enter voucher id",
      validPattern: "SPECIAL_CHARS_DESC",
      type: "text",
      validationRequired: false,
      isValid: true,
    },
    attachment: {
      label: "Attachments",
      name: "attachment",
      value: "",
      errorMsg: "Please enter attachment",
      validationRequired: false,
      isValid: true,
      actualValue: "",
    },
  };
  let util = new Util();
  let inputRef = React.createRef();
  useEffect(() => {
    if (!props.addExpense && props.expense) {
      let newFields = _.cloneDeep(fields);
      newFields.expenses_type.value = props.expense.servicearea
        ? {
            label: props.expense.servicearea.service_area,
            value: props.expense.servicearea.id,
          }
        : "";
      newFields.expense_datetime.value = props.expense.expense_datetime
        ? new Date(props.expense.expense_datetime)
        : null;
      newFields.price.value = props.expense.amount ? props.expense.amount : "";
      newFields.unit.value = props.expense.unit ? props.expense.unit : "";
      newFields.unit_value.value = props.expense.unit_value
        ? props.expense.unit_value
        : "";
      newFields.location.value = props.expense.location
        ? props.expense.location
        : "";
      newFields.voucher_id.value = props.expense.voucher_indent_id
        ? props.expense.voucher_indent_id
        : "";
      newFields.attachment.value = props.expense.attachement
        ? props.expense.attachement
        : "";
      setFields(newFields);
    } else {
      let newFields = _.cloneDeep(formFields);
      setFields(newFields);
    }
  }, [props.addExpense, props.expense]);
  const classes = useStyles();
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const { onClose, selectedValue, open, expenseSnackHandler } = props;
  const [spinner, setSpinner] = useState(false);
  const [isFormValidated, setIsFormValidated] = useState(false);
  const [createSpinner, setCreateSpinner] = useState(false);
  const [imageBorder, setImageBorder] = useState(null);
  const [otherDoc, setOtherDoc] = useState(null);
  const [docsOpen, setDocsOpen] = useState(false);
  const handleClose = () => {
    onClose(selectedValue);
  };
  let validateFields = new ValidateFields();
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    if (name === "unit" || name === "unit_value") {
      if (
        newFields["unit"]["value"].length > 0 &&
        newFields["unit_value"]["value"].length > 0
      ) {
        newFields["price"]["value"] =
          parseFloat(newFields["unit"]["value"]) *
          parseFloat(newFields["unit_value"]["value"]);
      }
    }
    if (name === "price") {
      newFields["unit"]["value"] = "";
      newFields["unit_value"]["value"] = "";
    }
    setFields(newFields);
  };
  Service = global.service;
  const addHandler = () => {
    let dataObj = _.cloneDeep({
      ...fields,
      expenses_type: {
        ...fields.expenses_type,
        value:
          fields.expenses_type.value.value === undefined
            ? fields.expenses_type.value
            : fields.expenses_type.value.value,
      },
    });
    const dataStatus = validateFields.validateFieldsData(dataObj);
    if (dataStatus.status) {
      setCreateSpinner(true);
      dataObj = {
        ...dataObj,
        tripsheet_id: JSON.parse(sessionStorage.getItem("tripsheet")).id,
        booking_id: props.booking_id,
      };
      const formData = new FormData();
      const tripsheet_id = JSON.parse(sessionStorage.getItem("tripsheet")).id;
      const booking_id = props.booking_id;
      const expense_type = dataObj.expenses_type.value;
      const expense_datetime = dataObj.expense_datetime.value;
      const price = dataObj.price.value;
      const unit = dataObj.unit.value;
      const unit_value = dataObj.unit_value.value;
      const location = dataObj.location.value;
      const voucher_id = dataObj.voucher_id.value;
      formData.append("tripsheet_id", tripsheet_id);
      formData.append("booking_id", booking_id);
      formData.append("expense_type", expense_type);
      formData.append("expense_datetime", expense_datetime);
      formData.append("price", price);
      formData.append("unit", unit);
      formData.append("unit_value", unit_value);
      formData.append("location", location);
      formData.append("voucher_id", voucher_id);
      formData.append("attachment", dataObj.attachment.value);
      formData.append("other_attachment", otherDoc);
      Service.CreateExpenses(formData)
        .then((res) => {
          setSpinner(false);
          onClose(selectedValue);
          setCreateSpinner(false);
          setFields(_.cloneDeep(formFields));
          setOtherDoc(null);
          setImageBorder(null);
          props.renderExpenses();
          props.getTripsheetSummary();
          props.getExpenseAttachments();
          expenseSnackHandler({
            open: true,
            severity: "success",
            message: "Expense Added Successfully!",
          });
        })
        .catch((error) => {
          setCreateSpinner(false);
          onClose(selectedValue);
          expenseSnackHandler({
            open: true,
            severity: "error",
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
        });
    } else {
      let newFields = _.cloneDeep(fields);
      newFields = dataStatus.data;
      setIsFormValidated(dataStatus.status);
      setFields(newFields);
    }
  };
  const updateHandler = () => {
    let dataObj = _.cloneDeep({
      ...fields,
      expenses_type: {
        ...fields.expenses_type,
        value:
          fields.expenses_type.value.value === undefined
            ? fields.expenses_type.value
            : fields.expenses_type.value.value,
      },
    });
    const dataStatus = validateFields.validateFieldsData(dataObj);
    if (dataStatus.status) {
      setCreateSpinner(true);
      dataObj = {
        ...dataObj,
        tripsheet_id: JSON.parse(sessionStorage.getItem("tripsheet")).id,
        expense_id: props.expense.id,
      };
      const formData = new FormData();
      const tripsheet_id = JSON.parse(sessionStorage.getItem("tripsheet")).id;
      const expense_id = props.expense.id;
      const expense_type = dataObj.expenses_type.value;
      const expense_datetime = dataObj.expense_datetime.value;
      const price = dataObj.price.value;
      const unit = dataObj.unit.value;
      const unit_value = dataObj.unit_value.value;
      const location = dataObj.location.value;
      const voucher_id = dataObj.voucher_id.value;
      formData.append("tripsheet_id", tripsheet_id);
      formData.append("expense_id", expense_id);
      formData.append("expense_type", expense_type);
      formData.append("expense_datetime", expense_datetime);
      formData.append("price", price);
      formData.append("unit", unit);
      formData.append("unit_value", unit_value);
      formData.append("location", location);
      formData.append("voucher_id", voucher_id);
      formData.append("attachment", dataObj.attachment.value);
      formData.append("other_attachment", otherDoc);
      Service.UpdateExpenses(formData)
        .then((res) => {
          setCreateSpinner(false);
          onClose(selectedValue);
          props.renderExpenses();
          props.getTripsheetSummary();
          props.getExpenseAttachments();
          setOtherDoc(null);
          setImageBorder(null);
          expenseSnackHandler({
            open: true,
            severity: "success",
            message: "Expense Updated Successfully!",
          });
        })
        .catch((error) => {
          setCreateSpinner(false);
          onClose(selectedValue);
          expenseSnackHandler({
            open: true,
            severity: "error",
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
        });
    } else {
      let newFields = _.cloneDeep(fields);
      newFields = dataStatus.data;
      setIsFormValidated(dataStatus.status);
      setFields(newFields);
    }
  };
  const autoCompleteChangeHandler = (value, name) => {
    if (value) {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = value;
      setFields(newFields);
    } else {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = { value: "", label: "" };
      setFields(newFields);
    }
  };
  const timeStampChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = util.formatDateTime(value);
    setFields(newFields);
  };
  const addBorder = (i, doc) => {
    setImageBorder(i);
    setOtherDoc(doc);
  };
  return (
    <div className={classes.root}>
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
        className={classes.main_dialog}
      >
        <div className={classes.dialog_container}>
          <div className={classes.dialog}>
            <DialogTitle id="simple-dialog-title">
              {props.addExpense ? "Add Expense" : "Update Expense"}
            </DialogTitle>
            {onClose ? (
              <IconButton
                aria-label="close"
                className={classes.closeButton}
                onClick={onClose}
              >
                <CloseIcon />
              </IconButton>
            ) : null}
          </div>
          <Grid container className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.AutoCompleteField
                fieldData={fields.expenses_type}
                autoCompleteChangeHandler={autoCompleteChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.price}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.DatePickerField
                fieldData={fields.expense_datetime}
                variant="outlined"
                timeStampChangeHandler={timeStampChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.unit}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.unit_value}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.location}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.voucher_id}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container className={classes.form_item}>
            <Grid item xs={12}>
              <input
                type="file"
                ref={inputRef}
                accept="image/*"
                style={{ display: "none" }}
                onChange={(e) => {
                  let newFields = _.cloneDeep(fields);
                  newFields["attachment"]["value"] = e.target.files[0];
                  setFields(newFields);
                }}
              />
              <Button
                style={{ textTransform: "none", width: "100%" }}
                variant="contained"
                disableElevation
                onClick={() => {
                  inputRef.current.click();
                }}
              >
                Upload Image
              </Button>
              <p style={{ margin: "0px" }}>
                {fields.attachment.value ? fields.attachment.value.name : null}
              </p>
            </Grid>
          </Grid>
          <Grid container className={classes.form_item}>
            <Grid item xs={12}>
              {props.tripDocs.length > 0 &&
              props.tripDocs[0].expense_images.length > 0 ? (
                <>
                  <div
                    style={{
                      textAlign: "center",
                      fontSize: "14px",
                      marginBottom: "5px",
                    }}
                  >
                    or
                  </div>
                  <div style={{ marginBottom: "5px", fontSize: "14px" }}>
                    Select Other Image
                  </div>
                </>
              ) : null}
              {props.tripDocs.length > 0 &&
              props.tripDocs[0].expense_images.length > 0
                ? props.tripDocs[0].expense_images.map((doc, i) => {
                    return (
                      <div style={{ display: "inline" }}>
                        {/* <Image width={100} height={100} src={`${doc[0]}`} /> */}
                        <img
                          src={`${doc[0]}`}
                          width={80}
                          height={80}
                          style={{
                            marginRight: "15px",
                            cursor: "pointer",
                            border: imageBorder === i ? "2px solid green" : "",
                            borderRadius: "4px",
                          }}
                          onClick={() => {
                            addBorder(i, doc[0]);
                          }}
                        />
                      </div>
                    );
                  })
                : null}
            </Grid>
          </Grid>
          <div style={{ textAlign: "right" }}>
            <Button
              variant="contained"
              margin="center"
              color="primary"
              className={classes.cancel_button}
              onClick={() => {
                handleClose();
              }}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              margin="right"
              color="primary"
              className={classes.add_button}
              onClick={() => {
                if (props.addExpense) {
                  addHandler();
                } else {
                  updateHandler();
                }
              }}
              startIcon={
                createSpinner ? (
                  <CircularProgress size={20} color={"#fff"} />
                ) : null
              }
            >
              {" "}
              {props.addExpense ? "Add" : "Update"}
            </Button>
          </div>
        </div>
      </Dialog>
    </div>
  );
}
SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  getTripsheetSummary: PropTypes.func.isRequired,
  getExpenseAttachments: PropTypes.func.isRequired,
  expenseSnackHandler: PropTypes.func.isRequired,
  booking_id: PropTypes.object.isRequired,
  exptypes: PropTypes.array.isRequired,
  addExpense: PropTypes.bool.isRequired,
  expense: PropTypes.object.isRequired,
  renderExpenses: PropTypes.func.isRequired,
  tripDocs: PropTypes.func.isRequired,
};
export default function CreateExpenses(props) {
  return (
    <SimpleDialog
      selectedValue={props.selectedValue}
      open={props.open}
      onClose={props.handleClose}
      data={props.data}
      getTripsheetSummary={props.getTripsheetSummary}
      getExpenseAttachments={props.getExpenseAttachments}
      expenseSnackHandler={props.expenseSnackHandler}
      booking_id={props.booking_id}
      exptypes={props.exptypes}
      addExpense={props.addExpense}
      expense={props.expense}
      renderExpenses={props.renderExpenses}
      tripDocs={props.tripDocs}
    />
  );
}
