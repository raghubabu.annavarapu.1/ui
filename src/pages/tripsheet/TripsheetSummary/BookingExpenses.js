import React from "react";
import { makeStyles, withStyles } from "@material-ui/core";
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableBody,
  Button,
  TableCell,
} from "@material-ui/core";
import { Image } from "antd";
const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#366E93",
    color: theme.palette.common.white,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);
function createData(
  Expense,
  Price,
  Unit,
  Unitvalue,
  Location,
  Datetime,
  Voucher,
  Attachements,
  Actions
) {
  return {
    Expense,
    Price,
    Unit,
    Unitvalue,
    Location,
    Datetime,
    Voucher,
    Attachements,
    Actions,
  };
}
const useStyles = makeStyles((theme) => ({
  table: {
    padding: "5px",
  },
}));
const BookingExpenses = React.forwardRef((props, ref) => {
  const classes = useStyles();
  return (
    <div ref={ref}>
      <TableContainer className={classes.table}>
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="center">Attachment</StyledTableCell>
              <StyledTableCell align="center">Expense type</StyledTableCell>
              <StyledTableCell align="center">Price</StyledTableCell>
              <StyledTableCell align="center">Unit</StyledTableCell>
              <StyledTableCell align="center">Unit value</StyledTableCell>
              <StyledTableCell align="center">Location</StyledTableCell>
              <StyledTableCell align="center">Date & time</StyledTableCell>
              <StyledTableCell align="center">
                Voucher/Indent ID#
              </StyledTableCell>
              <StyledTableCell align="center">Actions</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.expensesData.map((rec) => (
              <StyledTableRow key={rec.id}>
                <StyledTableCell align="center">
                  {rec.attachment ? (
                    <Image width={55} height={50} src={`${rec.attachment}`} />
                  ) : rec.other_attachment ? (
                    <Image
                      width={55}
                      height={50}
                      src={`${rec.other_attachment}`}
                    />
                  ) : (
                    "No Image"
                  )}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {rec.servicearea ? rec.servicearea.service_area : "--"}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {rec.amount ? rec.amount : "--"}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {rec.unit ? rec.unit : "--"}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {rec.unit_value ? rec.unit_value : "--"}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {rec.location ? rec.location : "--"}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {rec.expense_datetime &&
                  rec.expense_datetime !== "0000-00-00 00:00:00"
                    ? rec.expense_datetime
                    : "--"}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {rec.voucher_indent_id ? rec.voucher_indent_id : "--"}
                </StyledTableCell>
                <StyledTableCell align="center">
                  <Button
                    style={{
                      backgroundColor: "#366E93",
                      textTransform: "none",
                      color: "#fff",
                    }}
                    onClick={() => {
                      props.expenseHandler(rec);
                      props.handleClickOpen();
                    }}
                    selectedValue={props.selectedValue}
                  >
                    update
                    {rec.Actions}
                  </Button>
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
});
export default BookingExpenses;
