import React, { useEffect, useState } from "react";
import { Typography, Paper, Grid, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Header from "../../../components/header/index";
import { ArrowBack, MonetizationOn } from "@material-ui/icons";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import { useHistory } from "react-router-dom";
import TripSheetBooking from "./TripsheetBooking";
import { rupeeImage as rupee } from "../../../assets/index";
import UpdateTripsheet from "../UpdateTripsheet";
import CircularLoading from "../../../components/loader/circularLoading";
import AlertMessage from "../../../components/alertmessage/AlertMessage";
import _ from "lodash";
var Service;

const useStyles = makeStyles((theme) => ({
  summary_container: {
    padding: "30px 39px",
  },
  summary_head: {
    display: "flex",
  },
  alertBox: {
    padding: "10px 30px",
  },
  tripsheet: {
    padding: "0px 10px",
    fontWeight: "600",
    color: "#707070",
    opacity: "1",
    letterSpacing: "0px",
  },
  heading_text: {
    padding: "30px 0px 20px 0px",
    color: "#222B45",
    fontWeight: "600",
  },
  arrow_back: {
    cursor: "pointer",
    color: "#366E93",
  },
  tripsheet_info_1: {
    padding: "12px 16px 20px 16px",
  },
  info: {
    padding: "10px 0px 4px 0px",
    display: "flex",
    flexWrap: "wrap",
  },
  info_1_head: {
    display: "flex",
    padding: "0px 0px 10px 0px",
  },
  tripsheet_info_1_heading: {
    fontWeight: "bold",
    color: "#222B45",
    fontSize: "1.1rem",
    flexGrow: "1"
  },
  update_btn: {
    padding: "0px 20px",
    fontSize: "0.9rem",
    lineHeight: "1.74rem",
    backgroundColor: "#649B42",
    "&:hover": {
      background: "#649B42",
    },
    fontWeight: "500",
    cursor: "pointer",
    textTransform: "none"
  },
  root: {
    flexGrow: 1,
  },
  info_item: {
    padding: "14px 18px",
    borderRadius: "5px",
    background: "#FAFAFA",
    display: "flex",
    flexDirection: "column",
  },
  item_title: {
    fontSize: "0.9rem",
    color: "#366E93",
    fontWeight: "500",
  },
  item_data: {
    fontWeight: "550",
    fontSize: "0.9rem",
    color: "#000",
    letterSpacing: "0px",
  },
  tripsheet_info_2: {
    padding: "12px 0px",
    display: "flex",
    flexWrap: "wrap",
  },
  info_2_item: {
    borderLeft: "3px solid #81D322",
    width: "19.15%",
    margin: " 0px 10px 10px 0px",
    background: "#fff",
    padding: "12px 18px",
    display: "flex",
    justifyContent: "space-between",
  },
  info_2_item_price: {
    fontWeight: "bold",
    fontSize: "1rem",
    letterSpacing: "0px",
  },
  info_2_item_text: {
    fontSize: "0.8rem",
    color: "#366E93",
    fontWeight: "500",
    letterSpacing: "0",
  },
}));
const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
export default function TripsheetSummary(props) {
  const classes = useStyles();
  let history = useHistory();
  let dataObj = { owner_id: 450 };
  const [data, setData] = useState(null);
  const [open, setOpen] = useState(false);
  const [tripsheetDocs, setTripsheetDocs] = useState([]);
  const [expenseSnack, setExpenseSnack] = useState({
    open: false,
    severity: "",
    message: "",
  });
  const [selectedValue, setSelectedValue] = useState();
  const [spinner, setSpinner] = useState(false);
  const [exptypes, setExpenseTypes] = useState();
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  Service = global.service;
  useEffect(() => {
    getExpenseAttachments();
    dataObj = {
      ...dataObj,
      // tripsheet_id: JSON.parse(sessionStorage.getItem("tripsheet")).id,
      tripsheet_id: props.location.state.tripsheet_id,
    };
    setSpinner(true);
    getTripsheetSummary(dataObj);
    Service.getExpenseTypes()
      .then((res) => {
        let exptypes = res.data.map((type) => {
          return { label: type.service_area, value: type.id };
        });
        setExpenseTypes(exptypes);
      })
      .catch(() => { });
  }, []);
  const getExpenseAttachments = () => {
    Service.getExpenseAttachments({
      tripsheet_id: props.location.state.tripsheet_id,
    })
      .then((res) => {
        setTripsheetDocs(res.data);
      })
      .catch(() => { });
  };
  const getTripsheetSummary = () => {
    Service.getTripsheetSummary({
      tripsheet_id: props.location.state.tripsheet_id,
    })
      .then((res) => {
        setData(res.data);
        setSpinner(false);
      })
      .catch(() => {
        setSpinner(false);
      });
  };
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
    // setSelectedValue(value);
  };

  const getDriverFreightAmount = (orders) => {
    return orders
      .map((ord) => ord.order.frightAmount)
      .reduce((a, b) => a + b, 0);
  };

  const getTotalDriverIncentive = (orders) => {
    return orders
      .map((ord) => ord.order.driver_incentive)
      .reduce((a, b) => a + b, 0);
  };
  const openAlert = (severity, msg) => {
    setAlertData({
      open: true,
      severity: severity,
      message: msg,
    });
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  const expenseSnackHandler = (snack) => {
    let newExpenseSnack = _.cloneDeep(expenseSnack);
    newExpenseSnack["open"] = snack.open;
    newExpenseSnack["message"] = snack.message;
    newExpenseSnack["severity"] = snack.severity;
    setExpenseSnack(newExpenseSnack);
  };
  return (
    <>
      <div>
        <Header />
        {alertData.open ? (
          <div className={classes.alertBox}>
            <AlertMessage
              severity={alertData.severity}
              message={alertData.message}
              closeAlert={closeAlert}
            />
          </div>
        ) : null}
        <Snackbar
          open={expenseSnack.open}
          autoHideDuration={6000}
          onClose={() => {
            let newExpenseSnack = _.cloneDeep(expenseSnack);
            newExpenseSnack["open"] = false;
            newExpenseSnack["severity"] = "";
            newExpenseSnack["message"] = "";
            setExpenseSnack(newExpenseSnack);
          }}
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
        >
          <Alert
            onClose={() => {
              let newExpenseSnack = _.cloneDeep(expenseSnack);
              newExpenseSnack["open"] = false;
              newExpenseSnack["severity"] = "";
              newExpenseSnack["message"] = "";
              setExpenseSnack(newExpenseSnack);
            }}
            severity={expenseSnack.severity}
            sx={{ width: "100%" }}
          >
            {expenseSnack.message}
          </Alert>
        </Snackbar>
        <div className={classes.summary_container}>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div className={classes.summary_head}>
              <ArrowBack
                className={classes.arrow_back}
                onClick={() => {
                  history.push("./TripSheet");
                }}
              />
              <Typography className={classes.tripsheet}>
                Tripsheet ({props.location.state.tripsheet})
              </Typography>
            </div>
            {/* <Button 
              variant="contained"
              disableElevation
              color="primary"
              style={{ textTransform: "none" }}
              onClick={() => {
                history.push({
                  pathname: "/addTrip",
                  state: {
                    tripsheet_id: props.location.state.tripsheet_id,
                    tripsheet: props.location.state.tripsheet,
                  },
                });
              }}
            >
              Cancel Trip
            </Button> */}
          </div>
          {spinner ? (
            <CircularLoading />
          ) : (
            <>
              <Typography variant="h5" className={classes.heading_text}>
                Overall Summary
              </Typography>
              <UpdateTripsheet
                open={open}
                selectedValue={selectedValue}
                handleClose={handleClose}
                data={data}
                getTripsheetSummary={getTripsheetSummary}
                openAlert={openAlert}
              />
              <Paper elevation={1} className={classes.tripsheet_info_1}>
                <div className={classes.info_1_head}>
                  <Typography className={classes.tripsheet_info_1_heading}>
                    Trip-sheet Summary
                  </Typography>
                  {data && data.status === "Closed" ? null : (
                    <Button
                      variant="contained"
                      color="primary"
                      className={classes.update_btn}
                      onClick={() => {
                        handleClickOpen();
                      }}
                    >
                      Update
                    </Button>
                  )}
                </div>
                <div className={classes.root}>
                  <Grid container spacing={3}>
                    <Grid item xs={2}>
                      <div className={classes.info_item}>
                        <span className={classes.item_title}>Pilot name</span>
                        <span className={classes.item_data}>
                          {data && data.driver && data.driver.name}
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item}>
                        <span className={classes.item_title}>
                          Vehicle Number
                        </span>
                        <span className={classes.item_data}>
                          {data && data.truck.regi_no}
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item}>
                        <span className={classes.item_title}>Created On</span>
                        <span className={classes.item_data}>
                          {data && data.start_date}
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item}>
                        <span className={classes.item_title}>
                          Start Odometer
                        </span>
                        <span className={classes.item_data}>
                          {data && data.start_odometer}
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item}>
                        <span className={classes.item_title}>End Odometer</span>
                        <span className={classes.item_data}>
                          {data && data.end_odometer}
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item}>
                        <span className={classes.item_title}>Total km</span>
                        <span className={classes.item_data}>
                          {data && data.end_odometer - data.start_odometer}
                        </span>
                      </div>
                    </Grid>
                  </Grid>
                  <Grid container spacing={3}>
                    <Grid item xs={2}>
                      <div className={classes.info_item}>
                        <span className={classes.item_title}>
                          Total fuel filled
                        </span>
                        <span className={classes.item_data}>
                          {data && data.total_fuel_filled} lit
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item}>
                        <span className={classes.item_title}>Avg Mileage</span>
                        <span className={classes.item_data}>
                          {data &&
                            (data.end_odometer - data.start_odometer) /
                            data.total_fuel_filled
                            ? (data.end_odometer - data.start_odometer) /
                            data.total_fuel_filled
                            : 0}{" "}
                          Kmpl
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item}>
                        <span className={classes.item_title}>
                          Frieght Recieved
                        </span>
                        <span className={classes.item_data}>
                          {data && data.freight_received}
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item}>
                        <span className={classes.item_title}>
                          Fuel Shortage
                        </span>
                        <span className={classes.item_data}>
                          {data && data.diesel_shortage}
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item}>
                        <span className={classes.item_title}>
                          Material Damage
                        </span>
                        <span className={classes.item_data}>
                          {data && data.material_damage}
                        </span>
                      </div>
                    </Grid>
                  </Grid>
                </div>
              </Paper>
              <div className={classes.tripsheet_info_2}>
                <div className={classes.info_2_item}>
                  <div>
                    <span className={classes.info_2_item_price}>
                      Rs {data && data.total_expense}
                    </span>
                    <br />
                    <span className={classes.info_2_item_text}>
                      Total expenses
                    </span>
                  </div>
                  <img
                    src={rupee}
                    style={{
                      width: "24px",
                      height: "24px",
                    }}
                  />
                </div>
                <div className={classes.info_2_item}>
                  <div>
                    <span className={classes.info_2_item_price}>
                      Rs {data && data.advanced_paid_by_owner}
                    </span>
                    <br />
                    <span className={classes.info_2_item_text}>
                      Total advance paid to pilot
                    </span>
                  </div>
                  <img
                    src={rupee}
                    style={{
                      width: "24px",
                      height: "24px",
                    }}
                  />
                </div>
                <div className={classes.info_2_item}>
                  <div>
                    <span className={classes.info_2_item_price}>
                      Rs {data && data.advanced_paid_by_customer}
                    </span>
                    <br />
                    <span className={classes.info_2_item_text}>
                      Total advance paid by customer
                    </span>
                  </div>
                  <img
                    src={rupee}
                    style={{
                      width: "24px",
                      height: "24px",
                    }}
                  />
                </div>
                <div className={classes.info_2_item}>
                  <div>
                    <span className={classes.info_2_item_price}>
                      Rs{" "}
                      {data && data.orders.length > 0
                        ? getDriverFreightAmount(data.orders)
                        : null}
                    </span>
                    <br />
                    <span className={classes.info_2_item_text}>
                      Total return freight received by pilot
                    </span>
                  </div>
                  <img
                    src={rupee}
                    style={{
                      width: "24px",
                      height: "24px",
                    }}
                  />
                </div>
                <div className={classes.info_2_item}>
                  <div>
                    <span className={classes.info_2_item_price}>
                      Rs{" "}
                      {data &&
                        data.total_expense -
                        getDriverFreightAmount(data.orders)}
                    </span>
                    <br />
                    <span className={classes.info_2_item_text}>Net Income</span>
                  </div>
                  <img
                    src={rupee}
                    style={{
                      width: "24px",
                      height: "24px",
                    }}
                  />
                </div>
                <div className={classes.info_2_item}>
                  <div>
                    <span className={classes.info_2_item_price}>
                      Rs{" "}
                      {data &&
                        getDriverFreightAmount(data.orders) -
                        (data.advanced_paid_by_customer +
                          getDriverFreightAmount(data.orders))}
                    </span>
                    <br />
                    <span className={classes.info_2_item_text}>
                      Freight amount outstanding with Customer
                    </span>
                  </div>
                  <img
                    src={rupee}
                    style={{
                      width: "24px",
                      height: "24px",
                    }}
                  />
                </div>
                <div className={classes.info_2_item}>
                  <div>
                    <span className={classes.info_2_item_price}>
                      Rs{" "}
                      {data &&
                        getTotalDriverIncentive(data.orders) -
                        (data.advanced_paid_by_owner - data.total_expense)}
                    </span>
                    <br />
                    <span className={classes.info_2_item_text}>
                      {data &&
                        getTotalDriverIncentive(data.orders) -
                        (data.advanced_paid_by_owner - data.total_expense) >=
                        0
                        ? "Pay to pilot"
                        : "Collect from pilot"}
                    </span>
                  </div>
                  <img
                    src={rupee}
                    style={{
                      width: "24px",
                      height: "24px",
                    }}
                  />
                </div>
              </div>
              {data &&
                data.orders.map((booking) => {
                  let tripDocs = tripsheetDocs.filter(
                    (doc) => doc.trip_id === booking.order_id
                  );
                  return (
                    <div key={booking.id}>
                      <TripSheetBooking
                        booking={booking}
                        exptypes={exptypes}
                        getTripsheetSummary={getTripsheetSummary}
                        getExpenseAttachments={getExpenseAttachments}
                        expenseSnackHandler={expenseSnackHandler}
                        status={data.status}
                        tripDocs={tripDocs}
                      />
                      <br />
                    </div>
                  );
                })}
            </>
          )}
        </div>
      </div>
    </>
  );
}
