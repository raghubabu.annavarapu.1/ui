import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  Dialog,
  DialogTitle,
  makeStyles,
  IconButton,
  Grid,
  Button,
  CircularProgress,
  DialogContent,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import _ from "lodash";
import ValidateFields from "../../../validations/validateFields";
import * as Fields from "../../../sharedComponents/index";

const useStyles = makeStyles((theme) => ({
  dialog: {
    display: "flex",
    flexgrow: "1",
    fontWeight: "bold",
    justifyContent: "center",
    margin: "0px 70px ",
  },
  form_item: {
    marginBottom: "15px",
  },
  cancel_button: {
    backgroundColor: "#707070",
    textTransform: "none",
    margin: "0px 0px 10px 10px",
  },
  main_dialog: {
    margin: "auto",
  },
  dialog_container: {
    padding: "0px 20px",
  },
  add_button: {
    backgroundColor: "#215483",
    textTransform: "none",
    margin: "0px 0px 10px 10px",
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
}));
var Service;
function SimpleDialog(props) {
  const formFields = {
    advanced_paid_by_customer: {
      name: "advanced_paid_by_customer",
      label: "Advance Paid By Customer",
      value: "",
      type: "number",
      min: 0,
    },
    advanced_paid_by_owner: {
      name: "advanced_paid_by_owner",
      label: "Advance Paid By Owner",
      value: "",
      type: "number",
      min: 0,
    },
  };
  const classes = useStyles();
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const [spinner, setSpinner] = useState(false);
  const { open, onClose, getTripsheetSummary } = props;
  let validateFields = new ValidateFields();
  Service = global.service;
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    setFields(newFields);
  };
  const addHandler = () => {
    let dataObj = _.cloneDeep(fields);
    const dataStatus = validateFields.validateFieldsData(dataObj);
    if (dataStatus.status) {
      setSpinner(true);
      dataObj = {
        ...dataObj,
        tripsheet_id: JSON.parse(sessionStorage.getItem("tripsheet")).id,
        booking_id: props.booking_id,
      };
      Service.addAdvance(dataObj)
        .then(() => {
          setSpinner(false);
          onClose();
          getTripsheetSummary();
          let newFields = _.cloneDeep(formFields);
          setFields(newFields);
          props.expenseSnackHandler({
            open: true,
            severity: "success",
            message: "Advance Added Successfully!",
          });
        })
        .catch((error) => {
          setSpinner(false);
          props.expenseSnackHandler({
            open: true,
            severity: "error",
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
        });
    } else {
      let newFields = _.cloneDeep(fields);
      newFields = dataStatus.data;
      setFields(newFields);
    }
  };
  return (
    <div className={classes.root}>
      <Dialog
        aria-labelledby="simple-dialog-title"
        open={open}
        className={classes.main_dialog}
      >
        <div className={classes.dialog_container}>
          <div className={classes.dialog}>
            <DialogTitle id="simple-dialog-title">Add Advance</DialogTitle>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={onClose}
            >
              <CloseIcon />
            </IconButton>
          </div>
          <Grid container className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.advanced_paid_by_customer}
                inputChangeHandler={inputChangeHandler}
                variant="outlined"
              />
            </Grid>
          </Grid>
          <Grid container className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.advanced_paid_by_owner}
                inputChangeHandler={inputChangeHandler}
                variant="outlined"
              />
            </Grid>
          </Grid>
          <div style={{ textAlign: "right" }}>
            <Button
              variant="contained"
              margin="center"
              color="primary"
              className={classes.cancel_button}
              onClick={() => {
                onClose();
              }}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              margin="right"
              color="primary"
              className={classes.add_button}
              onClick={() => {
                addHandler();
              }}
              startIcon={
                spinner ? <CircularProgress size={20} color={"#fff"} /> : null
              }
            >
              Add
            </Button>
          </div>
        </div>
      </Dialog>
    </div>
  );
}
SimpleDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  getTripsheetSummary: PropTypes.func.isRequired,
  booking_id: PropTypes.object.isRequired,
  expenseSnackHandler: PropTypes.func.isRequired,
};
const AddAdvance = (props) => {
  return (
    <SimpleDialog
      open={props.open}
      onClose={props.handleClose}
      getTripsheetSummary={props.getTripsheetSummary}
      booking_id={props.booking_id}
      expenseSnackHandler={props.expenseSnackHandler}
    />
  );
};

export default AddAdvance;
