import React, { useState } from "react";
import { Paper, Typography, Button, CircularProgress } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import BookingExpenses from "./BookingExpenses";
import CreateExpense from "./CreateExpense";
import AddAdvance from "./AddAdvance";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import AccordionDetails from "@mui/material/AccordionDetails";
var Service;
const useStyles = makeStyles((theme) => ({
  booking_container: {
    padding: "12px 15px",
    borderTop: "3px solid #81D322",
  },
  booking_header: {
    display: "flex",
    justifyContent: "space-between",
    margin: "0px 0px 10px 0px",
    flexGrow: "1"
  },
  booking_heading: {
    fontWeight: "bold",
    color: "#222B45",
    fontSize: "1.1rem",
    flexGrow: "1"
  },
  booking_details: {
    display: "flex",
    flexWrap: "wrap",
    padding: "15px 0px",
  },
  br: {
    borderRight: "1px solid #212121",
  },
  detail_item: {
    padding: "0px 5px",
    width: "11.5%",
    margin: "0px 12px 8px 0px",
  },
  detail_item_text: {
    fontSize: "0.8rem",
    color: "#366E93",
    fontWeight: "500",
    letterSpacing: "0",
    verticalAlign: "top",
  },
  detail_item_info: {
    fontWeight: "750",
    fontSize: "0.8rem",
    letterSpacing: "0px",
    color: "#000000",
    verticalAlign: "bottom",
  },
  booking_prices: {
    display: "flex",
    flexWrap: "wrap",
    padding: "15px 0px",
  },
  pricing_item: {
    display: "flex",
    flexDirection: "column",
    padding: "15px",
    background: "#FAFAFA",
    width: "18.9%",
    margin: "0px 15px 10px 0px",
    borderRadius: "5px",
  },
  pricing_item_title: {
    fontSize: "0.9rem",
    color: "#366E93",
    fontWeight: "500",
    letterSpacing: "0px",
  },
  pricing_item_price: {
    fontSize: "1rem",
    fontWeight: "bold",
    letterSpacing: "0px",
  },
  exp_btns: {
    textAlign: "right",
    flexGrow: "1"
  },
  btn: {
    textTransform: "none",
    margin: "0px 10px",
    padding: "6px 18px",
  },
  booking_exp: {
    padding: "15px 0px",
  },
}));
export default function TripSheetBooking(props) {
  const classes = useStyles();
  const [expList, setExpList] = useState(false);
  const [open, setOpen] = useState(false);
  const [advanceOpen, setAdvanceOpen] = useState(false);
  const [addExpense, setAddExpense] = useState(false);
  const [selectedValue, setSelectedValue] = useState();
  const [expense, setExpense] = useState();
  const [expensesData, setExpensesData] = useState([]);
  const [spinner, setSpinner] = useState(false);
  Service = global.service;
  let { booking } = props;
  const executeScroll = () => {
    expensesRef.current &&
      expensesRef.current.scrollIntoView({ behavior: "smooth" });
  };
  const renderExpenses = () => {
    let dataObj = {
      owner_id: 450,
      booking_id: booking.order_id,
    };
    setSpinner(true);
    Service.getBookingExpenses(dataObj)
      .then((res) => {
        setExpensesData(res.data);
        setSpinner(false);
      })
      .catch(() => {
        setSpinner(false);
      });
  };
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
    setSelectedValue(value);
  };
  const getTotalExpenses = (order) => {
    return order.bookingexpenses
      .map((exp) => exp.amount)
      .reduce((a, b) => a + b, 0);
  };
  let expensesRef = React.createRef();
  const expenseHandler = (exp) => {
    setAddExpense(false);
    setExpense(exp);
  };
  const handleAdvanceClose = () => {
    setAdvanceOpen(false);
  };
  return (
    <>
      <Accordion elevation={1} className={classes.booking_container}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.booking_heading}>
            <div className={classes.booking_header}>
              <div style={{ flexGrow: "1" }}>Trip-{booking.order_id} (Lr No:{booking.order.LR ? booking.order.LR : ""})</div>
              {props.status === "Closed" ? null : (
                <div className={classes.exp_btns}>
                  <Button
                    variant="contained"
                    className={classes.btn}
                    onClick={() => {
                      setAdvanceOpen(true);
                    }}
                  >
                    Add Advance
                  </Button>
                  <Button
                    variant="contained"
                    color="secondary"
                    className={classes.btn}
                    onClick={() => {
                      setAddExpense(true);
                      handleClickOpen();
                    }}
                  >
                    Add expenses
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    className={classes.btn}
                    onClick={() => {
                      setExpList(!expList);
                      if (!expList) {
                        renderExpenses();
                        executeScroll();
                      }
                    }}
                    startIcon={
                      spinner ? (
                        <CircularProgress size={20} color={"#FFFFFF"} />
                      ) : null
                    }
                  >
                    {expList ? "Hide expenses" : "View expenses"}
                  </Button>
                </div>
              )}
            </div>
          </Typography>
          <AddAdvance
            open={advanceOpen}
            handleClose={handleAdvanceClose}
            booking_id={booking.order_id}
            getTripsheetSummary={props.getTripsheetSummary}
            expenseSnackHandler={props.expenseSnackHandler}
          />
          {props.exptypes ? (
            <CreateExpense
              open={open}
              exptypes={props.exptypes}
              handleClose={handleClose}
              booking_id={booking.order_id}
              selectedValue={selectedValue}
              addExpense={addExpense}
              expense={expense}
              renderExpenses={renderExpenses}
              getTripsheetSummary={props.getTripsheetSummary}
              getExpenseAttachments={props.getExpenseAttachments}
              expenseSnackHandler={props.expenseSnackHandler}
              tripDocs={props.tripDocs}
            />
          ) : null}
        </AccordionSummary>
        <AccordionDetails>
          <div className={classes.booking_details}>
            <div className={`${classes.detail_item} ${classes.br}`}>
              <span className={classes.detail_item_text}>LR/THC Number</span>
              <br />
              <span className={classes.detail_item_info}>{booking.order.LR}</span>
            </div>
            <div className={`${classes.detail_item} ${classes.br}`}>
              <span className={classes.detail_item_text}>Customer</span>
              <br />
              <span className={classes.detail_item_info}>
                {booking.customer_name}
              </span>
            </div>
            <div className={`${classes.detail_item} ${classes.br}`}>
              <span className={classes.detail_item_text}>Loading point</span>
              <br />
              <span className={classes.detail_item_info}>
                {booking.order.fromAddress.split(",").reverse()[3]} ,
                {booking.order.fromAddress.split(",").reverse()[2]}
              </span>
            </div>
            <div className={`${classes.detail_item} ${classes.br}`}>
              <span className={classes.detail_item_text}>Loading date</span>
              <br />
              <span className={classes.detail_item_info}>
                {booking.order.startDateTime}
              </span>
            </div>
            <div className={`${classes.detail_item} ${classes.br}`}>
              <span className={classes.detail_item_text}>Unloading point</span>
              <br />
              <span className={classes.detail_item_info}>
                {booking.order.toAddress.split(",").reverse()[3]} ,
                {booking.order.toAddress.split(",").reverse()[2]}
              </span>
            </div>
            <div className={`${classes.detail_item} ${classes.br}`}>
              <span className={classes.detail_item_text}>Unloading date</span>
              <br />
              <span className={classes.detail_item_info}>
                {booking.order.unloadDateTime}
              </span>
            </div>
            <div className={`${classes.detail_item} ${classes.br}`}>
              <span className={classes.detail_item_text}>Booking Status</span>
              <br />
              <span className={classes.detail_item_info}>Closed</span>
            </div>
            <div className={classes.detail_item}>
              <span className={classes.detail_item_text}>Booking Type</span>
              <br />
              <span className={classes.detail_item_info}>
                {booking.order.booking_type_name.charAt(0).toUpperCase() +
                  booking.order.booking_type_name.slice(1).toLowerCase()}
              </span>
            </div>
            <div className={classes.detail_item}>
              <span className={classes.detail_item_text}>Freight Amount</span>
              <br />
              <span className={classes.detail_item_info}>
                {booking.order.frightAmount} rs
              </span>
            </div>
          </div>
          <div className={classes.booking_prices}>
            <div className={classes.pricing_item}>
              <span className={classes.pricing_item_title}>
                Advance paid by fleet owner
              </span>
              <span className={classes.pricing_item_price}>
                {booking.order.advanced_paid_by_owner} rs
              </span>
            </div>
            <div className={classes.pricing_item}>
              <span className={classes.pricing_item_title}>
                Advance paid by customer
              </span>
              <span className={classes.pricing_item_price}>
                {booking.order.advanced_paid_by_customer} rs
              </span>
            </div>
            <div className={classes.pricing_item}>
              <span className={classes.pricing_item_title}>Total Expenses </span>
              <span className={classes.pricing_item_price}>
                {booking.order.bookingexpenses.length > 0
                  ? getTotalExpenses(booking.order)
                  : 0}{" "}
                rs
              </span>
            </div>
            <div className={classes.pricing_item}>
              <span className={classes.pricing_item_title}>Net Income</span>
              <span className={classes.pricing_item_price}>
                {getTotalExpenses(booking.order) - booking.order.frightAmount} rs
              </span>
            </div>
            <div className={classes.pricing_item}>
              <span className={classes.pricing_item_title}>
                Freight amount outstanding with customer
              </span>
              <span className={classes.pricing_item_price}>
                {booking.order.frightAmount -
                  (booking.order.advanced_paid_by_customer +
                    booking.order.frightAmount)}{" "}
                rs
              </span>
            </div>
          </div>
          <div className={classes.booking_exp} ref={expensesRef}>
            {expList ? (
              <BookingExpenses
                bookingId={booking.order_id}
                handleClickOpen={handleClickOpen}
                handleClose={handleClose}
                expenseHandler={expenseHandler}
                expensesData={expensesData}
              />
            ) : null}
          </div>
        </AccordionDetails>
      </Accordion>
    </>
  );
}
