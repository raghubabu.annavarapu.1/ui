import React, { useState } from "react";
import Header from "../../components/header";
import { Typography, Grid, Button } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import * as Fields from "../../sharedComponents";
import _ from "lodash";
import '../../common.css';

const AddDriver = () => {
  const history = useHistory();
  const formFields = {
    firstName: {
      name: "firstName",
      label: "FIRST NAME",
      value: "",
      topLabel: true,
    },
    lastName: {
      name: "lastName",
      label: "LAST NAME",
      value: "",
      topLabel: true,
    },
    mobileNumber: {
      name: "mobileNumber",
      label: "MOBILE NUMBER",
      value: "",
      topLabel: true,
    },
    emailId: {
      name: "emailId",
      label: "EMAIL ID",
      value: "",
      topLabel: true,
    },
    driverType: {
      name: "driverType",
      label: "DRIVER TYPE",
      value: "",
      topLabel: true,
    },
    dlNumber: {
      name: "dlNumber",
      label: "DL NUMBER",
      value: "",
      topLabel: true,
    },
    glAccount: {
      name: "glAccount",
      label: "GL ACCOUNT",
      value: "",
      topLabel: true,
    },
    flatNo: {
      name: "flatNo",
      label: "FLAT/SUITE NO",
      value: "",
      topLabel: true,
    },
    streetName: {
      name: "streetName",
      label: "STREET NAME",
      value: "",
      topLabel: true,
    },
    area: {
      name: "area",
      label: "AREA",
      value: "",
      topLabel: true,
    },
    state: {
      name: "state",
      label: "STATE",
      value: "",
      topLabel: true,
    },
    country: {
      name: "country",
      label: "COUNTRY",
      value: "",
      topLabel: true,
    },
    pincode: {
      name: "pincode",
      label: "PINCODE",
      value: "",
      topLabel: true,
    },
  };
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const inputChangeHandler = () => {};
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.push("./drivers");
              }}
            />
            Add Driver
          </Typography>
        </div>
        <div className="form_container">
          <div
            className="details_container border_bottom"
          >
            <Typography className="details_container_heading">
              User Details
            </Typography>
            <Grid
              container
              spacing={3}
              className="details_container_content"
            >
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.firstName}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.lastName}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={3}
              className="details_container_content"
            >
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.mobileNumber}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.emailId}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={3}
              className="details_container_content"
            >
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.driverType}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.dlNumber}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={3}
              className="details_container_content"
            >
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.glAccount}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
            </Grid>
          </div>
          <div className="details_container">
            <Typography className="details_container_heading">
              Address
            </Typography>
            <Grid
              container
              spacing={3}
              className="details_container_content"
            >
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.flatNo}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.streetName}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={3}
              className="details_container_content"
            >
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.area}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.state}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={3}
              className="details_container_content"
            >
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.country}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.pincode}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={3}
              className="details_container_content"
            >
              <Grid item xs={8}>
                <Grid container spacing={2}>
                  <Grid item xs={8}></Grid>
                  <Grid item xs={2}>
                    <Button
                      variant="outlined"
                      onClick={() => {
                        history.push("./drivers");
                      }}
                      className="cancel_button"
                    >
                      Cancel
                    </Button>
                  </Grid>
                  <Grid item xs={2}>
                    <Button
                      variant="outlined"
                      className="save_button"
                    >
                      Save
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddDriver;
