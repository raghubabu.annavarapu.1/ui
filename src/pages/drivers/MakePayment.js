import React, { useState } from "react";
import Header from "../../components/header";
import { Typography, Grid, Button } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import * as Fields from "../../sharedComponents";
import _ from "lodash";
import '../../common.css';

const MakePayment = () => {
  const history = useHistory();
  const formFields = {
    driverName: {
      name: "driverName",
      label: "DRIVER NAME",
      value: "",
      topLabel: true,
    },
    driverId: {
      name: "driverId",
      label: "DRIVER ID",
      value: "",
      topLabel: true,
    },
    paymentType: {
      name: "paymentType",
      label: "PAYMENT TYPE",
      value: "",
      topLabel: true,
    },
    referenceId: {
      name: "referenceId",
      label: "REFERENCE ID",
      value: "",
      topLabel: true,
    },
    amount: {
      name: "amount",
      label: "AMOUNT",
      value: "",
      topLabel: true,
    },
  };
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const inputChangeHandler = () => {};
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.push("./driverPayments");
              }}
            />
            Make Payment
          </Typography>
        </div>
        <div className="form_container">
          <div className="details_container">
            <Typography className="details_container_heading">
              Payment Details
            </Typography>
            <Grid
              container
              spacing={3}
              className="details_container_content"
            >
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.driverName}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.driverId}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={3}
              className="details_container_content"
            >
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.paymentType}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.referenceId}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={3}
              className="details_container_content"
            >
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.amount}
                  inputChangeHandler={inputChangeHandler}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid
              container
              spacing={3}
              className="details_container_content"
            >
              <Grid item xs={8}>
                <Grid container spacing={2}>
                  <Grid item xs={8}></Grid>
                  <Grid item xs={2}>
                    <Button
                      variant="outlined"
                      onClick={() => {
                        history.push("./driverPayments");
                      }}
                      className="cancel_button"
                    >
                      Cancel
                    </Button>
                  </Grid>
                  <Grid item xs={2}>
                    <Button
                      variant="outlined"
                      className="save_button"
                    >
                      Save
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MakePayment;
