import React, { useState } from "react";
import Header from "../../components/header";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { Typography, Button, Grid } from "@material-ui/core";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import HistoryIcon from "@mui/icons-material/History";
import MonetizationOnIcon from "@mui/icons-material/MonetizationOn";
import CloseIcon from "@mui/icons-material/Close";
import _ from "lodash";
import * as Fields from "../../sharedComponents";
import * as Components from "../../sharedComponents";
import * as CONFIG from "../../config/GlobalConstants";
import Avatar from "@mui/material/Avatar";
import Stack from "@mui/material/Stack";
import "../../common.css";

const headRows = [
  { id: "driver_id", disablePadding: true, label: "DRIVER ID" },
  { id: "driver_name", disablePadding: true, label: "DRIVER NAME" },
  { id: "licence", disablePadding: true, label: "LICENCE#" },
  { id: "address", disablePadding: true, label: "ADDRESS" },
  { id: "mobile", disablePadding: true, label: "MOBILE#" },
  {
    id: "driver_type",
    disablePadding: true,
    label: "DRIVER TYPE",
  },
  { id: "driver_gl_account", disablePadding: true, label: "DRIVER GL ACCOUNT" },
  { id: "actions", disablePadding: true, label: "" },
];

const tableData = [
  {
    driver_id: "12345678",
    driver_name: (
      <Stack direction="row" spacing={1} style={{ justifyContent: "center" }}>
        <div
          style={{
            padding: "2px",
            borderRadius: "50%",
            border: "1px solid #366E93",
          }}
        >
          <Avatar />{" "}
        </div>
        <span style={{ alignSelf: "center" }}>Venkateswarulu</span>
      </Stack>
    ),
    licence: "12345678",
    address: "Begumpet, Hyderabad",
    mobile: "9876543210",
    driver_type: "Full Type",
    driver_gl_account: "F000006D00035",
    actions: <Components.More options={CONFIG.DRIVER_MORE_OPTIONS} />,
  },
  {
    driver_id: "12345678",
    driver_name: (
      <Stack direction="row" spacing={1} style={{ justifyContent: "center" }}>
        <div
          style={{
            padding: "2px",
            borderRadius: "50%",
            border: "1px solid #366E93",
          }}
        >
          <Avatar />{" "}
        </div>
        <span style={{ alignSelf: "center" }}>Venkateswarulu</span>
      </Stack>
    ),
    licence: "12345678",
    address: "Begumpet, Hyderabad",
    mobile: "9876543210",
    driver_type: "Full Type",
    driver_gl_account: "F000006D00035",
    actions: <Components.More options={CONFIG.DRIVER_MORE_OPTIONS} />,
  },
  {
    driver_id: "12345678",
    driver_name: (
      <Stack direction="row" spacing={1} style={{ justifyContent: "center" }}>
        <div
          style={{
            padding: "2px",
            borderRadius: "50%",
            border: "1px solid #366E93",
          }}
        >
          <Avatar />{" "}
        </div>
        <span style={{ alignSelf: "center" }}>Venkateswarulu</span>
      </Stack>
    ),
    licence: "12345678",
    address: "Begumpet, Hyderabad",
    mobile: "9876543210",
    driver_type: "Full Type",
    driver_gl_account: "F000006D00035",
    actions: <Components.More options={CONFIG.DRIVER_MORE_OPTIONS} />,
  },
  {
    driver_id: "12345678",
    driver_name: (
      <Stack direction="row" spacing={1} style={{ justifyContent: "center" }}>
        <div
          style={{
            padding: "2px",
            borderRadius: "50%",
            border: "1px solid #366E93",
          }}
        >
          <Avatar />{" "}
        </div>
        <span style={{ alignSelf: "center" }}>Venkateswarulu</span>
      </Stack>
    ),
    licence: "12345678",
    address: "Begumpet, Hyderabad",
    mobile: "9876543210",
    driver_type: "Full Type",
    driver_gl_account: "F000006D00035",
    actions: <Components.More options={CONFIG.DRIVER_MORE_OPTIONS} />,
  },
];

const useStyles = makeStyles((theme) => ({
  header_buttons: {
    padding: "0px 15px",
  },
  client_button: {
    background: "#366E93",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    marginRight: "15px",
    "&:hover": {
      background: "#366E93",
    },
  },
  history_button: {
    background: "#649B42",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    "&:hover": {
      background: "#649B42",
    },
  },
  payment_button: {
    background: "#D08B1D",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    marginRight: "15px",
    "&:hover": {
      background: "#D08B1D",
    },
  },
  filter_buttons: {
    textAlign: "right",
    padding: "0px 15px",
  },
  clear_button: {
    padding: "6px 15px",
    background: "#D3710F0D",
    border: "1px solid #D3710F",
    color: "#D3710F",
  },
}));

const DriverList = () => {
  const classes = useStyles();
  const history = useHistory();
  const filterFields = {
    client: { label: "Search by Name", name: "client", value: "", options: [] },
    date: { label: "Select date", name: "date", value: "" },
  };
  const [filters, setFilters] = useState(_.cloneDeep(filterFields));
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  const [total, setTotal] = useState(100);
  const autoCompleteChangeHandler = () => {};
  const dateChangeHandler = () => {};
  const pageChangeHandler = () => {};
  const rowsPerPageChangeHandler = () => {};
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">Driver Details</Typography>
          <div className={classes.header_buttons}>
            <Button
              className={classes.client_button}
              startIcon={<AddCircleOutlinedIcon />}
              onClick={() => {
                history.push("./addDriver");
              }}
            >
              Add Driver
            </Button>
            <Button
              className={classes.payment_button}
              startIcon={<MonetizationOnIcon />}
              onClick={() => {}}
            >
              Make Payment
            </Button>
            <Button
              className={classes.history_button}
              startIcon={<HistoryIcon />}
            >
              History
            </Button>
          </div>
        </div>
        <div className="filter_box">
          <Grid container spacing={3}>
            <Grid item xs={9}>
              <Grid container spacing={2}>
                <Grid item xs={3}>
                  <Fields.AutoCompleteField
                    fieldData={filters.client}
                    autoCompleteChangeHandler={autoCompleteChangeHandler}
                    className="filter_field"
                  />
                </Grid>
                <Grid item xs={3}>
                  <Fields.DateField
                    fieldData={filters.date}
                    dateChangeHandler={dateChangeHandler}
                    variant="outlined"
                    className="filter_field"
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={3}>
              <div className={classes.filter_buttons}>
                <Button
                  className={classes.clear_button}
                  startIcon={<CloseIcon />}
                >
                  Clear
                </Button>
              </div>
            </Grid>
          </Grid>
        </div>
        <div>
          <Components.DataTable
            headRows={headRows}
            tableData={tableData}
            pagination={pagination}
            pageChangeHandler={pageChangeHandler}
            rowsPerPageChangeHandler={rowsPerPageChangeHandler}
            total={total}
          />
        </div>
      </div>
    </div>
  );
};

export default DriverList;
