import React, { useState, useEffect } from "react";
import Header from "../../components/header";
import "../../common.css";
import { Typography } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import * as Components from "../../sharedComponents";
import * as CONFIG from "../../config/GlobalConstants";
import _ from "lodash";
import CircularLoading from "../../components/loader/circularLoading";
const headRows = [
  { id: "id", disablePadding: true, label: "ID" },
  { id: "name", disablePadding: true, label: "NAME" },
  { id: "mobile", disablePadding: true, label: "CONTACT NUMBER" },
  { id: "email", disablePadding: true, label: "EMAIL ADDRESS" },
  { id: "emp_id", disablePadding: true, label: "EMPLOYEE ID" },
  {
    id: "role_name",
    disablePadding: true,
    label: "ROLE NAME",
  },
];
const filterFields = {
  user: { label: "Search by UserName", name: "user", value: "" },
};
const useStyles = makeStyles(() => ({
  header_buttons: {
    padding: "0px 15px",
  },
  add_button: {
    textTransform: "none",
    backgroundColor: "#649B42",
    padding: "8px 15px",
    "&:hover": {
      backgroundColor: "#649B42",
    },
  },
  clear_button: {
    padding: "6px 15px",
    background: "#D3710F0D",
    border: "1px solid #D3710F",
    color: "#D3710F",
    "&:hover": {
      background: "#D3710F0D",
    },
  },
  filter_buttons: {
    textAlign: "right",
    padding: "0px 15px",
  },
}));
var Service;
const GroupView = (props) => {
  const history = useHistory();
  const [filters, setFilters] = useState(_.cloneDeep(filterFields));
  const [tableData, setTableData] = useState([]);
  const [spinner, setSpinner] = useState(false);
  const inputChangeHandler = () => {};
  const pageChangeHandler = () => {};
  const rowsPerPageChangeHandler = () => {};
  Service = global.service;
  useEffect(() => {
    setSpinner(true);
    Service.getGroupMembers({ groupId: props.location.state.groupId })
      .then((res) => {
        let data = res.groupUsersList.map((groups) => {
          return {
            id: groups.id ? groups.id : "--",
            name: groups.name ? groups.name : "--",
            mobile: groups.mobile ? groups.mobile : "--",
            email: groups.email ? groups.email : "--",
            emp_id: groups.employeeId ? groups.employeeId : "--",
            role_name: CONFIG.USER_ROLES.filter(
              (tr) => parseInt(tr.value) === groups.roleId
            )[0].label,
          };
        });
        setTableData(data);
        setSpinner(false);
      })
      .catch(() => {
        setSpinner(false);
      });
  }, []);
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => history.push("./groups")}
            />
            Group Members (Group Name: {props.location.state.groupName})
          </Typography>
        </div>
        <div>
          {spinner ? (
            <CircularLoading />
          ) : (
            <Components.DataTable
              headRows={headRows}
              tableData={tableData}
              pagination={false}
              pageChangeHandler={pageChangeHandler}
              rowsPerPageChangeHandler={rowsPerPageChangeHandler}
            />
          )}
        </div>
      </div>
    </div>
  );
};
export default GroupView;
