import React, { useState } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Paper,
  CircularProgress,
} from "@material-ui/core";
import Draggable from "react-draggable";
import * as CONFIG from "../../config/GlobalConstants";

var Service;

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const DeleteGroup = (props) => {
  const [spinner, setSpinner] = useState(false);
  Service = global.service;
  const deleteGroupHandler = () => {
    setSpinner(true);
    Service.deleteGroup({ groupId: props.groupId })
      .then((error) => {
        setSpinner(false);
        props.handleClose();
        props.openAlert(
          CONFIG.ALERT_SEVERITY.success,
          "Group Deleted Successfully!"
        );
        props.renderGroups();
        window.scrollTo({ top: 0, behavior: "smooth" });
      })
      .catch((error) => {
        setSpinner(false);
        props.handleClose();
        props.openAlert(
          CONFIG.ALERT_SEVERITY.error,
          error.response && error.response.data
            ? error.response.data.message
            : "Something went wrong!"
        );
        window.scrollTo({ top: 0, behavior: "smooth" });
      });
  };
  return (
    <div>
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle
          style={{ cursor: "move", textAlign: "center" }}
          id="draggable-dialog-title"
        >
          Group ID {props.groupId}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure want to delete this group ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            autoFocus
            onClick={props.handleClose}
            color="primary"
            style={{ textTransform: "none" }}
          >
            Cancel
          </Button>
          <Button
            color="primary"
            style={{ textTransform: "none" }}
            onClick={() => {
              deleteGroupHandler();
            }}
            startIcon={spinner ? <CircularProgress size={20} /> : null}
          >
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default DeleteGroup;
