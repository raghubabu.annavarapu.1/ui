import React, { useEffect, useState } from "react";
import Header from "../../components/header";
import { makeStyles } from "@material-ui/core/styles";
import "../../common.css";
import { Button, Typography, Grid } from "@material-ui/core";
import DateRangeIcon from "@mui/icons-material/DateRange";
import TelegramIcon from "@mui/icons-material/Telegram";
import * as Components from "../../sharedComponents";
import * as CONFIG from "../../config/GlobalConstants";
import PeopleAltRoundedIcon from "@mui/icons-material/PeopleAltRounded";
import * as Fields from "../../sharedComponents";
import { Avatar } from "@mui/material";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import { useHistory } from "react-router-dom";
import CloseIcon from "@mui/icons-material/Close";
import CircularLoading from "../../components/loader/circularLoading";
import DeleteGroup from "./DeleteGroup";
import AlertMessage from "../../components/alertmessage/AlertMessage";
import _ from "lodash";
const filterFields = {
  user: { label: "Search by GroupName", name: "user", value: "" },
};
const headRows = [
  { id: "id", disablePadding: true, label: "ID" },
  { id: "groupname", disablePadding: true, label: "GROUP NAME" },
  { id: "created_on", disablePadding: true, label: "CREATED ON" },
  // { id: "updated_on", disablePadding: true, label: "UPDATED ON" },
  {
    id: "telegram_id",
    disablePadding: true,
    label: "TELEGRAM ID",
  },
  { id: "actions", disablePadding: true, label: "" },
];
const useStyles = makeStyles(() => ({
  clear_button: {
    padding: "6px 15px",
    background: "#D3710F0D",
    border: "1px solid #D3710F",
    color: "#D3710F",
    "&:hover": {
      background: "#D3710F0D",
    },
  },
  alertBox: {
    padding: "10px 20px 0px 20px"
  },
  filter_buttons: {
    textAlign: "right",
    padding: "0px 15px",
  },
  header_buttons: {
    padding: "0px 15px",
  },
  add_button: {
    textTransform: "none",
    backgroundColor: "#649B42",
    padding: "8px 15px",
    "&:hover": {
      backgroundColor: "#649B42",
    },
  },
}));
var Service;
const Groups = () => {
  const classes = useStyles();
  const [total, setTotal] = useState(0);
  const [filters, setFilters] = useState(_.cloneDeep(filterFields));
  const dateChangeHandler = () => { };
  const [tableData, setTableData] = useState([]);
  const [spinner, setSpinner] = useState(false);
  const [open, setOpen] = useState(false);
  const [groupId, setGroupId] = useState(null);
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  const history = useHistory();
  Service = global.service;
  useEffect(() => {
    window.scrollTo({ top: 0 });
    let dataObj = {
      skip: pagination.current * pagination.pageSize,
      limit: pagination.pageSize
    };
    renderGroups(dataObj);
  }, []);
  const renderGroups = (dataObj) => {
    setSpinner(true);
    Service.getGroups(dataObj)
      .then((res) => {
        let data = res.groups.map((groups) => {
          return {
            id: groups.id,
            groupname: groups.groupName,
            created_on: (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <stack>
                  <DateRangeIcon
                    color="red"
                    style={{
                      width: "25px",
                      height: "25px",
                      margin: "-2px 5px 0px 0px",
                      color: "#3D7396",
                      verticalAlign: "middle",
                    }}
                  />
                  {groups.createdAtRead}
                </stack>
              </div>
            ),
            telegram_id: (
              groups.telegramId ? <stack>
                <TelegramIcon
                  color="red"
                  style={{
                    width: "25px",
                    height: "25px",
                    margin: "-2px 5px 0px 0px",
                    color: "#3D7396",
                    verticalAlign: "middle",
                  }}
                />
                {groups.telegramId}
              </stack> : "--"
            ),
            actions: (
              <Components.DropDownMenu
                options={CONFIG.GROUPS_MORE_OPTIONS}
                data={{ groupId: groups.id, groupName: groups.groupName }}
                buttonClickHandler={(option) => {
                  setGroupId(groups.id);
                  moreOptionHandler(option, groups.id);
                }}
              />
            ),
          };
        });
        setTotal(res.totalCount);
        setTableData(data);
        setSpinner(false);
      })
      .catch(() => {
        setSpinner(false);
      });
  };
  const inputChangeHandler = () => { };
  const pageChangeHandler = (page) => {
    let dataObj = {
      skip: page * pagination.pageSize,
      limit: pagination.pageSize,
    };
    renderGroups(dataObj);
    setPagination({ ...pagination, current: page });
  };
  const rowsPerPageChangeHandler = (rowsPerPage) => {
    renderGroups({
      skip: pagination.current * rowsPerPage,
      limit: rowsPerPage,
    });
    setPagination({ ...pagination, pageSize: rowsPerPage });
  };
  const moreOptionHandler = (option, groupId) => {
    if (option.label === "Delete") {
      setOpen(true);
    }
  };
  const handleClose = () => {
    setOpen(false);
  };
  const openAlert = (severity, message) => {
    setAlertData({ open: true, severity: severity, message: message });
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <PeopleAltRoundedIcon style={{ verticalAlign: "middle", margin: "0px 10px 4px 0px" }} />
            Groups
          </Typography>
          <div className={classes.header_buttons}>
            <Button
              variant="contained"
              className={classes.add_button}
              color="primary"
              startIcon={<AddCircleOutlinedIcon />}
              onClick={() => history.push("./addGroup")}
            >
              Add Group
            </Button>
          </div>
        </div>
        {/* <div className="filter_box">
          <Grid container spacing={3}>
            <Grid item xs={9}>
              <Grid container spacing={2}>
                <Grid item xs={3}>
                  <Fields.InputField
                    fieldData={filters.user}
                    inputChangeHandler={inputChangeHandler}
                    className="filter_field"
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={3}>
              <div className={classes.filter_buttons}>
                <Button
                  className={classes.clear_button}
                  startIcon={<CloseIcon />}
                >
                  Clear
                </Button>
              </div>
            </Grid>
          </Grid>
        </div> */}
        <DeleteGroup
          open={open}
          handleClose={handleClose}
          groupId={groupId}
          renderGroups={() => {
            let dataObj = {
              skip: pagination.current * pagination.pageSize,
              limit: pagination.pageSize,
            };
            renderGroups(dataObj);
          }}
          openAlert={openAlert}
        />
        <div>
          {alertData.open ? (
            <div className={classes.alertBox}>
              <AlertMessage
                severity={alertData.severity}
                message={alertData.message}
                closeAlert={closeAlert}
              />
            </div>
          ) : null}
          {spinner ? (
            <CircularLoading />
          ) : (
            <Components.DataTable
              headRows={headRows}
              tableData={tableData}
              pagination={pagination}
              pageChangeHandler={pageChangeHandler}
              rowsPerPageChangeHandler={rowsPerPageChangeHandler}
              total={total}
            />
          )}
        </div>
      </div>
    </div>
  );
};
export default Groups;
