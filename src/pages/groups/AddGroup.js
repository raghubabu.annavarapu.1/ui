import React, { useState, useEffect } from "react";
import Header from "../../components/header";
import "../../common.css";
import { useHistory } from "react-router-dom";
import { Typography, Grid, Button } from "@material-ui/core";
import ArrowBack from "@material-ui/icons/ArrowBack";
import * as Fields from "../../sharedComponents";
import _ from "lodash";
import { makeStyles, CircularProgress } from "@material-ui/core";
import * as CONFIG from "../../config/GlobalConstants";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import Drawer from "@mui/material/Drawer";
import Members from "./Members";
import CloseIcon from "@mui/icons-material/Close";
import Chip from "@mui/material/Chip";
import ValidateFields from "../../validations/validateFields";
import AlertMessage from "../../components/alertmessage/AlertMessage";
import CircularLoading from "../../components/loader/circularLoading";
var Service;
const useStyles = makeStyles(() => ({
  addmember_btn: {
    textTransform: "none",
    backgroundColor: "#649B42",
    "&:hover": {
      backgroundColor: "#649B42",
    },
  },
  alertBox: {
    padding: "10px 20px 0px 20px",
  },
}));
const formFields = {
  groupName: {
    label: "Group Name",
    name: "groupName",
    value: "",
    type: "",
    errorMsg: "Please enter GroupName",
    validPattern: "SPECIAL_CHARS_DESC",
    isValid: true,
    topLabel: true,
    validationRequired: true,
  },
  telegramId: {
    label: "Telegram ID",
    name: "telegramId",
    value: "",
    type: "",
    errorMsg: "Please enter telegram id",
    validPattern: "SPECIAL_CHARS_DESC",
    isValid: true,
    topLabel: true,
    validationRequired: false,
  },
  externalMobileNumber1: {
    name: "externalMobileNumber1",
    label: "External Mobile Number 1",
    value: "",
    topLabel: true,
    isValid: true,
    errorMsg: "Please enter valid mobile number",
    maxLength: 10,
  },
  externalMobileNumber2: {
    name: "externalMobileNumber2",
    label: "External Mobile Number 2",
    value: "",
    topLabel: true,
    isValid: true,
    errorMsg: "Please enter valid mobile number",
    maxLength: 10,
  },
  externalEmails: {
    name: "externalEmails",
    label: "External Email",
    value: "",
    topLabel: true,
    isValid: true,
    errorMsg: "Please enter valid email",
  },
};
const AddGroup = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const [drawer, setDrawer] = useState(false);
  const [spinner, setSpinner] = useState(false);
  const [selectedMembers, setSelectedMembers] = useState([]);
  const [loader, setLoader] = useState(false);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  Service = global.service;
  useEffect(() => {
    if (props.location.state) {
      setLoader(true);
      Service.getGroup({ groupId: props.location.state.groupId })
        .then((res) => {
          let newFields = _.cloneDeep(fields);
          newFields["groupName"]["value"] = res.groupQueryResult.groupName;
          newFields["telegramId"]["value"] = res.groupQueryResult.telegramId;
          newFields["externalEmails"]["value"] =
            res.groupQueryResult.externalEmails ? res.groupQueryResult.externalEmails : "" ;
          if (
            res.groupQueryResult.externalMobileNumbers.split(",").length > 0
          ) {
            res.groupQueryResult.externalMobileNumbers
              .split(",")
              .forEach((num, i) => {
                newFields[`externalMobileNumber${i + 1}`]["value"] =
                  num.slice(3);
              });
          }
          setSelectedMembers([...res.groupQueryResult.groupUsers]);
          setFields(newFields);
          setLoader(false);
        })
        .catch(() => {
          setLoader(false);
        });
    }
  }, []);
  let validateFields = new ValidateFields();
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    newFields[name]["isValid"] = true;
    if (value.length === 0 && newFields[name]["validationRequired"]) {
      newFields[name]["isValid"] = false;
    }
    setFields(newFields);
  };
  const memberHandler = (members) => {
    setSelectedMembers(members);
  };
  const membersRef = React.createRef();
  const validateMobileEmail = () => {
    var valid = true;
    let newFields = _.cloneDeep(fields);
    for (var key in newFields) {
      if (newFields.hasOwnProperty(key)) {
        var val = newFields[key];
        if (
          (val.name === "externalMobileNumber1" && val.value !== "") ||
          (val.name === "externalMobileNumber2" && val.value !== "")
        ) {
          let re = /^[0-9]{10}$/;
          if (re.test(val.value)) {
            valid = true;
            newFields[val.name]["isValid"] = true;
          } else {
            valid = false;
            newFields[val.name]["isValid"] = false;
          }
        } else if (val.name === "externalEmails" && val.value !== "") {
          let re =
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          if (re.test(String(val.value).toLowerCase())) {
            valid = true;
            newFields[val.name]["isValid"] = true;
          } else {
            valid = false;
            newFields[val.name]["isValid"] = false;
          }
        }
      }
    }
    setFields(newFields);
    return { status: valid, data: newFields };
  };
  const addGroupHandler = () => {
    let dataObj = _.cloneDeep(fields);
    let dataStatus = validateFields.validateFieldsData(dataObj);
    let validPhoneEmail = validateMobileEmail();
    if (dataStatus.status && validPhoneEmail.status) {
      dataObj = { ...dataObj, groupUsers: [...selectedMembers] };
      setSpinner(true);
      Service.createGroup(dataObj)
        .then(() => {
          setSpinner(false);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.success,
            message: "Group created successfully!",
          });
          setFields(_.cloneDeep(formFields));
          setSelectedMembers([]);
          window.scrollTo({ top: 0, behavior: "smooth" });
        })
        .catch((error) => {
          setSpinner(false);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
          window.scrollTo({ top: 0, behavior: "smooth" });
        });
    } else {
      if (!validPhoneEmail.status) {
      } else {
        let newFields = _.cloneDeep(fields);
        newFields = dataStatus.data;
        setFields(newFields);
      }
    }
  };
  const updateGroupHandler = () => {
    let dataObj = _.cloneDeep(fields);
    let dataStatus = validateFields.validateFieldsData(dataObj);
    let validPhoneEmail = validateMobileEmail();
    if (dataStatus.status && validPhoneEmail.status) {
      dataObj = { ...dataObj, groupUsers: [...selectedMembers] };
      setSpinner(true);
      Service.updateGroup(dataObj, props.location.state.groupId)
        .then((res) => {
          setSpinner(false);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.success,
            message: "Group updated successfully!",
          });
          window.scrollTo({ top: 0, behavior: "smooth" });
        })
        .catch((error) => {
          console.log(error);
          setSpinner(false);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
          window.scrollTo({ top: 0, behavior: "smooth" });
        });
    } else {
      if (!validPhoneEmail.status) {
      } else {
        let newFields = _.cloneDeep(fields);
        newFields = dataStatus.data;
        setFields(newFields);
      }
    }
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => history.push("./groups")}
            />
            {props.location.state ? "Update group" : "Add Group"}
          </Typography>
          <div className={classes.header_buttons}>
            <Button
              variant="contained"
              className={classes.addmember_btn}
              color="primary"
              startIcon={<AddCircleOutlinedIcon />}
              onClick={() => {
                setDrawer(true);
              }}
            >
              Add Members
            </Button>
            <Drawer open={drawer} anchor="right" variant="persistent">
              <div style={{ width: "650px" }}>
                <div className="drawer_header">
                  <Typography variant="subtitle1">Select Members</Typography>
                  <CloseIcon
                    style={{ alignSelf: "center", cursor: "pointer" }}
                    onClick={() => {
                      membersRef.current.setMembers();
                      setDrawer(false);
                    }}
                  />
                </div>
                <Members
                  memberHandler={memberHandler}
                  ref={membersRef}
                  selectedMembers={selectedMembers}
                  drawer={drawer}
                />
              </div>
            </Drawer>
          </div>
        </div>
        {alertData.open ? (
          <div className={classes.alertBox}>
            <AlertMessage
              severity={alertData.severity}
              message={alertData.message}
              closeAlert={closeAlert}
            />
          </div>
        ) : null}
        {loader ? (
          <CircularLoading />
        ) : (
          <div className="form_container">
            <div style={{ padding: "20px" }}>
              <Typography className="details_container_heading">
                Group Details
              </Typography>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4}>
                  <Fields.InputField
                    fieldData={fields.groupName}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={4}>
                  <Fields.InputField
                    fieldData={fields.telegramId}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4}>
                  <Fields.InputField
                    fieldData={fields.externalMobileNumber1}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={4}>
                  <Fields.InputField
                    fieldData={fields.externalMobileNumber2}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4}>
                  <Fields.InputField
                    fieldData={fields.externalEmails}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </div>
            <div style={{ padding: "0px 20px" }}>
              {selectedMembers.length > 0 ? (
                <Typography
                  variant="subtitle1"
                  style={{ marginBottom: "10px" }}
                >
                  Selected Members
                </Typography>
              ) : null}
              {selectedMembers.map((mem, i) => (
                <Chip
                  key={i}
                  label={mem.name}
                  variant="outlined"
                  style={{ margin: "0px 8px 8px 0px" }}
                />
              ))}
            </div>
            <div>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={8}>
                  <Grid container spacing={2}>
                    <Grid item xs={8}></Grid>
                    <Grid item xs={2}>
                      <Button
                        variant="outlined"
                        className="cancel_button"
                        onClick={() => history.push("./groups")}
                      >
                        Cancel
                      </Button>
                    </Grid>
                    <Grid item xs={2}>
                      <Button
                        variant="contained"
                        className="save_button"
                        onClick={() => {
                          if (props.location.state) {
                            updateGroupHandler();
                          } else {
                            addGroupHandler();
                          }
                        }}
                        startIcon={
                          spinner ? (
                            <CircularProgress size={20} color={"#fff"} />
                          ) : null
                        }
                      >
                        {props.location.state ? "Update" : "Save"}
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
export default AddGroup;
