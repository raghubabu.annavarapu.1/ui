import React, {
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle,
} from "react";
import Table from "@material-ui/core/Table";
import PropTypes from "prop-types";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TablePagination from "@material-ui/core/TablePagination";
import Checkbox from "@material-ui/core/Checkbox";
import { makeStyles } from "@material-ui/core/styles";
import * as CONFIG from "../../config/GlobalConstants";
import _ from "lodash";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(3),
  },
  checkbox: {
    color: "#fff",
  },
  whiteColor: {
    color: "#fff",
    padding: "10px",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    width: 635,
    margin: "auto",
  },
  tableHead: {
    backgroundColor: "#366E93",
  },
  tableWrapper: {
    overflowX: "auto",
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

const headRows = [
  { id: "empid", label: "EMP ID", disablePadding: true },
  { id: "name", label: "NAME", disablePadding: true },
  {
    id: "email",
    label: "EMAIL",
    disablePadding: true,
  },
  {
    id: "mobile",
    label: "MOBILE",
    disablePadding: true,
  },
  {
    id: "role",
    label: "ROLE",
    disablePadding: true,
  },
];

var Service;

function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, numSelected, rowCount } = props;
  return (
    <TableHead className={classes.tableHead}>
      <TableRow>
        <TableCell width={42}>
          <Checkbox
            className={classes.checkbox}
            onChange={onSelectAllClick}
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={numSelected === rowCount}
          />
        </TableCell>
        {headRows.map((row) => (
          <TableCell
            key={row.id}
            align={"center"}
            className={classes.whiteColor}
          >
            {row.label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  numSelected: PropTypes.number.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const Members = (props, ref) => {
  const classes = useStyles();
  const [total, setTotal] = useState(0);
  const [tableData, setTableData] = useState([]);
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  const [selected, setSelected] = React.useState([]);
  Service = global.service;
  useEffect(() => {
    let dataObj = {
      skip: pagination.current * pagination.pageSize,
      limit: pagination.pageSize,
    };
    renderMembers(dataObj);
  }, []);
  const renderMembers = (dataObj) => {
    Service.getMembers(dataObj)
      .then((res) => {
        setTableData(res.groupsUsersList);
        setTotal(res.totalCount);
      })
      .catch(() => {});
  };
  useEffect(() => {
    setSelected(props.selectedMembers);
  }, [props.drawer]);
  const handleClick = (event, member) => {
    const selectedIndex = selected.findIndex(
      (mem) => mem.id === member.id && mem.roleId === member.roleId
    );
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, member);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    props.memberHandler(newSelected);
    setSelected(newSelected);
  };
  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelected = tableData.map((n) => n);
      props.memberHandler(newSelected);
      setSelected(newSelected);
      return;
    }
    props.memberHandler([]);
    setSelected([]);
  };
  const isSelected = (member) => {
    let isSelected = selected.filter(
      (mem) => mem.id === member.id && mem.roleId === member.roleId
    );
    if (isSelected.length > 0) {
      return true;
    } else {
      return false;
    }
  };
  const setMembers = () => {
    setSelected([]);
  };
  useImperativeHandle(ref, () => {
    return { setMembers: setMembers };
  });
  const pageChangeHandler = (event, page) => {
    let dataObj = {
      skip: page * pagination.pageSize,
      limit: pagination.pageSize,
    };
    renderMembers(dataObj);
    setPagination({ ...pagination, current: page });
  };
  const rowsPerPageChangeHandler = (e) => {
    let dataObj = {
      skip: pagination.current * e.target.value,
      limit: e.target.value
    }
    renderMembers(dataObj);
    setPagination({ ...pagination, pageSize: e.target.value });
  }
  return (
    <div className={classes.root} ref={ref}>
      <div className="custom_drawer_table">
        <TablePagination
          component="div"
          rowsPerPageOptions={[10, 25, 50, 100]}
          rowsPerPage={pagination.pageSize}
          page={pagination.current}
          count={total}
          onPageChange={pageChangeHandler}
          onRowsPerPageChange={rowsPerPageChangeHandler}
        />
        <Table className={classes.table}>
          <EnhancedTableHead
            classes={classes}
            onSelectAllClick={handleSelectAllClick}
            numSelected={selected.length}
            rowCount={tableData.length > 0 ? tableData.length : null}
          />
          <TableBody>
            {tableData.length > 0 &&
              tableData.map((mem, index) => {
                const isItemSelected = isSelected(mem);
                const labelId = `enhanced-table-checkbox-${index}`;
                return (
                  <TableRow
                    key={index}
                    role="checkbox"
                    selected={isItemSelected}
                    aria-checked={isItemSelected}
                    onClick={(event) => {
                      handleClick(event, mem);
                    }}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isItemSelected}
                        inputProps={{ "aria-labelledby": labelId }}
                      />
                    </TableCell>
                    <TableCell align="center">
                      {mem.employeeId ? mem.employeeId : "--"}
                    </TableCell>
                    <TableCell align="center">
                      {mem.name ? (
                        <div title={mem.name.length > 12 ? mem.name : null}>
                          {mem.name.length > 12
                            ? `${mem.name.substring(0, 10)}...`
                            : mem.name}
                        </div>
                      ) : (
                        "--"
                      )}
                    </TableCell>
                    <TableCell align="center">
                      {mem.email ? (
                        <div title={mem.email.length > 12 ? mem.email : null}>
                          {mem.email.length > 12
                            ? `${mem.email.substring(0, 10)}...`
                            : mem.email}
                        </div>
                      ) : (
                        "--"
                      )}
                    </TableCell>
                    <TableCell align="center">
                      {mem.mobile ? mem.mobile : "--"}
                    </TableCell>
                    <TableCell align="center">
                      {mem.roleId || mem.roleId === 0 ? (
                        <div
                          title={
                            CONFIG.USER_ROLES.filter(
                              (rec) => parseInt(rec.value) === mem.roleId
                            )[0].label.length > 12
                              ? `${
                                  CONFIG.USER_ROLES.filter(
                                    (rec) => parseInt(rec.value) === mem.roleId
                                  )[0].label
                                }`
                              : null
                          }
                        >
                          {CONFIG.USER_ROLES.filter(
                            (rec) => parseInt(rec.value) === mem.roleId
                          )[0].label.length > 12
                            ? `${CONFIG.USER_ROLES.filter(
                                (rec) => parseInt(rec.value) === mem.roleId
                              )[0].label.substring(0, 10)}...`
                            : CONFIG.USER_ROLES.filter(
                                (rec) => parseInt(rec.value) === mem.roleId
                              )[0].label}
                        </div>
                      ) : (
                        "--"
                      )}
                    </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </div>
    </div>
  );
};

export default forwardRef(Members);
