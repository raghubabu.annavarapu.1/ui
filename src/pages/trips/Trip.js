import React, { useState, useEffect } from "react";
import Header from "../../components/header";
import { makeStyles } from "@material-ui/core/styles";
import {
  Typography,
  Button,
  Avatar,
  Grid,
  Divider,
  Card,
} from "@material-ui/core";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import _ from "lodash";
import * as Fields from "../../sharedComponents";
import * as CONFIG from "../../config/GlobalConstants";
import { useHistory, useLocation } from "react-router-dom";
import CardContent from "@mui/material/CardContent";
import CircularLoading from "../../components/loader/circularLoading";
import * as Components from "../../sharedComponents";
import "../../common.css";
import { truckImage as truck } from "../../assets/index";
import { vehicleImage as vehicle } from "../../assets/index";
import DateRangeIcon from "@mui/icons-material/DateRange";
import Stack from "@mui/material/Stack";
import AlertMessage from "../../components/alertmessage/AlertMessage";
import PhoneAndroid from "@mui/icons-material/PhoneAndroid";
import Chip from "@mui/material/Chip";
import CloseIcon from "@mui/icons-material/Close";
import { vehicleImage2 as vehicle2 } from "../../assets/index";
import { vehicleImage3 as vehicle3 } from "../../assets/index";
import { vehicleImage4 as vehicle4 } from "../../assets/index";
import { vehicleImage5 as vehicle5 } from "../../assets/index";
import Util from "../../services/util";
const headRows = [
  { id: "TRIP_ID", disablePadding: true, label: "TRIP#" },
  { id: "VEHICLE", disablePadding: true, label: "VEHICLE" },
  { id: "DATE", disablePadding: true, label: "CREATED ON / TRIP TYPE" },
  { id: "CLIENT_NAME", disablePadding: true, label: "CLIENT NAME / REF NO" },
  { id: "DRIVER_NAME", disablePadding: true, label: "PILOT NAME/PILOT MOBILE" },
  { id: "ROUTE", label: "ROUTE", alignLeft: true, padding: true },
  { id: "STATUS", disablePadding: true, label: "STATUS" },
  { id: "Details", disablePadding: true, label: "" },
];
const useStyles = makeStyles((theme) => ({
  main_container: {
    background: "#F5F6F7",
    height: "calc(100vh - 64px)",
  },
  alertBox: {
    padding: "10px 20px",
  },
  header_box: {
    background: "#FFFFFF",
    padding: "20px 15px",
    display: "flex",
    justifyContent: "space-between",
  },
  header_text: {
    fontSize: "20px",
    fontWeight: "bold",
    lineHeight: "42px",
  },
  header_buttons: {
    padding: "0px 15px",
  },
  client_button: {
    textTransform: "none",
    backgroundColor: "#649B42",
    padding: "8px 15px",
    "&:hover": {
      backgroundColor: "#649B42",
    },
    color: "#fff",
  },
  update_button: {
    background: "#649B42",
    textTransform: "none",
    color: "#FFFFFF",
    marginRight: "15px",
    padding: "8px 15px",
    "&:hover": {
      background: "#649B42",
    },
  },
  close_button: {
    background: "#E15656",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    "&:hover": {
      background: "#E15656",
    },
  },
  filter_box: {
    padding: "20px 15px",
    display: "flex",
    justifyContent: "space-between",
    border: "2px solid #DBE0E7",
  },
  filter_field: {
    background: "#FFFFFF",
  },
  filter_buttons: {
    textAlign: "right",
    padding: "0px 15px",
  },
  edit_button: {
    background: "#D08B1D",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    marginRight: "15px",
    "&:hover": {
      background: "#D08B1D",
    },
  },
  clear_button: {
    padding: "8px 15px",
    background: "#D3710F0D",
    border: "1px solid #D3710F",
    color: "#D3710F",
    fontSize: "12px",
  },
  table_head: {
    padding: "10px 15px",
    display: "flex",
    justifyContent: "space-between",
  },
  selected_text: {
    color: "#929DAF",
    fontSize: "14px",
    letterSpacing: "0.5px",
  },
  row_paper: {
    background: "#FFFFFF",
    boxShadow: "rgba(0, 0, 0, 0.05) 0px 0px 0px 1px",
    borderRadius: "4px",
  },
  brmedium: {
    display: "block",
    marginBottom: "0.5em",
  },
  divider: {
    height: "12px",
  },
  detail_btn: {
    textTransform: "none",
    color: "#366E93",
  },
  delay_btn: {
    backgroundColor: "#E15656",
    borderRadius: "5em",
    textTransform: "none",
    color: "#fff",
    "&:hover": {
      background: "#E15656",
    },
  },
  sizeAvatar: {
    padding: "2px",
    width: "25px",
    height: "20px",
    marginTop: "2px",
  },
  feet_btn: {
    maxWidth: "60px",
    maxHeight: "22px",
    minWidth: "50px",
    minHeight: "30px",
    textTransform: "none",
  },
  list: {
    listStyle: "none",
    padding: "0px",
    margin: "0px",
  },
  list_item: {
    position: "relative",
    textAlign: "left",
    paddingLeft: "50px",
    "&::before": {
      content: '"\\25CF"',
      marginRight: "10px",
      fontSize: "22px",
      position: "absolute",
      left: "20px",
      top: "-7px",
    },
    "&:first-of-type": {
      paddingBottom: "10px",
    },
    "&:first-of-type::after": {
      content: '""',
      border: "1px dashed #707070",
      position: "absolute",
      left: "25.5px",
      height: "20px",
      top: "15px",
    },
    "&:first-of-type::before": {
      color: "#8CB369",
    },
    "&:last-of-type::before": {
      color: "red",
    },
  },
  select: {
    width: "180px",
    marginRight: "16px",
  },
  header_box: {
    display: "flex",
    padding: "20px 24px",
    background: "#ffff",
  },
  text: {
    fontSize: "14px",
    fontWeight: "bold",
    color: "#366E93",
    alignSelf: "center",
    padding: "10px",
    marginTop: "20px",
  },
}));
var Service;
const TripList = (props) => {
  let filterFields = {
    truckId: {
      name: "truckId",
      label: "Vehicles",
      value: { label: "", value: "" },
      options: [],
      validationRequired: false,
      isValid: true,
      errorMsg: "Please select vehicle",
      defaultValue: "All",
      placeholder: "Vehicle",
      allowClear: true,
      topLabel: true,
    },
    status: {
      name: "status",
      label: "Status",
      value: { label: "", value: "" },
      options: CONFIG.TRIP_STATUS.filter((opt) => opt.value !== "4"),
      validationRequired: false,
      isValid: true,
      defaultValue: "All",
      placeholder: "Status",
      allowClear: true,
      topLabel: true,
    },
    tripType: {
      name: "tripType",
      value: "",
    },
  };
  const search = useLocation().search;
  const trip_type = new URLSearchParams(search).get("tripType");
  const [filters, setFilters] = useState(_.cloneDeep(filterFields));
  const [tripType, setTripType] = useState(trip_type ? trip_type : "ALL");
  const classes = useStyles();
  const history = useHistory();
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  const [tableData, setTableData] = useState([]);
  const [statsData, setStatsData] = useState({
    tripsCount: "",
    ontimeCompletedTripsCount: "",
    delayedTripsCount: "",
    deviationTripsCount: "",
    completedTripsCount: "",
  });
  const [spinner, setSpinner] = useState(false);
  const [total, setTotal] = useState(0);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  Service = global.service;
  let util = new Util();
  useEffect(() => {
    const driver_id = new URLSearchParams(search).get("driver_id");
    let dataObj = {
      skip: pagination.current * pagination.pageSize,
      limit: pagination.pageSize,
      truckId: filters.truckId.value.value,
      status: filters.status.value.value,
      tripType: tripType,
      driverId: driver_id ? driver_id : "",
    };
    let newFilters = _.cloneDeep(filterFields);
    if (
      props.location.state &&
      props.location.state.filters &&
      props.location.state.pagination
    ) {
      newFilters["truckId"]["value"] =
        props.location.state.filters.truckId.value;
      newFilters["status"]["value"] = props.location.state.filters.status.value;
      setFilters(newFilters);
      setPagination({
        ...pagination,
        current:
          props.location.state.pagination.skip /
          props.location.state.pagination.limit,
        pageSize: props.location.state.pagination.limit,
      });
      dataObj = {
        ...dataObj,
        skip:
          props.location.state.pagination.skip ||
          props.location.state.pagination.skip === 0
            ? props.location.state.pagination.skip
            : pagination.current * pagination.pageSize,
        limit:
          props.location.state.pagination.limit ||
          props.location.state.pagination.limit === 0
            ? props.location.state.pagination.limit
            : pagination.pageSize,
        truckId: newFilters.truckId.value.value,
        status: newFilters.status.value.value,
      };
    }
    Service.getVehicles({ allVehicles: true })
      .then((res) => {
        let newFilters = _.cloneDeep(filters);
        let vehicleOptions = res.vehicles.map((veh) => {
          return { label: veh.registrationNumber, value: veh.id };
        });
        if (props.location.state && props.location.state.filters) {
          newFilters["truckId"]["value"] =
            props.location.state.filters.truckId.value;
          newFilters["status"]["value"] =
            props.location.state.filters.status.value;
        }
        newFilters["truckId"]["options"] = vehicleOptions;
        setFilters(newFilters);
      })
      .catch(() => {});
    renderTrips(dataObj, newFilters);
  }, []);
  const renderTrips = (dataObj, fil) => {
    setSpinner(true);
    Service.getTrips(dataObj)
      .then((res) => {
        let data = res.tripsData.map((trip) => {
          return {
            TRIP_ID: <div style={{ padding: "0px 20px" }}>{trip.id}</div>,
            VEHICLE: (
              <Stack
                direction="row"
                spacing={1}
                style={{ justifyContent: "center" }}
              >
                <div
                  style={{
                    padding: "2px",
                    borderRadius: "80%",
                    border: "1px solid #366E93",
                    position: "relative",
                  }}
                >
                  <Avatar style={{ background: "#3D739619" }}>
                    <img
                      src={truck}
                      style={{
                        width: "20px",
                        height: "15px",
                        marginTop: "2px",
                        backgroundColor: "#3D739619",
                      }}
                    />
                  </Avatar>
                  <div
                    style={{
                      height: "10px",
                      width: "10px",
                      background: trip.tatDelay ? "#F24B4B" : "#008300",
                      borderRadius: "100%",
                      position: "absolute",
                      top: "2px",
                      left: "0px",
                    }}
                  ></div>
                </div>
                <span style={{ alignSelf: "center", whiteSpace: "nowrap" }}>
                  {trip.truck.registrationNumber}
                </span>
              </Stack>
            ),
            DATE: (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <span>
                  <Stack
                    direction="row"
                    spacing={1}
                    style={{ justifyContent: "center" }}
                  >
                    <DateRangeIcon
                      color="red"
                      style={{
                        width: "25px",
                        height: "25px",
                        margin: "-2px 5px 0px 0px",
                        color: "#3D7396",
                        verticalAlign: "middle",
                      }}
                    />
                    {trip.createdAtRead}
                  </Stack>
                </span>
                <span>{trip.bookingType}</span>
              </div>
            ),
            CLIENT_NAME: (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <span>{trip.client ? trip.client.companyName : "--"}</span>
                <span>{trip.lr ? trip.lr : ""}</span>
              </div>
            ),
            DRIVER_NAME: (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <span>{trip.driver ? trip.driver.name : ""}</span>
                <span>
                  <div
                    direction="row"
                    spacing={1}
                    style={{ justifyContent: "center", display: "flex" }}
                  >
                    <PhoneAndroid
                      style={{
                        width: "15px",
                        height: "18px",
                        color: "#3D7396",
                        marginRight: "5px",
                      }}
                    />
                    {trip.driver ? trip.driver.mobile : ""}
                  </div>
                </span>
              </div>
            ),
            // DRIVER_PHNO: (
            //   <div
            //     direction="row"
            //     spacing={1}
            //     style={{ justifyContent: "center", display: "flex" }}
            //   >
            //     <PhoneAndroid
            //       style={{
            //         width: "15px",
            //         height: "18px",
            //         color: "#3D7396",
            //         marginRight: "5px",
            //       }}
            //     />
            //     {trip.driver ? trip.driver.mobile : ""}
            //   </div>
            // ),
            ROUTE:
              trip.fromAddress &&
              trip.toAddress &&
              trip.fromAddress.split(",").length > 0 &&
              trip.toAddress.split(",").length > 0 ? (
                <ul className={classes.list}>
                  <li className={classes.list_item}>
                    {trip.fromAddress.split(",").reverse()[2]
                      ? trip.fromAddress.split(",").reverse()[2]
                      : trip.fromAddress.split(",").reverse()[0]}
                  </li>
                  <li className={classes.list_item}>
                    {trip.toAddress.split(",").reverse()[2]
                      ? trip.toAddress.split(",").reverse()[2]
                      : trip.toAddress.split(",").reverse()[0]}
                  </li>
                </ul>
              ) : (
                <div style={{ padding: "0px 20px" }}>--</div>
              ),
            STATUS: (
              <div>
                <Chip
                  style={{
                    background: `${
                      CONFIG.TRIP_STATUS.filter(
                        (tr) => parseInt(tr.value) === trip.status
                      )[0].color
                    }`,
                    color: "#fff",
                  }}
                  label={
                    CONFIG.TRIP_STATUS.filter(
                      (tr) => parseInt(tr.value) === trip.status
                    )[0].label
                  }
                />
              </div>
            ),
            DETAILS: (
              <div>
                <Components.DropDownMenu
                  options={CONFIG.TRIP_MORE_OPTIONS}
                  data={{
                    tripId: trip.id,
                    tripType: dataObj.tripType ? dataObj.tripType : "ALL",
                    filters: fil,
                    pagination: { skip: dataObj.skip, limit: dataObj.limit },
                  }}
                />
              </div>
            ),
          };
        });
        switch (dataObj.tripType) {
          case "ONTIME":
            setTotal(res.ontimeCompletedTripsCount);
            break;
          case "DELAYED":
            setTotal(res.delayedTripsCount);
            break;
          case "DEVIATION":
            setTotal(res.deviationTripsCount);
            break;
          case "COMPLETED":
            setTotal(res.completedTripsCount);
            break;
          default:
            setTotal(res.tripsCount);
            break;
        }
        setTableData(data);
        setStatsData({
          tripsCount: res.tripsCount,
          ontimeCompletedTripsCount: res.ontimeCompletedTripsCount,
          delayedTripsCount: res.delayedTripsCount,
          deviationTripsCount: res.deviationTripsCount,
          completedTripsCount: res.completedTripsCount,
        });
        setSpinner(false);
      })
      .catch((error) => {
        setSpinner(false);
      });
  };
  const pageChangeHandler = (page) => {
    let fil = Object.values(filters).map((fil) => fil);
    let dataObj;
    if (fil.length > 0) {
      let filters = fil.map((fil) => {
        switch (fil.name) {
          case "truckId":
            return { truckId: fil.value.value };
          case "status":
            return { status: fil.value.value };
          case "tripType": {
            return { tripType: tripType };
          }
          default:
            return;
        }
      });
      filters = Object.assign({}, ...filters);
      dataObj = {
        skip: page * pagination.pageSize,
        limit: pagination.pageSize,
        ...filters,
      };
    } else {
      dataObj = {
        skip: page * pagination.pageSize,
        limit: pagination.pageSize,
      };
    }
    renderTrips(dataObj, filters);
    setPagination({ ...pagination, current: page });
  };
  const rowsPerPageChangeHandler = (rowsPerPage) => {
    let fil = Object.values(filters).map((fil) => fil);
    let dataObj;
    if (fil.length > 0) {
      let filters = fil.map((fil) => {
        switch (fil.name) {
          case "truckId":
            return { truckId: fil.value.value };
          case "status":
            return { status: fil.value.value };
          case "tripType": {
            return { tripType: tripType };
          }
          default:
            return;
        }
      });
      filters = Object.assign({}, ...filters);
      dataObj = {
        skip: pagination.current * rowsPerPage,
        limit: rowsPerPage,
        ...filters,
      };
    } else {
      dataObj = {
        skip: pagination.current * rowsPerPage,
        limit: rowsPerPage,
      };
    }
    renderTrips(dataObj, filters);
    setPagination({ ...pagination, pageSize: rowsPerPage });
  };
  const statHandler = (tripType) => {
    let newFilters = _.cloneDeep(filters);
    newFilters["tripType"]["value"] = tripType;
    let searchParam = util.queryParamsFromObj("", {
      tripType: tripType,
      vehicle:
        newFilters.truckId.value.value === undefined
          ? ""
          : newFilters.truckId.value.label,
      status:
        newFilters.status.value.value === undefined
          ? ""
          : newFilters.status.value.label,
    });
    setTripType(tripType);
    setPagination({
      current: CONFIG.PAGINATION.current,
      pageSize: CONFIG.PAGINATION.pageSize,
    });
    history.push({
      pathname: "/Trip",
      search: searchParam,
    });
    setFilters(newFilters);
    let dataObj = {
      skip: CONFIG.PAGINATION.current * CONFIG.PAGINATION.pageSize,
      limit: CONFIG.PAGINATION.pageSize,
      truckId: filters.truckId.value.value,
      status: filters.status.value.value,
      tripType: tripType,
    };
    renderTrips(dataObj, newFilters);
  };
  const autoCompleteChangeHandler = (value, name) => {
    let newFilters = _.cloneDeep(filters);
    let dataObj;
    if (value) {
      newFilters[name]["value"] = value;
      let searchParam = util.queryParamsFromObj("", {
        tripType: "ALL",
        vehicle:
          newFilters.truckId.value.value === undefined
            ? ""
            : newFilters.truckId.value.label,
        status:
          newFilters.status.value.value === undefined
            ? ""
            : newFilters.status.value.label,
      });
      setFilters(newFilters);
      setTripType("ALL");
      history.push({
        pathname: "/Trip",
        search: searchParam,
      });
      setPagination({ ...pagination, current: 0 });
      dataObj = {
        skip: pagination.current * pagination.pageSize,
        limit: pagination.pageSize,
        truckId:
          name === "truckId"
            ? value.value
            : newFilters.truckId.value.value === undefined
            ? newFilters.truckId.value
            : newFilters.truckId.value.value,
        status:
          name === "status"
            ? value.value
            : newFilters.status.value.value === undefined
            ? newFilters.status.value
            : newFilters.status.value.value,
      };
    } else {
      newFilters[name]["value"] = { label: "", value: "" };
      setFilters(newFilters);
      setTripType("ALL");
      let searchParam = util.queryParamsFromObj("", {
        tripType: "ALL",
        vehicle:
          newFilters.truckId.value.value === undefined
            ? newFilters.truckId.value
            : newFilters.truckId.value.label,
        status:
          newFilters.status.value.value === undefined
            ? newFilters.status.value
            : newFilters.status.value.label,
      });
      history.push({
        pathname: "/Trip",
        search: searchParam,
      });
      dataObj = {
        skip: pagination.current * pagination.pageSize,
        limit: pagination.pageSize,
        truckId:
          newFilters.truckId.value.value === undefined
            ? newFilters.truckId.value
            : newFilters.truckId.value.value,
        status:
          newFilters.status.value.value === undefined
            ? newFilters.status.value
            : newFilters.status.value.value,
      };
    }
    renderTrips(dataObj, newFilters);
  };
  const clearHandler = () => {
    let dataObj;
    let newFilters = _.cloneDeep(filters);
    newFilters.truckId.value = { value: "", label: "" };
    newFilters.status.value = { value: "", label: "" };
    setFilters(newFilters);
    dataObj = {
      skip: pagination.current * pagination.pageSize,
      limit: pagination.pageSize,
      truckId: newFilters.truckId.value.value,
      status: newFilters.status.value.value,
      tripType: "",
    };
    renderTrips(dataObj, newFilters);
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">Trips</Typography>
          <div className={classes.header_buttons}>
            <Button
              className={classes.client_button}
              startIcon={<AddCircleOutlinedIcon />}
              onClick={() => {
                history.push("./addTrip");
              }}
            >
              Add Trip
            </Button>
          </div>
        </div>
        {
          <div style={{ padding: "20px 24px 10px 24px", display: "flex" }}>
            <Card
              style={{
                padding: "5px",
                width: "20%",
                marginRight: "15px",
                cursor: "pointer",
                border: tripType === "ALL" ? "2px solid #1890ff" : "",
              }}
              onClick={() => statHandler("ALL")}
            >
              <CardContent>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div style={{ width: "75%" }}>
                    <Typography
                      style={{ fontWeight: "bold", fontSize: "20px" }}
                    >
                      {spinner ? (
                        <span style={{ padding: "29px" }}></span>
                      ) : (
                        statsData.tripsCount
                      )}
                    </Typography>
                    <Typography
                      style={{
                        color: "#366E93",
                        fontSize: "12px",
                        fontWeight: ["550"],
                        fontFamily: "Montserrat,SemiBold",
                      }}
                    >
                      Total Number of Trips
                    </Typography>
                  </div>
                  <div
                    style={{
                      alignSelf: "center",
                    }}
                  >
                    <Avatar
                      style={{ background: "#3D739619", padding: "24px" }}
                    >
                      <img
                        src={vehicle}
                        style={{
                          width: "23px",
                          height: "23px",
                        }}
                      />
                    </Avatar>
                  </div>
                </div>
              </CardContent>
            </Card>
            <Card
              style={{
                padding: "5px",
                width: "20%",
                marginRight: "15px",
                cursor: "pointer",
                border: tripType === "COMPLETED" ? "2px solid #1890ff" : "",
              }}
              onClick={() => statHandler("COMPLETED")}
            >
              <CardContent>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div style={{ width: "75%" }}>
                    <Typography
                      style={{ fontWeight: "bold", fontSize: "20px" }}
                    >
                      {spinner ? (
                        <span style={{ padding: "29px" }}></span>
                      ) : (
                        statsData.completedTripsCount
                      )}
                    </Typography>
                    <Typography
                      style={{
                        color: "#366E93",
                        fontSize: "12px",
                        fontWeight: ["550"],
                        fontFamily: "Montserrat,SemiBold",
                      }}
                    >
                      Completed Trips
                    </Typography>
                  </div>
                  <div
                    style={{
                      alignSelf: "center",
                    }}
                  >
                    <Avatar
                      style={{ background: "#3D739619", padding: "24px" }}
                    >
                      <img
                        src={vehicle2}
                        style={{
                          width: "29px",
                          height: "29px",
                        }}
                      />
                    </Avatar>
                  </div>
                </div>
              </CardContent>
            </Card>
            <Card
              style={{
                padding: "5px",
                cursor: "pointer",
                width: "20%",
                marginRight: "15px",
                border: tripType === "ONTIME" ? "2px solid #1890ff" : "",
              }}
              onClick={() => statHandler("ONTIME")}
            >
              <CardContent>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div style={{ width: "75%" }}>
                    <Typography
                      style={{ fontWeight: "bold", fontSize: "20px" }}
                    >
                      {spinner ? (
                        <span style={{ padding: "29px" }}></span>
                      ) : (
                        statsData.ontimeCompletedTripsCount
                      )}
                    </Typography>
                    <Typography
                      style={{
                        color: "#366E93",
                        fontSize: "12px",
                        fontWeight: ["550"],
                        fontFamily: "Montserrat,SemiBold",
                      }}
                    >
                      On Time Deliveries
                    </Typography>
                  </div>
                  <div
                    style={{
                      alignSelf: "center",
                    }}
                  >
                    <Avatar
                      style={{ background: "#3D739619", padding: "24px" }}
                    >
                      <img
                        src={vehicle5}
                        style={{
                          width: "30px",
                          height: "30px",
                        }}
                      />
                    </Avatar>
                  </div>
                </div>
              </CardContent>
            </Card>
            <Card
              style={{
                padding: "5px",
                cursor: "pointer",
                width: "20%",
                marginRight: "15px",
                border: tripType === "DELAYED" ? "2px solid #1890ff" : "",
              }}
              onClick={() => statHandler("DELAYED")}
            >
              <CardContent>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div style={{ width: "75%" }}>
                    <Typography
                      style={{ fontWeight: "bold", fontSize: "20px" }}
                    >
                      {spinner ? (
                        <span style={{ padding: "29px" }}></span>
                      ) : (
                        statsData.delayedTripsCount
                      )}
                    </Typography>
                    <Typography
                      style={{
                        color: "#366E93",
                        fontSize: "12px",
                        fontWeight: ["550"],
                        fontFamily: "Montserrat,SemiBold",
                      }}
                    >
                      Delayed Trips
                    </Typography>
                  </div>
                  <div
                    style={{
                      fontWeight: "bold",
                      alignSelf: "center",
                    }}
                  >
                    <Avatar
                      style={{ background: "#3D739619", padding: "24px" }}
                    >
                      <img
                        src={vehicle3}
                        style={{
                          width: "29px",
                          height: "29px",
                        }}
                      />
                    </Avatar>
                  </div>
                </div>
              </CardContent>
            </Card>
            <Card
              style={{
                padding: "5px",
                width: "20%",
                marginRight: "15px",
                background: "#efefef",
              }}
              // onClick={() => statHandler("DEVIATION")}
            >
              <CardContent>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div style={{ width: "75%" }}>
                    <Typography
                      style={{ fontWeight: "bold", fontSize: "20px" }}
                    >
                      {spinner ? (
                        <span style={{ padding: "29px" }}></span>
                      ) : (
                        statsData.deviationTripsCount
                      )}
                    </Typography>
                    <Typography
                      style={{
                        color: "#366E93",
                        fontSize: "12px",
                        fontWeight: ["550"],
                        fontFamily: "Montserrat,SemiBold",
                      }}
                    >
                      Trips Deviations (Coming Soon)
                    </Typography>
                  </div>
                  <div
                    style={{
                      fontWeight: "bold",
                      alignSelf: "center",
                    }}
                  >
                    <Avatar
                      style={{ background: "#3D739619", padding: "24px" }}
                    >
                      <img
                        src={vehicle4}
                        style={{
                          width: "29px",
                          height: "29px",
                        }}
                      />
                    </Avatar>
                  </div>
                </div>
              </CardContent>
            </Card>
          </div>
        }
        <div style={{ padding: "10px 24px 20px 24px" }}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Typography
                  className={classes.text}
                  style={{ fontFamily: "Montserrat,SemiBold" }}
                >
                  Filter by
                </Typography>
                <Grid item xs={2}>
                  <Fields.AntSelectableSearchField
                    fieldData={filters.truckId}
                    autoCompleteChangeHandler={autoCompleteChangeHandler}
                  />
                </Grid>
                <Grid item xs={2}>
                  <Fields.AntSelectableSearchField
                    fieldData={filters.status}
                    autoCompleteChangeHandler={autoCompleteChangeHandler}
                  />
                </Grid>
                <Grid item xs={2} style={{ alignSelf: "flex-end" }}>
                  <Button
                    className={classes.clear_button}
                    style={{ fontFamily: "Montserrat,SemiBold" }}
                    startIcon={<CloseIcon style={{ fontSize: "15px" }} />}
                    onClick={() => {
                      clearHandler();
                    }}
                  >
                    Clear
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>
        <Divider style={{ margin: "0px 20px 0px 20px" }} />
        <div>
          {alertData.open ? (
            <div className={classes.alertBox}>
              <AlertMessage
                severity={alertData.severity}
                message={alertData.message}
                closeAlert={closeAlert}
              />
            </div>
          ) : null}
          {spinner ? (
            <CircularLoading />
          ) : (
            <Components.DataTable
              headRows={headRows}
              tableData={tableData}
              pagination={pagination}
              total={total}
              pageChangeHandler={pageChangeHandler}
              rowsPerPageChangeHandler={rowsPerPageChangeHandler}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default TripList;
