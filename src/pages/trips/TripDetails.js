import React, { useEffect, useState, useRef } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Header from "../../components/header";
import { Typography, Box, TablePagination } from "@material-ui/core";
import VisibilityIcon from "@mui/icons-material/Visibility";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import CloseIcon from "@mui/icons-material/Close";
import EditIcon from "@mui/icons-material/Edit";
import AddIcon from "@mui/icons-material/Add";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import {
  Step,
  Stepper,
  StepLabel,
  Table,
  TableContainer,
  TableBody,
  TableHead,
  TableCell,
  TableRow,
  Button,
  Grid,
} from "@material-ui/core";
import CancelIcon from "@mui/icons-material/Cancel";
import { useHistory } from "react-router-dom";
import { Collapse } from "antd";
import CircularLoading from "../../components/loader/circularLoading";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import * as CONFIG from "../../config/GlobalConstants";
import UpdateIcon from "@mui/icons-material/Update";
import Chip from "@mui/material/Chip";
import "../../common.css";
import _ from "lodash";
import CancelTrip from "./CancelTrip";
import UpdateRouteDetails from "./UpdateRouteDetails";
import AlertMessage from "../../components/alertmessage/AlertMessage";
import Util from "../../services/util";
import { IconButton } from "@mui/material";
import LocationUpdate from "./createTrip/LocationUpdate";
import IndentReciept from "../indent/IndentReciept";
import AddCircleRoundedIcon from "@mui/icons-material/AddCircleRounded";
const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#B7ECFF",
    color: theme.palette.common.black,
    // fontFamily: "montserrat"
  },
}))(TableCell);
const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
      // fontFamily: "montserrat"
    },
  },
}))(TableRow);
const useStyles = makeStyles((theme) => ({
  main_list: {
    display: "flex",
    margin: "20px",
  },
  alertBox: {
    padding: "10px 12px",
  },
  addindent_btn: {
    textTransform: "none",
    padding: "8px 15px",
    // backgroundColor: "#649B42",
    marginRight: "15px",
    "&:hover": {
      // backgroundColor: "#649B42",
      padding: "8px 15px",
    },
  },
  table_column: {
    fontSize: "10px",
    padding: "8px 2px",
    lineHeight: "1rem",
  },
  table_data: {
    fontSize: "12px",
  },
  arrec: {
    marginRight: theme.spacing(2),
    cursor: "pointer",
    width: "30PX",
    height: "30px",
  },
  main_heading: {
    fontWeight: "bold",
    fontSize: "20PX",
  },
  sizeAvatar: {
    height: theme.spacing(2),
    width: theme.spacing(2),
  },
  travels_kms: {
    marginRight: theme.spacing(12),
    fontSize: "13px",
  },
  header_buttons: {
    padding: "0px 15px",
  },
  download_btn: {
    textTransform: "none",
    backgroundColor: "#888D202E",
    marginRight: theme.spacing(2),
    "&:hover": {
      backgroundColor: "#888D202E",
    },
  },
  editing_btn: {
    backgroundColor: "#3CAADB3C",
    textTransform: "none",
    marginRight: theme.spacing(2),
    "&:hover": {
      backgroundColor: "#3CAADB3C",
    },
  },
  delete_btn: {
    backgroundColor: "#E1565631",
    textTransform: "none",
    "&:hover": {
      backgroundColor: "#E1565631",
    },
  },
  root: {
    flexGrec: 1,
    padding: "20px",
  },
  item: {
    height: "100%",
  },
  info_item1: {
    padding: "14px 18px",
    borderRadius: "5px",
    display: "flex",
    flexDirection: "column",
    borderLeft: "3px solid #95DDF7",
    background: "#E8F9FF",
    height: "100%",
  },
  info_item2: {
    padding: "14px 18px",
    borderRadius: "5px",
    display: "flex",
    flexDirection: "column",
    borderLeft: "3px solid #FFD490",
    background: "#FFF6E8",
    height: "100%",
  },
  info_item3: {
    padding: "14px 18px",
    borderRadius: "5px",
    display: "flex",
    flexDirection: "column",
    borderLeft: "3px solid #FD859A",
    background: "#FFE8EC",
    height: "100%",
  },
  item_title: {
    fontSize: "0.8rem",
    padding: "2px",
  },
  item_data: {
    fontWeight: "550",
    fontSize: "0.9rem",
    color: "#000",
    letterSpacing: "0px",
  },
  cancel_button: {
    background: "#E15656",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    "&:hover": {
      background: "#E15656",
    },
  },
  update_trip: {
    background: "#649B42",
    textTransform: "none",
    color: "#FFFFFF",
    marginRight: "15px",
    padding: "8px 15px",
    "&:hover": {
      background: "#649B42",
    },
  },
}));
const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
const { Panel } = Collapse;
var Service;
const TripDetails = (props) => {
  const classes = useStyles();
  const [loader, setLoader] = useState(false);
  const [tripData, setTripData] = useState();
  const [documents, setDocuments] = useState();
  const [eLocks, setElocks] = useState([]);
  const [open, setOpen] = useState(false);
  const [routeDetailsOpen, setRouteDetailsOpen] = useState(false);
  const [dialog, setDialog] = useState([]);
  const [moreInfoOpen, setMoreInfoOpen] = useState(false);
  const [loc, setLoc] = useState(null);
  const [eLockImage, setElockImage] = useState([]);
  const [locationSnack, setLocationSnack] = useState({
    open: false,
    severity: "",
    message: "",
  });
  let util = new Util();
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  const fileViewHandler = (rec, i) => {
    const newOpen = [...dialog];
    newOpen[i] = true;
    setDialog(newOpen);
  };
  const elockImageViewHandler = (rec, i) => {
    const newOpen = [...eLockImage];
    newOpen[i] = { [rec]: true };
    setElockImage(newOpen);
  };
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  const [activeStep, setActiveStep] = useState();
  let history = useHistory();
  let locationUpdateRef = useRef();
  let routeDetailsRef = useRef();
  Service = global.service;
  let dataObj = {
    tripId: props.location.state.tripId,
  };
  useEffect(() => {
    setLoader(true);
    getTrip();
    window.scrollTo({ top: 0 });
  }, []);
  const getTrip = () => {
    Service.getTrip(dataObj)
      .then((res) => {
        let step = res.trip.tripWayPoints.filter((wp) => wp.status === "Exit");
        if (step.length > 0) {
          setActiveStep(step.length);
        } else {
          let noneStatus = res.trip.tripWayPoints.filter(
            (wp) => wp.status === "None"
          );
          if (noneStatus.length === res.trip.tripWayPoints.length) {
            setActiveStep(-1);
          } else {
            setActiveStep(0);
          }
        }
        setTripData(res);
        setLoader(false);
      })
      .catch((error) => {
        setLoader(false);
        setAlertData({
          open: true,
          severity: CONFIG.ALERT_SEVERITY.error,
          message:
            error.response && error.response.data
              ? error.response.data.message
              : "Something went wrong!",
        });
      });
  };
  const handleRouteDetailsClose = () => {
    setRouteDetailsOpen(false);
  };
  const handleCancelClose = () => {
    setOpen(false);
  };
  const handleClose = (rec, i) => {
    const newOpen = [...dialog];
    newOpen[i] = false;
    setDialog(newOpen);
  };
  useEffect(() => {
    setLoader(true);
    getTripsDocuments();
    getElocks();
  }, []);
  const getTripsDocuments = () => {
    Service.getTripsDocuments({ tripId: props.location.state.tripId })
      .then((res) => {
        // getTripsDocuments();
        setDocuments(res);
      })
      .catch((error) => {
        setLoader(false);
      });
  };
  const getElocks = () => {
    Service.getElocks({ tripId: props.location.state.tripId })
      .then((res) => {
        setElocks(res.securityTagDetails);
      })
      .catch(() => {});
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  const setAlert = (message, severity) => {
    setAlertData({
      open: true,
      severity:
        severity === "success"
          ? CONFIG.ALERT_SEVERITY.success
          : CONFIG.ALERT_SEVERITY.error,
      message: message,
    });
  };
  const current = new Date();
  const getVehicleStatus = () => {
    if (tripData.trip.status === 2 || tripData.trip.status === 10) {
      return CONFIG.TRIP_STATUS.filter(
        (tr) => parseInt(tr.value) === tripData.trip.status
      )[0].label;
    } else if (tripData.trip.status === 11) {
      return "Trip Cancelled";
    } else if (tripData.trip.status === 3) {
      let reached = tripData.trip.tripWayPoints.filter(
        (wp) => wp.status === "Entry"
      );
      let departed = tripData.trip.tripWayPoints.filter(
        (wp) => wp.status === "Exit"
      );
      return reached.length > 0
        ? `Vehicle reached ${
            reached[reached.length - 1].sequenceNumber > 0
              ? "waypoint"
              : "startpoint"
          }  ${
            reached[reached.length - 1].sequenceNumber > 0
              ? reached[reached.length - 1].sequenceNumber
              : ""
          } ${reached[reached.length - 1].name}`
        : departed.length > 0
        ? `Vehicle departed ${
            departed[departed.length - 1].sequenceNumber > 0
              ? "waypoint"
              : "startpoint"
          } ${
            departed[departed.length - 1].sequenceNumber > 0
              ? departed[departed.length - 1].sequenceNumber
              : ""
          } ${departed[departed.length - 1].name}`
        : "";
    } else if (tripData.trip.status === 4 && tripData.trip.tatDelay) {
      return Math.floor(tripData.trip.tatDelayTime / 60)
        ? `Delayed by ${Math.floor(
            tripData.trip.tatDelayTime / 60
          )} hour(s) : ${Math.floor(tripData.trip.tatDelayTime % 60)} minute(s)`
        : `Delayed by ${Math.floor(tripData.trip.tatDelayTime % 60)} minute(s)`;
    } else {
      return "No Delay";
    }
  };
  const handleMoreInfoOpen = () => {
    setMoreInfoOpen(false);
    locationUpdateRef.current.setActionType("");
    locationUpdateRef.current.resetFields();
  };
  const addMoreInfoHandler = () => {
    setMoreInfoOpen(false);
    locationUpdateRef.current.setActionType("");
    locationUpdateRef.current.resetFields();
  };
  const updateLocHandler = (rec, locations) => {
    setMoreInfoOpen(true);
    locationUpdateRef.current.setLocation(rec, locations);
  };
  const addLocationHandler = (rec) => {
    setMoreInfoOpen(true);
    locationUpdateRef.current.setActionType("add");
  };
  const locationSnackHandler = (snack) => {
    let newLocationSnack = _.cloneDeep(locationSnack);
    newLocationSnack["open"] = snack.open;
    newLocationSnack["message"] = snack.message;
    newLocationSnack["severity"] = snack.severity;
    setLocationSnack(newLocationSnack);
  };
  const handleClickOpen = () => {};
  return (
    <div>
      <Header />
      <Snackbar
        open={locationSnack.open}
        autoHideDuration={6000}
        onClose={() => {
          let newLocationSnack = _.cloneDeep(locationSnack);
          newLocationSnack["open"] = false;
          newLocationSnack["severity"] = "";
          newLocationSnack["message"] = "";
          setLocationSnack(newLocationSnack);
        }}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          onClose={() => {
            let newLocationSnack = _.cloneDeep(locationSnack);
            newLocationSnack["open"] = false;
            newLocationSnack["severity"] = "";
            newLocationSnack["message"] = "";
            setLocationSnack(newLocationSnack);
          }}
          severity={locationSnack.severity}
          sx={{ width: "100%" }}
        >
          {locationSnack.message}
        </Alert>
      </Snackbar>
      <LocationUpdate
        ref={locationUpdateRef}
        open={moreInfoOpen}
        handleClose={handleMoreInfoOpen}
        addMoreInfoHandler={addMoreInfoHandler}
        tripData={tripData}
        loc={loc}
        tripId={props.location.state.tripId}
        getTrip={getTrip}
        locationSnackHandler={locationSnackHandler}
      />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBackIcon
              onClick={() => {
                let searchParam = util.queryParamsFromObj("", {
                  tripType: props.location.state.tripType,
                  vehicle:
                    props.location.state.filters.truckId.value.value ===
                    undefined
                      ? ""
                      : props.location.state.filters.truckId.value.label,
                  status:
                    props.location.state.filters.status.value.value ===
                    undefined
                      ? ""
                      : props.location.state.filters.status.value.label,
                });
                history.push({
                  pathname: "/Trip",
                  search: searchParam,
                  state: {
                    tripType: props.location.state.tripType,
                    filters: props.location.state.filters,
                  },
                });
              }}
              className="arrow"
            />
            Trip Details{" "}
            {tripData ? (
              <>
                {`(${props.location.state.tripId}) / (${tripData.trip.truck.registrationNumber})`}{" "}
                <Chip
                  style={{
                    background: `${
                      CONFIG.TRIP_STATUS.filter(
                        (tr) => parseInt(tr.value) === tripData.trip.status
                      )[0].color
                    }`,
                    color: "#fff",
                    marginLeft: "13px",
                  }}
                  label={
                    CONFIG.TRIP_STATUS.filter(
                      (tr) => parseInt(tr.value) === tripData.trip.status
                    )[0].label
                  }
                />
              </>
            ) : null}
          </Typography>
          <div className={classes.header_buttons}>
            {tripData ? (
              <>
                {tripData.trip.bookingType === "TBB" &&
                tripData.trip.status !== 11 &&
                tripData.trip.status !== 4 ? (
                  <Button
                    style={{
                      textTransform: "none",
                      padding: "8px 15px",
                      marginRight: "15px",
                    }}
                    color="primary"
                    variant="contained"
                    disableElevation={true}
                    onClick={() => {
                      setRouteDetailsOpen(true);
                      routeDetailsRef.current.setDetails();
                    }}
                  >
                    Update Route Details
                  </Button>
                ) : null}
                {tripData.trip.status !== 11 ? (
                  <Button
                    variant="contained"
                    disableElevation={true}
                    className={classes.addindent_btn}
                    color="primary"
                    onClick={() => {
                      history.push({
                        pathname: "./addIndent",
                        state: { truckId: tripData.trip.truck.id },
                      });
                    }}
                    startIcon={<AddCircleRoundedIcon />}
                  >
                    Add Indent
                  </Button>
                ) : null}
                <Button
                  className={classes.update_trip}
                  startIcon={<UpdateIcon />}
                  disabled={tripData.trip.status !== 5}
                  onClick={() => {
                    history.push({
                      pathname: "./updateTrip",
                      state: {
                        tripId: props.location.state.tripId,
                        truck: { ...tripData.trip.truck },
                      },
                    });
                  }}
                >
                  Update Trip
                </Button>
                <Button
                  className={classes.cancel_button}
                  startIcon={<CancelIcon />}
                  disabled={
                    !(tripData.trip.status === 1 || tripData.trip.status === 2)
                  }
                  onClick={() => {
                    setOpen(true);
                  }}
                >
                  Cancel Trip
                </Button>
                <CancelTrip
                  open={open}
                  handleClose={handleCancelClose}
                  tripData={{ id: props.location.state.tripId }}
                  setAlert={setAlert}
                  getTrip={getTrip}
                />
                <UpdateRouteDetails
                  ref={routeDetailsRef}
                  open={routeDetailsOpen}
                  handleClose={handleRouteDetailsClose}
                  tripData={{ id: props.location.state.tripId }}
                  setAlert={setAlert}
                  getTrip={getTrip}
                  trip={tripData.trip}
                />
              </>
            ) : null}
          </div>
        </div>
        {alertData.open ? (
          <div className={classes.alertBox}>
            <AlertMessage
              severity={alertData.severity}
              message={alertData.message}
              closeAlert={closeAlert}
            />
          </div>
        ) : null}
        <div style={{ margin: "20px" }}>
          {loader ? (
            <CircularLoading />
          ) : tripData ? (
            <>
              <div className="form_container" style={{ position: "relative" }}>
                {/* <Grid container justifyContent="flex-end" style={{ padding: "20px" }}>
                  <Link to="/" target="_blank">
                    <Button variant="contained" onClick={handleButtonClick}
                    >
                      button
                    </Button>
                  </Link>
                </Grid> */}
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div style={{ marginRight: "10px" }}>
                    <ul
                      className="trip_details_ribbon_1"
                      style={{ height: "100%", margin: "0px" }}
                    >
                      <li style={{ display: "table-row" }}>
                        <span
                          style={{
                            display: "table-cell",
                            paddingRight: "4px",
                          }}
                        >
                          Vehicle Number
                        </span>
                        :{" "}
                        <b>
                          {tripData
                            ? tripData.trip.truck.registrationNumber
                            : ""}
                        </b>
                      </li>
                      <li style={{ display: "table-row" }}>
                        <span
                          style={{
                            display: "table-cell",
                            paddingRight: "4px",
                          }}
                        >
                          Vehicle Status
                        </span>
                        : <b>{getVehicleStatus()}</b>
                      </li>
                      <li style={{ display: "table-row" }}>
                        <span
                          style={{
                            display: "table-cell",
                            paddingRight: "4px",
                          }}
                        >
                          Waypoints Crossed
                        </span>
                        :{" "}
                        <b>
                          {tripData.trip.tripWayPoints.filter(
                            (wp) => wp.status === "Exit"
                          ).length === tripData.trip.tripWayPoints.length
                            ? `Vehicle crossed all waypoints between ${tripData.trip.fromCity} to ${tripData.trip.toCity}`
                            : tripData.trip.tripWayPoints.filter(
                                (wp) => wp.status === "Exit"
                              ).length > 0
                            ? `Vehicle Crossed ${
                                tripData.trip.tripWayPoints.filter(
                                  (wp) => wp.status === "Exit"
                                ).length - 1
                              } waypoints between ${
                                tripData.trip.fromCity
                              } to ${tripData.trip.toCity}`
                            : "NA"}
                        </b>
                      </li>
                    </ul>
                  </div>
                  <div>
                    <ul
                      className="trip_details_ribbon"
                      style={{ margin: "0px", height: "100%" }}
                    >
                      <li style={{ display: "table-row" }}>
                        <span
                          style={{
                            display: "table-cell",
                            paddingRight: "4px",
                          }}
                        >
                          Trip ID
                        </span>
                        : <b>{props.location.state.tripId}</b>
                      </li>
                      <li style={{ display: "table-row" }}>
                        <span
                          style={{
                            display: "table-cell",
                            paddingRight: "4px",
                          }}
                        >
                          Route Name
                        </span>
                        :{" "}
                        <b>
                          {tripData.trip.route && tripData.trip.route.routeName
                            ? tripData.trip.route.routeName
                            : "NA"}
                        </b>
                      </li>
                      <li style={{ display: "table-row" }}>
                        <span
                          style={{
                            display: "table-cell",
                            paddingRight: "4px",
                          }}
                        >
                          Contract Ref No
                        </span>
                        :{" "}
                        <b>
                          {tripData.trip.contract &&
                          tripData.trip.contract.customerContractReferenceNumber
                            ? tripData.trip.contract
                                .customerContractReferenceNumber
                            : "NA"}
                        </b>
                      </li>
                    </ul>
                  </div>
                </div>
                <br />
                <br />
                {activeStep !== undefined ? (
                  <Box sx={{ width: "100%" }} className="custom_stepper">
                    <Stepper alternativeLabel>
                      {tripData.trip.tripWayPoints.length > 0
                        ? tripData.trip.tripWayPoints.map((wp, i) => (
                            <Step
                              key={i}
                              completed={wp.status === "Exit"}
                              active={wp.status === "Entry"}
                            >
                              <StepLabel style={{ marginBottom: "4px" }}>
                                <Typography
                                  variant="body2"
                                  style={{
                                    fontSize: "12px",
                                    position: "absolute",
                                    width: "98%",
                                    textAlign: "center",
                                    top: "-22px",
                                  }}
                                >
                                  {i === 0
                                    ? "Start Point"
                                    : i > 0 &&
                                      i < tripData.trip.tripWayPoints.length - 1
                                    ? `Waypoint ${i}`
                                    : "End Point"}
                                </Typography>
                                <div>{wp.name}</div>
                              </StepLabel>
                              <div style={{ textAlign: "center" }}>
                                {i > 0 &&
                                i < tripData.trip.tripWayPoints.length - 1 &&
                                wp.status !== "None" ? (
                                  wp.transitDelay ? (
                                    <Chip
                                      style={{
                                        backgroundColor:
                                          wp.transitDelay === 1
                                            ? "#ffcccb"
                                            : wp.transitDelay === 0
                                            ? "#ddffdd"
                                            : "",
                                      }}
                                      label={
                                        wp.transitDelay === 0 ? (
                                          <span>arrived on time</span>
                                        ) : (
                                          <span>{`arrival delay by ${
                                            Math.floor(wp.transitDelayTime / 60)
                                              ? `${Math.floor(
                                                  wp.transitDelayTime / 60
                                                )} hour(s) : ${Math.floor(
                                                  wp.transitDelayTime % 60
                                                )} minute(s)`
                                              : `${Math.floor(
                                                  wp.transitDelayTime % 60
                                                )} minute(s)`
                                          }`}</span>
                                        )
                                      }
                                    />
                                  ) : null
                                ) : null}
                              </div>
                              <div
                                style={{ textAlign: "center", color: "orange" }}
                              >
                                {wp.holdingTime
                                  ? `Holding Time: ${
                                      Math.floor(wp.holdingTime / 60)
                                        ? `${Math.floor(
                                            wp.holdingTime / 60
                                          )} hour(s) ${Math.floor(
                                            wp.holdingTime % 60
                                          )} minute(s)`
                                        : `${Math.floor(
                                            wp.holdingTime % 60
                                          )} minute(s)`
                                    }`
                                  : ""}
                              </div>
                              <div
                                style={{ textAlign: "center", color: "green" }}
                              >
                                {(wp.status === "Exit" ||
                                  wp.status === "Entry") &&
                                wp.transitTime
                                  ? `Transit Time: ET - ${
                                      Math.floor(wp.transitTime / 60)
                                        ? `${Math.floor(
                                            wp.transitTime / 60
                                          )} hour(s) ${Math.floor(
                                            wp.transitTime % 60
                                          )} minute(s)`
                                        : `${Math.floor(
                                            wp.transitTime % 60
                                          )} minute(s)`
                                    } / AT - ${
                                      wp.transitDelay
                                        ? Math.floor(
                                            (wp.transitDelayTime +
                                              wp.transitTime) /
                                              60
                                          )
                                          ? `${Math.floor(
                                              (wp.transitDelayTime +
                                                wp.transitTime) /
                                                60
                                            )} hour(s) ${Math.floor(
                                              (wp.transitDelayTime +
                                                wp.transitTime) %
                                                60
                                            )} minute(s)`
                                          : `${Math.floor(
                                              (wp.transitDelayTime +
                                                wp.transitTime) %
                                                60
                                            )} minute(s)`
                                        : Math.floor(wp.transitTime / 60)
                                        ? `${Math.floor(
                                            wp.transitTime / 60
                                          )} hour(s) ${Math.floor(
                                            wp.transitTime % 60
                                          )} minute(s)`
                                        : `${Math.floor(
                                            wp.transitTime % 60
                                          )} minute(s)`
                                    }`
                                  : ""}
                              </div>
                            </Step>
                          ))
                        : null}
                    </Stepper>
                  </Box>
                ) : null}
                <div>
                  {tripData.trip.tripWayPoints.length > 0 ? (
                    <>
                      <div style={{ padding: "20px " }}>
                        <Typography style={{ fontWeight: "bold" }}>
                          Route Details
                        </Typography>
                      </div>
                      <div style={{ padding: "20px" }}>
                        <TableContainer>
                          <Table aria-label="customized table">
                            <TableHead style={{ backgroundColor: "#B7ECFF" }}>
                              <TableRow>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  NAME
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  LOCATION
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  ENTRY DATE
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  EXIT DATE
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  HOLDING DELAY
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  HOLDING DELAY TIME
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  TRANSIT DELAY
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  TRANSIT DELAY TIME
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  STATUS
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  ACTIONS
                                </StyledTableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {tripData.trip.tripWayPoints.map((rec) => {
                                let poppedLoc = rec.location.split(",");
                                poppedLoc.pop();
                                return (
                                  <StyledTableRow key={rec.id}>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {rec.name ? rec.name : "--"}
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                      title={rec.location}
                                    >
                                      {rec.location
                                        ? `${rec.location.split(",")[0]}...`
                                        : "--"}
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {rec.entryDate ? rec.entryDate : "--"}
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {rec.exitDate ? rec.exitDate : "--"}
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {rec.holdingDelay ? (
                                        <span style={{ color: "red" }}>
                                          Yes
                                        </span>
                                      ) : rec.holdingDelay === 0 ? (
                                        <span style={{ color: "#4FBA20" }}>
                                          No
                                        </span>
                                      ) : (
                                        "--"
                                      )}
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {rec.holdingDelay && rec.holdingDelayTime
                                        ? `${
                                            Math.floor(
                                              rec.holdingDelayTime / 60
                                            )
                                              ? `${Math.floor(
                                                  rec.holdingDelayTime / 60
                                                )} hour(s) : ${Math.floor(
                                                  rec.holdingDelayTime % 60
                                                )} minute(s)`
                                              : `${Math.floor(
                                                  rec.holdingDelayTime % 60
                                                )} minute(s)`
                                          }`
                                        : "--"}
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {rec.transitDelay ? (
                                        <span style={{ color: "red" }}>
                                          Yes
                                        </span>
                                      ) : rec.transitDelay === 0 ? (
                                        <span style={{ color: "#4FBA20" }}>
                                          No
                                        </span>
                                      ) : (
                                        "--"
                                      )}
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {rec.transitDelay && rec.transitDelayTime
                                        ? `${
                                            Math.floor(
                                              rec.transitDelayTime / 60
                                            )
                                              ? `${Math.floor(
                                                  rec.transitDelayTime / 60
                                                )} hour(s) : ${Math.floor(
                                                  rec.transitDelayTime % 60
                                                )} minute(s)`
                                              : `${Math.floor(
                                                  rec.transitDelayTime % 60
                                                )} minute(s)`
                                          }`
                                        : "--"}
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {rec.status === "Exit"
                                        ? "Departed"
                                        : rec.status === "Entry"
                                        ? "Reached"
                                        : "None"}
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      <IconButton
                                        onClick={() => {
                                          if (
                                            (rec.status === "None" ||
                                              rec.status === "Entry") &&
                                            tripData.trip.status !== 11 &&
                                            tripData.trip.status !== 4
                                          ) {
                                            setLoc(rec);
                                            updateLocHandler(
                                              rec,
                                              tripData.trip.tripWayPoints
                                            );
                                          }
                                        }}
                                      >
                                        <EditIcon
                                          style={{
                                            color: "#D08B1D",
                                            height: "18px",
                                            width: "18px",
                                          }}
                                        />
                                      </IconButton>
                                      {tripData.trip.bookingType === "TBB" ? (
                                        <IconButton
                                          style={{ marginLeft: "4px" }}
                                          onClick={() => {
                                            if (
                                              (rec.status === "None" ||
                                                rec.status === "Entry") &&
                                              tripData.trip.status !== 11 &&
                                              tripData.trip.status !== 4
                                            ) {
                                              setLoc(rec);
                                              addLocationHandler(rec);
                                            }
                                          }}
                                        >
                                          <AddIcon
                                            style={{
                                              color: "#D08B1D",
                                              height: "20px",
                                              width: "20px",
                                            }}
                                          />
                                        </IconButton>
                                      ) : null}
                                    </StyledTableCell>
                                  </StyledTableRow>
                                );
                              })}
                            </TableBody>
                          </Table>
                        </TableContainer>
                      </div>
                    </>
                  ) : null}
                </div>
                <div style={{ padding: "20px" }}>
                  <Grid container spacing={2}>
                    <Grid item xs={2}>
                      <Typography
                        style={{ fontWeight: "bold", padding: "0px" }}
                      >
                        Travel details
                      </Typography>
                    </Grid>
                    <Grid item xs={2}></Grid>
                    <Grid
                      item
                      xs={2}
                      style={{ borderRight: "0.01em solid #00000012" }}
                    ></Grid>
                    <Grid item xs={2}>
                      <Typography
                        style={{ fontWeight: "bold", padding: "0px" }}
                      >
                        Travel Time details
                      </Typography>
                    </Grid>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={2}></Grid>
                  </Grid>
                  <Grid
                    container
                    spacing={2}
                    style={{ margin: "0px -8px", padding: "8px 0px" }}
                  >
                    <Grid item xs={2}>
                      <div className={classes.info_item1}>
                        <span className={classes.item_title}>
                          ESTIMATED DISTANCE:
                        </span>
                        <span className={classes.item_data}>
                          {tripData.trip.travelDetails.estimatedDistance
                            ? `${tripData.trip.travelDetails.estimatedDistance} Km`
                            : "NA"}
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item2}>
                        <span className={classes.item_title}>
                          TRAVELLED DISTANCE:
                        </span>
                        <span className={classes.item_data}>
                          {tripData.trip.travelDetails.travelledDistance
                            ? `${tripData.trip.travelDetails.travelledDistance} Km`
                            : "NA"}
                        </span>
                      </div>
                    </Grid>
                    <Grid
                      item
                      xs={2}
                      style={{ borderRight: "0.01em solid #00000012" }}
                    >
                      <div className={classes.info_item3}>
                        <span className={classes.item_title}>
                          DISTANCE DIFFERENCE:
                        </span>
                        <span className={classes.item_data}>
                          {tripData.trip.travelDetails.differenceDistance
                            ? `${tripData.trip.travelDetails.differenceDistance} Km`
                            : "NA"}
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item1}>
                        <span className={classes.item_title}>
                          PICKUP DATE & TIME:
                        </span>
                        <span className={classes.item_data}>
                          {tripData.trip.travelTimeDetails.arrivalDateTime
                            ? tripData.trip.travelTimeDetails.arrivalDateTime
                            : "NA"}
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item2}>
                        <span className={classes.item_title}>
                          DROPOFF DATE & TIME:
                        </span>
                        <span className={classes.item_data}>
                          {tripData.trip.travelTimeDetails.departureDateTime
                            ? tripData.trip.travelTimeDetails.departureDateTime
                            : "NA"}
                        </span>
                      </div>
                    </Grid>
                    <Grid item xs={2}>
                      <div className={classes.info_item3}>
                        <span className={classes.item_title}>
                          TURN AROUND TIME(TAT):
                        </span>
                        <span className={classes.item_data}>
                          {tripData.trip.travelTimeDetails.turnAroundTat
                            ? `${
                                Math.floor(
                                  tripData.trip.travelTimeDetails
                                    .turnAroundTat / 60
                                )
                                  ? `${Math.floor(
                                      tripData.trip.travelTimeDetails
                                        .turnAroundTat / 60
                                    )} hour(s) : ${Math.floor(
                                      tripData.trip.travelTimeDetails
                                        .turnAroundTat % 60
                                    )} minute(s)`
                                  : `${Math.floor(
                                      tripData.trip.travelTimeDetails
                                        .turnAroundTat % 60
                                    )} minute(s)`
                              }`
                            : "NA"}
                        </span>
                      </div>
                    </Grid>
                  </Grid>
                  <Grid container spacing={2} style={{ padding: "0px" }}>
                    <Grid item xs={2}>
                      <Typography
                        style={{ color: "#8E8E8E", fontSize: "13px" }}
                      >
                        Opening Kms
                      </Typography>
                      <Typography style={{ fontSize: "13px" }}>
                        {tripData.trip.travelDetails.openingOdo
                          ? tripData.trip.travelDetails.openingOdo
                          : "NA"}
                      </Typography>
                    </Grid>
                    <Grid item xs={2}>
                      <Typography
                        style={{ color: "#8E8E8E", fontSize: "13px" }}
                      >
                        Closing kms
                      </Typography>
                      <Typography style={{ fontSize: "13px" }}>
                        {tripData.trip.travelDetails.closingOdo
                          ? tripData.trip.travelDetails.closingOdo
                          : "NA"}
                      </Typography>
                    </Grid>
                    <Grid
                      item
                      xs={2}
                      style={{ borderRight: "0.01em solid #00000012" }}
                    ></Grid>
                    {/* <Grid item xs={2}>
                      <Typography
                        style={{ color: "#8E8E8E", fontSize: "13px" }}
                      >
                        Vehicle Status
                      </Typography>
                      {tripData.trip.travelTimeDetails.tatDelay ? (
                        <Typography style={{ fontSize: "13px" }}>
                          {Math.floor(
                            tripData.trip.travelTimeDetails.tatDelayTime / 60
                          )
                            ? `Delayed by ${Math.floor(
                              tripData.trip.travelTimeDetails.tatDelayTime /
                              60
                            )} hour(s) : ${Math.floor(
                              tripData.trip.travelTimeDetails.tatDelayTime %
                              60
                            )} minute(s)`
                            : `Delayed by ${Math.floor(
                              tripData.trip.travelTimeDetails.tatDelayTime %
                              60
                            )} minute(s)`}
                        </Typography>
                      ) : (
                        <Typography style={{ fontSize: "13px" }}>
                          No Delay
                        </Typography>
                      )}
                    </Grid> */}
                  </Grid>
                </div>
                <div style={{ padding: "20px", display: "flex" }}>
                  {/* <Typography style={{ fontWeight: "bold", flexGrow: "1" }}>
                    Indent Details
                  </Typography> */}
                  {/* <Button variant="contained" className={classes.addindent_btn} color="primary" startIcon={<AddCircleRoundedIcon />}>
                    Add Indent
                  </Button> */}
                </div>
                <div style={{ padding: "20px" }}>
                  <Collapse>
                    <Panel header="E-Locks">
                      {eLocks.length > 0 ? (
                        <TableContainer>
                          <Table aria-label="customized tpxable">
                            <TableHead>
                              <TableRow>
                                <StyledTableCell
                                  align="center"
                                  style={{
                                    backgroundColor: "#366E93",
                                    color: "#FFF",
                                    fontSize: "16px",
                                    width: "50%",
                                  }}
                                >
                                  <div
                                    style={{ borderRight: "2px solid #FFF" }}
                                  >
                                    CheckIn
                                  </div>
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  style={{
                                    backgroundColor: "#366E93",
                                    color: "#FFF",
                                    fontSize: "16px",
                                    width: "20%",
                                  }}
                                >
                                  <div style={{ borderRight: "2px solid #FFF" }}>Status</div>
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  style={{
                                    backgroundColor: "#366E93",
                                    color: "#FFF",
                                    fontSize: "16px",
                                    width: "30%",
                                  }}
                                >
                                  <div
                                  >
                                    CheckOut
                                  </div>
                                </StyledTableCell>
                              </TableRow>
                            </TableHead>
                          </Table>
                          <Table aria-label="customized tpxable">
                            <TableHead>
                              <TableRow>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                  style={{ width: "12%", color: "#8E8E8E" }}
                                >
                                  SNO
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                  style={{ width: "12%", color: "#8E8E8E" }}
                                >
                                  QR CODE
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                  style={{ width: "12%", color: "#8E8E8E" }}
                                >
                                  DATE
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                  style={{ width: "12%", color: "#8E8E8E" }}
                                >
                                  SCANNED IMAGE
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                ></StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                  style={{ width: "14%", color: "#8E8E8E" }}
                                >
                                  DATE
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                  style={{ width: "14%", color: "#8E8E8E" }}
                                >
                                  SCANNED IMAGE
                                </StyledTableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {eLocks.length > 0 &&
                                eLocks.map((lock, i) => {
                                  return (
                                    <StyledTableRow>
                                      <StyledTableCell
                                        align="center"
                                        className={classes.table_data}
                                      >
                                        {i+1}
                                      </StyledTableCell>
                                      <StyledTableCell
                                        align="center"
                                        className={classes.table_data}
                                      >
                                        {lock.securityCode}
                                      </StyledTableCell>
                                      <StyledTableCell
                                        align="center"
                                        className={classes.table_data}
                                      >
                                        {lock.checkInReadDateTime
                                          ? lock.checkInReadDateTime
                                          : "--"}
                                      </StyledTableCell>
                                      <StyledTableCell
                                        align="center"
                                        className={classes.table_data}
                                      >
                                        <VisibilityIcon
                                          className="row_close_icon"
                                          style={{
                                            color: "#2E7DB2",
                                            cursor: "pointer",
                                          }}
                                          onClick={() => {
                                            elockImageViewHandler("checkin", i);
                                          }}
                                        />
                                        <Dialog
                                          open={
                                            eLockImage[i] &&
                                            eLockImage[i]["checkin"]
                                          }
                                          onClose={() => {
                                            let newOpen = [...eLockImage];
                                            newOpen[i] = { ["checkin"]: false };
                                            setElockImage(newOpen);
                                          }}
                                        >
                                          <div
                                            style={{
                                              display: "flex",
                                              justifyContent: "space-between",
                                            }}
                                          >
                                            <DialogTitle></DialogTitle>
                                            <CloseIcon
                                              style={{
                                                alignSelf: "center",
                                                cursor: "pointer",
                                                marginRight: "15px",
                                              }}
                                              onClick={() => {
                                                let newOpen = [...eLockImage];
                                                newOpen[i] = {
                                                  ["checkin"]: false,
                                                };
                                                setElockImage(newOpen);
                                              }}
                                            />
                                          </div>
                                          <DialogContent
                                            style={{ textAlign: "center" }}
                                          >
                                            {lock.checkInImage ? (
                                              <img
                                                src={`${lock.checkInImage}`}
                                                style={{
                                                  width: "100%",
                                                  height: "auto",
                                                }}
                                              />
                                            ) : (
                                              "No Image Found"
                                            )}
                                          </DialogContent>
                                        </Dialog>
                                      </StyledTableCell>
                                      <StyledTableCell
                                        align="center"
                                        className={classes.table_data}
                                        style={{
                                          color:
                                            lock.checkIn === lock.checkOut
                                              ? "#25B91F"
                                              : "#FF5757",
                                        }}
                                      >
                                        {lock.checkIn === lock.checkOut ? (
                                          <>
                                            CheckedOut{" "}
                                            <CheckCircleIcon
                                              style={{
                                                fontSize: "16px",
                                                verticalAlign: "middle",
                                                marginBottom: "2px",
                                              }}
                                            />
                                          </>
                                        ) : (
                                          <>
                                            Not CheckedOut{" "}
                                            <CancelIcon
                                              style={{
                                                fontSize: "16px",
                                                verticalAlign: "middle",
                                                marginBottom: "2px",
                                              }}
                                            />
                                          </>
                                        )}
                                      </StyledTableCell>
                                      <StyledTableCell
                                        align="center"
                                        className={classes.table_data}
                                      >
                                        {lock.checkOut && lock.checkOutReadDateTime
                                          ? lock.checkOutReadDateTime
                                          : ""}
                                      </StyledTableCell>
                                      <StyledTableCell
                                        align="center"
                                        className={classes.table_data}
                                      >
                                        {lock.checkOut ? <VisibilityIcon
                                          className="row_close_icon"
                                          style={{
                                            color: "#2E7DB2",
                                            cursor: "pointer",
                                          }}
                                          onClick={() => {
                                            elockImageViewHandler(
                                              "checkout",
                                              i
                                            );
                                          }}
                                        /> : ""}
                                        <Dialog
                                          open={
                                            eLockImage[i] &&
                                            eLockImage[i]["checkout"]
                                          }
                                          onClose={() => {
                                            let newOpen = [...eLockImage];
                                            newOpen[i] = {
                                              ["checkout"]: false,
                                            };
                                            setElockImage(newOpen);
                                          }}
                                        >
                                          <div
                                            style={{
                                              display: "flex",
                                              justifyContent: "space-between",
                                            }}
                                          >
                                            <DialogTitle></DialogTitle>
                                            <CloseIcon
                                              style={{
                                                alignSelf: "center",
                                                cursor: "pointer",
                                                marginRight: "15px",
                                              }}
                                              onClick={() => {
                                                let newOpen = [...eLockImage];
                                                newOpen[i] = {
                                                  ["checkout"]: false,
                                                };
                                                setElockImage(newOpen);
                                              }}
                                            />
                                          </div>
                                          <DialogContent
                                            style={{ textAlign: "center" }}
                                          >
                                            {lock.checkOutImage ? (
                                              <img
                                                src={`${lock.checkOutImage}`}
                                                style={{
                                                  width: "100%",
                                                  height: "auto",
                                                }}
                                              />
                                            ) : (
                                              "No Image Found"
                                            )}
                                          </DialogContent>
                                        </Dialog>
                                      </StyledTableCell>
                                    </StyledTableRow>
                                  );
                                })}
                            </TableBody>
                          </Table>
                        </TableContainer>
                      ) : (
                        <>
                          <div
                            style={{
                              textAlign: "center",
                              fontWeight: "bold",
                            }}
                          >
                            No E-Locks Found!
                          </div>
                        </>
                      )}
                    </Panel>
                  </Collapse>
                </div>
                <div style={{ padding: "20px" }}>
                  <Collapse>
                    <Panel header="Indent Details">
                      {tripData && tripData.trip.indentDetails.length > 0 ? (
                        <>
                          <TableContainer>
                            <Table aria-label="customized tpxable">
                              <TableHead>
                                <TableRow>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_column}
                                  >
                                    INDENT#
                                  </StyledTableCell>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_column}
                                  >
                                    VEHICLE#
                                  </StyledTableCell>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_column}
                                  >
                                    LR#
                                  </StyledTableCell>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_column}
                                  >
                                    VENDOR INFORMATION
                                  </StyledTableCell>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_column}
                                  >
                                    CREATED BY
                                  </StyledTableCell>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_column}
                                  >
                                    CREATED DATE
                                  </StyledTableCell>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_column}
                                  >
                                    STATUS
                                  </StyledTableCell>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_column}
                                  >
                                    AMOUNT(RS)
                                  </StyledTableCell>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_column}
                                  >
                                    ACTIONS
                                  </StyledTableCell>
                                </TableRow>
                              </TableHead>
                              <TableBody>
                                {tripData.trip.indentDetails.map((rec) => (
                                  <StyledTableRow>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {rec.id}
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {tripData.trip.truck &&
                                        tripData.trip.truck.registrationNumber}
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {tripData.trip && tripData.trip.lr
                                        ? tripData.trip.lr
                                        : "--"}
                                    </StyledTableCell>
                                    <StyledTableCell>
                                      <div className={classes.table_data}>
                                        <div
                                          style={{
                                            display: "flex",
                                            flexDirection: "column",
                                            alignItems: "center",
                                          }}
                                        >
                                          <span>
                                            {rec.vendor &&
                                            rec.vendor.companyName
                                              ? rec.vendor.companyName
                                              : "--"}
                                          </span>
                                          <span style={{ fontWeight: "bold" }}>
                                            {rec.vendor &&
                                              rec.vendor.mobileNumber}
                                          </span>
                                        </div>
                                      </div>
                                    </StyledTableCell>
                                    <StyledTableCell>
                                      <div className={classes.table_data}>
                                        <div
                                          style={{
                                            display: "flex",
                                            flexDirection: "column",
                                            alignItems: "center",
                                          }}
                                        >
                                          <span>
                                            {rec.createdBy &&
                                              rec.createdBy.name}
                                          </span>
                                          <span style={{ fontWeight: "bold" }}>
                                            {rec.createdBy &&
                                              rec.createdBy.roleName}
                                          </span>
                                        </div>
                                      </div>
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {rec.createdAtRead}
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      <Chip
                                        style={{
                                          background: `${
                                            CONFIG.INDENT_STATUS.filter(
                                              (tr) =>
                                                parseInt(tr.value) ===
                                                rec.status
                                            )[0].color
                                          }`,
                                          color: "#ffff",
                                        }}
                                        label={
                                          CONFIG.INDENT_STATUS.filter(
                                            (tr) =>
                                              parseInt(tr.value) === rec.status
                                          )[0].label
                                        }
                                      />
                                    </StyledTableCell>
                                    <StyledTableCell
                                      align="center"
                                      className={classes.table_data}
                                    >
                                      {rec.indentTotal.toFixed(2)}
                                    </StyledTableCell>
                                    <StyledTableCell align="center">
                                      {
                                        <IndentReciept
                                          indentId={rec.id}
                                          handleClickOpen={handleClickOpen}
                                        />
                                      }
                                    </StyledTableCell>
                                  </StyledTableRow>
                                ))}
                              </TableBody>
                            </Table>
                          </TableContainer>
                        </>
                      ) : (
                        <>
                          <div
                            style={{
                              textAlign: "center",
                              fontWeight: "bold",
                            }}
                          >
                            No Indents Found!
                          </div>
                        </>
                      )}
                    </Panel>
                  </Collapse>
                </div>
                <div style={{ padding: "20px" }}>
                  <Collapse>
                    <Panel header="Attachments">
                      {documents && documents.tripDocuments.length > 0 ? (
                        <TableContainer>
                          <Table aria-label="customized table">
                            <TableHead style={{ backgroundColor: "#B7ECFF" }}>
                              <TableRow>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  S.NO
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  ATTACHMENT NAME
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  ATTACHMENT TYPE
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  DATE&TIME
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_column}
                                >
                                  VIEW
                                </StyledTableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {documents.tripDocuments.map((rec, i) => (
                                <StyledTableRow key={rec.id}>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_data}
                                  >
                                    {i + 1}
                                  </StyledTableCell>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_data}
                                  >
                                    {rec.filename}
                                  </StyledTableCell>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_data}
                                  >
                                    {rec.documentType}
                                  </StyledTableCell>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_data}
                                  >
                                    {rec.createdAtRead}
                                  </StyledTableCell>
                                  <StyledTableCell
                                    align="center"
                                    className={classes.table_data}
                                  >
                                    <VisibilityIcon
                                      className="row_close_icon"
                                      onClick={() => {
                                        fileViewHandler(rec, i);
                                      }}
                                    />
                                    <Dialog
                                      open={dialog[i]}
                                      onClose={() => handleClose(rec, i)}
                                    >
                                      <div
                                        style={{
                                          display: "flex",
                                          justifyContent: "space-between",
                                        }}
                                      >
                                        <DialogTitle></DialogTitle>
                                        <CloseIcon
                                          style={{
                                            alignSelf: "center",
                                            cursor: "pointer",
                                            marginRight: "15px",
                                          }}
                                          onClick={() => {
                                            handleClose(rec, i);
                                          }}
                                        />
                                      </div>
                                      <DialogContent
                                        style={{ textAlign: "center" }}
                                      >
                                        <img
                                          src={`${rec.url}`}
                                          style={{
                                            width: "100%",
                                            height: "auto",
                                          }}
                                        />
                                      </DialogContent>
                                    </Dialog>
                                  </StyledTableCell>
                                </StyledTableRow>
                              ))}
                            </TableBody>
                          </Table>
                        </TableContainer>
                      ) : (
                        <div
                          style={{ textAlign: "center", fontWeight: "bold" }}
                        >
                          No Documents Found!
                        </div>
                      )}
                    </Panel>
                  </Collapse>
                </div>
              </div>
            </>
          ) : null}
        </div>
        <div style={{ margin: "20px" }}>
          {tripData && (
            <div className="form_container">
              <div style={{ padding: "20px" }}>
                <Collapse>
                  <Panel header="Driver Details">
                    {tripData && tripData.trip.tripDrivers.length > 0 ? (
                      <TableContainer>
                        <Table aria-label="customized table">
                          <TableHead style={{ backgroundColor: "#B7ECFF" }}>
                            <TableRow>
                              <StyledTableCell
                                align="center"
                                className={classes.table_column}
                              >
                                DRIVER NAME
                              </StyledTableCell>
                              <StyledTableCell
                                align="center"
                                className={classes.table_column}
                              >
                                MOBILE NUMBER
                              </StyledTableCell>
                              <StyledTableCell
                                align="center"
                                className={classes.table_column}
                              >
                                LICENCE #
                              </StyledTableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {tripData.trip.tripDrivers.map((rec) => (
                              <StyledTableRow key={rec.id}>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_data}
                                >
                                  {rec.name}
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_data}
                                >
                                  {rec.mobile}
                                </StyledTableCell>
                                <StyledTableCell
                                  align="center"
                                  className={classes.table_data}
                                >
                                  {rec.drivingLisence}
                                </StyledTableCell>
                              </StyledTableRow>
                            ))}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    ) : (
                      <div style={{ textAlign: "center", fontWeight: "bold" }}>
                        No Drivers Found!
                      </div>
                    )}
                  </Panel>
                </Collapse>
              </div>
              {tripData && tripData.trip.bookingType === "TBB" ? (
                <div style={{ padding: "20px" }}>
                  <Grid container spacing={4}>
                    <Grid item xs={12}>
                      <Typography
                        style={{ fontWeight: "bold", padding: "20px 0px" }}
                      >
                        Client Information
                      </Typography>
                      <Grid container spacing={4}>
                        <Grid item xs={3}>
                          <div className={classes.info_item1}>
                            <span className={classes.item_title}>
                              CLIENT COMPANY NAME:
                            </span>
                            <span className={classes.item_data}>
                              {tripData.trip.client &&
                              tripData.trip.client.companyName
                                ? tripData.trip.client.companyName
                                : "NA"}
                            </span>
                          </div>
                        </Grid>
                        <Grid item xs={3}>
                          <div className={classes.info_item2}>
                            <span className={classes.item_title}>
                              CLIENT CODE:
                            </span>
                            <span className={classes.item_data}>
                              {tripData.trip.client &&
                              tripData.trip.client.clientAliasCode
                                ? tripData.trip.client.clientAliasCode
                                : "NA"}
                            </span>
                          </div>
                        </Grid>
                        <Grid item xs={3}>
                          <div className={classes.info_item3}>
                            <span className={classes.item_title}>
                              CLIENT CONTACT NAME:
                            </span>
                            <span className={classes.item_data}>
                              {tripData.trip.client &&
                              `${tripData.trip.client.contactFirstName} ${tripData.trip.client.contactLastName}`
                                ? `${tripData.trip.client.contactFirstName} ${tripData.trip.client.contactLastName}`
                                : "NA"}
                            </span>
                          </div>
                        </Grid>
                        <Grid item xs={3}>
                          <div className={classes.info_item1}>
                            <span className={classes.item_title}>
                              CLIENT CONTACT NUMBER:
                            </span>
                            <span className={classes.item_data}>
                              {tripData.trip.client &&
                              tripData.trip.client.contactMobile
                                ? tripData.trip.client.contactMobile
                                : "NA"}
                            </span>
                          </div>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </div>
              ) : null}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
export default TripDetails;
