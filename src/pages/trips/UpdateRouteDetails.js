import React, {
  useState,
  useImperativeHandle,
  forwardRef,
  useRef,
} from "react";
import _ from "lodash";
import ValidateFields from "../../validations/validateFields";
import {
  Dialog,
  DialogTitle,
  Button,
  CircularProgress,
  DialogContent,
  DialogActions,
  Grid,
} from "@material-ui/core";
import CloseIcon from "@mui/icons-material/Close";
import * as Fields from "../../sharedComponents";
import PropTypes from "prop-types";
import InputAdornment from "@mui/material/InputAdornment";

var Service;

const SimpleDialog = React.forwardRef((props, ref) => {
  const formFields = {
    totalTATHours: {
      name: "totalTATHours",
      label: "Maximum TAT",
      value: "",
      type: "number",
      onlyInt: true,
      min: 0,
    },
    totalTATMin: {
      name: "totalTATMin",
      label: "Maximum TAT",
      value: "",
      type: "number",
      min: 0,
      max: 59,
    },
    maximumDistance: {
      name: "maximumDistance",
      label: "Maximum Distance",
      value: "",
      type: "number",
      min: 0,
    },
    maximumExpenses: {
      name: "maximumExpenses",
      label: "Maximum Expenses",
      value: "",
      type: "number",
      min: 0,
    },
    maximumFuel: {
      name: "maximumFuel",
      label: "Maximum Fuel",
      value: "",
      type: "number",
      min: 0,
    },
    maximumAdBlue: {
      name: "maximumAdBlue",
      label: "Maximum Adblue",
      value: "",
      type: "number",
      min: 0,
    },
    freightAmount: {
      name: "freightAmount",
      label: "Freight Amount",
      value: "",
      type: "number",
      min: 0,
    },
  };
  const { onClose, open, tripData, setAlert, getTrip, trip } = props;
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const [spinner, setSpinner] = useState(false);
  Service = global.service;
  let validateFields = new ValidateFields();
  const setDetails = () => {
    let newFields = _.cloneDeep(fields);
    newFields["freightAmount"]["value"] = trip.freightAmount;
    newFields["maximumAdBlue"]["value"] = trip.maxAdBlue;
    newFields["maximumDistance"]["value"] = trip.maxDistance;
    newFields["maximumExpenses"]["value"] = trip.maxExpense;
    newFields["maximumFuel"]["value"] = trip.maxFuelInLts;
    newFields["totalTATHours"]["value"] = Math.floor(trip.totalTat / 60);
    newFields["totalTATMin"]["value"] = Math.floor(trip.totalTat % 60);
    setFields(newFields);
  };
  useImperativeHandle(ref, () => {
    return {
      setDetails: setDetails,
    };
  });
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    newFields[name]["isValid"] = true;
    if (value.length === 0 && newFields[name]["validationRequired"]) {
      newFields[name]["isValid"] = false;
    }
    setFields(newFields);
  };
  const handleClose = () => {
    onClose();
  };
  const updateHandler = () => {
    let dataObj = _.cloneDeep(fields);
    let dataStatus = validateFields.validateFieldsData(dataObj);
    if (dataStatus.status) {
      setSpinner(true);
      Service.updateRouteDetails({ ...dataObj, tripId: tripData })
        .then(() => {
          setSpinner(false);
          handleClose();
          setAlert("Route details updated successfully!", "success");
          getTrip();
        })
        .catch((error) => {
          setSpinner(false);
          handleClose();
          setAlert(
            error.response && error.response.data
              ? error.response.data.message
              : "Something went wrong!",
            "error"
          );
        });
    }
  };
  return (
    <div ref={ref}>
      <Dialog open={open} onClose={handleClose}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <DialogTitle id="simple-dialog-title">
            Update Route Details
          </DialogTitle>
          <CloseIcon
            style={{
              marginRight: "20px",
              cursor: "pointer",
              alignSelf: "center",
              marginRight: "20px",
            }}
            onClick={() => {
              handleClose();
            }}
          />
        </div>
        <DialogContent style={{ width: "600px", height: "250px" }}>
          <Grid container spacing={4}>
            <Grid item xs={6}>
              <Fields.InputField
                fieldData={fields.totalTATHours}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Hours</InputAdornment>
                }
              />
            </Grid>
            <Grid item xs={6}>
              <Fields.InputField
                fieldData={fields.totalTATMin}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Mins</InputAdornment>
                }
              />
            </Grid>
          </Grid>
          <Grid container spacing={4}>
            <Grid item xs={6}>
              <Fields.InputField
                fieldData={fields.maximumDistance}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Kms</InputAdornment>
                }
              />
            </Grid>
            <Grid item xs={6}>
              <Fields.InputField
                fieldData={fields.maximumExpenses}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Rs</InputAdornment>
                }
              />
            </Grid>
          </Grid>
          <Grid container spacing={4}>
            <Grid item xs={6}>
              <Fields.InputField
                fieldData={fields.maximumFuel}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Lts</InputAdornment>
                }
              />
            </Grid>
            <Grid item xs={6}>
              <Fields.InputField
                fieldData={fields.maximumAdBlue}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Lts</InputAdornment>
                }
              />
            </Grid>
          </Grid>
          <Grid container spacing={4}>
            <Grid item xs={6}>
              <Fields.InputField
                fieldData={fields.freightAmount}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Rs</InputAdornment>
                }
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            disableElevation
            onClick={() => {
              handleClose();
            }}
          >
            Close
          </Button>
          <Button
            variant="contained"
            color="secondary"
            disableElevation
            onClick={() => {
              updateHandler();
            }}
            style={{ marginRight: "15px" }}
            startIcon={
              spinner ? <CircularProgress size={20} color={"#fff"} /> : null
            }
          >
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  tripData: PropTypes.object.isRequired,
  setAlert: PropTypes.object.isRequired,
  getTrip: PropTypes.object.isRequired,
  trip: PropTypes.object.isRequired,
};

const UpdateRouteDetails = (props, ref) => {
  let routeDetailsRef = useRef();
  const setDetails = () => {
    routeDetailsRef.current.setDetails();
  };
  useImperativeHandle(ref, () => {
    return {
      setDetails: setDetails,
    };
  });
  return (
    <div ref={ref}>
      <SimpleDialog
        ref={routeDetailsRef}
        open={props.open}
        onClose={props.handleClose}
        tripData={props.tripData}
        setAlert={props.setAlert}
        getTrip={props.getTrip}
        trip={props.trip}
      />
    </div>
  );
};

export default forwardRef(UpdateRouteDetails);
