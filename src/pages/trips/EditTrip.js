import React, { useState } from "react";
import Header from "../../components/header";
import { makeStyles } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { useHistory } from "react-router-dom";
import {
    Typography,
    TextField,
    Grid,
    Button,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import * as Fields from "../../sharedComponents/index";
import "../../common.css";

const useStyles = makeStyles((theme) => ({
    main_list: {
        display: "flex",
    },
    arrow: {
        cursor: "pointer",
        marginRight: theme.spacing(2),
        width: "30px",
        height: "30px",
    },
    main_heading: {
        fontWeight: "bold",
        fontSize: "20px",
        marginRight: theme.spacing(17),
    },
    main: {
        display: "flex",
        margin: "20px",
    },
    search_icon: {
        color: "#174A84",
        width: "25px",
    },
    field: {
        width: "300px",
    },
    details_container_content: {
        padding: "20px 0px",
    },
    button: {
        textAlign: "center",
        marginRight: theme.spacing(27),
    },
    cancel_btn: {
        backgroundColor: "#D0D1D1",
        "&:hover": {
            backgroundColor: "#D0D1D1",
        },

        textTransform: "none",
        marginRight: theme.spacing(2),
    },
    save_btn: {
        backgroundColor: "#366E93",
        "&:hover": {
            backgroundColor: "#366E93",
        },
        textTransform: "none",
    },
    fields: {
        display: "flex",
        margin: "30px",
    },
}));
export default function CloseTrip() {
    let history = useHistory();
    const inputChangeHandler = () => { };
    const classes = useStyles();
    const formFields = {
        date: {
            name: "date",
            label: "Arriva Date",
            value: new Date(),
            topLabel: true,
        },
        time: {
            name: "time",
            label: "Arival Time",
            value: new Date(),
            topLabel: true,
        },
        openkm: {
            name: "openkm",
            label: "DropOFF point",
            value: "",
            topLabel: true,
        },
        recieve: {
            name: "recieve",
            label: "Reciever Name",
            value: "",
            topLabel: true,
        },
        mobile: {
            name: "mobile",
            label: "Mobile Number",
            value: "",
            topLabel: true,
        },
    };
    const [fields, setFields] = useState(formFields);
    return (
        <div>
            <Header />
            <div className="main_container">
                <div className="header_box">
                    <Grid container>
                        <Grid item xs={2}>
                            <Typography className="header_text">
                                <ArrowBackIcon
                                    onClick={() => history.push("./trip")}
                                    className="arrow"
                                />
                                Edit Trip
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField
                                variant="outlined"
                                placeholder="Search by client"
                                className={classes.field}
                                inputProps={{ style: { fontSize: 16, color: "#174A84" } }}
                                InputProps={{
                                    endAdornment: <SearchIcon className={classes.search_icon} />,
                                }}
                            />
                        </Grid>
                    </Grid>
                </div>
                <div className="form_container">
                    <div
                        style={{
                            color: "#3B7CB4",
                            backgroundColor: "#F7F7F7",
                            padding: "10px",
                            width: "500px",
                            borderRadius: "10px",
                        }}
                    >
                        <Typography>
                            User can able to edit only Drop-off point and Receiver details
                        </Typography>
                    </div>
                    <div className="details_container border_bottom">
                        <Typography className="details_container_heading">
                            Loading Details
                        </Typography>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.openkm}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                        </Grid>
                    </div>
                    <div className="details_container">
                        <Typography className="details_container_heading">
                            Client Details
                        </Typography>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.recieve}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.mobile}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={8}>
                                <Grid container spacing={2}>
                                    <Grid item xs={8}></Grid>
                                    <Grid item xs={2}>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            className="cancel_button"
                                            onClick={() => { history.push("./trip") }}
                                        >
                                            Cancel
                                        </Button>
                                    </Grid>
                                    <Grid item xs={2}>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            className="save_button"
                                        >
                                            Save
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </div>
        </div>
    );
}
