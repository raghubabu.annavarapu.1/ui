import React, {
  useState,
  useRef,
  useEffect,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Button, Typography } from "@material-ui/core";
import "../../../common.css";
import { Grid } from "@material-ui/core";
import * as Fields from "../../../sharedComponents";
import _ from "lodash";
import InputAdornment from "@mui/material/InputAdornment";
import { makeStyles } from "@material-ui/core";
import {
  blueMarkerImage,
  redMarkerImage,
  orangeMarkerImage,
} from "../../../assets/index";
import Map from "../../clients/contracts/routes/Map";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import EditRoute from "./EditRoute";
import ValidateFields from "../../../validations/validateFields";
const useStyles = makeStyles((theme) => ({
  wayPoints_section: {
    border: "1px solid #000",
    borderRadius: "4px",
  },
  waypoint_button: {
    width: "100%",
    background: "#D08B1D",
    color: "#FFFFFF",
    padding: "10px 15px",
    "&:hover": {
      background: "#D08B1D",
      color: "#FFFFFF",
    },
    textTransform: "none",
  },
  map_header: {
    display: "flex",
    justifyContent: "space-between",
    padding: "8px 0px",
  },
}));
var Service;
const TripLocation = (props, ref) => {
  const classes = useStyles();
  const locFields = {
    pickupLocation: {
      name: "pickupLocation",
      label: "Location",
      value: "",
      topLabel: true,
      validationRequired: true,
      isValid: true,
      validPattern: "SPECIAL_CHARS_DESC",
      lat: null,
      lng: null,
      errorMsg: "Please select pickup location",
      ref: useRef(),
    },
    pickHoldingTimeHours: {
      name: "pickHoldingTimeHours",
      label: "Holding Time",
      value: "",
      topLabel: "true",
      type: "number",
      min: 0,
      onlyInt: true,
    },
    pickHoldingTimeMinutes: {
      name: "pickHoldingTimeMinutes",
      label: "Holding Time",
      value: "",
      topLabel: "true",
      type: "number",
      min: 0,
      max: 59,
      maxLength: 2,
    },
    pickupName: {
      name: "pickupName",
      label: "Name",
      value: "",
      topLabel: "true",
      validationRequired: true,
      isValid: true,
      validPattern: "SPECIAL_CHARS_DESC",
      lat: null,
      lng: null,
      errorMsg: "Please enter pickup name",
      ref: useRef(),
    },
    pickupLocationGroups: {
      name: "pickupLocationGroups",
      label: "Select Groups",
      value: [],
      actualValue: [],
    },
    dropoffLocationGroups: {
      name: "dropoffLocationGroups",
      label: "Select Groups",
      value: [],
      actualValue: [],
    },
    dropoffName: {
      name: "dropoffName",
      label: "Name",
      value: "",
      topLabel: "true",
      validationRequired: true,
      isValid: true,
      validPattern: "SPECIAL_CHARS_DESC",
      lat: null,
      lng: null,
      errorMsg: "Please enter dropoff name",
      ref: useRef(),
    },
    dropoffLocation: {
      name: "dropoffLocation",
      label: "Location",
      value: "",
      topLabel: true,
      validationRequired: true,
      isValid: true,
      validPattern: "SPECIAL_CHARS_DESC",
      lat: null,
      lng: null,
      errorMsg: "Please select pickup location",
      ref: useRef(),
    },
    dropHoldingTimeHours: {
      name: "dropHoldingTimeHours",
      label: "Dropoff Holding Time",
      value: "",
      topLabel: "true",
      type: "number",
      min: 0,
      onlyInt: true,
    },
    dropHoldingTimeMinutes: {
      name: "dropHoldingTimeMinutes",
      label: "Dropoff Holding Time",
      value: "",
      topLabel: "true",
      type: "number",
      min: 0,
      max: 59,
      maxLength: 2,
    },
  };
  const wayPoints = [
    {
      waypointName: {
        name: "waypointName",
        label: "Way Point Name",
        value: "",
        validationRequired: true,
        isValid: true,
        validPattern: "SPECIAL_CHARS_DESC",
        errorMsg: "Please enter waypoint name",
        topLabel: true,
        ref: useRef(),
      },
      transitHours: {
        name: "transitHours",
        label: "Transit Time",
        value: "",
        type: "number",
        min: 0,
        topLabel: true,
        onlyInt: true,
      },
      transitMinutes: {
        name: "transitMinutes",
        label: "Transit Time",
        value: "",
        type: "number",
        min: 0,
        topLabel: true,
        max: 59,
        maxLength: 2,
      },
      holdingHours: {
        name: "holdingHours",
        label: "Holding Time",
        value: "",
        type: "number",
        min: 0,
        topLabel: true,
        onlyInt: true,
      },
      holdingMinutes: {
        name: "holdingMinutes",
        label: "Holding Time",
        value: "",
        type: "number",
        min: 0,
        max: 59,
        topLabel: true,
        maxLength: 2,
      },
      location: {
        name: "location",
        label: "Way Point Location",
        value: "",
        validationRequired: true,
        isValid: true,
        lat: null,
        lng: null,
        topLabel: true,
        validPattern: "SPECIAL_CHARS_DESC",
        errorMsg: "Please enter waypoint location",
        placeholder: "Enter Location",
        completeValue: "",
        ref: useRef(),
      },
      groups: {
        name: "groups",
        label: "Select Groups",
        value: [],
        actualValue: [],
      },
      mobileNumber1: {
        name: "mobileNumber1",
        label: "Mobile Number 1",
        value: "",
        topLabel: true,
        maxLength: 10,
        isValid: true,
        errorMsg: "Please enter valid mobile number",
      },
      mobileNumber2: {
        name: "mobileNumber2",
        label: "Mobile Number 2",
        value: "",
        topLabel: true,
        maxLength: 10,
        isValid: true,
        errorMsg: "Please enter valid mobile number",
      },
      mobileNumber3: {
        name: "mobileNumber3",
        label: "Mobile Number 3",
        value: "",
        topLabel: true,
        maxLength: 10,
        isValid: true,
        errorMsg: "Please enter valid mobile number",
      },
    },
  ];
  const formFields = {
    commodity: {
      name: "commodity",
      label: "Commodity Type",
      value: { label: "", value: "" },
      options: [],
      topLabel: true,
      ref: useRef(),
    },
    routeName: {
      name: "routeName",
      label: "Route Name",
      value: "",
      isValid: true,
      validationRequired: true,
      topLabel: true,
      validPattern: "SPECIAL_CHARS_DESC",
      errorMsg: "Please enter route name",
      ref: useRef(),
    },
    onlyWaypointAlert: {
      name: "onlyWaypointAlert",
      label: "Only Waypoint Alert",
      value: 0,
    },
    totalTATHours: {
      name: "totalTATHours",
      label: "Maximum TAT (in hours)",
      value: "",
      type: "number",
      min: 0,
    },
    totalTATMin: {
      name: "totalTATMin",
      label: "Maximum TAT (in minutes)",
      value: "",
      type: "number",
      min: 0,
    },
    maximumExpenses: {
      name: "maximumExpenses",
      label: "Maximum Expenses",
      value: "",
      type: "number",
      min: 0,
    },
    maximumFuel: {
      name: "maximumFuel",
      label: "Maximum Fuel",
      value: "",
      type: "number",
      min: 0,
    },
    maximumAdBlue: {
      name: "maximumAdBlue",
      label: "Maximum Adblue",
      value: "",
      type: "number",
      min: 0,
    },
    maximumDistance: {
      name: "maximumDistance",
      label: "Maximum Distance",
      value: "",
      type: "number",
      min: 0,
    },
    freightAmount: {
      name: "freightAmount",
      label: "Freight Amount",
      value: "",
      type: "number",
      min: 0,
    },
    maximumMarketVehicleFreightAmt: {
      name: "maximumMarketVehicleFreightAmt",
      label: "Market Vehicle Freight Amount",
      value: "",
      type: "number",
      min: 0,
    },
    notes: {
      name: "notes",
      label: "Notes",
      value: "",
    },
  };
  Service = global.service;
  const [locationFields, setLocationFields] = useState(_.cloneDeep(locFields));
  const [wayPointFields, setWayPointFields] = useState(_.cloneDeep(wayPoints));
  const [groupOptions, setGroupOptions] = useState([]);
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const mapRef = useRef();
  let validateFields = new ValidateFields();
  const placeRef1 = useRef();
  const placeRef3 = useRef();
  const [polyLine, setPolyLine] = useState([]);
  const [moreInfoOpen, setMoreInfoOpen] = useState(false);
  const [zoom, setZoom] = useState();
  const [routeData, setRouteData] = useState({});
  useEffect(() => {
    Service.getGroups()
      .then((res) => {
        let data = res.groups.map((gp) => {
          return { id: gp.id, name: gp.groupName };
        });
        setGroupOptions(data);
      })
      .catch(() => {});
    Service.getCommodities()
      .then((res) => {
        let newFields = _.cloneDeep(fields);
        newFields["commodity"]["options"] = res.commodities.map((commodity) => {
          return { label: commodity.name, value: commodity.id };
        });
        setFields(newFields);
      })
      .catch(() => {});
  }, []);
  const setLocationHandler = (name, val, lat, lng) => {
    let newLocFields = _.cloneDeep(locationFields);
    if (val && lat && lng) {
      newLocFields[name]["value"] = val;
      newLocFields[name]["lat"] = lat;
      newLocFields[name]["lng"] = lng;
      newLocFields[name]["isValid"] = true;
      setLocationFields(newLocFields);
      mapRef.current.flyToLoc({ lt: lat, ln: lng }, name);
      if (mapRef.current.route) {
        mapRef.current.setRoute(false);
      }
    }
  };
  const setWayPointLocationHandler = (name, value, lat, lng, val, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    if (value && lat && lng) {
      newWayPointFields[i][name]["value"] = value;
      newWayPointFields[i][name]["lat"] = lat;
      newWayPointFields[i][name]["lng"] = lng;
      newWayPointFields[i][name]["isValid"] = true;
      newWayPointFields[i][name]["completeValue"] = val;
      setWayPointFields(newWayPointFields);
      mapRef.current.flyToLoc({ lt: lat, ln: lng }, name, i);
      if (mapRef.current.route) {
        mapRef.current.setRoute(false);
      }
    }
  };
  const locInputChangeHandler = (value, name) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields[name]["value"] = value;
    newLocFields[name]["isValid"] = true;
    if (value.length === 0 && newLocFields[name]["validationRequired"]) {
      newLocFields[name]["isValid"] = false;
    }
    setLocationFields(newLocFields);
  };
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    newFields[name]["isValid"] = true;
    if (value.length === 0 && newFields[name]["validationRequired"]) {
      newFields[name]["isValid"] = false;
    }
    setFields(newFields);
  };
  const waypointInputChangeHandler = (value, name, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields[i][name]["value"] = value;
    newWayPointFields[i][name]["isValid"] = true;
    if (
      value.length === 0 &&
      newWayPointFields[i][name]["validationRequired"]
    ) {
      newWayPointFields[i][name]["isValid"] = false;
    }
    setWayPointFields(newWayPointFields);
  };
  const wayPointGroupChangeHandler = (value, name, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields[i][name]["value"] = value.join(",");
    newWayPointFields[i][name]["actualValue"] = value;
    setWayPointFields(newWayPointFields);
  };
  const locGroupChangeHandler = (value, name) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields[name]["value"] = value.join(",");
    newLocFields[name]["actualValue"] = value;
    setLocationFields(newLocFields);
  };
  const autoCompleteChangeHandler = (value, name) => {
    if (value) {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = value;
      newFields[name]["isValid"] = true;
      setFields(newFields);
    } else {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = { value: "", label: "" };
      if (newFields[name]["validationRequired"]) {
        newFields[name]["isValid"] = false;
      }
      setFields(newFields);
    }
  };
  const [plRefs, setPlRefs] = useState([]);
  useEffect(() => {
    setPlRefs((plRefs) =>
      Array(100)
        .fill()
        .map((_, i) => plRefs[i] || React.createRef())
    );
  }, [wayPointFields.length]);
  useEffect(() => {
    wayPointFields.forEach((wp, i) => {
      if (plRefs[i]) {
        plRefs[i].current.setPlace({
          label: wp.location.value ? wp.location.value : "",
          value: {
            description: wp.location.value ? wp.location.value : "",
          },
        });
      }
    });
  }, [wayPointFields.length]);
  const addWayPointHandler = (i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields.push(wayPoints[0]);
    setWayPointFields(newWayPointFields);
  };
  const wayPointRemoveHandler = (i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields.splice(i, 1);
    setWayPointFields(newWayPointFields);
  };
  const setPickup = (coords, loc) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields["pickupLocation"]["value"] = loc[0].formatted_address;
    newLocFields["pickupLocation"]["lat"] = coords.lat;
    newLocFields["pickupLocation"]["lng"] = coords.lng;
    setLocationFields(newLocFields);
    placeRef1.current.setPlace({
      label: loc[0].formatted_address,
      value: {
        description: loc[0].formatted_address,
        place_id: loc[0].place_id,
        reference: loc[0].place_id,
        types: loc[0].types,
      },
    });
  };
  const setDropoff = (coords, loc) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields["dropoffLocation"]["value"] = loc[0].formatted_address;
    newLocFields["dropoffLocation"]["lat"] = coords.lat;
    newLocFields["dropoffLocation"]["lng"] = coords.lng;
    setLocationFields(newLocFields);
    placeRef3.current.setPlace({
      label: loc[0].formatted_address,
      value: {
        description: loc[0].formatted_address,
        place_id: loc[0].place_id,
        reference: loc[0].place_id,
        types: loc[0].types,
      },
    });
  };
  const setWp = (coords, loc, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields[i]["waypointName"]["value"] = loc[0].formatted_address
      .split(",")
      .reverse()[2];
    newWayPointFields[i]["location"]["value"] = loc[0].formatted_address;
    newWayPointFields[i]["location"]["lat"] = coords.lat;
    newWayPointFields[i]["location"]["lng"] = coords.lng;
    newWayPointFields[i]["location"]["completeValue"] = {
      label: loc[0].formatted_address,
      value: {
        description: loc[0].formatted_address,
        place_id: loc[0].place_id,
        reference: loc[0].place_id,
        types: loc[0].types,
      },
    };
    setWayPointFields(newWayPointFields);
    plRefs[i].current.setPlace({
      label: loc[0].formatted_address,
      value: {
        description: loc[0].formatted_address,
        place_id: loc[0].place_id,
        reference: loc[0].place_id,
        types: loc[0].types,
      },
    });
  };
  const handleMoreInfoOpen = () => {
    setMoreInfoOpen(false);
  };
  const addMoreInfoHandler = () => {
    setMoreInfoOpen(false);
  };
  const setRoute = (route) => {
    let newFields = _.cloneDeep(fields);
    let newLocFields = _.cloneDeep(locationFields);
    let newWayPointFields = _.cloneDeep(wayPointFields);
    if (Object.keys(route).length === 0) {
      mapRef.current.setRoute(false);
    }
    newFields["commodity"]["value"] = {
      label:
        route.commodity && route.commodity.name ? route.commodity.name : "",
      value: route.commodity && route.commodity.id ? route.commodity.id : "",
    };
    newFields["routeName"]["value"] = route.routeName ? route.routeName : "";
    newFields["freightAmount"]["value"] = route.freightAmount;
    newFields["maximumAdBlue"]["value"] = route.maxAdBlue;
    newFields["maximumDistance"]["value"] = route.maxDistance;
    newFields["maximumExpenses"]["value"] = route.maximumExpenses;
    newFields["maximumFuel"]["value"] = route.maxFuelInLts;
    newFields["maximumMarketVehicleFreightAmt"]["value"] =
      route.maxMarketFreightAmount;
    newFields["totalTATHours"]["value"] = Math.floor(route.totalTAT / 60);
    newFields["totalTATMin"]["value"] = Math.floor(route.totalTAT % 60);
    newFields["notes"]["value"] = route.notes;
    newFields["onlyWaypointAlert"]["value"] = route.onlyWaypointAlert;
    newLocFields["pickupName"]["value"] = route.pickupName
      ? route.pickupName
      : "";
    newLocFields["pickupLocation"]["value"] = route.pickupLocationAddress
      ? route.pickupLocationAddress
      : "";
    newLocFields["pickupLocation"]["lat"] = route.pickupLocationLatitude
      ? route.pickupLocationLatitude
      : null;
    newLocFields["pickupLocation"]["lng"] = route.pickupLocationLongitude
      ? route.pickupLocationLongitude
      : null;
    placeRef1.current.setPlace({
      label: route.pickupLocationAddress ? route.pickupLocationAddress : "",
      value: {
        description: route.pickupLocationAddress
          ? route.pickupLocationAddress
          : "",
      },
    });
    newLocFields["dropoffLocation"]["value"] = route.dropoffLocationAddress
      ? route.dropoffLocationAddress
      : "";
    newLocFields["dropoffLocation"]["lat"] = route.dropoffLocationLatitude
      ? route.dropoffLocationLatitude
      : null;
    newLocFields["dropoffLocation"]["lng"] = route.dropoffLocationLongitude
      ? route.dropoffLocationLongitude
      : null;
    placeRef3.current.setPlace({
      label: route.dropoffLocationAddress ? route.dropoffLocationAddress : "",
      value: {
        description: route.dropoffLocationAddress
          ? route.dropoffLocationAddress
          : "",
      },
    });
    newLocFields["pickupLocationGroups"]["actualValue"] =
      route.pickupLocationGroups ? route.pickupLocationGroups.split(",") : [];
    newLocFields["pickupLocationGroups"]["value"] = route.pickupLocationGroups
      ? route.pickupLocationGroups
      : [];
    newLocFields["dropoffLocationGroups"]["actualValue"] =
      route.dropoffLocationGroups ? route.dropoffLocationGroups.split(",") : [];
    newLocFields["dropoffLocationGroups"]["value"] = route.dropoffLocationGroups
      ? route.dropoffLocationGroups
      : [];
    newLocFields["dropoffName"]["value"] = route.dropoffName
      ? route.dropoffName
      : "";
    newLocFields["pickupLocation"]["value"] = route.pickupLocationAddress
      ? route.pickupLocationAddress
      : "";
    newLocFields["pickHoldingTimeHours"]["value"] = route.pickupHoldingTime
      ? Math.floor(route.pickupHoldingTime / 60)
      : "";
    newLocFields["pickHoldingTimeMinutes"]["value"] = route.pickupHoldingTime
      ? Math.floor(route.pickupHoldingTime % 60)
      : "";
    newLocFields["dropHoldingTimeHours"]["value"] = route.dropoffHoldingTime
      ? Math.floor(route.dropoffHoldingTime / 60)
      : "";
    newLocFields["dropHoldingTimeMinutes"]["value"] = route.dropoffHoldingTime
      ? Math.floor(route.dropoffHoldingTime % 60)
      : "";
    newWayPointFields =
      route.wayPoints &&
      route.wayPoints.map((wp, i) => {
        return {
          waypointName: {
            name: "waypointName",
            label: "Way Point Name",
            value: wp.waypointName ? wp.waypointName : "",
            validationRequired: true,
            isValid: true,
            validPattern: "SPECIAL_CHARS_DESC",
            errorMsg: "Please enter waypoint name",
            topLabel: true,
          },
          transitHours: {
            name: "transitHours",
            label: "Transit Time",
            value: wp.transitTime ? Math.floor(wp.transitTime / 60) : "",
            type: "number",
            min: 0,
            topLabel: true,
            onlyInt: true,
          },
          transitMinutes: {
            name: "transitMinutes",
            label: "Transit Time",
            value: wp.transitTime ? Math.floor(wp.transitTime % 60) : "",
            type: "number",
            min: 0,
            topLabel: true,
            max: 59,
            maxLength: 2,
          },
          holdingHours: {
            name: "holdingHours",
            label: "Holding Time",
            value: wp.holdingTime ? Math.floor(wp.holdingTime / 60) : "",
            type: "number",
            min: 0,
            topLabel: true,
            onlyInt: true,
          },
          holdingMinutes: {
            name: "holdingMinutes",
            label: "Holding Time",
            value: wp.holdingTime ? Math.floor(wp.holdingTime % 60) : "",
            type: "number",
            min: 0,
            max: 59,
            topLabel: true,
            maxLength: 2,
          },
          location: {
            name: "location",
            label: "Way Point Location",
            value: wp.address ? wp.address : "",
            validationRequired: true,
            isValid: true,
            lat: wp.latitude ? wp.latitude : null,
            lng: wp.longitude ? wp.longitude : null,
            topLabel: true,
            validPattern: "SPECIAL_CHARS_DESC",
            errorMsg: "Please enter waypoint location",
            placeholder: "Enter Location",
            completeValue: "",
          },
          groups: {
            name: "groups",
            label: "Select Groups",
            value: wp.groups ? wp.groups : [],
            actualValue: wp.groups ? wp.groups.split(",") : [],
            validationRequired: false,
          },
          mobileNumber1: {
            name: "mobileNumber1",
            label: "Mobile Number 1",
            value:
              wp.mobileNumbers && wp.mobileNumbers.split(",")[0]
                ? wp.mobileNumbers.split(",")[0].slice(3)
                : "",
            topLabel: true,
            maxLength: 10,
            isValid: true,
            errorMsg: "Please enter valid mobile number",
          },
          mobileNumber2: {
            name: "mobileNumber2",
            label: "Mobile Number 2",
            value:
              wp.mobileNumbers && wp.mobileNumbers.split(",")[1]
                ? wp.mobileNumbers.split(",")[1].slice(3)
                : "",
            topLabel: true,
            maxLength: 10,
            isValid: true,
            errorMsg: "Please enter valid mobile number",
          },
          mobileNumber3: {
            name: "mobileNumber3",
            label: "Mobile Number 3",
            value:
              wp.mobileNumbers && wp.mobileNumbers.split(",")[2]
                ? wp.mobileNumbers.split(",")[2].slice(3)
                : "",
            topLabel: true,
            maxLength: 10,
            isValid: true,
            errorMsg: "Please enter valid mobile number",
          },
        };
      });
    setRouteData(route);
    setFields(newFields);
    setLocationFields(newLocFields);
    if (newWayPointFields) {
      setWayPointFields(newWayPointFields);
      route.wayPoints.forEach((wp, i) => {
        plRefs[i].current.setPlace({
          label: wp.address ? wp.address : "",
          value: {
            description: wp.address ? wp.address : "",
          },
        });
      });
    } else {
      newWayPointFields = [
        {
          waypointName: {
            name: "waypointName",
            label: "Way Point Name",
            value: "",
            validationRequired: true,
            isValid: true,
            validPattern: "SPECIAL_CHARS_DESC",
            errorMsg: "Please enter waypoint name",
            topLabel: true,
          },
          transitHours: {
            name: "transitHours",
            label: "Transit Time",
            value: "",
            type: "number",
            min: 0,
            topLabel: true,
            onlyInt: true,
          },
          transitMinutes: {
            name: "transitMinutes",
            label: "Transit Time",
            value: "",
            type: "number",
            min: 0,
            topLabel: true,
            max: 59,
            maxLength: 2,
          },
          holdingHours: {
            name: "holdingHours",
            label: "Holding Time",
            value: "",
            type: "number",
            min: 0,
            topLabel: true,
            onlyInt: true,
          },
          holdingMinutes: {
            name: "holdingMinutes",
            label: "Holding Time",
            value: "",
            type: "number",
            min: 0,
            max: 59,
            topLabel: true,
            maxLength: 2,
          },
          location: {
            name: "location",
            label: "Way Point Location",
            value: "",
            validationRequired: true,
            isValid: true,
            lat: null,
            lng: null,
            topLabel: true,
            validPattern: "SPECIAL_CHARS_DESC",
            errorMsg: "Please enter waypoint location",
            placeholder: "Enter Location",
            completeValue: "",
          },
          groups: {
            name: "groups",
            label: "Select Groups",
            value: [],
            actualValue: [],
            validationRequired: false,
          },
          mobileNumber1: {
            name: "mobileNumber1",
            label: "Mobile Number 1",
            value: "",
            topLabel: true,
            maxLength: 10,
            isValid: true,
            errorMsg: "Please enter valid mobile number",
          },
          mobileNumber2: {
            name: "mobileNumber2",
            label: "Mobile Number 2",
            value: "",
            topLabel: true,
            maxLength: 10,
            isValid: true,
            errorMsg: "Please enter valid mobile number",
          },
          mobileNumber3: {
            name: "mobileNumber3",
            label: "Mobile Number 3",
            value: "",
            topLabel: true,
            maxLength: 10,
            isValid: true,
            errorMsg: "Please enter valid mobile number",
          },
        },
      ];
      setWayPointFields(newWayPointFields);
      if (Object.keys(route).length === 0) {
        newWayPointFields.forEach((wp, i) => {
          plRefs[i].current.setPlace({
            label: "",
            value: {
              description: "",
            },
          });
        });
      }
    }
  };
  const validateMobileNumber = (row) => {
    const rowFields = _.cloneDeep(row);
    let valid = [];
    for (var key in rowFields) {
      if (rowFields.hasOwnProperty(key)) {
        var val = rowFields[key];
        if (
          (val.name === "mobileNumber1" ||
            val.name === "mobileNumber2" ||
            val.name === "mobileNumber3") &&
          val.value !== ""
        ) {
          let re = /^[0-9]{10}$/;
          if (re.test(val.value)) {
            valid.push(true);
            rowFields[val.name]["isValid"] = true;
          } else {
            valid.push(false);
            rowFields[val.name]["isValid"] = false;
          }
        }
      }
    }
    return {
      status: valid.filter((mob) => mob === false).length > 0 ? false : true,
      data: rowFields,
    };
  };
  const getNonEmptyRows = () => {
    const wpFields = _.cloneDeep(wayPointFields);
    const nonEmptyFields = wpFields.filter((wp) => {
      if (!checkAllfieldsEmpty(wp)) {
        return wp;
      }
    });
    return nonEmptyFields;
  };
  const checkAllfieldsEmpty = (obj) => {
    var status = true;
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        var val = obj[key];
        if (val.value.length !== 0) {
          status = false;
        }
      }
    }
    return status;
  };
  const validateRouteFields = () => {
    const dataStatus = validateFields.validateFieldsData(fields);
    let newFields = _.cloneDeep(fields);
    setFields(newFields);
    let erroredFields = Object.keys(dataStatus.data).filter(
      (key) => dataStatus.data[key].isValid === false
    );
    if (erroredFields.length > 0) {
      if (fields[erroredFields[0]]) {
        formContainerRef.current.scrollTop =
          fields[erroredFields[0]].ref.current.offsetTop - 150;
      }
    }
    return dataStatus;
  };
  const validateLocation = () => {
    const dataStatus = validateFields.validateFieldsData(locationFields);
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields = dataStatus.data;
    setLocationFields(newLocFields);
    let erroredFields = Object.keys(dataStatus.data).filter(
      (key) => dataStatus.data[key].isValid === false
    );
    if (erroredFields.length > 0) {
      if (locationFields[erroredFields[0]]) {
        formContainerRef.current.scrollTop =
          locationFields[erroredFields[0]].ref.current.offsetTop - 150;
      }
    }
    return dataStatus;
  };
  const validateWaypoints = () => {
    const nonEmptyRows = getNonEmptyRows();
    var valid = true;
    const waypoints = nonEmptyRows.map((row) => {
      const dataStatus = validateFields.validateFieldsData(row);
      const validMobileNumber = validateMobileNumber(row);
      valid = !(dataStatus.status && validMobileNumber.status)
        ? dataStatus.status && validMobileNumber.status
        : valid;
      return { ...dataStatus.data, ...validMobileNumber.data };
    });
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields = [...waypoints];
    setWayPointFields(newWayPointFields);
    return { status: valid, wayPoints: [...waypoints] };
  };
  useImperativeHandle(ref, () => {
    return {
      locationFields: locationFields,
      wayPointFields: wayPointFields,
      fields: fields,
      validateRouteFields: validateRouteFields,
      validateLocation: validateLocation,
      validateWaypoints: validateWaypoints,
      setRoute: setRoute,
      mapRef: mapRef,
    };
  });
  const radioChangeHandler = (value) => {
    let newFields = _.cloneDeep(fields);
    newFields["onlyWaypointAlert"]["value"] = value;
    setFields(newFields);
  };
  let formContainerRef = useRef();
  return (
    <div style={{ padding: "20px" }} ref={ref}>
      <Typography className="header_text">Location Details</Typography>
      <div>
        <EditRoute
          open={moreInfoOpen}
          handleClose={handleMoreInfoOpen}
          addMoreInfoHandler={addMoreInfoHandler}
          inputChangeHandler={inputChangeHandler}
          radioChangeHandler={radioChangeHandler}
          route={routeData}
        />
        <Grid
          container
          spacing={3}
          style={{ height: "calc(90vh - 178px)", overflowY: "auto" }}
        >
          <Grid
            item
            xs={4}
            style={{ height: "calc(90vh - 178px)", overflowY: "auto" }}
            ref={formContainerRef}
          >
            <Grid container spacing={2} style={{ marginTop: "5px" }}>
              <Grid
                item
                xs={12}
                className="custom_select"
                ref={fields.commodity.ref}
              >
                <Fields.AutoCompleteField
                  fieldData={fields.commodity}
                  variant="outlined"
                  autoCompleteChangeHandler={autoCompleteChangeHandler}
                />
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12} ref={fields.routeName.ref}>
                <Fields.InputField
                  fieldData={fields.routeName}
                  variant="outlined"
                  inputChangeHandler={inputChangeHandler}
                />
              </Grid>
            </Grid>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <label
                  style={{
                    fontSize: "15px",
                    backgroundColor: "white",
                    bottom: "-12px",
                    left: "30px",
                    position: "relative",
                    fontWeight: "bold",
                  }}
                >
                  Pick Up
                </label>
                <div className={classes.wayPoints_section}>
                  <div style={{ padding: "25px 20px" }}>
                    <Grid container spacing={5}>
                      <Grid item xs={12} ref={locationFields.pickupName.ref}>
                        <Fields.InputField
                          fieldData={locationFields.pickupName}
                          variant="outlined"
                          inputChangeHandler={locInputChangeHandler}
                        />
                      </Grid>
                    </Grid>
                    <Grid
                      container
                      spacing={5}
                      ref={locationFields.pickupLocation.ref}
                    >
                      <Grid item xs={12}>
                        <Fields.PlaceSearchField
                          fieldData={locationFields.pickupLocation}
                          variant="outlined"
                          setLocationHandler={setLocationHandler}
                          ref={placeRef1}
                        />
                      </Grid>
                    </Grid>
                    <Grid container spacing={5}>
                      <Grid item xs={12}>
                        <Fields.AntMultiSelectField
                          fieldData={locationFields.pickupLocationGroups}
                          options={groupOptions}
                          selectChangeHandler={locGroupChangeHandler}
                        />
                      </Grid>
                    </Grid>
                    <Grid
                      container
                      spacing={2}
                      className="details_container_content"
                    >
                      <Grid item xs={6}>
                        <Fields.InputField
                          fieldData={locationFields.pickHoldingTimeHours}
                          variant="outlined"
                          inputChangeHandler={locInputChangeHandler}
                          endAdornment={
                            <InputAdornment position="end">
                              Hours
                            </InputAdornment>
                          }
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <Fields.InputField
                          fieldData={locationFields.pickHoldingTimeMinutes}
                          variant="outlined"
                          inputChangeHandler={locInputChangeHandler}
                          endAdornment={
                            <InputAdornment position="end">Mins</InputAdornment>
                          }
                        />
                      </Grid>
                    </Grid>
                  </div>
                </div>
              </Grid>
            </Grid>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <label
                  style={{
                    fontSize: "15px",
                    backgroundColor: "white",
                    bottom: "-12px",
                    left: "30px",
                    position: "relative",
                    fontWeight: "bold",
                  }}
                >
                  WayPoint
                </label>
                <div className={classes.wayPoints_section}>
                  <div
                    label="error"
                    style={{
                      padding: "25px 20px",
                    }}
                  >
                    {wayPointFields.length > 0 ? (
                      <div className={classes.wayPoints_section}>
                        {wayPointFields.map((wp, i) => {
                          return (
                            <div
                              key={i}
                              style={{
                                padding: "25px 20px",
                                borderBottom:
                                  i + 1 < wayPointFields.length
                                    ? "1px dashed black"
                                    : "",
                              }}
                            >
                              <Grid container spacing={5}>
                                <Grid item xs={12} ref={wp.waypointName.ref}>
                                  <Fields.InputField
                                    fieldData={wp.waypointName}
                                    variant="outlined"
                                    inputChangeHandler={(value, name) => {
                                      waypointInputChangeHandler(
                                        value,
                                        name,
                                        i
                                      );
                                    }}
                                  />
                                </Grid>
                              </Grid>
                              <Grid container spacing={5}>
                                <Grid item xs={12} ref={wp.location.ref}>
                                  <Fields.PlaceSearchField
                                    ref={plRefs[i]}
                                    fieldData={wp.location}
                                    variant="outlined"
                                    wayPointFields={[...wayPointFields]}
                                    setLocationHandler={(
                                      name,
                                      value,
                                      lat,
                                      lng,
                                      val
                                    ) => {
                                      setWayPointLocationHandler(
                                        name,
                                        value,
                                        lat,
                                        lng,
                                        val,
                                        i
                                      );
                                    }}
                                  />
                                </Grid>
                              </Grid>
                              <Grid container spacing={4}>
                                <Grid item xs={12}>
                                  <Fields.AntMultiSelectField
                                    fieldData={wp.groups}
                                    options={groupOptions}
                                    selectChangeHandler={(value, name) => {
                                      wayPointGroupChangeHandler(
                                        value,
                                        name,
                                        i
                                      );
                                    }}
                                  />
                                </Grid>
                              </Grid>
                              <Grid container spacing={4}>
                                <Grid item xs={6}>
                                  <Fields.InputField
                                    fieldData={wp.transitHours}
                                    variant="outlined"
                                    inputChangeHandler={(value, name) => {
                                      waypointInputChangeHandler(
                                        value,
                                        name,
                                        i
                                      );
                                    }}
                                    endAdornment={
                                      <InputAdornment position="end">
                                        Hours
                                      </InputAdornment>
                                    }
                                  />
                                </Grid>
                                <Grid item xs={6}>
                                  <Fields.InputField
                                    fieldData={wp.transitMinutes}
                                    variant="outlined"
                                    inputChangeHandler={(value, name) => {
                                      waypointInputChangeHandler(
                                        value,
                                        name,
                                        i
                                      );
                                    }}
                                    endAdornment={
                                      <InputAdornment position="end">
                                        Mins
                                      </InputAdornment>
                                    }
                                  />
                                </Grid>
                              </Grid>
                              <Grid container spacing={4}>
                                <Grid item xs={6}>
                                  <Fields.InputField
                                    fieldData={wp.holdingHours}
                                    variant="outlined"
                                    inputChangeHandler={(value, name) => {
                                      waypointInputChangeHandler(
                                        value,
                                        name,
                                        i
                                      );
                                    }}
                                    endAdornment={
                                      <InputAdornment position="end">
                                        Hours
                                      </InputAdornment>
                                    }
                                  />
                                </Grid>
                                <Grid item xs={6}>
                                  <Fields.InputField
                                    fieldData={wp.holdingMinutes}
                                    variant="outlined"
                                    inputChangeHandler={(value, name) => {
                                      waypointInputChangeHandler(
                                        value,
                                        name,
                                        i
                                      );
                                    }}
                                    endAdornment={
                                      <InputAdornment position="end">
                                        Mins
                                      </InputAdornment>
                                    }
                                  />
                                </Grid>
                              </Grid>
                              <Grid container spacing={4}>
                                <Grid item xs={12}>
                                  <Fields.InputField
                                    fieldData={wp.mobileNumber1}
                                    variant="outlined"
                                    inputChangeHandler={(value, name) => {
                                      waypointInputChangeHandler(
                                        value,
                                        name,
                                        i
                                      );
                                    }}
                                  />
                                </Grid>
                              </Grid>
                              <Grid container spacing={4}>
                                <Grid item xs={12}>
                                  <Fields.InputField
                                    fieldData={wp.mobileNumber2}
                                    variant="outlined"
                                    inputChangeHandler={(value, name) => {
                                      waypointInputChangeHandler(
                                        value,
                                        name,
                                        i
                                      );
                                    }}
                                  />
                                </Grid>
                              </Grid>
                              <Grid container spacing={4}>
                                <Grid item xs={12}>
                                  <Fields.InputField
                                    fieldData={wp.mobileNumber3}
                                    variant="outlined"
                                    inputChangeHandler={(value, name) => {
                                      waypointInputChangeHandler(
                                        value,
                                        name,
                                        i
                                      );
                                    }}
                                  />
                                </Grid>
                              </Grid>
                              <Grid container spacing={4}>
                                {wayPointFields.length === i + 1 ? (
                                  <Grid item xs={3}>
                                    <Button
                                      variant="contained"
                                      color="primary"
                                      disableElevation
                                      style={{
                                        width: "100%",
                                        textTransform: "none",
                                      }}
                                      onClick={() => {
                                        const dataStatus =
                                          validateFields.validateFieldsData(wp);
                                        if (dataStatus.status) {
                                          addWayPointHandler(i);
                                        } else {
                                          let erroredFields = Object.keys(
                                            dataStatus.data
                                          ).filter(
                                            (key) =>
                                              wayPointFields[i][key]
                                                .isValid === false
                                          );
                                          let newWayPointFields =
                                            _.cloneDeep(wayPointFields);
                                          newWayPointFields.splice(
                                            i,
                                            1,
                                            dataStatus.data
                                          );
                                          setWayPointFields(newWayPointFields);
                                          if (erroredFields.length > 0) {
                                            formContainerRef.current.scrollTop =
                                              wayPointFields[i][
                                                erroredFields[0]
                                              ].ref.current.offsetTop - 150;
                                          }
                                        }
                                      }}
                                    >
                                      <AddIcon />
                                    </Button>
                                  </Grid>
                                ) : (
                                  <Grid item xs={3}>
                                    <Button
                                      variant="contained"
                                      color="secondary"
                                      disableElevation
                                      style={{
                                        width: "100%",
                                        textTransform: "none",
                                      }}
                                      onClick={() => {
                                        wayPointRemoveHandler(i);
                                      }}
                                    >
                                      <RemoveIcon />
                                    </Button>
                                  </Grid>
                                )}
                              </Grid>
                            </div>
                          );
                        })}
                      </div>
                    ) : (
                      <Button
                        variant="outlined"
                        onClick={() => {
                          let newWayPointFields = _.cloneDeep(wayPointFields);
                          newWayPointFields.push(wayPoints[0]);
                          setWayPointFields(newWayPointFields);
                        }}
                      >
                        Add Waypoints
                      </Button>
                    )}
                  </div>
                </div>
              </Grid>
            </Grid>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <label
                  style={{
                    fontSize: "15px",
                    backgroundColor: "white",
                    bottom: "-12px",
                    left: "30px",
                    position: "relative",
                    fontWeight: "bold",
                  }}
                >
                  Drop Off
                </label>
                <div className={classes.wayPoints_section}>
                  <div
                    style={{
                      padding: "25px 20px",
                    }}
                  >
                    <Grid container spacing={5}>
                      <Grid item xs={12} ref={locationFields.dropoffName.ref}>
                        <Fields.InputField
                          fieldData={locationFields.dropoffName}
                          variant="outlined"
                          inputChangeHandler={locInputChangeHandler}
                        />
                      </Grid>
                    </Grid>
                    <Grid container spacing={5}>
                      <Grid
                        item
                        xs={12}
                        ref={locationFields.dropoffLocation.ref}
                      >
                        <Fields.PlaceSearchField
                          fieldData={locationFields.dropoffLocation}
                          variant="outlined"
                          setLocationHandler={setLocationHandler}
                          ref={placeRef3}
                        />
                      </Grid>
                    </Grid>
                    <Grid container spacing={5}>
                      <Grid item xs={12}>
                        <Fields.AntMultiSelectField
                          fieldData={locationFields.dropoffLocationGroups}
                          options={groupOptions}
                          selectChangeHandler={locGroupChangeHandler}
                        />
                      </Grid>
                    </Grid>
                    <Grid container spacing={4}>
                      <Grid item xs={6}>
                        <Fields.InputField
                          fieldData={locationFields.dropHoldingTimeHours}
                          variant="outlined"
                          inputChangeHandler={locInputChangeHandler}
                          endAdornment={
                            <InputAdornment position="end">
                              Hours
                            </InputAdornment>
                          }
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <Fields.InputField
                          fieldData={locationFields.dropHoldingTimeMinutes}
                          variant="outlined"
                          inputChangeHandler={locInputChangeHandler}
                          endAdornment={
                            <InputAdornment position="end">Mins</InputAdornment>
                          }
                        />
                      </Grid>
                    </Grid>
                  </div>
                </div>
              </Grid>
            </Grid>
            <Grid containerspacing={2} className="details_container_content">
              <Grid item xs={12}>
                <Button
                  className={classes.waypoint_button}
                  onClick={() => {
                    setMoreInfoOpen(true);
                  }}
                >
                  Edit Fixed Route Details
                </Button>
              </Grid>
            </Grid>
          </Grid>
          <Grid
            item
            xs={8}
            style={{ height: "calc(90vh - 178px)", overflowY: "auto" }}
          >
            <div className={classes.map_header}>
              <span>
                Pickup Location :{" "}
                <img src={blueMarkerImage} width={20} height={20} />
              </span>
              <span>
                Waypoint Location :{" "}
                <img src={orangeMarkerImage} width={20} height={20} />
              </span>
              <span>
                Dropoff Location :{" "}
                <img src={redMarkerImage} width={20} height={20} />
              </span>
            </div>
            <Map
              ref={mapRef}
              waypoints={[...wayPointFields]}
              pickupLocation={locationFields.pickupLocation}
              dropoffLocation={locationFields.dropoffLocation}
              setPickup={setPickup}
              setDropoff={setDropoff}
              setWp={setWp}
              height={"calc(100vh - 332px)"}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};
export default forwardRef(TripLocation);
