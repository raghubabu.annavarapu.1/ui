import React, {
  useEffect,
  useState,
  useRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import {
  Dialog,
  DialogTitle,
  makeStyles,
  IconButton,
  DialogContent,
  DialogActions,
  Grid,
  Button,
  CircularProgress,
} from "@material-ui/core";
import PropTypes from "prop-types";
import CloseIcon from "@material-ui/icons/Close";
import _ from "lodash";
import * as Fields from "../../../sharedComponents/index";
import InputAdornment from "@mui/material/InputAdornment";
import {
  blueMarkerImage,
  redMarkerImage,
  orangeMarkerImage,
} from "../../../assets/index";
import Map from "../../clients/contracts/routes/Map";
import ValidateFields from "../../../validations/validateFields";
var Service;
const useStyles = makeStyles((theme) => ({
  dialog: {
    height: "100%",
    width: "70vw",
  },
  cancel_button: {
    backgroundColor: "#707070",
    textTransform: "none",
    margin: "0px 0px 10px 10px",
  },
  dialog_container: {
    padding: " 20px",
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  form_item: {
    marginBottom: "15px",
  },
  center_align: {
    textAlign: "right",
  },
  wayPoints_section: {
    border: "1px solid #000",
    borderRadius: "4px",
  },
  map_header: {
    padding: "8px 0px",
  },
}));
const SimpleDialog = React.forwardRef((props, ref) => {
  const classes = useStyles();
  const {
    onClose,
    open,
    onOk,
    inputChangeHandler,
    tripData,
    loc,
    tripId,
    getTrip,
    locationSnackHandler,
  } = props;
  const locFields = {
    pickupName: {
      name: "pickupName",
      label: "Name",
      value: "",
      topLabel: "true",
      validationRequired: true,
      isValid: true,
      validPattern: "SPECIAL_CHARS_DESC",
      lat: null,
      lng: null,
      errorMsg: "Please select pickup name",
    },
    pickupLocation: {
      name: "pickupLocation",
      label: "Location",
      value: "",
      topLabel: true,
      validationRequired: true,
      isValid: true,
      validPattern: "SPECIAL_CHARS_DESC",
      lat: null,
      lng: null,
      errorMsg: "Please select pickup location",
    },
    pickHoldingTimeHours: {
      name: "pickHoldingTimeHours",
      label: "Holding Time",
      value: "",
      topLabel: "true",
      type: "number",
      min: 0,
      onlyInt: true,
    },
    pickHoldingTimeMinutes: {
      name: "pickHoldingTimeMinutes",
      label: "Holding Time",
      value: "",
      topLabel: "true",
      type: "number",
      min: 0,
      max: 59,
      maxLength: 2,
    },
    pickupLocationGroups: {
      name: "pickupLocationGroups",
      label: "Select groups",
      value: [],
      actualValue: [],
    },
    dropoffName: {
      name: "dropoffName",
      label: "Name",
      value: "",
      topLabel: "true",
      validationRequired: true,
      isValid: true,
      validPattern: "SPECIAL_CHARS_DESC",
      lat: null,
      lng: null,
      errorMsg: "Please select dropoff name",
    },
    dropoffLocation: {
      name: "dropoffLocation",
      label: "Location",
      value: "",
      topLabel: true,
      validationRequired: true,
      isValid: true,
      validPattern: "SPECIAL_CHARS_DESC",
      lat: null,
      lng: null,
      errorMsg: "Please select dropoff location",
    },
    dropoffLocationGroups: {
      name: "dropoffLocationGroups",
      label: "Select Groups",
      value: [],
      actualValue: [],
    },
    dropoffTimeHours: {
      name: "dropoffTimeHours",
      label: "Holding Time",
      value: "",
      topLabel: "true",
      type: "number",
      min: 0,
      onlyInt: true,
    },
    dropoffTimeMinutes: {
      name: "dropoffTimeMinutes",
      label: "Holding Time",
      value: "",
      topLabel: "true",
      type: "number",
      min: 0,
      max: 59,
      maxLength: 2,
    },
  };
  const wayPoints = [
    {
      waypointName: {
        name: "waypointName",
        label: "Name",
        value: "",
        validationRequired: true,
        isValid: true,
        validPattern: "SPECIAL_CHARS_DESC",
        errorMsg: "Please enter waypoint name",
        topLabel: true,
      },
      location: {
        name: "location",
        label: "Way Point Location",
        value: "",
        validationRequired: true,
        isValid: true,
        lat: null,
        lng: null,
        topLabel: true,
        validPattern: "SPECIAL_CHARS_DESC",
        errorMsg: "Please enter waypoint location",
        placeholder: "Enter Location",
        completeValue: "",
      },
      groups: {
        name: "groups",
        label: "Select Groups",
        value: [],
        actualValue: [],
        validationRequired: false,
      },
      transitHours: {
        name: "transitHours",
        label: "Transit Time",
        value: "",
        type: "number",
        min: 0,
        topLabel: true,
        onlyInt: true,
      },
      transitMinutes: {
        name: "transitMinutes",
        label: "Transit Time",
        value: "",
        type: "number",
        min: 0,
        topLabel: true,
        max: 59,
        maxLength: 2,
      },
      holdingHours: {
        name: "holdingHours",
        label: "Holding Time",
        value: "",
        type: "number",
        min: 0,
        topLabel: true,
        onlyInt: true,
      },
      holdingMinutes: {
        name: "holdingMinutes",
        label: "Holding Time",
        value: "",
        type: "number",
        min: 0,
        max: 59,
        topLabel: true,
        maxLength: 2,
      },
      mobileNumber1: {
        name: "mobileNumber1",
        label: "Mobile Number 1",
        value: "",
        topLabel: true,
        maxLength: 10,
        isValid: true,
        errorMsg: "Please enter valid mobile number",
      },
      mobileNumber2: {
        name: "mobileNumber2",
        label: "Mobile Number 2",
        value: "",
        topLabel: true,
        maxLength: 10,
        isValid: true,
        errorMsg: "Please enter valid mobile number",
      },
      mobileNumber3: {
        name: "mobileNumber3",
        label: "Mobile Number 3",
        value: "",
        topLabel: true,
        maxLength: 10,
        isValid: true,
        errorMsg: "Please enter valid mobile number",
      },
    },
  ];
  const handleClose = () => {
    onClose();
  };
  const [locationFields, setLocationFields] = useState(_.cloneDeep(locFields));
  const [wayPointFields, setWayPointFields] = useState(_.cloneDeep(wayPoints));
  const [spinner, setSpinner] = useState(false);
  const [locType, setLocType] = useState(null);
  const [groupOptions, setGroupOptions] = useState([]);
  const placeRef1 = useRef();
  const placeRef2 = useRef();
  const placeRef3 = useRef();
  const mapRef = useRef();
  const [actionType, setActionType] = useState(null);
  Service = global.service;
  useEffect(() => {
    Service.getGroups()
      .then((res) => {
        let data = res.groups.map((gp) => {
          return { id: gp.id, name: gp.groupName };
        });
        setGroupOptions(data);
      })
      .catch(() => {});
  }, []);
  useEffect(() => {
    setTimeout(() => {
      setField();
    }, 100);
  }, [locType]);
  const setField = () => {
    if (locType === "PICKUP") {
      placeRef1.current.setPlace({
        label: loc.location,
        value: {
          description: loc.location,
        },
      });
    }
    if (locType === "WAYPOINT") {
      placeRef2.current.setPlace({
        label: loc.location,
        value: {
          description: loc.location,
        },
      });
    }
    if (locType === "DROPOFF") {
      placeRef3.current.setPlace({
        label: loc.location,
        value: {
          description: loc.location,
        },
      });
    }
  };
  const setLocation = (rec, locations) => {
    if (rec.sequenceNumber === 0) {
      let newLocFields = _.cloneDeep(locationFields);
      let newWayPointFields = _.cloneDeep(wayPoints);
      newLocFields["pickupName"]["value"] = rec.name;
      newLocFields["pickupLocationGroups"]["actualValue"] =
        rec.groups && rec.groups.length > 0 ? rec.groups.split(",") : [];
      newLocFields["pickupLocationGroups"]["value"] =
        rec.groups && rec.groups.length > 0 ? rec.groups : [];
      newLocFields["pickHoldingTimeHours"]["value"] = Math.floor(
        rec.holdingTime / 60
      );
      newLocFields["pickHoldingTimeMinutes"]["value"] = Math.floor(
        rec.holdingTime % 60
      );
      newLocFields["pickupLocation"]["lat"] = rec.latitude;
      newLocFields["pickupLocation"]["lng"] = rec.longitude;
      newLocFields["pickupLocation"]["value"] = rec.location;
      newLocFields["dropoffName"]["value"] = "";
      newLocFields["dropoffLocationGroups"]["actualValue"] = [];
      newLocFields["dropoffLocationGroups"]["value"] = [];
      newLocFields["dropoffTimeHours"]["value"] = "";
      newLocFields["dropoffTimeMinutes"]["value"] = "";
      newLocFields["dropoffLocation"]["lat"] = null;
      newLocFields["dropoffLocation"]["lng"] = null;
      newLocFields["dropoffLocation"]["value"] = "";
      setLocType("PICKUP");
      setLocationFields(newLocFields);
      setWayPointFields(newWayPointFields);
    } else if (rec.sequenceNumber === locations.length - 1) {
      let newLocFields = _.cloneDeep(locationFields);
      let newWayPointFields = _.cloneDeep(wayPoints);
      newLocFields["pickupName"]["value"] = "";
      newLocFields["pickupLocationGroups"]["actualValue"] = [];
      newLocFields["dropoffLocationGroups"]["value"] = [];
      newLocFields["pickHoldingTimeHours"]["value"] = "";
      newLocFields["pickHoldingTimeMinutes"]["value"] = "";
      newLocFields["pickupLocation"]["lat"] = null;
      newLocFields["pickupLocation"]["lng"] = null;
      newLocFields["pickupLocation"]["value"] = "";
      newLocFields["dropoffName"]["value"] = rec.name;
      newLocFields["dropoffLocationGroups"]["actualValue"] =
        rec.groups && rec.groups.length > 0 ? rec.groups.split(",") : [];
      newLocFields["dropoffLocationGroups"]["value"] =
        rec.groups && rec.groups.length > 0 ? rec.groups : [];
      newLocFields["dropoffTimeHours"]["value"] = Math.floor(
        rec.holdingTime / 60
      );
      newLocFields["dropoffTimeMinutes"]["value"] = Math.floor(
        rec.holdingTime % 60
      );
      newLocFields["dropoffLocation"]["lat"] = rec.latitude;
      newLocFields["dropoffLocation"]["lng"] = rec.longitude;
      newLocFields["dropoffLocation"]["value"] = rec.location;
      setLocType("DROPOFF");
      setLocationFields(newLocFields);
      setWayPointFields(newWayPointFields);
    } else {
      let newWayPointFields = _.cloneDeep(wayPointFields);
      let newLocFields = _.cloneDeep(locFields);
      newWayPointFields[0]["waypointName"]["value"] = rec.name;
      newWayPointFields[0]["groups"]["actualValue"] =
        rec.groups && rec.groups.length > 0 ? rec.groups.split(",") : [];
      newWayPointFields[0]["groups"]["value"] =
        rec.groups && rec.groups.length > 0 ? rec.groups : [];
      newWayPointFields[0]["transitHours"]["value"] = Math.floor(
        rec.transitTime / 60
      );
      newWayPointFields[0]["transitMinutes"]["value"] = Math.floor(
        rec.transitTime % 60
      );
      newWayPointFields[0]["holdingHours"]["value"] = Math.floor(
        rec.holdingTime / 60
      );
      newWayPointFields[0]["holdingMinutes"]["value"] = Math.floor(
        rec.holdingTime % 60
      );
      newWayPointFields[0]["location"]["lat"] = rec.latitude;
      newWayPointFields[0]["location"]["lng"] = rec.longitude;
      newWayPointFields[0]["location"]["value"] = rec.location;
      newWayPointFields[0]["mobileNumber1"]["value"] =
        rec.mobileNumbers && rec.mobileNumbers.split(",")[0]
          ? rec.mobileNumbers.split(",")[0].slice(3)
          : "";
      newWayPointFields[0]["mobileNumber2"]["value"] =
        rec.mobileNumbers && rec.mobileNumbers.split(",")[1]
          ? rec.mobileNumbers.split(",")[1].slice(3)
          : "";
      newWayPointFields[0]["mobileNumber3"]["value"] =
        rec.mobileNumbers && rec.mobileNumbers.split(",")[2]
          ? rec.mobileNumbers.split(",")[2].slice(3)
          : "";
      setLocType("WAYPOINT");
      setWayPointFields(newWayPointFields);
      setLocationFields(newLocFields);
    }
  };
  const resetFields = () => {
    let newLocFields = _.cloneDeep(locFields);
    let newWayPointFields = _.cloneDeep(wayPoints);
    setLocationFields(newLocFields);
    setWayPointFields(newWayPointFields);
  };
  useImperativeHandle(ref, () => {
    return {
      setLocation: setLocation,
      setActionType: setActionType,
      resetFields: resetFields,
    };
  });
  const locInputChangeHandler = (value, name) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields[name]["value"] = value;
    newLocFields[name]["isValid"] = true;
    if (value.length === 0 && newLocFields[name]["validationRequired"]) {
      newLocFields[name]["isValid"] = false;
    }
    setLocationFields(newLocFields);
  };
  const setLocationHandler = (name, val, lat, lng) => {
    let newLocFields = _.cloneDeep(locationFields);
    if (val && lat && lng) {
      newLocFields[name]["value"] = val;
      newLocFields[name]["lat"] = lat;
      newLocFields[name]["lng"] = lng;
      newLocFields[name]["isValid"] = true;
      setLocationFields(newLocFields);
      mapRef.current.flyToLoc({ lt: lat, ln: lng }, name);
      if (mapRef.current.route) {
        mapRef.current.setRoute(false);
      }
    }
  };
  const locGroupChangeHandler = (value, name) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields[name]["value"] = value.join(",");
    newLocFields[name]["actualValue"] = value;
    setLocationFields(newLocFields);
  };
  const waypointInputChangeHandler = (value, name, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields[i][name]["value"] = value;
    newWayPointFields[i][name]["isValid"] = true;
    if (
      value.length === 0 &&
      newWayPointFields[i][name]["validationRequired"]
    ) {
      newWayPointFields[i][name]["isValid"] = false;
    }
    setWayPointFields(newWayPointFields);
  };
  const setWayPointLocationHandler = (name, value, lat, lng, val, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    if (value && lat && lng) {
      newWayPointFields[i][name]["value"] = value;
      newWayPointFields[i][name]["lat"] = lat;
      newWayPointFields[i][name]["lng"] = lng;
      newWayPointFields[i][name]["isValid"] = true;
      newWayPointFields[i][name]["completeValue"] = val;
      setWayPointFields(newWayPointFields);
      mapRef.current.flyToLoc({ lt: lat, ln: lng }, name, i);
      if (mapRef.current.route) {
        mapRef.current.setRoute(false);
      }
    }
  };
  const wayPointGroupChangeHandler = (value, name, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields[i][name]["value"] = value.join(",");
    newWayPointFields[i][name]["actualValue"] = value;
    setWayPointFields(newWayPointFields);
  };
  const setPickup = (coords, loc) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields["pickupLocation"]["value"] = loc[0].formatted_address;
    newLocFields["pickupLocation"]["lat"] = coords.lat;
    newLocFields["pickupLocation"]["lng"] = coords.lng;
    setLocationFields(newLocFields);
    placeRef1.current.setPlace({
      label: loc[0].formatted_address,
      value: {
        description: loc[0].formatted_address,
        place_id: loc[0].place_id,
        reference: loc[0].place_id,
        types: loc[0].types,
      },
    });
  };
  const setWp = (coords, loc, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields[i]["waypointName"]["value"] = loc[0].formatted_address
      .split(",")
      .reverse()[2];
    newWayPointFields[i]["location"]["value"] = loc[0].formatted_address;
    newWayPointFields[i]["location"]["lat"] = coords.lat;
    newWayPointFields[i]["location"]["lng"] = coords.lng;
    newWayPointFields[i]["location"]["completeValue"] = {
      label: loc[0].formatted_address,
      value: {
        description: loc[0].formatted_address,
        place_id: loc[0].place_id,
        reference: loc[0].place_id,
        types: loc[0].types,
      },
    };
    setWayPointFields(newWayPointFields);
    placeRef2.current.setPlace({
      label: loc[0].formatted_address,
      value: {
        description: loc[0].formatted_address,
        place_id: loc[0].place_id,
        reference: loc[0].place_id,
        types: loc[0].types,
      },
    });
  };
  const setDropoff = (coords, loc) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields["dropoffLocation"]["value"] = loc[0].formatted_address;
    newLocFields["dropoffLocation"]["lat"] = coords.lat;
    newLocFields["dropoffLocation"]["lng"] = coords.lng;
    setLocationFields(newLocFields);
    placeRef3.current.setPlace({
      label: loc[0].formatted_address,
      value: {
        description: loc[0].formatted_address,
        place_id: loc[0].place_id,
        reference: loc[0].place_id,
        types: loc[0].types,
      },
    });
  };
  let validateFields = new ValidateFields();
  const addLocHandler = () => {
    let dataObj = _.cloneDeep(locationFields);
    let wpDataObj = _.cloneDeep(wayPointFields[0]);
    if (loc.sequenceNumber === 0) {
      let dataStatus = validateFields.validateFieldsData(wpDataObj);
      if (dataStatus.status) {
        let reqData = { tripId: tripId, updateType: "PICKUP", ...wpDataObj };
        setSpinner(true);
        Service.addTripLocation(reqData)
          .then((res) => {
            setSpinner(false);
            handleClose();
            getTrip();
            locationSnackHandler({
              open: true,
              severity: "success",
              message: "Waypoint Created Successfully!",
            });
          })
          .catch((error) => {
            setSpinner(false);
            handleClose();
            locationSnackHandler({
              open: true,
              severity: "error",
              message:
                error.response && error.response.data
                  ? error.response.data.message
                  : "Something went wrong!",
            });
          });
      } else {
        let newWayPointFields = _.cloneDeep(wayPointFields);
        newWayPointFields = [dataStatus.data];
        setWayPointFields(newWayPointFields);
      }
      return;
    }
    if (loc.sequenceNumber === tripData.trip.tripWayPoints.length - 1) {
      var {
        pickupName,
        pickupLocation,
        pickupLocationGroups,
        pickHoldingTimeHours,
        pickHoldingTimeMinutes,
        ...dropRest
      } = dataObj;
      let dataStatus = validateFields.validateFieldsData(dropRest);
      if (dataStatus.status) {
        let reqData = { tripId: tripId, updateType: "DROPOFF", ...dropRest };
        setSpinner(true);
        Service.addTripLocation(reqData)
          .then((res) => {
            setSpinner(false);
            handleClose();
            getTrip();
            locationSnackHandler({
              open: true,
              severity: "success",
              message: "Dropoff Created Successfully!",
            });
          })
          .catch((error) => {
            setSpinner(false);
            handleClose();
            locationSnackHandler({
              open: true,
              severity: "error",
              message:
                error.response && error.response.data
                  ? error.response.data.message
                  : "Something went wrong!",
            });
          });
      } else {
        let newLocFields = _.cloneDeep(locationFields);
        newLocFields = dataStatus.data;
        setLocationFields({
          ...newLocFields,
          pickupName,
          pickupLocation,
          pickupLocationGroups,
          pickHoldingTimeMinutes,
          pickHoldingTimeHours,
        });
      }
    } else {
      let dataStatus = validateFields.validateFieldsData(wpDataObj);
      if (dataStatus.status) {
        let reqData = {
          tripId: tripId,
          updateType: "WAYPOINT",
          waypointId: loc.id,
          ...wpDataObj,
        };
        setSpinner(true);
        Service.addTripLocation(reqData)
          .then((res) => {
            setSpinner(false);
            handleClose();
            getTrip();
            locationSnackHandler({
              open: true,
              severity: "success",
              message: "Waypoint Created Successfully!",
            });
          })
          .catch((error) => {
            setSpinner(false);
            handleClose();
            locationSnackHandler({
              open: true,
              severity: "error",
              message:
                error.response && error.response.data
                  ? error.response.data.message
                  : "Something went wrong!",
            });
          });
      } else {
        let newWayPointFields = _.cloneDeep(wayPointFields);
        newWayPointFields = [dataStatus.data];
        setWayPointFields(newWayPointFields);
      }
    }
  };
  const updateHandler = () => {
    let dataObj = _.cloneDeep(locationFields);
    let wpDataObj = _.cloneDeep(wayPointFields[0]);
    if (locType === "PICKUP") {
      var {
        dropoffName,
        dropoffLocation,
        dropoffLocationGroups,
        dropoffTimeHours,
        dropoffTimeMinutes,
        ...pickRest
      } = dataObj;
      let dataStatus = validateFields.validateFieldsData(pickRest);
      if (dataStatus.status) {
        let reqData = { tripId: tripId, updateType: locType, ...pickRest };
        setSpinner(true);
        Service.updateTripLocation(reqData)
          .then(() => {
            setSpinner(false);
            setLocType("");
            handleClose();
            getTrip();
            locationSnackHandler({
              open: true,
              severity: "success",
              message: "Location Updated Successfully!",
            });
          })
          .catch((error) => {
            setSpinner(false);
            setLocType("");
            handleClose();
            locationSnackHandler({
              open: true,
              severity: "error",
              message:
                error.response && error.response.data
                  ? error.response.data.message
                  : "Something went wrong!",
            });
          });
      } else {
        let newLocFields = _.cloneDeep(locationFields);
        newLocFields = dataStatus.data;
        setLocationFields({
          ...newLocFields,
          dropoffName,
          dropoffLocation,
          dropoffLocationGroups,
          dropoffTimeHours,
          dropoffTimeMinutes,
        });
      }
    }
    if (locType === "DROPOFF") {
      var {
        pickupName,
        pickupLocation,
        pickupLocationGroups,
        pickHoldingTimeHours,
        pickHoldingTimeMinutes,
        ...dropRest
      } = dataObj;
      let dataStatus = validateFields.validateFieldsData(dropRest);
      if (dataStatus.status) {
        let reqData = { tripId: tripId, updateType: locType, ...dropRest };
        setSpinner(true);
        Service.updateTripLocation(reqData)
          .then(() => {
            setSpinner(false);
            setLocType("");
            handleClose();
            getTrip();
            locationSnackHandler({
              open: true,
              severity: "success",
              message: "Location Updated Successfully!",
            });
          })
          .catch((error) => {
            setSpinner(false);
            setLocType("");
            handleClose();
            locationSnackHandler({
              open: true,
              severity: "error",
              message:
                error.response && error.response.data
                  ? error.response.data.message
                  : "Something went wrong!",
            });
          });
      } else {
        let newLocFields = _.cloneDeep(locationFields);
        newLocFields = dataStatus.data;
        setLocationFields({
          ...newLocFields,
          pickupName,
          pickupLocation,
          pickupLocationGroups,
          pickHoldingTimeMinutes,
          pickHoldingTimeHours,
        });
      }
    }
    if (locType === "WAYPOINT") {
      let dataStatus = validateFields.validateFieldsData(wpDataObj);
      if (dataStatus.status) {
        let reqData = {
          tripId: tripId,
          updateType: locType,
          waypointId: loc.id,
          ...wpDataObj,
        };
        setSpinner(true);
        Service.updateTripLocation(reqData)
          .then((res) => {
            setSpinner(false);
            setLocType("");
            handleClose();
            getTrip();
            locationSnackHandler({
              open: true,
              severity: "success",
              message: "Location Updated Successfully!",
            });
          })
          .catch((error) => {
            setSpinner(false);
            setLocType("");
            handleClose();
            locationSnackHandler({
              open: true,
              severity: "error",
              message:
                error.response && error.response.data
                  ? error.response.data.message
                  : "Something went wrong!",
            });
          });
      } else {
        let newWayPointFields = _.cloneDeep(wayPointFields);
        newWayPointFields = [dataStatus.data];
        setWayPointFields(newWayPointFields);
      }
    }
  };
  return (
    <div ref={ref}>
      <Dialog
        aria-labelledby="simple-dialog-title"
        open={open}
        onClose={() => {
          handleClose();
        }}
        maxWidth={"lg"}
      >
        <div className={classes.dialog_container}>
          <div className={classes.dialog}>
            {onClose ? (
              <IconButton
                aria-label="close"
                className={classes.closeButton}
                onClick={() => {
                  setLocType("");
                  handleClose();
                }}
              >
                <CloseIcon />
              </IconButton>
            ) : null}
            {loc ? (
              <>
                <DialogTitle style={{ textAlign: "center" }}>
                  {loc.sequenceNumber === 0 ? (
                    actionType === "add" ? (
                      <>
                        Waypoint{" "}
                        <img src={orangeMarkerImage} width={25} height={25} />
                      </>
                    ) : (
                      <>
                        Pick Up Location{" "}
                        <img src={blueMarkerImage} width={25} height={25} />
                      </>
                    )
                  ) : loc.sequenceNumber ===
                    tripData.trip.tripWayPoints.length - 1 ? (
                    <>
                      Drop Off Location{" "}
                      <img src={redMarkerImage} width={25} height={25} />
                    </>
                  ) : (
                    <>
                      Waypoint{" "}
                      <img src={orangeMarkerImage} width={25} height={25} />
                    </>
                  )}
                </DialogTitle>
                <DialogContent style={{ height: "460px" }}>
                  <Grid container spacing={4}>
                    <Grid
                      item
                      xs={5}
                      style={{ height: "460px", overflowY: "auto" }}
                    >
                      <fieldset
                        style={{
                          border: "1px solid black",
                          borderRadius: "4px",
                        }}
                      >
                        <legend
                          style={{
                            fontSize: "15px",
                            width: "auto",
                            fontWeight: "bold",
                          }}
                        >
                          {loc.sequenceNumber === 0
                            ? actionType === "add"
                              ? "Waypoint"
                              : "Pick Up"
                            : loc.sequenceNumber ===
                              tripData.trip.tripWayPoints.length - 1
                            ? "Dropoff"
                            : "Waypoint"}
                        </legend>
                        {loc.sequenceNumber === 0 ? (
                          actionType === "add" ? (
                            <>
                              {wayPointFields.map((wp, i) => {
                                return (
                                  <div
                                    key={i}
                                    style={{
                                      borderBottom:
                                        i + 1 < wayPointFields.length
                                          ? "1px dashed black"
                                          : "",
                                    }}
                                  >
                                    <Grid
                                      container
                                      className="details_container_content"
                                    >
                                      <Grid item xs={12}>
                                        <Fields.InputField
                                          fieldData={wp.waypointName}
                                          variant="outlined"
                                          inputChangeHandler={(value, name) => {
                                            waypointInputChangeHandler(
                                              value,
                                              name,
                                              i
                                            );
                                          }}
                                        />
                                      </Grid>
                                    </Grid>
                                    <Grid
                                      container
                                      className="details_container_content"
                                    >
                                      <Grid item xs={12}>
                                        <Fields.PlaceSearchField
                                          ref={placeRef2}
                                          fieldData={wp.location}
                                          variant="outlined"
                                          wayPointFields={[...wayPointFields]}
                                          setLocationHandler={(
                                            name,
                                            value,
                                            lat,
                                            lng,
                                            val
                                          ) => {
                                            setWayPointLocationHandler(
                                              name,
                                              value,
                                              lat,
                                              lng,
                                              val,
                                              i
                                            );
                                          }}
                                        />
                                      </Grid>
                                    </Grid>
                                    <Grid
                                      container
                                      className="details_container_content"
                                    >
                                      <Grid item xs={12}>
                                        <Fields.AntMultiSelectField
                                          fieldData={wp.groups}
                                          options={groupOptions}
                                          selectChangeHandler={(
                                            value,
                                            name
                                          ) => {
                                            wayPointGroupChangeHandler(
                                              value,
                                              name,
                                              i
                                            );
                                          }}
                                        />
                                      </Grid>
                                    </Grid>
                                    <Grid
                                      container
                                      className="details_container_content"
                                      spacing={2}
                                    >
                                      <Grid item xs={6}>
                                        <Fields.InputField
                                          fieldData={wp.transitHours}
                                          variant="outlined"
                                          inputChangeHandler={(value, name) => {
                                            waypointInputChangeHandler(
                                              value,
                                              name,
                                              i
                                            );
                                          }}
                                          endAdornment={
                                            <InputAdornment position="end">
                                              Hours
                                            </InputAdornment>
                                          }
                                        />
                                      </Grid>
                                      <Grid item xs={6}>
                                        <Fields.InputField
                                          fieldData={wp.transitMinutes}
                                          variant="outlined"
                                          inputChangeHandler={(value, name) => {
                                            waypointInputChangeHandler(
                                              value,
                                              name,
                                              i
                                            );
                                          }}
                                          endAdornment={
                                            <InputAdornment position="end">
                                              Mins
                                            </InputAdornment>
                                          }
                                        />
                                      </Grid>
                                    </Grid>
                                    <Grid
                                      container
                                      className="details_container_content"
                                      spacing={2}
                                    >
                                      <Grid item xs={6}>
                                        <Fields.InputField
                                          fieldData={wp.holdingHours}
                                          variant="outlined"
                                          inputChangeHandler={(value, name) => {
                                            waypointInputChangeHandler(
                                              value,
                                              name,
                                              i
                                            );
                                          }}
                                          endAdornment={
                                            <InputAdornment position="end">
                                              Hours
                                            </InputAdornment>
                                          }
                                        />
                                      </Grid>
                                      <Grid item xs={6}>
                                        <Fields.InputField
                                          fieldData={wp.holdingMinutes}
                                          u
                                          variant="outlined"
                                          inputChangeHandler={(value, name) => {
                                            waypointInputChangeHandler(
                                              value,
                                              name,
                                              i
                                            );
                                          }}
                                          endAdornment={
                                            <InputAdornment position="end">
                                              Mins
                                            </InputAdornment>
                                          }
                                        />
                                      </Grid>
                                    </Grid>
                                    <Grid
                                      container
                                      className="details_container_content"
                                    >
                                      <Grid item xs={12}>
                                        <Fields.InputField
                                          fieldData={wp.mobileNumber1}
                                          variant="outlined"
                                          inputChangeHandler={(value, name) => {
                                            waypointInputChangeHandler(
                                              value,
                                              name,
                                              i
                                            );
                                          }}
                                        />
                                      </Grid>
                                    </Grid>
                                    <Grid
                                      container
                                      className="details_container_content"
                                    >
                                      <Grid item xs={12}>
                                        <Fields.InputField
                                          fieldData={wp.mobileNumber2}
                                          variant="outlined"
                                          inputChangeHandler={(value, name) => {
                                            waypointInputChangeHandler(
                                              value,
                                              name,
                                              i
                                            );
                                          }}
                                        />
                                      </Grid>
                                    </Grid>
                                    <Grid
                                      container
                                      className="details_container_content"
                                    >
                                      <Grid item xs={12}>
                                        <Fields.InputField
                                          fieldData={wp.mobileNumber3}
                                          variant="outlined"
                                          inputChangeHandler={(value, name) => {
                                            waypointInputChangeHandler(
                                              value,
                                              name,
                                              i
                                            );
                                          }}
                                        />
                                      </Grid>
                                    </Grid>
                                  </div>
                                );
                              })}
                            </>
                          ) : (
                            <>
                              <Grid
                                container
                                className="details_container_content"
                              >
                                <Grid item xs={12}>
                                  <Fields.InputField
                                    variant="outlined"
                                    fieldData={locationFields.pickupName}
                                    inputChangeHandler={locInputChangeHandler}
                                  />
                                </Grid>
                              </Grid>
                              <Grid
                                container
                                className="details_container_content"
                              >
                                <Grid item xs={12}>
                                  <Fields.PlaceSearchField
                                    fieldData={locationFields.pickupLocation}
                                    setLocationHandler={setLocationHandler}
                                    ref={placeRef1}
                                  />
                                </Grid>
                              </Grid>
                              <Grid
                                container
                                className="details_container_content"
                              >
                                <Grid item xs={12}>
                                  <Fields.AntMultiSelectField
                                    fieldData={
                                      locationFields.pickupLocationGroups
                                    }
                                    options={groupOptions}
                                    selectChangeHandler={locGroupChangeHandler}
                                  />
                                </Grid>
                              </Grid>
                              <Grid
                                container
                                spacing={2}
                                className="details_container_content"
                              >
                                <Grid item xs={6}>
                                  <Fields.InputField
                                    fieldData={
                                      locationFields.pickHoldingTimeHours
                                    }
                                    variant="outlined"
                                    inputChangeHandler={locInputChangeHandler}
                                    endAdornment={
                                      <InputAdornment position="end">
                                        Hours
                                      </InputAdornment>
                                    }
                                  />
                                </Grid>
                                <Grid item xs={6}>
                                  <Fields.InputField
                                    fieldData={
                                      locationFields.pickHoldingTimeMinutes
                                    }
                                    variant="outlined"
                                    inputChangeHandler={locInputChangeHandler}
                                    endAdornment={
                                      <InputAdornment position="end">
                                        Mins
                                      </InputAdornment>
                                    }
                                  />
                                </Grid>
                              </Grid>
                            </>
                          )
                        ) : loc.sequenceNumber ===
                          tripData.trip.tripWayPoints.length - 1 ? (
                          <>
                            <Grid
                              container
                              className="details_container_content"
                            >
                              <Grid item xs={12}>
                                <Fields.InputField
                                  variant="outlined"
                                  fieldData={locationFields.dropoffName}
                                  inputChangeHandler={locInputChangeHandler}
                                />
                              </Grid>
                            </Grid>
                            <Grid
                              container
                              className="details_container_content"
                            >
                              <Grid item xs={12}>
                                <Fields.PlaceSearchField
                                  fieldData={locationFields.dropoffLocation}
                                  setLocationHandler={setLocationHandler}
                                  ref={placeRef3}
                                />
                              </Grid>
                            </Grid>
                            <Grid
                              container
                              className="details_container_content"
                            >
                              <Grid item xs={12}>
                                <Fields.AntMultiSelectField
                                  fieldData={
                                    locationFields.dropoffLocationGroups
                                  }
                                  options={groupOptions}
                                  selectChangeHandler={locGroupChangeHandler}
                                />
                              </Grid>
                            </Grid>
                            <Grid
                              container
                              spacing={2}
                              className="details_container_content"
                            >
                              <Grid item xs={6}>
                                <Fields.InputField
                                  fieldData={locationFields.dropoffTimeHours}
                                  variant="outlined"
                                  inputChangeHandler={locInputChangeHandler}
                                  endAdornment={
                                    <InputAdornment position="end">
                                      Hours
                                    </InputAdornment>
                                  }
                                />
                              </Grid>
                              <Grid item xs={6}>
                                <Fields.InputField
                                  fieldData={locationFields.dropoffTimeMinutes}
                                  variant="outlined"
                                  inputChangeHandler={locInputChangeHandler}
                                  endAdornment={
                                    <InputAdornment position="end">
                                      Mins
                                    </InputAdornment>
                                  }
                                />
                              </Grid>
                            </Grid>
                          </>
                        ) : (
                          <>
                            {wayPointFields.map((wp, i) => {
                              return (
                                <div
                                  key={i}
                                  style={{
                                    borderBottom:
                                      i + 1 < wayPointFields.length
                                        ? "1px dashed black"
                                        : "",
                                  }}
                                >
                                  <Grid
                                    container
                                    className="details_container_content"
                                  >
                                    <Grid item xs={12}>
                                      <Fields.InputField
                                        fieldData={wp.waypointName}
                                        variant="outlined"
                                        inputChangeHandler={(value, name) => {
                                          waypointInputChangeHandler(
                                            value,
                                            name,
                                            i
                                          );
                                        }}
                                      />
                                    </Grid>
                                  </Grid>
                                  <Grid
                                    container
                                    className="details_container_content"
                                  >
                                    <Grid item xs={12}>
                                      <Fields.PlaceSearchField
                                        ref={placeRef2}
                                        fieldData={wp.location}
                                        variant="outlined"
                                        wayPointFields={[...wayPointFields]}
                                        setLocationHandler={(
                                          name,
                                          value,
                                          lat,
                                          lng,
                                          val
                                        ) => {
                                          setWayPointLocationHandler(
                                            name,
                                            value,
                                            lat,
                                            lng,
                                            val,
                                            i
                                          );
                                        }}
                                      />
                                    </Grid>
                                  </Grid>
                                  <Grid
                                    container
                                    className="details_container_content"
                                  >
                                    <Grid item xs={12}>
                                      <Fields.AntMultiSelectField
                                        fieldData={wp.groups}
                                        options={groupOptions}
                                        selectChangeHandler={(value, name) => {
                                          wayPointGroupChangeHandler(
                                            value,
                                            name,
                                            i
                                          );
                                        }}
                                      />
                                    </Grid>
                                  </Grid>
                                  <Grid
                                    container
                                    className="details_container_content"
                                    spacing={2}
                                  >
                                    <Grid item xs={6}>
                                      <Fields.InputField
                                        fieldData={wp.transitHours}
                                        variant="outlined"
                                        inputChangeHandler={(value, name) => {
                                          waypointInputChangeHandler(
                                            value,
                                            name,
                                            i
                                          );
                                        }}
                                        endAdornment={
                                          <InputAdornment position="end">
                                            Hours
                                          </InputAdornment>
                                        }
                                      />
                                    </Grid>
                                    <Grid item xs={6}>
                                      <Fields.InputField
                                        fieldData={wp.transitMinutes}
                                        variant="outlined"
                                        inputChangeHandler={(value, name) => {
                                          waypointInputChangeHandler(
                                            value,
                                            name,
                                            i
                                          );
                                        }}
                                        endAdornment={
                                          <InputAdornment position="end">
                                            Mins
                                          </InputAdornment>
                                        }
                                      />
                                    </Grid>
                                  </Grid>
                                  <Grid
                                    container
                                    className="details_container_content"
                                    spacing={2}
                                  >
                                    <Grid item xs={6}>
                                      <Fields.InputField
                                        fieldData={wp.holdingHours}
                                        variant="outlined"
                                        inputChangeHandler={(value, name) => {
                                          waypointInputChangeHandler(
                                            value,
                                            name,
                                            i
                                          );
                                        }}
                                        endAdornment={
                                          <InputAdornment position="end">
                                            Hours
                                          </InputAdornment>
                                        }
                                      />
                                    </Grid>
                                    <Grid item xs={6}>
                                      <Fields.InputField
                                        fieldData={wp.holdingMinutes}
                                        u
                                        variant="outlined"
                                        inputChangeHandler={(value, name) => {
                                          waypointInputChangeHandler(
                                            value,
                                            name,
                                            i
                                          );
                                        }}
                                        endAdornment={
                                          <InputAdornment position="end">
                                            Mins
                                          </InputAdornment>
                                        }
                                      />
                                    </Grid>
                                  </Grid>
                                  <Grid
                                    container
                                    className="details_container_content"
                                  >
                                    <Grid item xs={12}>
                                      <Fields.InputField
                                        fieldData={wp.mobileNumber1}
                                        variant="outlined"
                                        inputChangeHandler={(value, name) => {
                                          waypointInputChangeHandler(
                                            value,
                                            name,
                                            i
                                          );
                                        }}
                                      />
                                    </Grid>
                                  </Grid>
                                  <Grid
                                    container
                                    className="details_container_content"
                                  >
                                    <Grid item xs={12}>
                                      <Fields.InputField
                                        fieldData={wp.mobileNumber2}
                                        variant="outlined"
                                        inputChangeHandler={(value, name) => {
                                          waypointInputChangeHandler(
                                            value,
                                            name,
                                            i
                                          );
                                        }}
                                      />
                                    </Grid>
                                  </Grid>
                                  <Grid
                                    container
                                    className="details_container_content"
                                  >
                                    <Grid item xs={12}>
                                      <Fields.InputField
                                        fieldData={wp.mobileNumber3}
                                        variant="outlined"
                                        inputChangeHandler={(value, name) => {
                                          waypointInputChangeHandler(
                                            value,
                                            name,
                                            i
                                          );
                                        }}
                                      />
                                    </Grid>
                                  </Grid>
                                </div>
                              );
                            })}
                          </>
                        )}
                      </fieldset>
                    </Grid>
                    <Grid item xs={7} style={{ height: "460px" }}>
                      <Map
                        ref={mapRef}
                        waypoints={[...wayPointFields]}
                        pickupLocation={locationFields.pickupLocation}
                        dropoffLocation={locationFields.dropoffLocation}
                        setPickup={setPickup}
                        setDropoff={setDropoff}
                        setWp={setWp}
                        height={"440px"}
                        sequenceNumber={
                          actionType === "add"
                            ? loc &&
                              loc.sequenceNumber > 0 &&
                              loc.sequenceNumber + 1
                            : loc && loc.sequenceNumber
                        }
                      />
                    </Grid>
                  </Grid>
                </DialogContent>
                <DialogActions>
                  <Button
                    style={{ textTransform: "none" }}
                    variant="contained"
                    disableElevation
                    onClick={() => {
                      setLocType("");
                      handleClose();
                    }}
                  >
                    Cancel
                  </Button>
                  <Button
                    style={{ textTransform: "none" }}
                    variant="contained"
                    color="primary"
                    disableElevation
                    onClick={() => {
                      if (actionType === "add") {
                        addLocHandler();
                      } else {
                        updateHandler();
                      }
                    }}
                    startIcon={
                      spinner ? (
                        <CircularProgress size={20} color={"#fff"} />
                      ) : null
                    }
                  >
                    {actionType === "add" ? "Add" : "Update"}
                  </Button>
                </DialogActions>
              </>
            ) : null}
          </div>
        </div>
      </Dialog>
    </div>
  );
});

SimpleDialog.propTypes = {
  onOk: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  inputChangeHandler: PropTypes.func.isRequired,
  tripData: PropTypes.object.isRequired,
  loc: PropTypes.object.isRequired,
  tripId: PropTypes.number.isRequired,
  getTrip: PropTypes.func.isRequired,
  locationSnackHandler: PropTypes.func.isRequired,
};

const LocationUpdate = (props, ref) => {
  let locationUpdateRef = useRef();
  const setLocation = (rec, locations) => {
    locationUpdateRef.current.setLocation(rec, locations);
  };
  const setActionType = (action) => {
    locationUpdateRef.current.setActionType(action);
  };
  const resetFields = () => {
    locationUpdateRef.current.resetFields();
  };
  useImperativeHandle(ref, () => {
    return {
      setLocation: setLocation,
      setActionType: setActionType,
      resetFields: resetFields,
    };
  });
  return (
    <div ref={ref}>
      <SimpleDialog
        ref={locationUpdateRef}
        open={props.open}
        onClose={props.handleClose}
        onOk={props.addMoreInfoHandler}
        inputChangeHandler={props.inputChangeHandler}
        tripData={props.tripData}
        loc={props.loc}
        tripId={props.tripId}
        getTrip={props.getTrip}
        locationSnackHandler={props.locationSnackHandler}
      />
    </div>
  );
};

export default forwardRef(LocationUpdate);