import React, { useState, useEffect, useRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Grid, Button, CircularProgress } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import _ from "lodash";
import * as Fields from "../../../sharedComponents";
import * as CONFIG from "../../../config/GlobalConstants";
import * as Components from "../../../sharedComponents";
import "../../../common.css";
import AlertMessage from "../../../components/alertmessage/AlertMessage";
import ValidateFields from "../../../validations/validateFields";
import InputAdornment from "@mui/material/InputAdornment";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import Util from "../../../utils";
import moment from "moment";
import TripLocation from "./TripLocation";
import Drawer from "@mui/material/Drawer";
import CloseIcon from "@mui/icons-material/Close";
import axios from "axios";
var Service;
const useStyles = makeStyles((theme) => ({
  alertBox: {
    padding: "10px 12px",
  },
  main_content: {
    padding: "20px",
  },
  details_container: {
    padding: "20px 5px",
  },
  border_bottom: {
    borderBottom: "1px solid #e0e0e0",
  },
  details_container_heading: {
    color: "#A7B1BF",
    fontSize: "24px",
  },
  details_container_content: {
    padding: "20px 0px",
  },
  waypoints_text_content: {
    cursor: "pointer",
    color: "#4880FF",
  },
  waypoints_text_icon: {
    verticalAlign: "bottom",
    color: "#4880FF",
    paddingRight: "5px",
  },
  bar: { listStyle: "none", padding: "0px" },
  wayPoints_section: {
    border: "1px solid #000",
    borderRadius: "4px",
  },
  listItem: {
    position: "relative",
    "&::before": {
      content: '"\\25CF"',
      marginRight: "10px",
      fontSize: "22px",
      position: "absolute",
      left: "-15px",
      top: "7px",
    },
    "&::after": {
      position: "absolute",
      left: "-14px",
      top: "20px",
      content: '""',
      borderLeft: "2px dashed black",
      marginLeft: "5px",
      height: "88%",
    },
    "&:first-of-type::after": {
      top: "20px",
    },
    "&:last-of-type::after": {
      top: "-58%",
    },
  },
  listFirstItem: {
    position: "relative",
    "&::before": {
      content: '"\\25CF"',
      marginRight: "10px",
      fontSize: "22px",
      position: "absolute",
      left: "-15px",
      top: "7px",
    },
    "&:first-of-type::after": {
      height: "0px",
    },
  },
  editroute_btn: {
    textTransform: "none",
    backgroundColor: "#D08B1D",
    "&:hover": {
      backgroundColor: "#D08B1D",
    },
  },
}));

const TripForm = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const range = (start, end) => {
    const result = [];
    for (let i = start; i < end; i++) {
      result.push(i);
    }
    return result;
  };
  const formFields = {
    bookingType: {
      name: "bookingType",
      value: CONFIG.TRIP_TYPES[0],
      isValid: true,
      label: "Trip Type",
      options: CONFIG.TRIP_TYPES,
      topLabel: true,
      validationRequired: true,
      errorMsg: "Please select trip type",
      validPattern: "SPECIAL_CHARS_DESC",
    },
    clientId: {
      name: "clientId",
      isValid: true,
      value: { label: "", value: "" },
      label: "Client Name",
      options: [],
      topLabel: true,
      validationRequired: true,
      errorMsg: "Please select client",
      validPattern: "SPECIAL_CHARS_DESC",
    },
    routeId: {
      name: "routeId",
      isValid: true,
      value: { label: "", value: "" },
      label: "CRN-Route Name",
      options: [],
      topLabel: true,
      validationRequired: true,
      errorMsg: "Please select route",
      validPattern: "SPECIAL_CHARS_DESC",
    },
    truckId: {
      name: "truckId",
      isValid: true,
      value: "",
      label: "Vehicle",
      options: [],
      topLabel: true,
      validationRequired: true,
      validPattern: "SPECIAL_CHARS_DESC",
      errorMsg: "Please select vehicle",
      readOnly: props.path === "addTrip" ? false : true,
    },
    invoiceNo: {
      name: "invoiceNo",
      isValid: true,
      value: "",
      options: [],
      label: "LR/Gate Pass/THC",
      topLabel: true,
      validationRequired: false,
      errorMsg: "Please enter invoice no",
      validPattern: "SPECIAL_CHARS_DESC",
    },
    pickupLocation: {
      name: "pickupLocation",
      value: "",
      label: "Pickup Location",
      topLabel: true,
      isValid: true,
      validationRequired: true,
      validPattern: "SPECIAL_CHARS_DESC",
      lat: null,
      lng: null,
      errorMsg: "Please select pickup location",
    },
    dropoffLocation: {
      name: "dropoffLocation",
      value: "",
      label: "Dropoff Location",
      topLabel: true,
      isValid: true,
      validationRequired: true,
      validPattern: "SPECIAL_CHARS_DESC",
      lat: null,
      lng: null,
      errorMsg: "Please select dropoff location",
    },
    commodityTypeId: {
      name: "commodityTypeId",
      label: "Commodity",
      value: { label: "", value: "" },
      topLabel: true,
      validationRequired: false,
      options: [],
    },
    freightAmount: {
      name: "freightAmount",
      label: "Freight Amount",
      value: "",
      topLabel: true,
      validationRequired: false,
      type: "number",
      min: 0,
    },
    commodityMetricValue: {
      name: "commodityMetricValue",
      label: "Commodity Metric Value",
      value: "",
      type: "number",
      min: 0,
      topLabel: true,
      isValid: true,
      validationRequired: false,
      errorMsg: "Please enter metric value",
    },
    commodityMetricType: {
      name: "commodityMetricType",
      label: "Commodity Metric Type",
      value: { label: "", value: "" },
      options: [],
      validationRequired: false,
      topLabel: true,
      isValid: true,
      errorMsg: "Please select metric type",
    },
    amountToDriver: {
      name: "amountToDriver",
      label: "Amount To Driver",
      value: "",
      validationRequired: false,
      topLabel: true,
      type: "number",
      min: 0,
    },
    scheduledPickupDatetime: {
      name: "scheduledPickupDatetime",
      label: "Scheduled Pickup Date Time",
      value: null,
      topLabel: true,
      minDate: new Date(),
      isValid: true,
      errorMsg: "",
      disabledDate: (current) => {
        return current && current < moment().subtract(1, "days").endOf("day");
      },
    },
    scheduledDropoffDatetime: {
      name: "scheduledDropoffDatetime",
      label: "Scheduled Dropoff Date Time",
      value: null,
      topLabel: true,
      isValid: true,
      errorMsg: "",
      minDate: new Date(),
      disabledDate: (current) => {
        return current && current < moment().subtract(1, "days").endOf("day");
      },
    },
  };
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const [routes, setRoutes] = useState();
  const [contractId, setContractId] = useState();
  const [percent, setPercent] = useState();
  const [files, setFiles] = useState([]);
  const [drawer, setDrawer] = useState(false);
  const [route, setRoute] = useState(null);
  const validateFields = new ValidateFields();
  let util = new Util();
  const uploadRef = useRef();
  const tripLocationRef = useRef();
  const autoCompleteChangeHandler = (value, name) => {
    if (value) {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = value;
      if (name === "clientId") {
        tripLocationRef.current.setRoute({});
        newFields["routeId"]["value"] = { label: "", value: "" };
        newFields["routeId"]["options"] = routes
          .filter(
            (route) => route.client && parseInt(value.value) === route.client.id
          )
          .map((route) => {
            return {
              label: `${
                route.contract
                  ? route.contract.customerContractReferenceNumber
                  : ""
              } ${route.routeName}`,
              value: `${route.id}`,
            };
          });
      }
      if (name === "routeId") {
        let contract = routes.filter(
          (route) => route.id === parseInt(value.value)
        );
        if(tripLocationRef.current.mapRef.current.route){
          tripLocationRef.current.mapRef.current.setRoute(false);
        }
        setContractId(contract[0].contract.id);
        Service.getRoute({ routeId: value.value })
          .then((res) => {
            setRoute(res.route);
            tripLocationRef.current.setRoute(res.route);
          })
          .catch((error) => {
            setAlertData({
              open: true,
              severity: CONFIG.ALERT_SEVERITY.error,
              message:
                error.response && error.response.data
                  ? error.response.data.message
                  : error.message,
            });
            window.scrollTo({ top: 0, behavior: "smooth" });
          });
      }
      newFields[name]["isValid"] = true;
      setFields(newFields);
    } else {
      let newFields = _.cloneDeep(fields);
      if (name === "routeId") {
        setRoute(null);
      }
      newFields[name]["value"] = { value: "", label: "" };
      setFields(newFields);
    }
  };
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    newFields[name]["isValid"] = true;
    if (value.length === 0 && newFields[name]["validationRequired"]) {
      newFields[name]["isValid"] = false;
    }
    setFields(newFields);
  };
  const setLocationHandler = (name, val, lat, lng) => {
    let newLocFields = _.cloneDeep(fields);
    if (val && lat && lng) {
      newLocFields[name]["value"] = val;
      newLocFields[name]["lat"] = lat;
      newLocFields[name]["lng"] = lng;
      setFields(newLocFields);
    }
  };
  const locFieldsHandler = (value, name) => {
    let newLocFields = _.cloneDeep(fields);
    newLocFields[name]["value"] = value;
    setFields(newLocFields);
  };
  const [spinner, setSpinner] = useState(false);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  Service = global.service;
  let dataObj = {
    onlyMeta: true,
  };
  useEffect(() => {
    const promise1 = Service.getClients(dataObj);
    const promise2 = Service.getVehicles(dataObj);
    const promise3 = Service.getRoutes(dataObj);
    const promise4 = Service.getCommodities();
    const promise5 = Service.getTripUnits();
    Promise.all([promise1, promise2, promise3, promise4, promise5])
      .then((res) => {
        setSpinner(false);
        let newFields = _.cloneDeep(fields);
        newFields["clientId"]["options"] = res[0].clients.map((clients) => {
          return { label: `${clients.companyName}`, value: `${clients.id}` };
        });
        newFields["truckId"]["options"] = res[1].vehicles.map((vehicles) => {
          return {
            label: `${vehicles.registrationNumber}`,
            value: `${vehicles.id}`,
          };
        });
        newFields["bookingType"]["value"] = CONFIG.TRIP_TYPES[0];
        if (props.path === "updateTrip") {
          newFields["truckId"]["value"] = {
            label: props.truck.registrationNumber,
            value: props.truck.id,
          };
        }
        newFields["commodityTypeId"]["options"] = res[3].commodities.map(
          (commodity) => {
            return { label: commodity.name, value: commodity.id };
          }
        );
        newFields["commodityMetricType"]["options"] = res[4].units.map(
          (unit) => {
            return { label: unit.unitName, value: unit.unitCode };
          }
        );
        setRoutes(res[2].routes);
        setFields(newFields);
      })
      .catch((error) => {
        setSpinner(false);
      });
  }, []);
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  const validateDates = (validMetrics) => {
    var newFields = _.cloneDeep(fields);
    var status = true;
    var pickDate = newFields["scheduledPickupDatetime"].value;
    var dropDate = newFields["scheduledDropoffDatetime"].value;
    let currentDate = new Date().getTime();
    if (pickDate && dropDate) {
      pickDate = new Date(pickDate).getTime();
      dropDate = new Date(dropDate).getTime();
      if (parseInt(pickDate / 100000) >= parseInt(dropDate / 100000)) {
        status = false;
        newFields["scheduledPickupDatetime"]["isValid"] = false;
        newFields["scheduledDropoffDatetime"]["isValid"] = false;
        newFields["scheduledPickupDatetime"]["errorMsg"] =
          "Pick Time Should be less than Drop Time";
        newFields["scheduledDropoffDatetime"]["errorMsg"] =
          "Drop Time Should be more than Pick Time";
      } else {
        status = true;
        newFields["scheduledPickupDatetime"]["isValid"] = true;
        newFields["scheduledDropoffDatetime"]["isValid"] = true;
        newFields["scheduledPickupDatetime"]["errorMsg"] = "";
        newFields["scheduledDropoffDatetime"]["errorMsg"] = "";
      }
    }
    if (
      (pickDate && pickDate < currentDate) ||
      (dropDate && dropDate < currentDate)
    ) {
      status = false;
      if (pickDate && pickDate < currentDate) {
        newFields["scheduledPickupDatetime"]["isValid"] = false;
        newFields["scheduledPickupDatetime"]["errorMsg"] =
          "Please enter valid pickup date";
      }
      if (dropDate && dropDate < currentDate) {
        newFields["scheduledDropoffDatetime"]["isValid"] = false;
        newFields["scheduledDropoffDatetime"]["errorMsg"] =
          "Please enter valid dropoff date";
      }
    }
    if(validMetrics){
      setFields(newFields);
    }
    return status;
  };
  const validateMetrics = () => {
    var status = true;
    var newFields = _.cloneDeep(fields);
    if (
      newFields["commodityMetricValue"]["value"].length > 0 &&
      newFields["commodityMetricType"]["value"].value
    ) {
      newFields["commodityMetricValue"]["isValid"] = true;
      newFields["commodityMetricType"]["isValid"] = true;
      status = true;
    } else if (
      newFields["commodityMetricValue"]["value"].length === 0 &&
      !newFields["commodityMetricType"]["value"].value
    ) {
      newFields["commodityMetricValue"]["isValid"] = true;
      newFields["commodityMetricType"]["isValid"] = true;
      status = true;
    } else {
      if (newFields["commodityMetricValue"]["value"].length === 0) {
        newFields["commodityMetricValue"]["isValid"] = false;
      } else if (!newFields["commodityMetricType"]["value"].value) {
        newFields["commodityMetricType"]["isValid"] = false;
      }
      status = false;
    }
    setFields(newFields);
    return status;
  };
  const addTripHandler = () => {
    let dataObj = _.cloneDeep(fields);
    dataObj = {
      ...dataObj,
      truckId: {
        ...fields.truckId,
        value:
          fields.truckId.value.value === undefined
            ? fields.truckId.value
            : fields.truckId.value.value,
      },
      bookingType: {
        ...fields.bookingType,
        value:
          fields.bookingType.value.value === undefined
            ? fields.bookingType.value
            : fields.bookingType.value.value,
      },
      commodityTypeId: {
        ...fields.commodityTypeId,
        value:
          fields.commodityTypeId.value.value === undefined
            ? fields.commodityTypeId.value
            : fields.commodityTypeId.value.value,
      },
      commodityMetricType: {
        ...fields.commodityMetricType,
        value:
          fields.commodityMetricType.value.value === undefined
            ? fields.commodityMetricType.value
            : fields.commodityMetricType.value.value,
      },
      clientId: {
        ...fields.clientId,
        value:
          fields.clientId.value.value === undefined
            ? fields.clientId.value
            : fields.clientId.value.value,
      },
      routeId: {
        ...fields.routeId,
        value:
          fields.routeId.value.value === undefined
            ? fields.routeId.value
            : fields.routeId.value.value,
      },
    };
    if (dataObj.bookingType.value === "TBB") {
      var { pickupLocation, dropoffLocation, ...tripRest } = dataObj;
    }
    if (
      dataObj.bookingType.value === "TOPAY" ||
      dataObj.bookingType.value === "RETURN"
    ) {
      var { clientId, routeId, ...tripRest } = dataObj;
    }
    let dataStatus = validateFields.validateFieldsData(tripRest);
    let validMetrics = true;
    if (
      dataObj.bookingType.value === "TOPAY" ||
      dataObj.bookingType.value === "RETURN"
    ) {
      validMetrics = validateMetrics();
    }
    let validDates = validateDates(validMetrics);
    let validRouteFields = tripLocationRef.current.validateRouteFields();
    let validLocation = tripLocationRef.current.validateLocation();
    let validWaypoints = tripLocationRef.current.validateWaypoints();
    if (
      dataObj.bookingType.value === "TBB" &&
      (!validRouteFields.status ||
        !validLocation.status ||
        !validWaypoints.status)
    ) {
      setDrawer(true);
      return;
    }
    if (dataStatus.status && validDates && validMetrics) {
      setSpinner(true);
      let url = `https://router.project-osrm.org/route/v1/driving/${util.getLngLatString(
        dataObj.bookingType.value === "TBB" ? {
          pickup: tripLocationRef.current.locationFields.pickupLocation,
          dropoff: tripLocationRef.current.locationFields.dropoffLocation,
          wayPointFields: tripLocationRef.current.wayPointFields,
        } : {
          pickup: fields.pickupLocation,
          dropoff: fields.dropoffLocation,
          wayPointFields : []
        }
      )}?overview=simplified`;
      axios.get(url).then((res) => {
        Service.createTrip({
          ...dataObj,
          polyLine: res.data.routes[0].geometry,
          contractId: contractId,
          ...(fields.scheduledPickupDatetime.value
            ? {
                scheduledPickupDatetime: moment(
                  new Date(fields.scheduledPickupDatetime.value)
                )
                  .utc()
                  .format("YYYY-MM-DD HH:mm:ss"),
              }
            : { scheduledPickupDatetime: null }),
          ...(fields.scheduledDropoffDatetime.value
            ? {
                scheduledDropoffDatetime: moment(
                  new Date(fields.scheduledDropoffDatetime.value)
                )
                  .utc()
                  .format("YYYY-MM-DD HH:mm:ss"),
              }
            : { scheduledDropoffDatetime: null }),
          ...(props.location.state && props.location.state.tripsheet_id
            ? { tripSheetId: props.location.state.tripsheet_id }
            : {}),
          routeFields: tripLocationRef.current.fields,
          routeLocFields: tripLocationRef.current.locationFields,
          routeWaypointFields: tripLocationRef.current.wayPointFields,
        })
          .then((res) => {
            if (files.length > 0) {
              files.forEach((file, i) => {
                const formData = new FormData();
                formData.append("file", file);
                formData.append("documentType", "LR");
                Service.uploadTripDocument(formData, { id: res.trip.id })
                  .then(() => {
                    if (i + 1 === files.length) {
                      let newFields = _.cloneDeep(formFields);
                      setAlertData({
                        open: true,
                        severity: CONFIG.ALERT_SEVERITY.success,
                        message:
                          props.path === "addTrip"
                            ? "Trip created successfully!"
                            : "Trip updated successfully!",
                      });
                      setFields(newFields);
                      setSpinner(false);
                      setFiles([]);
                      setRoute(null);
                      uploadRef.current.clearAttachments();
                      window.scrollTo({ top: 0, behavior: "smooth" });
                    }
                  })
                  .catch((error) => {
                    setSpinner(false);
                    setAlertData({
                      open: true,
                      severity: CONFIG.ALERT_SEVERITY.error,
                      message:
                        error.response && error.response.data
                          ? error.response.data.message
                          : "Something went wrong!",
                    });
                    window.scrollTo({ top: 0, behavior: "smooth" });
                  });
              });
            } else {
              let newFields = _.cloneDeep(formFields);
              newFields["truckId"]["options"] = [...fields.truckId.options];
              newFields["clientId"]["options"] = [...fields.clientId.options];
              newFields["commodityTypeId"]["options"] = [...fields.commodityTypeId.options];
              newFields["commodityMetricType"]["options"] = [...fields.commodityMetricType.options];
              setFields(newFields);
              setAlertData({
                open: true,
                severity: CONFIG.ALERT_SEVERITY.success,
                message: "Trip created successfully!",
              });
              setSpinner(false);
              setFiles([]);
              setDrawer(false);
              setRoute(null);
              uploadRef.current.clearAttachments();
              window.scrollTo({ top: 0, behavior: "smooth" });
            }
          })
          .catch((error) => {
            setSpinner(false);
            setAlertData({
              open: true,
              severity: CONFIG.ALERT_SEVERITY.error,
              message:
                error.response && error.response.data
                  ? error.response.data.message
                  : "Something went wrong!",
            });
            window.scrollTo({ top: 0, behavior: "smooth" });
          });
      }).catch(() => {
        Service.createTrip({
          ...dataObj,
          contractId: contractId,
          ...(fields.scheduledPickupDatetime.value
            ? {
                scheduledPickupDatetime: moment(
                  new Date(fields.scheduledPickupDatetime.value)
                )
                  .utc()
                  .format("YYYY-MM-DD HH:mm:ss"),
              }
            : { scheduledPickupDatetime: null }),
          ...(fields.scheduledDropoffDatetime.value
            ? {
                scheduledDropoffDatetime: moment(
                  new Date(fields.scheduledDropoffDatetime.value)
                )
                  .utc()
                  .format("YYYY-MM-DD HH:mm:ss"),
              }
            : { scheduledDropoffDatetime: null }),
          ...(props.location.state && props.location.state.tripsheet_id
            ? { tripSheetId: props.location.state.tripsheet_id }
            : {}),
          routeFields: tripLocationRef.current.fields,
          routeLocFields: tripLocationRef.current.locationFields,
          routeWaypointFields: tripLocationRef.current.wayPointFields,
        })
          .then((res) => {
            if (files.length > 0) {
              files.forEach((file, i) => {
                const formData = new FormData();
                formData.append("file", file);
                formData.append("documentType", "LR");
                Service.uploadTripDocument(formData, { id: res.trip.id })
                  .then(() => {
                    if (i + 1 === files.length) {
                      let newFields = _.cloneDeep(formFields);
                      setAlertData({
                        open: true,
                        severity: CONFIG.ALERT_SEVERITY.success,
                        message:
                          props.path === "addTrip"
                            ? "Trip created successfully!"
                            : "Trip updated successfully!",
                      });
                      setFields(newFields);
                      setSpinner(false);
                      setFiles([]);
                      setRoute(null);
                      uploadRef.current.clearAttachments();
                      window.scrollTo({ top: 0, behavior: "smooth" });
                    }
                  })
                  .catch((error) => {
                    setSpinner(false);
                    setAlertData({
                      open: true,
                      severity: CONFIG.ALERT_SEVERITY.error,
                      message:
                        error.response && error.response.data
                          ? error.response.data.message
                          : "Something went wrong!",
                    });
                    window.scrollTo({ top: 0, behavior: "smooth" });
                  });
              });
            } else {
              let newFields = _.cloneDeep(formFields);
              newFields["truckId"]["options"] = [...fields.truckId.options];
              newFields["clientId"]["options"] = [...fields.clientId.options];
              newFields["commodityTypeId"]["options"] = [...fields.commodityTypeId.options];
              newFields["commodityMetricType"]["options"] = [...fields.commodityMetricType.options];
              setFields(newFields);
              setAlertData({
                open: true,
                severity: CONFIG.ALERT_SEVERITY.success,
                message: "Trip created successfully!",
              });
              setSpinner(false);
              setFiles([]);
              setDrawer(false);
              setRoute(null);
              uploadRef.current.clearAttachments();
              window.scrollTo({ top: 0, behavior: "smooth" });
            }
          })
          .catch((error) => {
            setSpinner(false);
            setAlertData({
              open: true,
              severity: CONFIG.ALERT_SEVERITY.error,
              message:
                error.response && error.response.data
                  ? error.response.data.message
                  : "Something went wrong!",
            });
            window.scrollTo({ top: 0, behavior: "smooth" });
          });
      })
    } else {
      if (!validDates || !validMetrics) {
        window.scrollTo({ top: 0, behavior: "smooth" });
      } else {
        let newFields = _.cloneDeep(fields);
        newFields = {
          ...fields,
          ...dataStatus.data,
          ...(dataStatus.data.bookingType.value === "TBB"
            ? {
                bookingType: {
                  ...dataStatus.data.bookingType,
                  value: dataStatus.data.bookingType.value
                    ? CONFIG.TRIP_TYPES.filter(
                        (opt) => opt.value === dataStatus.data.bookingType.value
                      )[0]
                    : { label: "", value: "" },
                },
                clientId: {
                  ...dataStatus.data.clientId,
                  value: dataStatus.data.clientId.value
                    ? newFields.clientId.options.filter(
                        (opt) => opt.value === dataStatus.data.clientId.value
                      )[0]
                    : { label: "", value: "" },
                },
                routeId: {
                  ...dataStatus.data.routeId,
                  value: dataStatus.data.routeId.value
                    ? newFields.routeId.options.filter(
                        (opt) => opt.value === dataStatus.data.routeId.value
                      )[0]
                    : { label: "", value: "" },
                },
              }
            : {
                bookingType: {
                  ...dataStatus.data.bookingType,
                  value: dataStatus.data.bookingType.value
                    ? CONFIG.TRIP_TYPES.filter(
                        (opt) => opt.value === dataStatus.data.bookingType.value
                      )[0]
                    : { label: "", value: "" },
                },
                commodityTypeId: {
                  ...dataStatus.data.commodityTypeId,
                  value: dataStatus.data.commodityTypeId.value
                    ? newFields.commodityTypeId.options.filter(
                        (opt) =>
                          parseInt(opt.value) ===
                          dataStatus.data.commodityTypeId.value
                      )[0]
                    : { label: "", value: "" },
                },
                commodityMetricType: {
                  ...dataStatus.data.commodityMetricType,
                  value: dataStatus.data.commodityMetricType.value
                    ? newFields.commodityMetricType.options.filter(
                        (opt) =>
                          parseInt(opt.value) ===
                          dataStatus.data.commodityMetricType.value
                      )[0]
                    : { label: "", value: "" },
                },
              }),
          ...(props.path === "addTrip"
            ? {
                truckId: {
                  ...dataStatus.data.truckId,
                  value: dataStatus.data.truckId.value
                    ? newFields.truckId.options.filter(
                        (opt) => opt.value === dataStatus.data.truckId.value
                      )[0]
                    : { label: "", value: "" },
                },
              }
            : {
                truckId: {
                  ...dataStatus.data.truckId,
                  value: {
                    label: props.truck.registrationNumber,
                    value: props.truck.id,
                  },
                },
              }),
        };
        setFields(newFields);
        window.scrollTo({ top: 0, behavior: "smooth" });
      }
    }
  };
  const uploadHandler = (file) => {
    let newFiles = [...files];
    newFiles.push(file[0]);
    setFiles(newFiles);
  };
  const fileRemoveHandler = (i) => {
    let newFiles = [...files];
    newFiles.splice(i, 1);
    setFiles(newFiles);
  };
  const timeStampChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    if (value) {
      newFields[name]["value"] = moment(value);
      newFields[name]["isValid"] = true;
      if (name === "scheduledPickupDatetime") {
        newFields["scheduledDropoffDatetime"]["minDate"] = new Date(
          moment(value)
        );
        newFields["scheduledDropoffDatetime"]["disabledDate"] = (d) => {
          return !d || d.isBefore(util.formatDate(new Date(moment(value))));
        };
      }
      if (name === "scheduledDropoffDatetime") {
        newFields["scheduledPickupDatetime"]["maxDate"] = new Date(value);
        newFields["scheduledPickupDatetime"]["disabledDate"] = (d) => {
          let date = new Date(moment(value));
          date.setDate(date.getDate() + 1);
          return !d || d.isAfter(util.formatDate(date));
        };
      }
    } else {
      newFields[name]["value"] = null;
      newFields[name]["isValid"] = true;
      if (name === "scheduledDropoffDatetime") {
        newFields["scheduledPickupDatetime"]["disabledDate"] = (current) => {
          return current && current < moment().subtract(1, "days").endOf("day");
        };
      }
      if (name === "scheduledPickupDatetime") {
        newFields["scheduledDropoffDatetime"]["disabledDate"] = (current) => {
          return current && current < moment().subtract(1, "days").endOf("day");
        };
      }
    }
    setFields(newFields);
  };
  return (
    <>
      {alertData.open ? (
        <div className={classes.alertBox}>
          <AlertMessage
            severity={alertData.severity}
            message={alertData.message}
            closeAlert={closeAlert}
          />
        </div>
      ) : null}
      <div className="form_container">
        <div className="details_container border_bottom">
          <div style={{ display: "flex" }}>
            <Typography
              className="details_container_heading"
              style={{ flexGrow: "1" }}
            >
              Trip Details
            </Typography>
            <div>
              {route &&
              (fields.bookingType.value.value === undefined
                ? fields.bookingType.value
                : fields.bookingType.value.value) === "TBB" ? (
                <Button
                  className={classes.editroute_btn}
                  variant="contained"
                  color="primary"
                  onClick={() => {
                    setDrawer(true);
                  }}
                >
                  Edit Route Information
                </Button>
              ) : null}
              <Drawer open={drawer} anchor="bottom" variant="persistent">
                <CloseIcon
                  onClick={() => {
                    setDrawer(false);
                  }}
                  cursor="pointer"
                  style={{ alignSelf: "flex-end", margin: "5px", color: "red" }}
                />

                <TripLocation
                  drawer={drawer}
                  route={fields.routeId}
                  routeData={route}
                  ref={tripLocationRef}
                />
              </Drawer>
            </div>
          </div>
          <Grid container spacing={3} className="details_container_content">
            <Grid item xs={4} className={"custom_select"}>
              <Fields.AntSelectableSearchField
                fieldData={fields.truckId}
                autoCompleteChangeHandler={autoCompleteChangeHandler}
              />
            </Grid>
            <Grid item xs={4} className={"custom_select"}>
              <Fields.AntSelectableSearchField
                fieldData={fields.bookingType}
                autoCompleteChangeHandler={autoCompleteChangeHandler}
              />
            </Grid>
          </Grid>
          {(fields.bookingType.value.value === undefined
            ? fields.bookingType.value
            : fields.bookingType.value.value) === "TBB" ? (
            <Grid container spacing={3} className="details_container_content">
              <Grid item xs={4} className={"custom_select"}>
                <Fields.AntSelectableSearchField
                  fieldData={fields.clientId}
                  autoCompleteChangeHandler={autoCompleteChangeHandler}
                />
              </Grid>
              <Grid item xs={4} className={"custom_select"}>
                <Fields.AntSelectableSearchField
                  fieldData={fields.routeId}
                  autoCompleteChangeHandler={autoCompleteChangeHandler}
                />
              </Grid>
            </Grid>
          ) : (fields.bookingType.value.value === undefined
              ? fields.bookingType.value
              : fields.bookingType.value.value) === "TOPAY" ||
            (fields.bookingType.value.value === undefined
              ? fields.bookingType.value
              : fields.bookingType.value.value) === "RETURN" ? (
            <>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4}>
                  <Fields.PlaceSearchField
                    fieldData={fields.pickupLocation}
                    setLocationHandler={setLocationHandler}
                    variant="outlined"
                    emptyLocHandler={locFieldsHandler}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Fields.PlaceSearchField
                    fieldData={fields.dropoffLocation}
                    setLocationHandler={setLocationHandler}
                    variant="outlined"
                    emptyLocHandler={locFieldsHandler}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} className="custom_select">
                  <Fields.AntSelectableSearchField
                    fieldData={fields.commodityTypeId}
                    autoCompleteChangeHandler={autoCompleteChangeHandler}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Fields.InputField
                    fieldData={fields.freightAmount}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4}>
                  <Fields.InputField
                    fieldData={fields.commodityMetricValue}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={4} className="custom_select">
                  <Fields.AntSelectableSearchField
                    fieldData={fields.commodityMetricType}
                    autoCompleteChangeHandler={autoCompleteChangeHandler}
                  />
                </Grid>
              </Grid>
            </>
          ) : null}
          <Grid container spacing={3} className="details_container_content">
            <Grid item xs={4}>
              <Fields.AntDatePickerField
                fieldData={fields.scheduledPickupDatetime}
                timeStampChangeHandler={timeStampChangeHandler}
              />
            </Grid>
            <Grid item xs={4}>
              <Fields.AntDatePickerField
                fieldData={fields.scheduledDropoffDatetime}
                timeStampChangeHandler={timeStampChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} className="details_container_content">
            <Grid item xs={4}>
              <Fields.InputField
                fieldData={fields.invoiceNo}
                inputChangeHandler={inputChangeHandler}
                variant="outlined"
              />
            </Grid>
            <Grid item xs={4}>
              <Fields.InputField
                fieldData={fields.amountToDriver}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
              />
            </Grid>
          </Grid>
        </div>
        <div className="details_container">
          <Typography className="details_container_heading">
            Upload LR/Gate pass/THC
          </Typography>
          <Grid container spacing={3} className="details_container_content">
            <Grid item xs={8}>
              <Components.UploadDocuments
                ref={uploadRef}
                uploadHandler={uploadHandler}
                data={{ attachments: [...files] }}
                percentCompleted={percent}
                label={"Upload max 5 documents"}
                max={5}
                fileRemoveHandler={fileRemoveHandler}
                removeFile={true}
              />
            </Grid>
          </Grid>
        </div>
        <Grid container spacing={3} className="details_container_content">
          <Grid item xs={8}>
            <Grid container spacing={2}>
              <Grid item xs={8}></Grid>
              <Grid item xs={2}>
                <Button
                  variant="outlined"
                  onClick={() => {
                    if (
                      props.location.state &&
                      props.location.state.tripsheet_id
                    ) {
                      history.push({
                        pathname: "./TripsheetSummary",
                        state: {
                          tripsheet_id: props.location.state.tripsheet_id,
                          tripsheet: props.location.state.tripsheet,
                        },
                      });
                    } else {
                      history.push("./trip");
                    }
                  }}
                  className="cancel_button"
                >
                  Cancel
                </Button>
              </Grid>
              <Grid item xs={2}>
                <Button
                  className="save_button"
                  onClick={() => {
                    addTripHandler();
                  }}
                  startIcon={
                    spinner ? (
                      <CircularProgress size={20} color={"#fff"} />
                    ) : null
                  }
                >
                  {props.path === "addTrip" ? "Save" : "Update"}
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </>
  );
};
export default TripForm;
