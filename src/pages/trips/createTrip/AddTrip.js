import React from "react";
import { useHistory } from "react-router-dom";
import { Typography } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import Header from "../../../components/header";
import TripForm from "./TripForm";
import "../../../common.css";

const AddTrip = (props) => {
  const history = useHistory();
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                if (props.location.state && props.location.state.tripsheet_id) {
                  history.push({
                    pathname: "./TripsheetSummary",
                    state: {
                      tripsheet_id: props.location.state.tripsheet_id,
                      tripsheet: props.location.state.tripsheet,
                    },
                  });
                } else {
                  history.push("./trip");
                }
              }}
            />
            Add Trip
          </Typography>
        </div>
        <TripForm
          path={props.location.pathname.substring(1)}
          location={props.location}
        />
      </div>
    </div>
  );
};
export default AddTrip;
