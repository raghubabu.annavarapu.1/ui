import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Typography, Grid } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import Header from "../../../components/header";
import TripForm from "./TripForm";
import _ from "lodash";
import "../../../common.css";

const UpdateTrip = (props) => {
  const history = useHistory();
  useEffect(() => {}, []);
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Grid container>
            <Grid item xs={2}>
              <Typography className="header_text">
                <ArrowBack
                  className="arrow"
                  onClick={() => {
                    history.push({
                      pathname: "./tripDetails",
                      state: { tripId: props.location.state.tripId },
                    });
                  }}
                />
                Update Trip
              </Typography>
            </Grid>
          </Grid>
        </div>
        <TripForm
          path={props.location.pathname.substring(1)}
          truck={props.location.state.truck}
        />
      </div>
    </div>
  );
};

export default UpdateTrip;
