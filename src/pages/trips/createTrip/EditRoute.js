import React, { useState, useEffect } from "react";
import {
  Dialog,
  DialogTitle,
  makeStyles,
  IconButton,
  Grid,
  Button,
  Radio,
  RadioGroup
} from "@material-ui/core";
import { FormControlLabel, FormLabel } from "@mui/material";
import PropTypes from "prop-types";
import CloseIcon from "@material-ui/icons/Close";
import _ from "lodash";
import * as Fields from "../../../sharedComponents/index";
import InputAdornment from "@mui/material/InputAdornment";
const useStyles = makeStyles((theme) => ({
  dialog: {
    display: "flex",
    flexgrow: "1",
    fontWeight: "bold",
    justifyContent: "center",
    margin: "0px 100px ",
  },
  cancel_button: {
    backgroundColor: "#707070",
    textTransform: "none",
    margin: "0px 0px 10px 10px",
  },
  dialog_container: {
    padding: "0px 20px",
    width: "550px",
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  form_item: {
    marginBottom: "15px",
  },
  center_align: {
    textAlign: "center",
  },
}));
function SimpleDialog(props) {
  const classes = useStyles();
  const { onClose, open, onOk, inputChangeHandler, route, wayPointAlertHandler } = props;
  const formFields = {
    onlyWaypointAlert : {
      name: "onlyWaypointAlert",
      label: "Only Waypoint Alert",
      value: 0
    },
    totalTATHours: {
      name: "totalTATHours",
      label: "Maximum TAT",
      value:
        route && route.totalTAT
          ? parseInt(`${route.totalTAT / 60}`.split(".")[0])
          : "",
      type: "number",
      onlyInt: true,
      min: 0,
    },
    totalTATMin: {
      name: "totalTATMin",
      label: "Maximum TAT",
      value:
        route && route.totalTAT
          ? (parseInt(`${route.totalTAT / 60}`.split(".")[1]) / 10) * 60
          : "",
      type: "number",
      min: 0,
      max: 59,
    },
    maximumDistance: {
      name: "maximumDistance",
      label: "Maximum Distance",
      value: route && route.maxDistance ? route.maxDistance : "",
      type: "number",
      min: 0,
    },
    maximumExpenses: {
      name: "maximumExpenses",
      label: "Maximum Expenses",
      value: route && route.maximumExpenses ? route.maximumExpenses : "",
      type: "number",
      min: 0,
    },
    maximumFuel: {
      name: "maximumFuel",
      label: "Maximum Fuel",
      value: route && route.maxFuelInLts ? route.maxFuelInLts : "",
      type: "number",
      min: 0,
    },
    maximumAdBlue: {
      name: "maximumAdBlue",
      label: "Maximum Adblue",
      value: route && route.maxAdBlue ? route.maxAdBlue : "",
      type: "number",
      min: 0,
    },
    freightAmount: {
      name: "freightAmount",
      label: "Freight Amount",
      value: route && route.freightAmount ? route.freightAmount : "",
      type: "number",
      min: 0,
    },
    maximumMarketVehicleFreightAmt: {
      name: "maximumMarketVehicleFreightAmt",
      label: "Market Vehicle Freight Amount",
      value:
        route && route.maxMarketFreightAmount
          ? route.maxMarketFreightAmount
          : "",
      type: "number",
      min: 0,
    },
    notes: {
      name: "notes",
      label: "Notes",
      value: route && route.notes ? route.notes : "",
      topLabel: true,
    },
  };
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  useEffect(() => {
    if (route) {
      let newFields = _.cloneDeep(fields);
      newFields["onlyWaypointAlert"]["value"] = route.onlyWaypointAlert;
      newFields["totalTATHours"]["value"] = Math.floor(route.totalTAT/60);
      newFields["totalTATMin"]["value"] = Math.floor(route.totalTAT%60);
      newFields["maximumDistance"]["value"] = route.maxDistance;
      newFields["maximumAdBlue"]["value"] = route.maxAdBlue;
      newFields["freightAmount"]["value"] = route.freightAmount;
      newFields["maximumExpenses"]["value"] = route.maximumExpenses;
      newFields["maximumFuel"]["value"] = route.maxFuelInLts;
      newFields["maximumMarketVehicleFreightAmt"]["value"] = route.maxMarketFreightAmount;
      newFields["notes"]["value"] = route.notes;
      setFields(newFields);
    }
  }, [route]);
  const handleClose = () => {
    onClose();
  };
  const onChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    setFields(newFields);
    inputChangeHandler(value, name);
  };
  const radioChangeHandler = (e) => {
    let newFields = _.cloneDeep(fields);
    newFields["onlyWaypointAlert"]["value"] = Number(e.target.value);
    setFields(newFields);
    wayPointAlertHandler(Number(e.target.value));
  }
  return (
    <div>
      <Dialog
        aria-labelledby="simple-dialog-title"
        open={open}
        onClose={handleClose}
        maxWidth={"lg"}
      >
        <div className={classes.dialog_container}>
          <div className={classes.dialog}>
            <DialogTitle id="simple-dialog-title">More Information</DialogTitle>
            {onClose ? (
              <IconButton
                aria-label="close"
                className={classes.closeButton}
                onClick={onClose}
              >
                <CloseIcon />
              </IconButton>
            ) : null}
          </div>
          <Grid container spacing={3} className={classes.form_item}>
          <Grid item xs={12}>
              <FormLabel>{fields.onlyWaypointAlert.label}</FormLabel>
              <RadioGroup row value={fields.onlyWaypointAlert.value} onChange={radioChangeHandler}>
                <FormControlLabel control={<Radio/>} value={0} label={"True"} style={{ border: "none" }}/>
                <FormControlLabel control={<Radio/>} value={1} label={"False"} style={{ border: "none" }} />
              </RadioGroup>
            </Grid>
            <Grid item xs={6}>
              <Fields.InputField
                fieldData={fields.totalTATHours}
                variant="outlined"
                inputChangeHandler={onChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Hours</InputAdornment>
                }
              />
            </Grid>
            <Grid item xs={6}>
              <Fields.InputField
                fieldData={fields.totalTATMin}
                variant="outlined"
                inputChangeHandler={onChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Mins</InputAdornment>
                }
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.maximumDistance}
                variant="outlined"
                inputChangeHandler={onChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Kms</InputAdornment>
                }
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.maximumExpenses}
                variant="outlined"
                inputChangeHandler={onChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Rs</InputAdornment>
                }
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.maximumFuel}
                variant="outlined"
                inputChangeHandler={onChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Lts</InputAdornment>
                }
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.maximumAdBlue}
                variant="outlined"
                inputChangeHandler={onChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Lts</InputAdornment>
                }
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.freightAmount}
                variant="outlined"
                inputChangeHandler={onChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Rs</InputAdornment>
                }
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.maximumMarketVehicleFreightAmt}
                variant="outlined"
                inputChangeHandler={onChangeHandler}
                endAdornment={
                  <InputAdornment position="end">Rs</InputAdornment>
                }
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.TextAreaField
                fieldData={fields.notes}
                variant="outlined"
                inputChangeHandler={onChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.form_item}>
            <Grid item xs={12} className={classes.center_align}>
              <Button
                variant="contained"
                color="primary"
                disableElevation
                style={{ marginRight: "10px" }}
                onClick={onOk}
              >
                Ok
              </Button>
              <Button
                variant="contained"
                color="secondary"
                disableElevation
                onClick={onClose}
              >
                cancel
              </Button>
            </Grid>
          </Grid>
        </div>
      </Dialog>
    </div>
  );
}
SimpleDialog.propTypes = {
  onOk: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  inputChangeHandler: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired,
  wayPointAlertHandler: PropTypes.func.isRequired
};
const EditRoute = (props) => {
  return (
    <SimpleDialog
      open={props.open}
      onClose={props.handleClose}
      onOk={props.addMoreInfoHandler}
      inputChangeHandler={props.inputChangeHandler}
      route={props.route}
      wayPointAlertHandler={props.radioChangeHandler}
    />
  );
};
export default EditRoute;
