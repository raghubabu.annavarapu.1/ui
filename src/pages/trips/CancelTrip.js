import React, { useState } from "react";
import _ from "lodash";
import ValidateFields from "../../validations/validateFields";
import {
  Dialog,
  DialogTitle,
  Button,
  CircularProgress,
  DialogContent,
  DialogActions,
} from "@material-ui/core";
import CloseIcon from "@mui/icons-material/Close";
import * as Fields from "../../sharedComponents";
import PropTypes from "prop-types";

var Service;

function SimpleDialog(props) {
  let cancelFeilds = {
    remarks: {
      name: "remarks",
      label: "Remarks",
      value: "",
      topLabel: true,
      isValid: true,
      validationRequired: true,
      validPattern: "SPECIAL_CHARS_DESC",
      errorMsg: "Please enter remarks",
    },
  };
  const [fields, setFields] = useState(_.cloneDeep(cancelFeilds));
  const [spinner, setSpinner] = useState(false);
  Service = global.service;
  let validateFields = new ValidateFields();
  const { onClose, open, tripData, setAlert, getTrip } = props;
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    newFields[name]["isValid"] = true;
    if (value.length === 0 && newFields[name]["validationRequired"]) {
      newFields[name]["isValid"] = false;
    }
    setFields(newFields);
  };
  const handleClose = () => {
    onClose();
  };
  const cancelHandler = () => {
    let dataObj = _.cloneDeep(fields);
    let dataStatus = validateFields.validateFieldsData(dataObj);
    if (dataStatus.status) {
      setSpinner(true);
      Service.cancelTrip(dataObj, tripData)
        .then((res) => {
          setSpinner(false);
          handleClose();
          setAlert("Trip cancelled successfully!", "success");
          getTrip();
        })
        .catch((error) => {
          setSpinner(false);
          setAlert(
            error.response && error.response.data
              ? error.response.data.message
              : "Something went wrong!",
            "error"
          );
        });
    } else {
      let newFields = _.cloneDeep(fields);
      newFields = dataStatus.data;
      setFields(newFields);
    }
  };
  return (
    <div>
      <Dialog open={open} onClose={handleClose}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <DialogTitle id="simple-dialog-title">
            Are you sure want to cancel trip ?
          </DialogTitle>
          <CloseIcon
            style={{
              marginRight: "20px",
              cursor: "pointer",
              alignSelf: "center",
              marginRight: "20px",
            }}
            onClick={() => {
              handleClose();
            }}
          />
        </div>
        <DialogContent style={{ width: "600px" }}>
          <Fields.TextAreaField
            fieldData={fields.remarks}
            inputChangeHandler={inputChangeHandler}
            variant="outlined"
          />
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            disableElevation
            onClick={() => {
              handleClose();
            }}
          >
            Close
          </Button>
          <Button
            variant="contained"
            color="secondary"
            disableElevation
            onClick={() => {
              cancelHandler();
            }}
            style={{ marginRight: "15px" }}
            startIcon={
              spinner ? <CircularProgress size={20} color={"#fff"} /> : null
            }
          >
            Cancel Trip
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  tripData: PropTypes.object.isRequired,
  setAlert: PropTypes.object.isRequired,
  getTrip: PropTypes.object.isRequired,
};

const CancelTrip = (props) => {
  return (
    <div>
      <SimpleDialog
        open={props.open}
        onClose={props.handleClose}
        tripData={props.tripData}
        setAlert={props.setAlert}
        getTrip={props.getTrip}
      />
    </div>
  );
};

export default CancelTrip;
