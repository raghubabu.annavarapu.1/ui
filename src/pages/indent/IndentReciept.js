import React, { useState, useEffect, useCallback } from "react";
import PropTypes from "prop-types";
import Dialog from "@mui/material/Dialog";
import { Card, makeStyles } from "@material-ui/core";
import { DialogContent } from "@material-ui/core";
import {
  Divider,
  Typography,
  Button,
  List,
  ListItem,
  Grid,
} from "@material-ui/core";
import RemoveRedEyeOutlinedIcon from "@mui/icons-material/RemoveRedEyeOutlined";
import PersonRoundedIcon from "@mui/icons-material/PersonRounded";
import LocalPhoneRoundedIcon from "@mui/icons-material/LocalPhoneRounded";
import DownloadIcon from "@mui/icons-material/Download";
import LocalPrintshopIcon from "@mui/icons-material/LocalPrintshop";
import domtoimage from "dom-to-image";
import CloseIcon from "@mui/icons-material/Close";
import ReactToPrint from "react-to-print";
import CircularLoading from "../../components/loader/circularLoading";
import jsPDF from "jspdf";
import "../../css/indent_receipt.css";
import * as html2canvas from "html2canvas";
const useStyles = makeStyles((theme) => ({
  main_card: {
    backgroundColor: "#F8F8F8",
    padding: "10px",
  },
  text_line: {
    fontWeight: "bold",
  },
  download_btn: {
    textTransform: "none",
    backgroundColor: "#3E9828",
    align: "center",
    marginTop: "30px",
    "&:hover": {
      background: "#3E9828",
    },
    marginRight: theme.spacing(2),
  },
  print_btn: {
    textTransform: "none",
    backgroundColor: "#F35C47",
    align: "center",
    marginTop: "30px",
    "&:hover": {
      background: "#F35C47",
    },
    marginRight: theme.spacing(2),
  },
  indent: {
    height: "70vh",
    backgroundColor: "#fff",
    "&::before": {
      contet: "",
      position: "absolute",
      width: "100%",
      height: "10px",
      display: "block",
      backgroundSize: " 20px 40px",
    },
  },
  icon: {
    width: "25px",
    height: "20px",
    color: "#3D7396",
    justifyContent: "center",
    marginRight: "5px",
  },
  name: {
    fontSize: "12px",
    fontWeight: "bold",
  },
  text: {
    color: "#8E8E8E",
    padding: "10px",
    fontSize: "12px",
    marginRight: "15px",
  },
  amount: {
    fontWeight: "bold",
  },
  total_indent: {
    fontWeight: "bold",
    fontSize: "15px",
  },
  total_amt: {
    fontWeight: "bold",
    fontSize: "15px",
  },
  data: {
    fontSize: "14px",
    fontWeight: "bold",
  },
  indent_id: {
    display: "flex",
    fontWeight: "bold",
    marginTop: "10px",
  },
}));
function SimpleDialog(props) {
  const classes = useStyles();
  const [receiptWidth, setReceiptWidth] = useState("670px");
  const { onClose, selectedValue, open, useEffect, indentId, indentData } =
    props;
  const handleClose = () => {
    onClose(selectedValue);
  };
  const reactToPrintTrigger = useCallback(() => {
    return (
      <Button
        variant="contained"
        color="primary"
        className={classes.print_btn}
        startIcon={<LocalPrintshopIcon />}
      >
        Print Indent
      </Button>
    );
  }, []);
  const handleBeforePrint = () => {
    handleClose();
  };
  let printRef;
  const downloadPdf = () => {
    html2canvas(document.querySelector("#indent_receipt")).then((canvas) => {
      const imgData = canvas.toDataURL("image/png");
      const pdf = new jsPDF({
        orientation: "portrait",
        size: "A4",
        unit: "pt",
      });
      const imgProps = pdf.getImageProperties(imgData);
      const pdfWidth = pdf.internal.pageSize.getWidth();
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      pdf.addImage(imgData, "PNG", 0, 15, pdfWidth - 15, pdfHeight);
      pdf.save(`${indentData.id}.pdf`);
    });
  };
  return (
    <Dialog open={open} onClose={handleClose} maxWidth={"700px"}>
      {" "}
      <CloseIcon
        onClick={handleClose}
        cursor="pointer"
        style={{ alignSelf: "flex-end", margin: "5px", color: "red" }}
      />
      {indentData ? (
        <>
          <div style={{ alignSelf: "center", width: receiptWidth }}>
            <DialogContent
              className={classes.indent}
              id="indent"
              style={{ width: receiptWidth }}
            >
              <div id="indent_receipt">
                <div
                  className={"receipt_container"}
                  ref={(el) => (printRef = el)}
                >
                  <div className={"receipt_header"}>
                    <b style={{ textTransform: "capitalize" }}>
                      {indentData ? indentData.company.companyName : ""}
                    </b>
                    <span className={"addr"}>
                      {indentData ? indentData.address : ""}
                    </span>
                  </div>
                  <div className={"memo"}>
                    <b style={{ paddingBottom: "5px" }}>CREDIT MEMO</b>
                    <Grid container spacing={2}>
                      <Grid item xs={3}>
                        <b
                          style={{
                            textAlign: "left",
                            padding: "0px 5px 0px 12px",
                          }}
                        >
                          Indent No.
                        </b>
                      </Grid>
                      <Grid item xs={3}>
                        {indentData ? indentData.id : ""}
                      </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                      <Grid item xs={3}>
                        <b
                          style={{
                            textAlign: "left",
                            padding: "0px 5px 0px 12px",
                          }}
                        >
                          LR No./THC No.
                        </b>
                      </Grid>
                      <Grid item xs={3}>
                        {indentData && indentData.trip
                          ? indentData.trip.lr
                          : ""}
                      </Grid>
                      <Grid item xs={2}>
                        <b
                          style={{
                            textAlign: "left",
                            padding: "0px 5px 0px 12px",
                          }}
                        >
                          Date
                        </b>
                      </Grid>
                      <Grid item xs={4}>
                        {indentData ? indentData.indentDateRead : ""}
                      </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                      <Grid item xs={3}>
                        <b
                          style={{
                            textAlign: "left",
                            padding: "0px 5px 0px 12px",
                          }}
                        >
                          Vendor Company
                        </b>
                      </Grid>
                      <Grid item xs={3}>
                        {indentData && indentData.vendor
                          ? indentData.vendor.companyName
                          : ""}
                      </Grid>
                      <Grid item xs={2}>
                        <b
                          style={{
                            textAlign: "left",
                            padding: "0px 5px 0px 12px",
                          }}
                        >
                          V / No
                        </b>
                      </Grid>
                      <Grid item xs={4}>
                        {indentData ? indentData.truck.registrationNumber : ""}
                      </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                      <Grid item xs={3}>
                        <b
                          style={{
                            textAlign: "left",
                            padding: "0px 5px 0px 12px",
                          }}
                        >
                          Contract Name
                        </b>
                      </Grid>
                      <Grid item xs={3}>
                        {indentData && indentData.contract
                          ? indentData.contract.customerContractReferenceNumber
                          : ""}
                      </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                      <Grid item xs={3}>
                        <b
                          style={{
                            textAlign: "left",
                            padding: "0px 5px 0px 12px",
                          }}
                        >
                          C / Name
                        </b>
                      </Grid>
                      <Grid item xs={3}>
                        {indentData && indentData.clientComapnyName
                          ? indentData.clientComapnyName
                          : ""}
                      </Grid>
                    </Grid>
                    <Grid container spacing={2} style={{}}>
                      <Grid item xs={3}>
                        <b
                          style={{
                            textAlign: "left",
                            padding: "0px 5px 0px 12px",
                          }}
                        >
                          Route Name
                        </b>
                      </Grid>
                      <Grid item xs={4}>
                        {indentData && indentData.trip
                          ? indentData.trip.routeName
                          : ""}
                      </Grid>
                    </Grid>
                    <table>
                      <tr>
                        <th style={{ padding: "5px" }} className={"table_head"}>
                          Particulars
                        </th>
                        <th style={{ padding: "5px" }} className={"table_head"}>
                          Quantity
                        </th>
                        <th style={{ padding: "5px" }} className={"table_head"}>
                          Price
                        </th>
                        <th
                          style={{ padding: "5px" }}
                          className={"table_head_last"}
                        >
                          Amount
                        </th>
                      </tr>
                      {indentData &&
                      indentData.items &&
                      indentData.items.length > 0
                        ? indentData.items.map((indent) => (
                            <tr>
                              <td
                                style={{
                                  textAlign: "center",
                                  padding: "5px 8px",
                                }}
                                className={"table_data"}
                              >
                                {indent.indentType.name}
                              </td>
                              <td
                                style={{
                                  textAlign: "right",
                                  padding: "5px 8px",
                                }}
                                className={"table_data"}
                              >
                                {parseFloat(indent.quantity).toFixed(2)}
                              </td>
                              <td
                                style={{
                                  textAlign: "right",
                                  padding: "5px 8px",
                                }}
                                className={"table_data"}
                              >
                                {parseFloat(indent.unitQuantity).toFixed(2)}
                              </td>
                              <td
                                style={{
                                  textAlign: "right",
                                  padding: "5px 8px",
                                }}
                                className={"table_data_last"}
                              >
                                {parseFloat(indent.total).toFixed(2)}
                              </td>
                            </tr>
                          ))
                        : null}
                      <tr>
                        <td
                          colSpan={3}
                          style={{ borderRight: "2px solid black" }}
                        >
                          <b style={{ textAlign: "right", padding: "5px 8px" }}>
                            Total
                          </b>
                        </td>
                        <td style={{ textAlign: "right", padding: "5px 8px" }}>
                          {indentData
                            ? parseFloat(indentData.indentTotal).toFixed(2)
                            : ""}
                        </td>
                      </tr>
                    </table>
                    <div className={"receipt_footer"}>
                      <Grid container spacing={2}>
                        <Grid item xs={6}></Grid>
                        <Grid item xs={6} style={{ textAlign: "center" }}>
                          For{" "}
                          <span
                            style={{ fontWeight: "bold", fontSize: "16px" }}
                          >
                            {indentData ? indentData.company.companyName : ""}
                          </span>
                        </Grid>
                      </Grid>
                      <Grid container spacing={2}>
                        <Grid item xs={6} style={{ fontWeight: "bold" }}>
                          E. & O. E.
                        </Grid>
                        <Grid item xs={6} style={{ textAlign: "center" }}>
                          Signature
                        </Grid>
                      </Grid>
                    </div>
                  </div>
                </div>
              </div>
              <div style={{ margin: "15px 20px" }}>
                <p>
                  <b>Created by :</b>{" "}
                  {indentData &&
                  indentData.createdBy &&
                  indentData.createdBy.name
                    ? indentData.createdBy.name
                    : ""}
                </p>
                <p>
                  <b>Requested by :</b>{" "}
                  {indentData &&
                  indentData.requestedBy &&
                  indentData.requestedBy.name
                    ? indentData.requestedBy.name
                    : ""}
                </p>
                <p>
                  <b>Approved by :</b>{" "}
                  {indentData &&
                  indentData.approvedBy &&
                  indentData.approvedBy.roleName
                    ? indentData.approvedBy.roleName
                    : ""}
                </p>
              </div>
            </DialogContent>
          </div>
        </>
      ) : (
        <>
          <CircularLoading />
        </>
      )}
      <Typography align="center" style={{ marginBottom: "20px" }}>
        <Button
          variant="contained"
          color="primary"
          className={classes.download_btn}
          onClick={() => {
            downloadPdf();
          }}
          startIcon={<DownloadIcon />}
        >
          Download
        </Button>
        <ReactToPrint
          content={() => printRef}
          trigger={reactToPrintTrigger}
          onBeforePrint={handleBeforePrint}
        />
      </Typography>
    </Dialog>
  );
}
SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.string.isRequired,
  indentId: PropTypes.string.isRequired,
  useEffect: PropTypes.func.isRequired,
  indentData: PropTypes.func.isRequired,
};
export default function SimpleDialogDemo(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [data, setData] = useState([]);
  const [spinner, setSpinner] = useState(false);
  const [loader, setLoader] = useState(false);
  const [indentData, setIndentData] = useState();
  const [selectedValue, setSelectedValue] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
    Service.getIndent(dataObj)
      .then((res) => {
        setIndentData(res);
        setLoader(false);
      })
      .catch((e) => {
        setSpinner(false);
        setLoader(false);
      });
  };
  const handleClose = (value) => {
    setOpen(false);
    setSelectedValue(value);
  };
  var Service;
  Service = global.service;
  let dataObj = {
    indentId: props.indentId,
  };
  return (
    <div>
      <div onClick={handleClickOpen}>
        <RemoveRedEyeOutlinedIcon
          style={{ color: "#1269A8", cursor: "pointer" }}
        />
      </div>
      <SimpleDialog
        selectedValue={selectedValue}
        open={open}
        onClose={handleClose}
        indentId={props.indentId}
        useEffect={useEffect}
        indentData={indentData}
      />
    </div>
  );
}
