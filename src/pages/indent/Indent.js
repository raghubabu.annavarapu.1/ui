import React, { useState, useEffect } from "react";
import Header from "../../components/header";
import "../../common.css";
import { Typography, Button, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import AddCircleRoundedIcon from "@mui/icons-material/AddCircleRounded";
import * as Fields from "../../sharedComponents";
import { useHistory } from "react-router-dom";
import CircularLoading from "../../components/loader/circularLoading";
import _ from "lodash";
import * as Components from "../../sharedComponents";
import AlertMessage from "../../components/alertmessage/AlertMessage";
import * as CONFIG from "../../config/GlobalConstants";
import RemoveRedEyeOutlinedIcon from "@mui/icons-material/RemoveRedEyeOutlined";
import Chip from "@mui/material/Chip";
import IndentReciept from "./IndentReciept";
const headRows = [
  // { id: "S_NO", disablePadding: true, label: "S.NO" },
  { id: "INDENT_ID", disablePadding: true, label: "INDENT#" },
  { id: "VEHICLE_NUMBER", disablePadding: true, label: "VEHICLE#" },
  { id: "LR_NUMBER", disablePadding: true, label: "LR#" },
  {
    id: "VENDOR_INFORMATION",
    disablePadding: true,
    label: "VENDOR INFORMATION",
  },
  { id: "CREATED BY", disablePadding: true, label: "CREATED BY" },
  { id: "CREATED DATE", disablePadding: true, label: "CREATED DATE" },
  { id: "STATUS", disablePadding: true, label: "STATUS" },
  { id: "AMOUNT", disablePadding: true, label: "AMOUNT(RS)" },
  { id: "ACTIONS", disablePadding: true, label: "ACTIONS" },
];
const useStyles = makeStyles((theme) => ({
  addindent_btn: {
    textTransform: "none",
    backgroundColor: "#649B42",
    "&:hover": {
      backgroundColor: "#649B42",
    },
    padding: "8px 15px",
    marginRight: "12px",
  },
  update_btn: {
    textTransform: "none",
    backgroundColor: "#649B42",
    "&:hover": {
      backgroundColor: "#649B42",
    },
    marginRight: "15px",
  },
  cancel_btn: {
    textTransform: "none",
    backgroundColor: "#E15656",
    "&:hover": {
      backgroundColor: "#E15656",
    },
  },
  select: {
    width: "180px",
    marginRight: "16px",
  },
  header_box: {
    display: "flex",
    padding: "10px 24px 20px 24px",
    background: "#ffff",
  },
  text: {
    fontSize: "14px",
    fontWeight: "bold",
    color: "#366E93",
    marginTop: "25px",
    marginRight: "15px",
    alignSelf: "center",
  },
  table_data: {
    fontSize: "14px",
  },
}));
var Service;
const Indent = () => {
  let filterFields = {
    truckId: {
      name: "truckId",
      label: "Vehicles",
      value: { label: "", value: "" },
      options: [],
      validationRequired: false,
      isValid: true,
      errorMsg: "Please select vehicle",
      allowClear: true,
      topLabel: true
    },
    status: {
      name: "status",
      label: "Status",
      value: { label: "", value: "" },
      options: CONFIG.INDENT_STATUS.filter((ind) => ind.value === "2"),
      validationRequired: false,
      isValid: true,
      defaultValue: "All",
      allowClear: true,
      topLabel: true
    },
  };
  const [filters, setFilters] = useState(_.cloneDeep(filterFields));
  const classes = useStyles();
  const history = useHistory();
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  useEffect(() => {
    window.scrollTo({ top: 0 });
    let dataObj = {
      skip: pagination.current * pagination.pageSize,
      limit: pagination.pageSize,
    };
    renderIndents(dataObj);
  }, []);
  const [tableData, setTableData] = useState([]);
  const [spinner, setSpinner] = useState(false);
  const [total, setTotal] = useState(0);
  const [indentIsOpen, setIndentIsOpen] = useState(false);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  var i = 1;
  function handleClickOpen() {
    setIndentIsOpen(false);
  }
  const renderIndents = (dataObj) => {
    setSpinner(true);
    Service.getIndents(dataObj)
      .then((res) => {
        let data = res.indents.map((indents) => {
          return {
            INDENT_ID: (
              <div className={classes.table_data}>
                {indents.id ? indents.id : "--"}
              </div>
            ),
            VEHICLE_NUMBER: (
              <div>
                <div className={classes.table_data}>
                  {indents.truck.registrationNumber
                    ? indents.truck.registrationNumber
                    : "--"}
                </div>
              </div>
            ),
            LR_NUMBER: (
              <div className={classes.table_data}>
                {indents.trip && indents.trip.lr ? indents.trip.lr : "--"}
              </div>
            ),
            VENDOR_INFORMATION: (
              <div className={classes.table_data}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <span>{indents.vendor && indents.vendor.companyName ? indents.vendor.companyName : "--"}</span>
                  <span style={{ fontWeight: "bold" }}>
                    {indents.vendor && indents.vendor.mobileNumber ? indents.vendor.mobileNumber : "--"}
                  </span>
                </div>
              </div>
            ),
            CREATED_BY: (
              <div className={classes.table_data}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <span>
                    {indents.createdBy.name ? indents.createdBy.name : "--"}
                  </span>
                  <span style={{ fontWeight: "bold" }}>
                    {indents.createdBy.roleName
                      ? indents.createdBy.roleName
                      : "--"}
                  </span>
                </div>
              </div>
            ),
            CREATED_DATE: (
              <div className={classes.table_data}>
                {indents.createdAtRead ? indents.createdAtRead : "--"}
              </div>
            ),
            STATUS: (
              <div>
                <Chip
                  style={{
                    background: `${CONFIG.INDENT_STATUS.filter(
                      (tr) => parseInt(tr.value) === indents.status
                    )[0].color
                      }`,
                    color: "#ffff",
                  }}
                  label={
                    CONFIG.INDENT_STATUS.filter(
                      (tr) => parseInt(tr.value) === indents.status
                    )[0].label
                  }
                />
              </div>
            ),
            AMOUNT: (
              <div className={classes.table_data}>
                {indents.indentTotal ? indents.indentTotal.toFixed(2) : "--"}
              </div>
            ),
            ACTIONS: (
              <div className={classes.table_data}>
                {/* <RemoveRedEyeOutlinedIcon style={{ color: "#1269A8", cursor: "pointer" }} /> */}
                <IndentReciept
                  indentId={indents.id}
                  handleClickOpen={handleClickOpen}
                />
              </div>
            ),
          };
        });
        setTotal(res.totalCount);
        setTableData(data);
        setSpinner(false);
      })
      .catch((error) => {
        setSpinner(false);
      });
  };
  useEffect(() => {
    let dataObj = { allVehicles: true };
    const promise1 = Service.getVehicles(dataObj);
    Promise.all([promise1])
      .then((res) => {
        let newFilters = _.cloneDeep(filters);
        let vehicleOptions = res[0].vehicles.map((veh) => {
          return { label: veh.registrationNumber, value: veh.id };
        });
        newFilters["truckId"]["options"] = vehicleOptions;
        setFilters(newFilters);
      })
      .catch(() => { });
  }, []);
  const autoCompleteChangeHandler = (value, name) => {
    let newFilters = _.cloneDeep(filters);
    let dataObj;
    if (value) {
      newFilters[name]["value"] = value;
      setFilters(newFilters);
      setPagination({ ...pagination, current: 0 });
      dataObj = {
        skip: pagination.current * pagination.pageSize,
        limit: pagination.pageSize,
        truckId:
          name === "truckId"
            ? value.value
            : newFilters.truckId.value.value === undefined
              ? newFilters.truckId.value
              : newFilters.truckId.value.value,
        status:
          name === "status"
            ? value.value
            : newFilters.status.value.value === undefined
              ? newFilters.status.value
              : newFilters.status.value.value,
      };
    } else {
      newFilters[name]["value"] = { label: "", value: "" };
      setFilters(newFilters);
      dataObj = {
        skip: pagination.current * pagination.pageSize,
        limit: pagination.pageSize,
        truckId:
          newFilters.truckId.value.value === undefined
            ? newFilters.truckId.value
            : newFilters.truckId.value.value,
        status:
          newFilters.status.value.value === undefined
            ? newFilters.status.value
            : newFilters.status.value.value,
      };
    }
    renderIndents(dataObj);
  };
  const pageChangeHandler = (page) => {
    let fil = Object.values(filters).filter((fil) => fil.value);
    let dataObj;
    if (fil.length > 0) {
      let filters = fil.map((fil) => {
        switch (fil.name) {
          case "truckId":
            return { truckId: fil.value.value };
          case "status":
            return { status: fil.value.value };
          default:
            return;
        }
      });
      filters = Object.assign({}, ...filters);
      dataObj = {
        skip: page * pagination.pageSize,
        limit: pagination.pageSize,
        ...filters,
      };
    } else {
      dataObj = {
        skip: page * pagination.pageSize,
        limit: pagination.pageSize,
      };
    }
    renderIndents(dataObj);
    setPagination({ ...pagination, current: page });
  };
  const rowsPerPageChangeHandler = (rowsPerPage) => {
    let fil = Object.values(filters).filter((fil) => fil.value);
    let dataObj;
    if (fil.length > 0) {
      let filters = fil.map((fil) => {
        switch (fil.name) {
          case "truckId":
            return { truckId: fil.value.value };
          case "status":
            return { status: fil.value.value };
          default:
            return;
        }
      });
      filters = Object.assign({}, ...filters);
      dataObj = {
        skip: pagination.current * rowsPerPage,
        limit: rowsPerPage,
        ...filters,
      };
    } else {
      dataObj = {
        skip: pagination.current * rowsPerPage,
        limit: rowsPerPage,
      };
    }
    renderIndents(dataObj);
    setPagination({ ...pagination, pageSize: rowsPerPage });
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  Service = global.service;
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">Indents</Typography>
          <div className="header_buttons">
            <Button
              variant="contained"
              className={classes.addindent_btn}
              color="primary"
              startIcon={<AddCircleRoundedIcon />}
              onClick={() => {
                history.push("./addIndent");
              }}
            >
              Add Indents
            </Button>
          </div>
        </div>
        <div className={classes.header_box}>
          <Typography className={classes.text}>Filter by</Typography>
          <div className={classes.select}>
            <Fields.AntSelectableSearchField
              fieldData={filters.truckId}
              autoCompleteChangeHandler={autoCompleteChangeHandler}
            />
          </div>
          <div className={classes.select}>
            <Fields.AntSelectableSearchField
              fieldData={filters.status}
              autoCompleteChangeHandler={autoCompleteChangeHandler}
            />
          </div>
        </div>
        <div>
          {alertData.open ? (
            <div className={classes.alertBox}>
              <AlertMessage
                severity={alertData.severity}
                message={alertData.message}
                closeAlert={closeAlert}
              />
            </div>
          ) : null}
          <div>
            {/* {spinner ? (
                            <CircularLoading />
                        ) : ( */}
            {spinner ? (
              <CircularLoading />
            ) : (
              <Components.DataTable
                headRows={headRows}
                tableData={tableData}
                pagination={pagination}
                total={total}
                pageChangeHandler={pageChangeHandler}
                rowsPerPageChangeHandler={rowsPerPageChangeHandler}
              />
            )}
            {/* )} */}
          </div>
        </div>
      </div>
    </div>
  );
};
export default Indent;
