import { CircularProgress, Typography } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import "../../common.css";
import Header from "../../components/header";
import { useHistory } from "react-router-dom";
import { makeStyles, Button, Grid } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import ListIcon from "@mui/icons-material/List";
import _ from "lodash";
import * as Fields from "../../sharedComponents";
import DynamicIndents from "./DynamicIndents";
import ValidateFields from "../../validations/validateFields";
import AlertMessage from "../../components/alertmessage/AlertMessage";
import * as CONFIG from "../../config/GlobalConstants";
import { truckImage } from "../../assets";
var Service;
const useStyles = makeStyles((theme) => ({
  addindent_btn: {
    textTransform: "none",
    backgroundColor: "#1269A8",
    "&:hover": {
      backgroundColor: "#1269A8",
    },
    padding: "8px 15px",
    marginRight: "12px",
  },
  alertBox: {
    padding: "10px 0px",
  },
  header_top: {
    display: "flex",
    justifyContent: "space-between",
  },
  indent_table_content: {
    padding: "20px 24px",
  },
}));
const AddIndent = (props) => {
  const history = useHistory();
  const classes = useStyles();
  Service = global.service;
  let expenses = [
    {
      indentTypeId: {
        name: "indentTypeId",
        label: "Service Type",
        value: { label: "", value: "" },
        options: [],
        validationRequired: true,
        isValid: true,
        errorMsg: "Please select service type",
        validPattern: "NUMERIC",
        placeholder: "Service Type",
      },
      quantity: {
        name: "quantity",
        label: "Quantity",
        value: "",
        type: "number",
        min: 0,
        validationRequired: true,
        isValid: true,
        errorMsg: "Please enter quantity",
        validPattern: "FLOAT",
      },
      unitQuantity: {
        name: "unitQuantity",
        label: "Unit Price",
        value: "",
        type: "number",
        min: 0,
        validationRequired: true,
        isValid: true,
        errorMsg: "Please enter unit price",
        validPattern: "FLOAT",
      },
      total: {
        name: "total",
        label: "Total Price",
        value: "",
        type: "number",
        min: 0,
        readOnly: true,
        validationRequired: true,
        isValid: true,
        errorMsg: "Please enter total price",
        validPattern: "FLOAT",
      },
      indentRemarks: {
        name: "indentRemarks",
        label: "Remarks",
        value: "",
      },
    },
  ];
  let formFields = {
    truckId: {
      name: "truckId",
      label: "Vehicle Number",
      value: { label: "", value: "" },
      options: [],
      validationRequired: true,
      isValid: true,
      errorMsg: "Please select vehicle",
      validPattern: "NUMERIC",
      topLabel: true,
    },
    tripId: {
      name: "tripId",
      label: "Trip Id",
      value: "",
      topLabel: true,
    },
    vendorId: {
      name: "vendorId",
      label: "Vendor Company Name",
      value: { label: "", value: "" },
      options: [],
      validationRequired: true,
      validPattern: "NUMERIC",
      isValid: true,
      errorMsg: "Please select vendor company name",
      topLabel: true,
    },
    contractId: {
      name: "contractId",
      label: "Vendor Contract",
      value: { label: "", value: "" },
      options: [],
      topLabel: true,
    },
    vendorMobile: {
      name: "vendorMobile",
      label: "Vendor Contact Number",
      value: "",
      isValid: true,
      errorMsg: "Please enter valid mobile number",
      maxLength: 10,
      topLabel: true,
    },
    indentAlias: {
      name: "indentAlias",
      label: "Indent Alias Code",
      value: "",
      topLabel: true,
    },
    indentDate: {
      name: "indentDate",
      label: "Indent Creation Date",
      value: new Date(),
      validationRequired: true,
      isValid: true,
      errorMsg: "Please enter date",
      validPattern: "DATE",
      topLabel: true,
    },
    requestedUserId: {
      name: "requestedUserId",
      label: "Indent Requested By",
      value: { label: "", value: "" },
      options: [],
      validationRequired: true,
      isValid: true,
      errorMsg: "Please select user",
      validPattern: "NUMERIC",
      topLabel: true,
    },
    requestedUserRoleId: {
      name: "requestedUserRoleId",
      label: "Role",
      value: { label: "", value: "" },
      options: [],
      validationRequired: true,
      isValid: true,
      errorMsg: "Please select role",
      validPattern: "NUMERIC",
      topLabel: true,
    },
    comments: {
      name: "comments",
      label: "Additonal Notes",
      value: "",
      topLabel: "true",
    },
  };
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const [indents, setIndents] = useState([...expenses]);
  const [indentOptions, setIndentOptions] = useState([]);
  const [spinner, setSpinner] = useState(false);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  let validateFields = new ValidateFields();
  useEffect(() => {
    window.scrollTo({ top: 0 });
    renderFieldOptions();
  }, []);
  const renderFieldOptions = () => {
    let profile = JSON.parse(sessionStorage.getItem("profile"));
    let dataObj = { allVehicles: true };
    const promise1 = Service.getVehicles(dataObj);
    const promise2 = Service.getRoles();
    const promise3 = Service.getIndentTypes();
    const promise4 = Service.getVendors({ onlyMeta: true });
    const promise5 = Service.getRole({ roleId: Number(profile.role_id) });
    let promise6 = new Promise((resolve, reject) => {});
    if (props.location.state && props.location.state.vendorId) {
      promise6 = Service.getContracts({
        onlyMeta: true,
        vendorId: props.location.state.vendorId,
      });
    }
    if(props.location.state && props.location.state.truckId) {
      promise6 = Service.getLatestTrip({ vehicleId: Number(props.location.state.truckId) })
    }
    Promise.all(
      props.location.state && (props.location.state.vendorId || props.location.state.truckId)
        ? [promise1, promise2, promise3, promise4, promise5, promise6]
        : [promise1, promise2, promise3, promise4, promise5]
    )
      .then((res) => {
        let newFields = _.cloneDeep(formFields);
        let newIndents = [...expenses];
        let vehicleOptions = res[0].vehicles.map((veh) => {
          return { label: veh.registrationNumber, value: veh.id };
        });
        let roleOptions = res[1].roles.map((role) => {
          return { label: role.roleName, value: role.id };
        });
        let serviceTypeOptions = res[2].types.map((type) => {
          return { label: type.name, value: type.id };
        });
        let vendorOptions = res[3].vendors.map((ven) => {
          return { label: ven.companyName, value: ven.id };
        });
        let userOptions = res[4].users.map((user) => {
          return { label: user.name, value: user.id };
        });
        newFields["truckId"]["options"] = vehicleOptions;
        if (props.location.state && props.location.state.truckId) {
          newFields["truckId"]["value"] = vehicleOptions.filter(
            (item) =>
              Number(item.value) === Number(props.location.state.truckId)
          )[0];
        }
        newFields["requestedUserRoleId"]["options"] = roleOptions;
        newFields["requestedUserRoleId"]["value"] = roleOptions.filter(
          (item) => Number(item.value) === Number(profile.role_id)
        )[0];
        newFields["requestedUserId"]["options"] = userOptions;
        newFields["requestedUserId"]["value"] = userOptions.filter(
          (item) => item.label === profile.name
        )[0];
        newIndents[0]["indentTypeId"]["options"] = serviceTypeOptions;
        newFields["vendorId"]["options"] = vendorOptions;
        if (props.location.state && props.location.state.vendorId) {
          newFields["vendorId"]["value"] = vendorOptions.filter(
            (item) =>
              Number(item.value) === Number(props.location.state.vendorId)
          )[0];
          newFields["contractId"]["options"] = res[5].contracts.map((item) => {
            return {
              label: item.customerContractReferenceNumber,
              value: item.id,
            };
          });
        }
        if(props.location.state && props.location.state.truckId){
          newFields["tripId"]["value"] = res[5].tripId ? res[5].tripId : "";
        }
        setIndentOptions([...serviceTypeOptions]);
        setFields(newFields);
        setIndents(newIndents);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const autoCompleteChangeHandler = (value, name) => {
    if (value) {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = value;
      newFields[name]["isValid"] = true;
      if (name === "requestedUserRoleId") {
        Service.getRole({ roleId: Number(value.value) })
          .then((res) => {
            newFields["requestedUserId"]["options"] = res.users.map((user) => {
              return { label: user.name, value: user.id };
            });
            newFields["requestedUserId"]["value"] = { label: "", value: "" };
            setFields(newFields);
          })
          .catch(() => {});
      } else if (name === "vendorId") {
        Service.getContracts({ onlyMeta: true, vendorId: value.value })
          .then((res) => {
            newFields["contractId"]["options"] = res.contracts.map((user) => {
              return {
                label: user.customerContractReferenceNumber,
                value: user.id,
              };
            });
            newFields["contractId"]["value"] = { label: "", value: "" };
            setFields(newFields);
          })
          .catch(() => {});
      } else if (name === "truckId") {
        Service.getLatestTrip({ vehicleId: Number(value.value) })
          .then((res) => {
            newFields["tripId"]["value"] = res.tripId ? res.tripId : "";
            setFields(newFields);
          })
          .catch((error) => {});
      } else {
        setFields(newFields);
      }
    } else {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = { value: "", label: "" };
      if (name === "requestedUserRoleId") {
        newFields["requestedUserId"]["value"] = { value: "", label: "" };
        if (newFields["requestedUserId"]["validationRequired"]) {
          newFields["requestedUserId"]["isValid"] = false;
        }
      }
      if (newFields[name]["validationRequired"]) {
        newFields[name]["isValid"] = false;
      }
      setFields(newFields);
    }
  };
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    if (value.length === 0 && newFields[name]["validationRequired"]) {
      newFields[name]["isValid"] = false;
    }
    if(name === "tripId" && value.length > 0){
      let re = /^[0-9]+$/;
      if(re.test(value)){
        newFields[name]["value"] = value;
        newFields[name]["isValid"] = true;
        setFields(newFields);
      }else{
        return;
      }
    }else{
      newFields[name]["value"] = value;
      newFields[name]["isValid"] = true;
      setFields(newFields);
    }
  };
  const timeStampChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    setFields(newFields);
  };
  const addMoreHandler = () => {
    let newIndents = [...indents];
    newIndents.push(expenses[0]);
    newIndents[newIndents.length - 1]["indentTypeId"]["options"] =
      indentOptions;
    setIndents(newIndents);
  };
  const removeHandler = (i) => {
    let newIndents = [...indents];
    newIndents.splice(i, 1);
    setIndents(newIndents);
  };
  const indentInputChangeHandler = (value, name, i) => {
    let newIndents = _.cloneDeep(indents);
    newIndents[i][name]["value"] = value;
    if (name === "quantity" || name === "unitQuantity") {
      if (
        newIndents[i]["quantity"]["value"].length > 0 &&
        newIndents[i]["unitQuantity"]["value"].length > 0
      ) {
        newIndents[i]["total"]["value"] =
          parseFloat(newIndents[i]["quantity"]["value"]) *
          parseFloat(newIndents[i]["unitQuantity"]["value"]);
      }
    }
    setIndents(newIndents);
  };
  const indentAutocompleteChangeHandler = (value, name, i) => {
    if (value) {
      let newIndents = _.cloneDeep(indents);
      newIndents[i][name]["value"] = value;
      newIndents[i][name]["isValid"] = true;
      setIndents(newIndents);
    } else {
      let newIndents = _.cloneDeep(indents);
      newIndents[i][name]["value"] = { label: "", value: "" };
      if (newIndents[i][name]["validationRequired"]) {
        newIndents[i][name]["isValid"] = false;
      }
      setIndents(newIndents);
    }
  };
  const checkAllFieldsEmpty = (obj) => {
    var status = true;
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        var val = obj[key]["value"] ? obj[key]["value"] : obj[key];
        if (val.value !== "") {
          status = false;
        }
      }
    }
    return status;
  };
  const getNonEmptyRows = () => {
    const indentFields = [...indents];
    const indentInputs = indentFields.filter((ind) => {
      if (!checkAllFieldsEmpty(ind)) {
        return ind;
      }
    });
    return indentInputs;
  };
  const validateIndents = () => {
    const nonEmptyRows = getNonEmptyRows();
    var valid = true;
    const indentInputs = nonEmptyRows.map((row) => {
      let dataObj = {
        ...row,
        indentTypeId: {
          ...row.indentTypeId,
          value:
            row.indentTypeId.value.value === undefined
              ? row.indentTypeId.value
              : row.indentTypeId.value.value,
        },
      };
      const dataStatus = validateFields.validateFieldsData(dataObj);
      valid = !dataStatus.status ? dataStatus.status : valid;
      let field = { ...row };
      return {
        ...dataStatus.data,
        indentTypeId: {
          ...dataStatus.data.indentTypeId,
          value: dataStatus.data.indentTypeId.value
            ? field.indentTypeId.options.filter(
                (opt) => opt.value === dataStatus.data.indentTypeId.value
              )[0]
            : { label: "", value: "" },
        },
      };
    });
    let newIndents = [...indents];
    newIndents = indentInputs;
    setIndents(newIndents);
    if (newIndents.length === 0) {
      return { status: false };
    }
    return { status: valid };
  };
  const addIndentHandler = () => {
    let dataObj = {
      ...fields,
      truckId: {
        ...fields.truckId,
        value:
          fields.truckId.value.value === undefined
            ? fields.truckId.value
            : fields.truckId.value.value,
      },
      requestedUserRoleId: {
        ...fields.requestedUserRoleId,
        value:
          fields.requestedUserRoleId.value.value === undefined
            ? fields.requestedUserRoleId.value
            : fields.requestedUserRoleId.value.value,
      },
      requestedUserId: {
        ...fields.requestedUserId,
        value:
          fields.requestedUserId.value.value === undefined
            ? fields.requestedUserId.value
            : fields.requestedUserId.value.value,
      },
      vendorId: {
        ...fields.vendorId,
        value:
          fields.vendorId.value.value === undefined
            ? fields.vendorId.value
            : fields.vendorId.value.value,
      },
      contractId: {
        ...fields.contractId,
        value:
          fields.contractId.value.value === undefined
            ? fields.contractId.value
            : fields.contractId.value.value,
      },
    };
    let dataStatus = validateFields.validateFieldsData(dataObj);
    const validIndents = validateIndents();
    if (dataStatus.status && validIndents.status) {
      setSpinner(true); 
      Service.createIndent(dataObj, indents)
        .then((res) => {
          setSpinner(false);
          let newFields = _.cloneDeep(formFields);
          let newIndents = [...expenses];
          setFields(newFields);
          setIndents(newIndents);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.success,
            message: "Indent created successfully!",
          });
          window.scrollTo({ top: 0, behavior: "smooth" });
          renderFieldOptions();
        })
        .catch((error) => {
          setSpinner(false);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
          window.scrollTo({ top: 0, behavior: "smooth" });
        });
    } else {
      let newFields = _.cloneDeep(fields);
      newFields = {
        ...dataStatus.data,
        truckId: {
          ...dataStatus.data.truckId,
          value:
            dataStatus.data.truckId.value || dataStatus.data.truckId.value === 0
              ? newFields.truckId.options.filter(
                  (opt) => opt.value === dataStatus.data.truckId.value
                )[0]
              : { label: "", value: "" },
        },
        requestedUserRoleId: {
          ...dataStatus.data.requestedUserRoleId,
          value:
            dataStatus.data.requestedUserRoleId.value ||
            dataStatus.data.requestedUserRoleId.value === 0
              ? newFields.requestedUserRoleId.options.filter(
                  (opt) =>
                    opt.value === dataStatus.data.requestedUserRoleId.value
                )[0]
              : { label: "", value: "" },
        },
        requestedUserId: {
          ...dataStatus.data.requestedUserId,
          value:
            dataStatus.data.requestedUserId.value ||
            dataStatus.data.requestedUserId.value === 0
              ? newFields.requestedUserId.options.filter(
                  (opt) => opt.value === dataStatus.data.requestedUserId.value
                )[0]
              : { label: "", value: "" },
        },
        vendorId: {
          ...dataStatus.data.vendorId,
          value:
            dataStatus.data.vendorId.value ||
            dataStatus.data.vendorId.value === 0
              ? newFields.vendorId.options.filter(
                  (opt) => opt.value === dataStatus.data.vendorId.value
                )[0]
              : { label: "", value: "" },
        },
        contractId: {
          ...dataStatus.data.contractId,
          value:
            dataStatus.data.contractId.value ||
            dataStatus.data.contractId.value === 0
              ? newFields.contractId.options.filter(
                  (opt) => opt.value === dataStatus.data.contractId.value
                )[0]
              : { label: "", value: "" },
        },
      };
      setFields(newFields);
    }
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.goBack();
              }}
            />
            Add Indent
          </Typography>
        </div>
        <div>
          {alertData.open ? (
            <div className={classes.alertBox}>
              <AlertMessage
                severity={alertData.severity}
                message={alertData.message}
                closeAlert={closeAlert}
              />
            </div>
          ) : null}
        </div>
        <div style={{ padding: "20px 24px" }}>
          <Grid container spacing={3} style={{ marginBottom: "10px" }}>
            <Grid item xs={3}>
              <Fields.AntSelectableSearchField
                fieldData={fields.truckId}
                autoCompleteChangeHandler={autoCompleteChangeHandler}
                disabled={props.location.state && props.location.state.truckId ? true : false}
              />
            </Grid>
            <Grid item xs={3}>
              <Fields.InputField
                fieldData={fields.tripId}
                inputChangeHandler={inputChangeHandler}
                variant="outlined"
                disabled={props.location.state && props.location.state.truckId ? true : false}
              />
            </Grid>
            <Grid item xs={3}>
              <Fields.AntSelectableSearchField
                fieldData={fields.requestedUserRoleId}
                autoCompleteChangeHandler={autoCompleteChangeHandler}
              />
            </Grid>
            <Grid item xs={3}>
              <Fields.AntSelectableSearchField
                fieldData={fields.requestedUserId}
                autoCompleteChangeHandler={autoCompleteChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={3}>
              <Fields.AntSelectableSearchField
                fieldData={fields.vendorId}
                autoCompleteChangeHandler={autoCompleteChangeHandler}
                disabled={props.location.state && props.location.state.vendorId ? true : false}
              />
            </Grid>
            <Grid item xs={3}>
              <Fields.AntSelectableSearchField
                fieldData={fields.contractId}
                autoCompleteChangeHandler={autoCompleteChangeHandler}
              />
            </Grid>
            <Grid item xs={3}>
              <Fields.DatePickerField
                fieldData={fields.indentDate}
                timeStampChangeHandler={timeStampChangeHandler}
                variant="outlined"
              />
            </Grid>
            <Grid item xs={3}>
              <Fields.InputField
                fieldData={fields.indentAlias}
                inputChangeHandler={inputChangeHandler}
                variant="outlined"
              />
            </Grid>
          </Grid>
        </div>
        <div className="custom_datagrid">
          <DynamicIndents
            indents={[...indents]}
            addMoreHandler={addMoreHandler}
            removeHandler={removeHandler}
            indentInputChangeHandler={indentInputChangeHandler}
            indentAutocompleteChangeHandler={indentAutocompleteChangeHandler}
          />
        </div>
        {indents.length === 0 ? (
          <span style={{ margin: "20px", color: "red" }}>
            Please add atleast one service type
          </span>
        ) : null}
        <div className="custom_datagrid">
          <Fields.TextAreaField
            fieldData={fields.comments}
            inputChangeHandler={inputChangeHandler}
          />
        </div>
        <div className="custom_datagrid">
          <Grid container spacing={1}>
            <Grid item xs={10}></Grid>
            <Grid item xs={1}>
              <Button
                className="cancel_button"
                variant="contained"
                disableElevation
                onClick={() => {
                  history.goBack();
                }}
              >
                Cancel
              </Button>
            </Grid>
            <Grid item xs={1}>
              <Button
                className="save_button"
                variant="contained"
                disableElevation
                onClick={() => {
                  addIndentHandler();
                }}
              >
                {spinner ? (
                  <CircularProgress size={20} color={"#fff"} />
                ) : (
                  "Add Indent"
                )}
              </Button>
            </Grid>
          </Grid>
        </div>
      </div>
    </div>
  );
};
export default AddIndent;
