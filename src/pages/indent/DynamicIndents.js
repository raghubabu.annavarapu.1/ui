import React from "react";
import { makeStyles, Grid, Button } from "@material-ui/core";
import * as Fields from "../../sharedComponents";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";

const useStyles = makeStyles((theme) => ({
  list: {
    background: "#366E93",
    borderTopRightRadius: "5px",
    borderTopLeftRadius: "5px",
    padding: "15px 5px",
  },
  list_head_item: {
    color: "#FFFFFF",
    padding: "0px 15px",
  },
  head_item: {
    borderRight: "2px solid #fff",
  },
  indent: {
    background: "#fff",
    padding: "15px 5px",
  },
  indent_item: {
    padding: "0px 15px",
  },
  add_more: {
    color: "#1B65CB",
    cursor: "pointer",
  },
}));

const DynamicIndents = (props) => {
  const classes = useStyles();
  return (
    <>
      <div style={{ padding: "0px 7px" }}>
        <Grid container className={classes.list} spacing={2}>
          <Grid item xs={3} className={classes.list_head_item}>
            <div className={classes.head_item}>Service Type</div>
          </Grid>
          <Grid item xs={2} className={classes.list_head_item}>
            <div className={classes.head_item}>Quantity</div>
          </Grid>
          <Grid item xs={2} className={classes.list_head_item}>
            <div className={classes.head_item}>Unit Price</div>
          </Grid>
          <Grid item xs={2} className={classes.list_head_item}>
            <div className={classes.head_item}>Total Price</div>
          </Grid>
          <Grid item xs={3} className={classes.list_head_item}>
            <div>Remarks</div>
          </Grid>
        </Grid>
      </div>
      <br />
      {props.indents.map((exp, i) => {
        return (
          <div key={i} style={{ padding: "0px 7px" }}>
            <Grid container spacing={2} className={classes.indent}>
              <Grid item xs={3} className={classes.indent_item}>
                <Fields.AntSelectableSearchField
                  fieldData={exp.indentTypeId}
                  variant="outlined"
                  autoCompleteChangeHandler={(value, name) => {
                    props.indentAutocompleteChangeHandler(value, name, i);
                  }}
                />
              </Grid>
              <Grid item xs={2} className={classes.indent_item}>
                <Fields.InputField
                  fieldData={exp.quantity}
                  variant="outlined"
                  inputChangeHandler={(value, name) => {
                    props.indentInputChangeHandler(value, name, i);
                  }}
                />
              </Grid>
              <Grid item xs={2} className={classes.indent_item}>
                <Fields.InputField
                  fieldData={exp.unitQuantity}
                  variant="outlined"
                  inputChangeHandler={(value, name) => {
                    props.indentInputChangeHandler(value, name, i);
                  }}
                />
              </Grid>
              <Grid item xs={2} className={classes.indent_item}>
                <Fields.InputField
                  fieldData={exp.total}
                  variant="outlined"
                  inputChangeHandler={(value, name) => {
                    props.indentInputChangeHandler(value, name, i);
                  }}
                />
              </Grid>
              <Grid item xs={2} className={classes.indent_item}>
                <Fields.InputField
                  fieldData={exp.indentRemarks}
                  variant="outlined"
                  inputChangeHandler={(value, name) => {
                    props.indentInputChangeHandler(value, name, i);
                  }}
                />
              </Grid>
              <Grid
                item
                xs={1}
                className={classes.indent_item}
                style={{ textAlign: "center" }}
              >
                <Button
                  variant="outlined"
                  onClick={() => {
                    props.removeHandler(i);
                  }}
                >
                  <RemoveIcon />
                </Button>
              </Grid>
            </Grid>
            <br />
          </div>
        );
      })}
      {props.indents.length < 5 ? (
        <div style={{ padding: "0px 7px" }}>
          <Grid container spacing={2} className={classes.indent}>
            <Grid item xs={3}>
              <span
                className={classes.add_more}
                onClick={() => {
                  props.addMoreHandler();
                }}
              >
                {" "}
                <AddIcon
                  style={{ verticalAlign: "middle", fontSize: "14px" }}
                />{" "}
                Add more
              </span>
            </Grid>
          </Grid>
        </div>
      ) : null}
    </>
  );
};

export default DynamicIndents;
