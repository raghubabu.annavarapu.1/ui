import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { Grid, CircularProgress } from "@material-ui/core";
import * as Fields from "../../sharedComponents/index";
import { InputAdornment } from "@material-ui/core";
import _ from "lodash";
import VisibilityIcon from "@material-ui/icons/Visibility";
import AlertMessage from "../../components/alertmessage/AlertMessage";
import {
  logoImage as Logo,
  backgroundImage as Background,
} from "../../assets/index";
import { Link, Typography, Button } from "@material-ui/core";
import ValidateFields from "../../validations/validateFields";
import * as CONFIG from "../../config/GlobalConstants";
import { useLocation } from "react-router-dom";
import { parse } from "qs";

var Service;
var Session;
const useStyles = makeStyles((theme) => ({
  img_style: {
    width: "250px",
    height: "107px",
    display: "block",
    margin: "auto",
  },
  paper_style: {
    padding: 20,
    width: 280,
    margin: "20px auto",
    borderRadius: 10,
  },
  alertBox: {
    padding: "0px",
  },
  forgot_password: {
    fontSize: "10pt",
    color: "#366E93",
    fontWeight: "bold",
  },
  button: {
    backgroundColor: "#366E93",
    color: "#FFFFFF",
    textTransform: "none",
  },
  registernow: {
    fontSize: "10pt",
    color: "#366E93",
    fontWeight: "bold",
  },
  account: {
    fontSize: "10pt",
    color: "#777777",
  },
  bottom_fields: {
    textAlign: "center",
    marginTop: "5%",
    padding: "10px",
  },
}));
export default function Signin(props) {
  Service = global.service;
  Session = global.session;
  const formFields = {
    email: {
      name: "email",
      value: "",
      placeholder: "email or mobile number",
      errorMsg: "Please enter email or mobile number",
      validPattern: "EMAIL_MOBILE",
      validationRequired: true,
      isValid: true,
      type: "text",
      min: 0,
    },
    password: {
      name: "password",
      value: "",
      placeholder: "password",
      errorMsg: "Please enter password",
      validPattern: "PASSWORD",
      validationRequired: true,
      isValid: true,
      type: "password",
      min: 0,
    },
  };
  const classes = useStyles();
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const [spinner, setSpinner] = useState(false);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  const winWidth = window.innerWidth;
  const winHeight = window.innerHeight;
  let { search } = useLocation();
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    newFields[name]["isValid"] = true;
    if (value.length === 0 && newFields[name]["validationRequired"]) {
      newFields[name]["isValid"] = false;
    }
    setFields(newFields);
  };
  let validateFields = new ValidateFields();
  const handleLogin = (event) => {
    event.preventDefault();
    let dataObj = _.cloneDeep(fields);
    const dataStatus = validateFields.validateFieldsData(dataObj);
    if (dataStatus.status) {
      setSpinner(true);
      Service.userLogin(dataObj)
        .then((res) => {
          if (res.status) {
            setSpinner(false);
            Session.set('BearerToken', res.token);
            Session.set('profile', res.profile);
            window.location = '/dashboard';
          } else {
            setSpinner(false);
            let newFields = _.cloneDeep(fields);
            newFields = dataStatus.data;
            setFields(newFields);
            setAlertData({
              open: true,
              severity: CONFIG.ALERT_SEVERITY.error,
              message: res.message,
            });
          }
        })
        .catch((error) => {
          setSpinner(false);
          let newFields = _.cloneDeep(fields);
          newFields = dataStatus.data;
          setFields(newFields);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message: error.message,
          });
        });
    } else {
      let newFields = _.cloneDeep(fields);
      newFields = dataStatus.data;
      setFields(newFields);
    }
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };

  const handleLoginByToken = (path) => {
    setAlertData({
      open: true,
      severity: CONFIG.ALERT_SEVERITY.info,
      message: "verifying token...please wait",
    });
    Service.userLoginByToken()
      .then((response) => {
        if (response.status) {
          // console.log("Response: ", response);
          global.session.set('BearerToken', response.token);
          global.session.set('profile', response.profile);
          window.location = path;
        } else {
          global.session.remove("BearerToken");
          global.session.remove("profile");
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message: "Invalid URL",
          });
        }
      }).catch((err) => {
        // console.log(err);
        global.session.remove("BearerToken");
        global.session.remove("profile");
        setAlertData({
          open: true,
          severity: CONFIG.ALERT_SEVERITY.error,
          message: err.toString(),
        });
      });
  }

  useEffect(() => {
    const query = parse(props.location.search, {
      ignoreQueryPrefix: true
    })
    if (query && query.token && query.token.length > 0) {
      let token = query.token.replace(/ /g, "+");
      global.session.set('BearerToken', token);
      if (query.header && query.header.length > 0 && query.header == "hide") {
        global.session.set('hideHeader', true);
      }
      let path = window.location.href.replace("token=", "").replace(query.token, "").trim();
      if (path.endsWith("?") || path.endsWith("&")) {
        path = path.substring(0, path.length - 1)
      }
      handleLoginByToken(path);
    }
  }, []);

  return (
    <div>
      {alertData.open ? (
        <div className={classes.alertBox}>
          <AlertMessage
            severity={alertData.severity}
            message={alertData.message}
            closeAlert={closeAlert}
          />
        </div>
      ) : null}
      <Paper
        style={{
          backgroundImage: `url(${Background})`,
          width: winWidth,
          height: winHeight,
          backgroundSize: "contain",
        }}
      >
        <div>
          <img src={Logo} className={classes.img_style} />
        </div>
        <div>
          <Paper elevation={10} className={classes.paper_style}>
            <h2>Sign In</h2>
            <form noValidate autoComplete="off">
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Fields.InputField
                    fullWidth={true}
                    fieldData={fields.email}
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Fields.InputField
                    fieldData={fields.password}
                    inputChangeHandler={inputChangeHandler}
                    endAdornment={
                      <InputAdornment style={{ backgroundColor: "#777777" }}>
                        <VisibilityIcon
                          style={{ cursor: "pointer" }}
                          onClick={() => {
                            let newFields = _.cloneDeep(fields);
                            if (newFields.password.type === "password") {
                              newFields.password.type = "text";
                              setFields(newFields);
                            } else {
                              newFields.password.type = "password";
                              setFields(newFields);
                            }
                          }}
                        />
                      </InputAdornment>
                    }
                  />
                </Grid>
              </Grid>
              <div style={{ marginTop: -10, marginBottom: 50 }}>
                <div style={{ float: "right", marginTop: 5 }}>
                  <Link href="#" className={classes.forgot_password}>
                    {/* Forgot Password? */}
                  </Link>
                </div>
              </div>
              <Button
                type="submit"
                color="primary"
                variant="contained"
                fullWidth={true}
                className={classes.button}
                onClick={handleLogin}
              >
                {spinner ? (
                  <CircularProgress size={24} color={"#FFFFFF"} />
                ) : (
                  "Sign In"
                )}
              </Button>
              <Typography className={classes.bottom_fields}>
                <label className={classes.account}>
                  {/* Don't have an Account..? */}
                </label>
                <Link href="#" className={classes.registernow}>
                  {/* Register Now */}
                </Link>
              </Typography>
            </form>
          </Paper>
        </div>
      </Paper>
    </div>
  );
}