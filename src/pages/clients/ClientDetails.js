import React, { useState, useEffect, useRef } from "react";
import { Typography, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import Header from "../../components/header";
import CircularLoading from "../../components/loader/circularLoading";
import AlertMessage from "../../components/alertmessage/AlertMessage";
import * as CONFIG from "../../config/GlobalConstants";
import * as Fields from "../../sharedComponents";
import "../../common.css";
import _ from "lodash";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Chip from "@mui/material/Chip";
import { cleanData } from "jquery";
const useStyles = makeStyles((theme) => ({
  alertBox: {
    padding: "10px 0px",
  },
}));
var Service;
const ClientDetails = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const [loader, setLoader] = useState(false);
  const [clientData, setClientData] = useState();
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  const [percent, setPercent] = useState();
  const uploadRef = useRef();
  Service = global.service;
  useEffect(() => {
    window.scrollTo({ top: 0 });
    if (props.location.state) {
      setLoader(true);
      Service.getClient({ clientId: props.location.state.clientId })
        .then((res) => {
          setClientData(res);
          setLoader(false);
        })
        .catch(() => {
          setLoader(false);
        });
    } else {
      global.session.remove("BearerToken");
      global.session.remove("profile");
      global.session.remove("hideHeader");
      window.location = "/";
    }
  }, []);
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  const uploadHandler = (uploadedFile) => {
    const formData = new FormData();
    formData.append("file", uploadedFile[0]);
    const config = {
      onUploadProgress: function (progressEvent) {
        var percentCompleted = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total
        );
        setPercent(percentCompleted);
      },
    };
    Service.uploadClientDocument(
      formData,
      { id: props.location.state.clientId },
      config
    )
      .then((res) => {
        uploadRef.current.addAttachment();
        setAlertData({
          open: true,
          severity: CONFIG.ALERT_SEVERITY.success,
          message: "File Uploaded successfully!",
        });
      })
      .catch((error) => {
        setAlertData({
          open: true,
          severity: CONFIG.ALERT_SEVERITY.error,
          message: error.response && error.response.data ? error.response.data.message : "Something went wrong!",
        });
      });
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.push("./clients");
              }}
            />
            Client Details
            <>
              {clientData ? (
                <Chip
                  style={{
                    background: `${CONFIG.STATUSES.filter(
                      (tr) => parseInt(tr.value) === clientData.clientDetails.status
                    )[0].color
                      }`,
                    color: "#fff",
                    marginLeft: "13px",
                  }}
                  label={
                    CONFIG.STATUSES.filter(
                      (tr) => parseInt(tr.value) === clientData.clientDetails.status
                    )[0].label
                  }
                />
              ) : null}
            </>
          </Typography>
        </div>
        {loader ? (
          <CircularLoading />
        ) : clientData ? (
          <div>
            <div className="form_container">
              <div className="details_container">
                <Typography className="details_container_heading">
                  Client Information
                </Typography>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Company Name
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.companyName}{" "}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          variant="body2"
                          color="textSecondary"
                          gutterBottom
                        >
                          Client Name
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.contactFirstName}{" "}
                          {clientData.clientDetails.contactLastName}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Mobile Number
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.countryCode}{" "}
                          {clientData.clientDetails.contactMobile}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        // variant="h6"
                        // component="div"
                        >
                          Client Alias Code
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.clientAliasCode}{" "}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Client Type
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.clientType}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Client Code (GL Code)
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.systemGeneratedClientCode}{" "}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Contact Email Address
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.contactEmailAddress}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </Grid>
              </div>
            </div>
            <div className="form_container">
              <div className="details_container">
                <Typography className="details_container_heading">
                  Other Custom Details
                </Typography>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          VAT
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.customDetails.vat ? clientData.clientDetails.customDetails.vat : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          variant="body2"
                          color="textSecondary"
                          gutterBottom
                        >
                          TAX
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.customDetails.tax ? clientData.clientDetails.customDetails.tax : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          PAN
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.customDetails.pan ? clientData.clientDetails.customDetails.pan : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          TAN
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.customDetails.tan ? clientData.clientDetails.customDetails.tan : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          TIN
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.customDetails.tin ? clientData.clientDetails.customDetails.tin : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          CIN
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.customDetails.cin ? clientData.clientDetails.customDetails.cin : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary">
                          GST
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.gst ? clientData.clientDetails.gst : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary">
                          GST Type
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.gstType ? clientData.clientDetails.gstType : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={6}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary">
                          Additional Notes
                        </Typography>
                        <Typography variant="h6" component="div">
                          {clientData.clientDetails.notes ? clientData.clientDetails.notes : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </Grid>
              </div>
            </div>
            {clientData.addresses.length === 0 ? null : (
              <div className="form_container">
                <div className="details_container">
                  <Typography className="details_container_heading">
                    Address Details
                  </Typography>
                  <Grid
                    container
                    spacing={3}
                    className="details_container_content"
                  >
                    {clientData.addresses.map((addr, i) => (
                      <Grid item xs={12} key={i}>
                        <Typography gutterBottom variant="body2" color="textSecondary">
                          {`Client Address ${i + 1}`}
                        </Typography>
                        <Typography variant="h6" component="div">
                          {addr.addressLine1}
                          {addr.addressLine2 ? `, ${addr.addressLine2}` : ""}
                          {addr.addressLine3
                            ? `, ${addr.addressLine3}`
                            : ""}, {addr.city},{addr.faxNo},{addr.postalCode}, {addr.state},{" "}
                          {addr.country}
                        </Typography>
                      </Grid>
                    ))}
                  </Grid>
                </div>
              </div>
            )}
            {clientData.attachments &&
              clientData.attachments.attachments.length > 0 ? <div className="form_container">
              <div className="details_container">
                {alertData.open ? (
                  <div className={classes.alertBox}>
                    <AlertMessage
                      severity={alertData.severity}
                      message={alertData.message}
                      closeAlert={closeAlert}
                    />
                  </div>
                ) : null}
                <Typography className="details_container_heading">
                  Documents
                </Typography>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Fields.UploadDocuments
                    ref={uploadRef}
                    uploadHandler={uploadHandler}
                    data={clientData}
                    percentCompleted={percent}
                    label={"Upload max 5 documents"}
                    max={5}
                    viewFile={true}
                    onlyView={true}
                    getAttachments={Service.getClientDocuments}
                    item={clientData.clientDetails}
                  />
                </Grid>
              </div>
            </div> : null}
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default ClientDetails;
