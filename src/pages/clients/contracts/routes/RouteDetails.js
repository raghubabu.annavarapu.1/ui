import React, { useEffect, useState } from "react";
import Header from "../../../../components/header";
import { useHistory } from "react-router-dom";
import { ArrowBack } from "@material-ui/icons";
import { Typography, Grid } from "@material-ui/core";
import "../../../../common.css";
import CircularLoading from "../../../../components/loader/circularLoading";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import AlertMessage from "../../../../components/alertmessage/AlertMessage";
import * as CONFIG from "../../../../config/GlobalConstants";
import _ from "lodash";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  alertBox: {
    padding: "10px 12px",
  },
  input_adorment: { padding: "0px 15px 0px 2px" },
}));

var Service;

const RouteDetails = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const [loader, setLoader] = useState(false);
  const [routeData, setRouteData] = useState();
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  Service = global.service;
  useEffect(() => {
    setLoader(true);
    if (props.location.state) {
      Service.getRoute({ routeId: props.location.state.routeId })
        .then((res) => {
          setRouteData(res.route);
          setLoader(false);
        })
        .catch((error) => {
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
          setLoader(false);
        });
    } else {
      global.session.remove("BearerToken");
      global.session.remove("profile");
      global.session.remove("hideHeader");
      window.location = "/";
    }
  }, []);
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.push({
                  pathname: "./contractRoutes",
                  state: {
                    clientId: props.location.state.clientId,
                    clientName: props.location.state.clientName,
                    contractId: props.location.state.contractId,
                    contractName: props.location.state.contractName
                  },
                });
              }}
            />
            Route Details
          </Typography>
        </div>
        {alertData.open ? (
          <div className={classes.alertBox}>
            <AlertMessage
              severity={alertData.severity}
              message={alertData.message}
              closeAlert={closeAlert}
            />
          </div>
        ) : null}
        {loader ? (
          <CircularLoading />
        ) : routeData ? (
          <div>
            <div className="form_container">
              <div className="details_container">
                <Typography className="details_container_heading">
                  Route Information
                </Typography>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Company Name
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.client.companyName}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Route Name
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.routeName}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Contract Reference #
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.contract.customerContractReferenceNumber}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Total TAT
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.totalTAT
                            ? `${
                                Math.floor(routeData.totalTAT / 60)
                                  ? `${Math.floor(
                                      routeData.totalTAT / 60
                                    )} hours : ${Math.floor(
                                      routeData.totalTAT % 60
                                    )} minutes`
                                  : `${Math.floor(
                                      routeData.totalTAT % 60
                                    )} minutes`
                              }`
                            : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={6}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Pickup Location
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.pickupLocationAddress}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={6}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Dropoff Location
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.dropoffLocationAddress}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Commodity Type
                        </Typography>
                        <Typography
                          variant="h6"
                          component="div"
                          style={{
                            whiteSpace: "nowrap",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                          }}
                        >
                          {routeData.commodity && routeData.commodity.name
                            ? `${routeData.commodity.name}`
                            : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Maximum Distance
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.maxDistance
                            ? `${routeData.maxDistance} km`
                            : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Maximum Fuel
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.maxFuelInLts
                            ? `${routeData.maxFuelInLts} lts`
                            : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Maximum AdBlue
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.maxAdBlue
                            ? `${routeData.maxAdBlue}`
                            : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Freight Amount
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.freightAmount
                            ? `₹ ${routeData.freightAmount}`
                            : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  {/* <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Freight Type
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.freightType
                            ? `${routeData.freightType}`
                            : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid> */}
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Maximum Expenses
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.maximumExpenses
                            ? `₹ ${routeData.maximumExpenses}`
                            : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Maximum Market Freight Amount
                        </Typography>
                        <Typography variant="h6" component="div">
                          {routeData.maxMarketFreightAmount
                            ? `₹ ${routeData.maxMarketFreightAmount}`
                            : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </Grid>
              </div>
            </div>
            <div className="form_container">
              <div className="details_container">
                <Typography className="details_container_heading">
                  Route
                </Typography>
                <Box sx={{ width: "100" }}>
                  <Stepper alternativeLabel activeStep={-1}>
                    <Step>
                      <StepLabel>
                        <Typography
                          variant="body2"
                          style={{ fontWeight: "bold" }}
                        >
                          Start Point
                        </Typography>
                        {routeData.pickupLocationAddress.split(",")[0]},{" "}
                        {routeData.pickupLocationAddress.split(",")[1]},
                        {
                          routeData.pickupLocationAddress.split(",")[
                            routeData.pickupLocationAddress.split(",").length -
                              2
                          ]
                        }
                        <span style={{ color: "orange" }}>
                          {routeData.pickupHoldingTime
                            ? `, Holding Time: ${Math.floor(
                                routeData.pickupHoldingTime / 60
                              )} hours : ${Math.floor(
                                routeData.pickupHoldingTime % 60
                              )} minutes`
                            : ""}
                        </span>
                      </StepLabel>
                    </Step>
                    {routeData.wayPoints.length > 0
                      ? routeData.wayPoints.map((wp, i) => (
                          <Step key={i}>
                            {wp.address ? (
                              <StepLabel>
                                <Typography
                                  variant="body2"
                                  style={{ fontWeight: "bold" }}
                                >
                                  Waypoint {i + 1}
                                </Typography>
                                {wp.address.split(",")[0]},{" "}
                                {wp.address.split(",")[1]},
                                {
                                  wp.address.split(",")[
                                    wp.address.split(",").length - 2
                                  ]
                                }{" "}
                                <span style={{ color: "green" }}>
                                  {wp.transitTime
                                    ? `, Transit Time: ${Math.floor(
                                        wp.transitTime / 60
                                      )} hours : ${Math.floor(
                                        wp.transitTime % 60
                                      )} minutes`
                                    : ""}{" "}
                                </span>
                                <span style={{ color: "orange" }}>
                                  {wp.holdingTime
                                    ? `, Holding Time: ${Math.floor(
                                        wp.holdingTime / 60
                                      )} hours : ${Math.floor(
                                        wp.holdingTime % 60
                                      )} minutes`
                                    : ""}
                                </span>
                                {wp.groupsInfo && wp.groupsInfo.length > 0 ? (
                                  <>
                                    <Typography variant="subtitle1">
                                      Groups:{" "}
                                    </Typography>
                                    {wp.groupsInfo.map((gp, i) => (
                                      <span>
                                        {gp.groupName}
                                        {wp.groupsInfo.length === 1 ||
                                        i === wp.groupsInfo.length - 1
                                          ? null
                                          : ", "}
                                      </span>
                                    ))}
                                  </>
                                ) : null}
                              </StepLabel>
                            ) : (
                              <StepLabel></StepLabel>
                            )}
                          </Step>
                        ))
                      : null}
                    <Step>
                      <StepLabel>
                        <Typography
                          variant="body2"
                          style={{ fontWeight: "bold" }}
                        >
                          End Point
                        </Typography>
                        {routeData.dropoffLocationAddress.split(",")[0]},{" "}
                        {routeData.dropoffLocationAddress.split(",")[1]},
                        {
                          routeData.dropoffLocationAddress.split(",")[
                            routeData.dropoffLocationAddress.split(",").length -
                              2
                          ]
                        }
                        <span style={{ color: "orange" }}>
                          {routeData.dropoffHoldingTime
                            ? `, Holding Time: ${Math.floor(
                                routeData.dropoffHoldingTime / 60
                              )} hours : ${Math.floor(
                                routeData.dropoffHoldingTime % 60
                              )} minutes`
                            : ""}
                        </span>
                      </StepLabel>
                    </Step>
                  </Stepper>
                </Box>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default RouteDetails;
