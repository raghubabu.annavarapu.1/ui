import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { ArrowBack } from "@material-ui/icons";
import { Typography, Button } from "@material-ui/core";
import Header from "../../../../components/header";
import * as Components from "../../../../sharedComponents";
import * as CONFIG from "../../../../config/GlobalConstants";
import "../../../../common.css";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import { makeStyles } from "@material-ui/core/styles";
import EditIcon from "@mui/icons-material/Edit";
import CircularLoading from "../../../../components/loader/circularLoading";
var Service;

const useStyles = makeStyles((theme) => ({
  add_route_button: {
    background: "#649B42",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    "&:hover": {
      background: "#649B42",
    },
  },
  header_buttons: {
    padding: "0px 15px",
  },
}));

const headRows = [
  { id: "name", disablePadding: true, label: "NAME", width: 40 },
  { id: "contract_no", disablePadding: true, label: "CONTRACT#", width: 40 },
  { id: "route_id", disablePadding: true, label: "ROUTE ID", width: 20 },
  // { id: "sender", disablePadding: true, label: "SENDER" },
  // { id: "receiver", disablePadding: true, label: "RECEIVER" },
  {
    id: "pickup_location",
    disablePadding: true,
    label: "PICKUP LOCATION",
    width: 150,
    alignLeft: true
  },
  { id: "dropoff_location", disablePadding: true, label: "DROPOFF LOCATION", width: 150, alignLeft: true },
  { id: "freight_amount", disablePadding: true, label: "FREIGHT AMOUNT", width: 30 },
  // { id: "freight_type", disablePadding: true, label: "FREIGHT TYPE" },
  { id: "actions", disablePadding: true, label: "", width: 10 },
  // { id: "surchange_amount", disablePadding: true, label: "SURCHANGE AMOUNT" },
  // { id: "actions", disablePadding: true, label: "" },
];
const RouteList = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  const [total, setTotal] = useState(0);
  const [loader, setLoader] = useState(false);
  const [tableData, setTableData] = useState([]);
  Service = global.service;
  useEffect(() => {
    if(props.location.state){
      let dataObj = {
        ...{
          clientId: props.location.state.clientId,
          contractId: props.location.state.contractId,
        },
        skip: pagination.current * pagination.pageSize,
        limit: pagination.pageSize,
      };
      renderRoutes(dataObj);
    }else{
      global.session.remove("BearerToken");
      global.session.remove("profile");
      global.session.remove("hideHeader");
      window.location = "/";
    }
  }, []);
  const renderRoutes = (dataObj) => {
    setLoader(true);
    Service.getRoutes(dataObj)
      .then((res) => {
        let data = res.routes.map((route) => {
          return {
            name: <div style={{ textAlign: "center" }}> {route.routeName}</div>,
            contract_no: <div style={{ textAlign: "center" }}>{route.contract.customerContractReferenceNumber
              ? route.contract.customerContractReferenceNumber
              : "--"}</div>,
            route_id: <div style={{ textAlign: "center" }}>{route.id}</div>,
            pickup_location: <div style={{ textAlign: "left" }} title={route.pickupLocationAddress}> {route.pickupLocationAddress.split(',')[0]}...</div>,
            dropoff_location: <div style={{ textAlign: "left" }} title={route.pickupLocationAddress}>{route.dropoffLocationAddress.split(',')[0]}...</div>,
            freight_amount: <div style={{ textAlign: "center" }}>{route.freightAmount ? `₹ ${route.freightAmount}` : "--"}</div>,
            // freight_type: "None",
            actions: (
              <div style={{ textAlign: "center" }}>
                <Components.DropDownMenu
                  options={CONFIG.ROUTE_MORE_OPTIONS}
                  data={{
                    routeId: route.id,
                    clientId: props.location.state.clientId,
                    clientName: props.location.state.clientName,
                    contractId: props.location.state.contractId,
                    contractName: props.location.state.contractName
                  }}
                />
              </div>
            ),
          };
        });
        setTableData(data);
        setTotal(res.totalCount);
        setLoader(false);
      })
      .catch(() => {
        setLoader(false);
      });
  };
  const pageChangeHandler = (page) => {
    let dataObj = {
      skip: page * pagination.pageSize,
      limit: pagination.pageSize,
      clientId: props.location.state.clientId,
      contractId: props.location.state.contractId,
    };
    renderRoutes(dataObj);
    setPagination({ ...pagination, current: page });
  };
  const rowsPerPageChangeHandler = (rowsPerPage) => {
    let dataObj = {
      skip: pagination.current * rowsPerPage,
      limit: rowsPerPage,
      clientId: props.location.state.clientId,
      contractId: props.location.state.contractId,
    };
    renderRoutes(dataObj);
    setPagination({ ...pagination, pageSize: rowsPerPage });
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.push({
                  pathname: "./clientContracts",
                  state: { clientId: props.location.state.clientId, clientName: props.location.state.clientName },
                });
              }}
            />
            Routes (Client Name: {props.location.state.clientName} / Contract: {props.location.state.contractName})
          </Typography>
          <div className={classes.header_buttons}>
            <Button
              className={classes.add_route_button}
              startIcon={<AddCircleOutlinedIcon />}
              onClick={() => {
                history.push({
                  pathname: "./addRoute",
                  state: {
                    clientId: props.location.state.clientId,
                    clientName: props.location.state.clientName,
                    contractId: props.location.state.contractId,
                    contractName: props.location.state.contractName
                  },
                });
              }}
            >
              Add New Route
            </Button>
          </div>
        </div>
        <div>
          {loader ? (
            <CircularLoading />
          ) : (
            // <Components.DataGridTable/>
            <Components.DataTable
              headRows={headRows}
              tableData={tableData}
              total={total}
              pagination={pagination}
              pageChangeHandler={pageChangeHandler}
              rowsPerPageChangeHandler={rowsPerPageChangeHandler}
            />
          )}
        </div>
      </div>
    </div>
  );
};
export default RouteList;
