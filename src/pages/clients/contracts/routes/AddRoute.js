import React, { useState, useEffect, useRef } from "react";
import Header from "../../../../components/header";
import { useHistory } from "react-router-dom";
import { ArrowBack } from "@material-ui/icons";
import { Typography, Button, Grid, CircularProgress } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import "../../../../common.css";
import _ from "lodash";
import * as Fields from "../../../../sharedComponents";
import * as CONFIG from "../../../../config/GlobalConstants";
import ValidateFields from "../../../../validations/validateFields";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import RouteMap from "./RouteMap";
import AddWaypoint from "./AddWaypoint";
import AddFixedRoute from "./AddFixedRoute";
import AlertMessage from "../../../../components/alertmessage/AlertMessage";
import Map from "./Map";
import InputAdornment from "@mui/material/InputAdornment";
import axios from "axios";
import {
  blueMarkerImage,
  redMarkerImage,
  orangeMarkerImage,
} from "../../../../assets/index";
import Utils from "../../../../utils";

var Service;

const useStyles = makeStyles((theme) => ({
  waypoint_button: {
    width: "100%",
    background: "#D08B1D",
    color: "#FFFFFF",
    padding: "10px 15px",
    "&:hover": {
      background: "#D08B1D",
      color: "#FFFFFF",
    },
  },
  map_button: {
    width: "100%",
    background: "#2A1265",
    color: "#FFFFFF",
    padding: "10px 15px",
    "&:hover": {
      background: "#2A1265",
      color: "#FFFFFF",
    },
  },
  map_header: {
    display: "flex",
    justifyContent: "space-between",
    padding: "8px 0px",
  },
  alertBox: {
    padding: "10px 12px",
  },
}));

const AddRoute = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const wayPoints = [
    {
      waypointName: {
        name: "waypointName",
        label: "Way Point Name",
        value: "",
        validationRequired: true,
        isValid: true,
        validPattern: "SPECIAL_CHARS_DESC",
        errorMsg: "Please enter waypoint name",
        topLabel: true,
        ref: useRef(),
      },
      transitHours: {
        name: "transitHours",
        label: "Transit Time",
        value: "",
        type: "number",
        min: 0,
        topLabel: true,
        onlyInt: true,
      },
      transitMinutes: {
        name: "transitMinutes",
        label: "Transit Time",
        value: "",
        type: "number",
        min: 0,
        topLabel: true,
        max: 59,
        maxLength: 2,
      },
      holdingHours: {
        name: "holdingHours",
        label: "Holding Time",
        value: "",
        type: "number",
        min: 0,
        topLabel: true,
        onlyInt: true,
      },
      holdingMinutes: {
        name: "holdingMinutes",
        label: "Holding Time",
        value: "",
        type: "number",
        min: 0,
        max: 59,
        topLabel: true,
        maxLength: 2,
      },
      location: {
        name: "location",
        label: "Way Point Location",
        value: "",
        validationRequired: true,
        isValid: true,
        lat: null,
        lng: null,
        topLabel: true,
        validPattern: "SPECIAL_CHARS_DESC",
        errorMsg: "Please enter waypoint location",
        placeholder: "Enter Location",
        completeValue: "",
        ref: useRef(),
      },
      groups: {
        name: "groups",
        label: "Select Groups",
        value: [],
        actualValue: [],
        validationRequired: false,
      },
      mobileNumber1: {
        name: "mobileNumber1",
        label: "Mobile Number 1",
        value: "",
        topLabel: true,
        maxLength: 10,
        isValid: true,
        errorMsg: "Please enter valid mobile number",
      },
      mobileNumber2: {
        name: "mobileNumber2",
        label: "Mobile Number 2",
        value: "",
        topLabel: true,
        maxLength: 10,
        isValid: true,
        errorMsg: "Please enter valid mobile number",
      },
      mobileNumber3: {
        name: "mobileNumber3",
        label: "Mobile Number 3",
        value: "",
        topLabel: true,
        maxLength: 10,
        isValid: true,
        errorMsg: "Please enter valid mobile number",
      },
    },
  ];
  const formFields = {
    commodity: {
      name: "commodity",
      label: "Commodity Type",
      value: { label: "", value: "" },
      options: [],
      topLabel: true,
      ref: useRef(),
    },
    freightType: {
      name: "freightType",
      label: "Freight Type",
      value: { label: "", value: "" },
      options: CONFIG.FREIGHT_TYPES,
      topLabel: true,
    },
    routeName: {
      name: "routeName",
      label: "Route Name",
      value: "",
      isValid: true,
      validationRequired: true,
      topLabel: true,
      validPattern: "SPECIAL_CHARS_DESC",
      errorMsg: "Please enter route name",
      ref: useRef(),
    },
    onlyWaypointAlert: {
      name: "onlyWaypointAlert",
      label: "Only Waypoint Alert",
      value: 0,
    },
    totalTATHours: {
      name: "totalTATHours",
      label: "Maximum TAT (in hours)",
      value: "",
      type: "number",
      min: 0,
    },
    totalTATMin: {
      name: "totalTATMin",
      label: "Maximum TAT (in minutes)",
      value: "",
      type: "number",
      min: 0,
    },
    maximumExpenses: {
      name: "maximumExpenses",
      label: "Maximum Expenses",
      value: "",
      type: "number",
      min: 0,
    },
    maximumFuel: {
      name: "maximumFuel",
      label: "Maximum Fuel",
      value: "",
      type: "number",
      min: 0,
    },
    maximumAdBlue: {
      name: "maximumAdBlue",
      label: "Maximum Adblue",
      value: "",
      type: "number",
      min: 0,
    },
    maximumDistance: {
      name: "maximumDistance",
      label: "Maximum Distance",
      value: "",
      type: "number",
      min: 0,
    },
    freightAmount: {
      name: "freightAmount",
      label: "Freight Amount",
      value: "",
      type: "number",
      min: 0,
    },
    maximumMarketVehicleFreightAmt: {
      name: "maximumMarketVehicleFreightAmt",
      label: "Market Vehicle Freight Amount",
      value: "",
      type: "number",
      min: 0,
    },
    notes: {
      name: "notes",
      label: "Notes",
      value: "",
    },
  };
  const locFields = {
    pickupName: {
      name: "pickupName",
      label: "Name",
      value: "",
      validationRequired: true,
      isValid: true,
      validPattern: "SPECIAL_CHARS_DESC",
      errorMsg: "Please enter pickup name",
      topLabel: true,
      ref: useRef(),
    },
    pickupLocation: {
      name: "pickupLocation",
      label: "Location",
      value: "",
      topLabel: true,
      validationRequired: true,
      isValid: true,
      validPattern: "SPECIAL_CHARS_DESC",
      lat: null,
      lng: null,
      errorMsg: "Please select pickup location",
      ref: useRef(),
    },
    pickupLocationGroups: {
      name: "pickupLocationGroups",
      label: "Select Groups",
      value: [],
      actualValue: [],
      validationRequired: false,
    },
    pickHoldingTimeHours: {
      name: "pickHoldingTimeHours",
      label: "Holding Time",
      value: "",
      topLabel: "true",
      type: "number",
      min: 0,
      onlyInt: true,
    },
    pickHoldingTimeMinutes: {
      name: "pickHoldingTimeMinutes",
      label: "Holding Time",
      value: "",
      topLabel: "true",
      type: "number",
      min: 0,
      max: 59,
      maxLength: 2,
    },
    dropoffName: {
      name: "dropoffName",
      label: "Name",
      value: "",
      topLabel: true,
      validationRequired: true,
      validPattern: "SPECIAL_CHARS_DESC",
      isValid: true,
      errorMsg: "Please enter dropoff name",
      ref: useRef(),
    },
    dropoffLocation: {
      name: "dropoffLocation",
      label: "Location",
      value: "",
      topLabel: true,
      validationRequired: true,
      isValid: true,
      validPattern: "SPECIAL_CHARS_DESC",
      lat: null,
      lng: null,
      errorMsg: "Please select dropoff location",
      ref: useRef(),
    },
    dropoffLocationGroups: {
      name: "dropoffLocationGroups",
      label: "Select groups",
      value: [],
      actualValue: [],
      validationRequired: false,
    },
    dropHoldingTimeHours: {
      name: "dropHoldingTimeHours",
      label: "Holding Time",
      value: "",
      topLabel: "true",
      type: "number",
      min: 0,
      onlyInt: true,
    },
    dropHoldingTimeMinutes: {
      name: "dropHoldingTimeMinutes",
      label: "Holding Time",
      value: "",
      topLabel: "true",
      type: "number",
      min: 0,
      max: 59,
      maxLength: 2,
    },
  };
  const [locationFields, setLocationFields] = useState(_.cloneDeep(locFields));
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  // const [open, setOpen] = useState(false);
  const [moreInfoOpen, setMoreInfoOpen] = useState(false);
  const [spinner, setSpinner] = useState(false);
  const [wayPointFields, setWayPointFields] = useState(_.cloneDeep(wayPoints));
  // const [wayPointIndex, setWayPointIndex] = useState();
  const [zoom, setZoom] = useState(5);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  const [polyLine, setPolyLine] = useState([]);
  const [plRefs, setPlRefs] = useState([]);
  const [groupOptions, setGroupOptions] = useState([]);
  const placeRef1 = useRef();
  const placeRef2 = useRef();
  const placeRef3 = useRef();
  const mapRef = useRef();
  let validateFields = new ValidateFields();
  let utils = new Utils();
  Service = global.service;
  useEffect(() => {
    window.scroll({ top: 0 });
    Service.getGroups()
      .then((res) => {
        let data = res.groups.map((gp) => {
          return { id: gp.id, name: gp.groupName };
        });
        setGroupOptions(data);
      })
      .catch(() => {});
    Service.getCommodities()
      .then((res) => {
        let newFields = _.cloneDeep(fields);
        newFields["commodity"]["options"] = res.commodities.map((commodity) => {
          return { label: commodity.name, value: commodity.id };
        });
        setFields(newFields);
      })
      .catch(() => {});
  }, []);
  useEffect(() => {
    setPlRefs((plRefs) =>
      Array(wayPointFields.length)
        .fill()
        .map((_, i) => plRefs[i] || React.createRef())
    );
  }, [wayPointFields.length]);
  let formContainerRef = useRef();
  const addWayPointHandler = (i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields.push(wayPoints[0]);
    setWayPointFields(newWayPointFields);
  };
  const wayPointRemoveHandler = (i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    let newLocFields = _.cloneDeep(locationFields);
    let newPolyLine = [...polyLine];
    let pickline;
    let dropline;
    let wps;
    newWayPointFields.splice(i, 1);
    pickline =
      newLocFields.pickupLocation.lat && newLocFields.pickupLocation.lng
        ? [newLocFields.pickupLocation.lat, newLocFields.pickupLocation.lng]
        : pickline;
    dropline =
      newLocFields.dropoffLocation.lat && newLocFields.dropoffLocation.lng
        ? [newLocFields.dropoffLocation.lat, newLocFields.dropoffLocation.lng]
        : dropline;
    wps = newWayPointFields
      .filter((wp) => wp.location.lat && wp.location.lng)
      .map((wp) => {
        return [wp.location.lat, wp.location.lng];
      });
    newPolyLine =
      pickline && dropline
        ? [pickline, ...wps, dropline]
        : pickline
        ? [pickline, ...wps]
        : dropline
        ? [...wps, dropline]
        : [...wps];
    setPolyLine(newPolyLine);
    setWayPointFields(newWayPointFields);
  };
  const handleMoreInfoOpen = () => {
    setMoreInfoOpen(false);
  };
  const addMoreInfoHandler = () => {
    setMoreInfoOpen(false);
  };
  const autoCompleteChangeHandler = (value, name) => {
    if (value) {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = value;
      newFields[name]["isValid"] = true;
      setFields(newFields);
    } else {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = { value: "", label: "" };
      if (newFields[name]["validationRequired"]) {
        newFields[name]["isValid"] = false;
      }
      setFields(newFields);
    }
  };
  const radioChangeHandler = (value) => {
    let newFields = _.cloneDeep(fields);
    newFields["onlyWaypointAlert"]["value"] = value;
    setFields(newFields);
  };
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    newFields[name]["isValid"] = true;
    if (value.length === 0 && newFields[name]["validationRequired"]) {
      newFields[name]["isValid"] = false;
    }
    setFields(newFields);
  };
  const locInputChangeHandler = (value, name) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields[name]["value"] = value;
    newLocFields[name]["isValid"] = true;
    if (value.length === 0 && newLocFields[name]["validationRequired"]) {
      newLocFields[name]["isValid"] = false;
    }
    setLocationFields(newLocFields);
  };
  const setLocationHandler = (name, val, lat, lng) => {
    let newLocFields = _.cloneDeep(locationFields);
    let newPolyLine = [...polyLine];
    let wayPoints = _.cloneDeep(wayPointFields);
    if (val && lat && lng) {
      newLocFields[name]["value"] = val;
      newLocFields[name]["lat"] = lat;
      newLocFields[name]["lng"] = lng;
      newLocFields[name]["isValid"] = true;
      setLocationFields(newLocFields);
      mapRef.current.flyToLoc({ lt: lat, ln: lng }, name);
      let pickline;
      let dropline;
      let wps;
      if (mapRef.current.route) {
        mapRef.current.setRoute(false);
      }
      pickline =
        newLocFields.pickupLocation.lat && newLocFields.pickupLocation.lng
          ? [newLocFields.pickupLocation.lat, newLocFields.pickupLocation.lng]
          : pickline;
      dropline =
        newLocFields.dropoffLocation.lat && newLocFields.dropoffLocation.lng
          ? [newLocFields.dropoffLocation.lat, newLocFields.dropoffLocation.lng]
          : dropline;
      wps = wayPoints
        .filter((wp) => wp.location.lat && wp.location.lng)
        .map((wp) => {
          return [wp.location.lat, wp.location.lng];
        });
      newPolyLine =
        pickline && dropline
          ? [pickline, ...wps, dropline]
          : pickline
          ? [pickline, ...wps]
          : dropline
          ? [...wps, dropline]
          : [...wps];
      setPolyLine(newPolyLine);
    }
  };
  const setWayPointLocationHandler = (name, value, lat, lng, val, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    let newLocFields = _.cloneDeep(locationFields);
    let newPolyLine = [...polyLine];
    if (value && lat && lng) {
      newWayPointFields[i][name]["value"] = value;
      newWayPointFields[i][name]["lat"] = lat;
      newWayPointFields[i][name]["lng"] = lng;
      newWayPointFields[i][name]["isValid"] = true;
      newWayPointFields[i][name]["completeValue"] = val;
      setWayPointFields(newWayPointFields);
      mapRef.current.flyToLoc({ lt: lat, ln: lng }, name, i);
      if (mapRef.current.route) {
        mapRef.current.setRoute(false);
      }
      let pickline;
      let dropline;
      let wps;
      pickline =
        newLocFields.pickupLocation.lat && newLocFields.pickupLocation.lng
          ? [newLocFields.pickupLocation.lat, newLocFields.pickupLocation.lng]
          : pickline;
      dropline =
        newLocFields.dropoffLocation.lat && newLocFields.dropoffLocation.lng
          ? [newLocFields.dropoffLocation.lat, newLocFields.dropoffLocation.lng]
          : dropline;
      wps = newWayPointFields
        .filter((wp) => wp.location.lat && wp.location.lng)
        .map((wp) => {
          return [wp.location.lat, wp.location.lng];
        });
      newPolyLine =
        pickline && dropline
          ? [pickline, ...wps, dropline]
          : pickline
          ? [pickline, ...wps]
          : dropline
          ? [...wps, dropline]
          : [...wps];
      setPolyLine(newPolyLine);
    }
  };
  const waypointInputChangeHandler = (value, name, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields[i][name]["value"] = value;
    newWayPointFields[i][name]["isValid"] = true;
    if (
      value.length === 0 &&
      newWayPointFields[i][name]["validationRequired"]
    ) {
      newWayPointFields[i][name]["isValid"] = false;
    }
    setWayPointFields(newWayPointFields);
  };
  const locFieldsHandler = (value, name) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields[name]["value"] = value;
    setLocationFields(newLocFields);
  };
  const getNonEmptyRows = () => {
    const wpFields = _.cloneDeep(wayPointFields);
    const nonEmptyFields = wpFields.filter((wp) => {
      if (!checkAllfieldsEmpty(wp)) {
        return wp;
      }
    });
    return nonEmptyFields;
  };
  const checkAllfieldsEmpty = (obj) => {
    var status = true;
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        var val = obj[key];
        if (val.value.length !== 0) {
          status = false;
        }
      }
    }
    return status;
  };
  const wpLocHandler = (value, name, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields[i][name]["value"] = value;
    setWayPointFields(newWayPointFields);
  };
  const validateMobileNumber = (row) => {
    const rowFields = _.cloneDeep(row);
    let valid = [];
    for (var key in rowFields) {
      if (rowFields.hasOwnProperty(key)) {
        var val = rowFields[key];
        if (
          (val.name === "mobileNumber1" ||
            val.name === "mobileNumber2" ||
            val.name === "mobileNumber3") &&
          val.value !== ""
        ) {
          let re = /^[0-9]{10}$/;
          if (re.test(val.value)) {
            valid.push(true);
            rowFields[val.name]["isValid"] = true;
          } else {
            valid.push(false);
            rowFields[val.name]["isValid"] = false;
          }
        }
      }
    }
    return {
      status: valid.filter((mob) => mob === false).length > 0 ? false : true,
      data: rowFields,
    };
  };
  const validateWaypoints = () => {
    const nonEmptyRows = getNonEmptyRows();
    var valid = true;
    const waypoints = nonEmptyRows.map((row) => {
      const dataStatus = validateFields.validateFieldsData(row);
      const validMobileNumber = validateMobileNumber(row);
      valid = !(dataStatus.status && validMobileNumber.status)
        ? dataStatus.status && validMobileNumber.status
        : valid;
      return { ...dataStatus.data, ...validMobileNumber.data };
      // valid = !dataStatus.status ? dataStatus.status : valid;
      // return dataStatus.data;
    });
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields = [...waypoints];
    setWayPointFields(newWayPointFields);
    return { status: valid, wayPoints: [...waypoints] };
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  const addRouteHandler = () => {
    let { clientId, contractId } = props.location.state;
    let dataObj = { clientId, contractId, ...fields, ...locationFields };
    let modifiedFields = {
      ...fields,
      commodity: {
        ...fields.commodity,
        value:
          fields.commodity.value.value === undefined
            ? fields.commodity.value
            : fields.commodity.value.value,
      },
      freightType: {
        ...fields.freightType,
        value:
          fields.freightType.value.value === undefined
            ? fields.freightType.value
            : fields.freightType.value.value,
      },
    };
    dataObj = { ...dataObj, ...modifiedFields };
    const dataStatus = validateFields.validateFieldsData(dataObj);
    const validWaypoints = validateWaypoints();
    if (dataStatus.status && validWaypoints.status) {
      setSpinner(true);
      let url = `https://router.project-osrm.org/route/v1/driving/${utils.getLngLatString(
        {
          pickup: locationFields.pickupLocation,
          dropoff: locationFields.dropoffLocation,
          wayPointFields,
        }
      )}?overview=simplified`;
      axios
        .get(url)
        .then((res) => {
          dataObj = {
            ...dataObj,
            polyLine: res.data.routes[0].geometry,
            wayPoints: [...validWaypoints.wayPoints],
          };
          Service.createRoute(dataObj)
            .then((res) => {
              let newFields = _.cloneDeep(formFields);
              let newLocFields = _.cloneDeep({
                ...locFields,
              });
              let newWayPointFields = _.cloneDeep(wayPoints);
              setFields(newFields);
              setLocationFields(newLocFields);
              setWayPointFields(newWayPointFields);
              setPolyLine([]);
              mapRef.current.setRoute(false);
              setAlertData({
                open: true,
                severity: CONFIG.ALERT_SEVERITY.success,
                message: "Route created successfully!",
              });
              setSpinner(false);
              window.scrollTo({ top: 0, behavior: "smooth" });
              placeRef1.current.clearValue();
              placeRef3.current.clearValue();
            })
            .catch((error) => {
              setSpinner(false);
              setAlertData({
                open: true,
                severity: CONFIG.ALERT_SEVERITY.error,
                message:
                  error.response && error.response.data
                    ? error.response.data.message
                    : "Something went wrong!",
              });
              window.scrollTo({ top: 0, behavior: "smooth" });
            });
        })
        .catch(() => {
          dataObj = { ...dataObj, wayPoints: [...validWaypoints.wayPoints] };
          Service.createRoute(dataObj)
            .then((res) => {
              let newFields = _.cloneDeep(formFields);
              let newLocFields = _.cloneDeep({
                ...locFields,
              });
              let newWayPointFields = _.cloneDeep(wayPoints);
              setFields(newFields);
              setLocationFields(newLocFields);
              setWayPointFields(newWayPointFields);
              setPolyLine([]);
              mapRef.current.setRoute(false);
              setAlertData({
                open: true,
                severity: CONFIG.ALERT_SEVERITY.success,
                message: "Route created successfully!",
              });
              setSpinner(false);
              window.scrollTo({ top: 0, behavior: "smooth" });
              placeRef1.current.clearValue();
              placeRef3.current.clearValue();
            })
            .catch((error) => {
              setSpinner(false);
              setAlertData({
                open: true,
                severity: CONFIG.ALERT_SEVERITY.error,
                message:
                  error.response && error.response.data
                    ? error.response.data.message
                    : "Something went wrong!",
              });
              window.scrollTo({ top: 0, behavior: "smooth" });
            });
        });
    } else {
      let newFields = _.cloneDeep(fields);
      let newLocFields = _.cloneDeep(locationFields);
      let erroredFields = Object.keys(dataStatus.data).filter(
        (key) => dataStatus.data[key].isValid === false
      );
      newLocFields = Object.fromEntries(
        Object.entries(dataStatus.data).filter(
          ([key, value]) =>
            key === "pickupLocation" ||
            key === "dropoffLocation" ||
            key === "pickHoldingTimeHours" ||
            key === "pickHoldingTimeMinutes" ||
            key === "dropHoldingTimeHours" ||
            key === "dropHoldingTimeMinutes" ||
            key === "pickupName" ||
            key === "dropoffName" ||
            key === "pickupLocationGroups" ||
            key === "dropoffLocationGroups"
        )
      );
      newFields = Object.fromEntries(
        Object.entries(dataStatus.data).filter(
          ([key, value]) =>
            key !== "clientId" &&
            key !== "contractId" &&
            key !== "pickupLocation" &&
            key !== "dropoffLocation" &&
            key !== "pickHoldingTimeHours" &&
            key !== "pickHoldingTimeMinutes" &&
            key !== "dropHoldingTimeMinutes" &&
            key !== "dropHoldingTimeHours" &&
            key !== "pickupName" &&
            key !== "dropoffName" &&
            key !== "pickupLocationGroups" &&
            key !== "dropoffLocationGroups"
        )
      );
      setFields(newFields);
      setLocationFields(newLocFields);
      if (erroredFields.length > 0) {
        if (fields[erroredFields[0]]) {
          formContainerRef.current.scrollTop =
            fields[erroredFields[0]].ref.current.offsetTop - 150;
        } else if (locationFields[erroredFields[0]]) {
          formContainerRef.current.scrollTop =
            locationFields[erroredFields[0]].ref.current.offsetTop - 150;
        }
      }
    }
  };
  const setPickup = (coords, loc) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields["pickupLocation"]["value"] = loc[0].formatted_address;
    newLocFields["pickupLocation"]["lat"] = coords.lat;
    newLocFields["pickupLocation"]["lng"] = coords.lng;
    setLocationFields(newLocFields);
    placeRef1.current.setPlace({
      label: loc[0].formatted_address,
      value: {
        description: loc[0].formatted_address,
        place_id: loc[0].place_id,
        reference: loc[0].place_id,
        types: loc[0].types,
      },
    });
  };
  const setDropoff = (coords, loc) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields["dropoffLocation"]["value"] = loc[0].formatted_address;
    newLocFields["dropoffLocation"]["lat"] = coords.lat;
    newLocFields["dropoffLocation"]["lng"] = coords.lng;
    setLocationFields(newLocFields);
    placeRef3.current.setPlace({
      label: loc[0].formatted_address,
      value: {
        description: loc[0].formatted_address,
        place_id: loc[0].place_id,
        reference: loc[0].place_id,
        types: loc[0].types,
      },
    });
  };
  const setWp = (coords, loc, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields[i]["waypointName"]["value"] = loc[0].formatted_address
      .split(",")
      .reverse()[2];
    newWayPointFields[i]["location"]["value"] = loc[0].formatted_address;
    newWayPointFields[i]["location"]["lat"] = coords.lat;
    newWayPointFields[i]["location"]["lng"] = coords.lng;
    newWayPointFields[i]["location"]["completeValue"] = {
      label: loc[0].formatted_address,
      value: {
        description: loc[0].formatted_address,
        place_id: loc[0].place_id,
        reference: loc[0].place_id,
        types: loc[0].types,
      },
    };
    setWayPointFields(newWayPointFields);
    plRefs[i].current.setPlace({
      label: loc[0].formatted_address,
      value: {
        description: loc[0].formatted_address,
        place_id: loc[0].place_id,
        reference: loc[0].place_id,
        types: loc[0].types,
      },
    });
  };
  const wayPointGroupChangeHandler = (value, name, i) => {
    let newWayPointFields = _.cloneDeep(wayPointFields);
    newWayPointFields[i][name]["value"] = value.join(",");
    newWayPointFields[i][name]["actualValue"] = value;
    setWayPointFields(newWayPointFields);
  };
  const locGroupChangeHandler = (value, name) => {
    let newLocFields = _.cloneDeep(locationFields);
    newLocFields[name]["value"] = value.join(",");
    newLocFields[name]["actualValue"] = value;
    setLocationFields(newLocFields);
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.push({
                  pathname: "./contractRoutes",
                  state: {
                    clientId: props.location.state.clientId,
                    clientName: props.location.state.clientName,
                    contractId: props.location.state.contractId,
                    contractName: props.location.state.contractName
                  },
                });
              }}
            />
            Add Route
          </Typography>
        </div>
        {alertData.open ? (
          <div className={classes.alertBox}>
            <AlertMessage
              severity={alertData.severity}
              message={alertData.message}
              closeAlert={closeAlert}
            />
          </div>
        ) : null}
        <div className="add_route_form_container">
          <AddFixedRoute
            open={moreInfoOpen}
            handleClose={handleMoreInfoOpen}
            addMoreInfoHandler={addMoreInfoHandler}
            inputChangeHandler={inputChangeHandler}
            radioChangeHandler={radioChangeHandler}
          />
          <Grid
            container
            spacing={3}
            style={{ height: "calc(100vh - 178px)", overflowY: "auto" }}
          >
            <Grid
              item
              xs={4}
              style={{ height: "calc(100vh - 178px)", overflowY: "auto" }}
              ref={formContainerRef}
            >
              <div className="border_bottom">
                <Grid container spacing={2}>
                  <Grid
                    item
                    xs={12}
                    className="custom_select"
                    ref={fields.commodity.ref}
                  >
                    <Fields.AntSelectableSearchField
                      fieldData={fields.commodity}
                      autoCompleteChangeHandler={autoCompleteChangeHandler}
                    />
                  </Grid>
                </Grid>
                <Grid container spacing={2}>
                  <Grid item xs={12} ref={fields.routeName.ref}>
                    <Fields.InputField
                      fieldData={fields.routeName}
                      variant="outlined"
                      inputChangeHandler={inputChangeHandler}
                    />
                  </Grid>
                </Grid>
              </div>
              <div className="details_container">
                <Typography className="details_container_heading">
                  Location Details
                </Typography>
                <fieldset
                  style={{ border: "1px solid black", borderRadius: "4px" }}
                >
                  <legend
                    style={{
                      fontSize: "15px",
                      width: "auto",
                      fontWeight: "bold",
                    }}
                  >
                    Pick Up
                  </legend>
                  <Grid container spacing={2}>
                    <Grid item xs={12} ref={locationFields.pickupName.ref}>
                      <Fields.InputField
                        variant="outlined"
                        fieldData={locationFields.pickupName}
                        inputChangeHandler={locInputChangeHandler}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={2}>
                    <Grid item xs={12} ref={locationFields.pickupLocation.ref}>
                      <Fields.PlaceSearchField
                        fieldData={locationFields.pickupLocation}
                        variant="outlined"
                        setLocationHandler={setLocationHandler}
                        emptyLocHandler={locFieldsHandler}
                        ref={placeRef1}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <Fields.AntMultiSelectField
                        fieldData={locationFields.pickupLocationGroups}
                        options={groupOptions}
                        selectChangeHandler={locGroupChangeHandler}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={2}>
                    <Grid item xs={6}>
                      <Fields.InputField
                        fieldData={locationFields.pickHoldingTimeHours}
                        variant="outlined"
                        inputChangeHandler={locInputChangeHandler}
                        endAdornment={
                          <InputAdornment position="end">Hours</InputAdornment>
                        }
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Fields.InputField
                        fieldData={locationFields.pickHoldingTimeMinutes}
                        variant="outlined"
                        inputChangeHandler={locInputChangeHandler}
                        endAdornment={
                          <InputAdornment position="end">Mins</InputAdornment>
                        }
                      />
                    </Grid>
                  </Grid>
                </fieldset>
                <fieldset
                  style={{
                    border: "1px solid black",
                    borderRadius: "4px",
                    margin: "15px 0px",
                  }}
                >
                  <legend
                    style={{
                      fontSize: "15px",
                      width: "auto",
                      fontWeight: "bold",
                    }}
                  >
                    Waypoints
                  </legend>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      {wayPointFields.length > 0 ? (
                        <div className={classes.wayPoints_section}>
                          {wayPointFields.map((wp, i) => {
                            return (
                              <div
                                key={i}
                                style={{
                                  borderBottom:
                                    i + 1 < wayPointFields.length
                                      ? "1px dashed black"
                                      : "",
                                }}
                              >
                                <Grid container spacing={5}>
                                  <Grid item xs={12} ref={wp.waypointName.ref}>
                                    <Fields.InputField
                                      fieldData={wp.waypointName}
                                      variant="outlined"
                                      inputChangeHandler={(value, name) => {
                                        waypointInputChangeHandler(
                                          value,
                                          name,
                                          i
                                        );
                                      }}
                                    />
                                  </Grid>
                                </Grid>
                                <Grid container spacing={5}>
                                  <Grid item xs={12} ref={wp.location.ref}>
                                    <Fields.PlaceSearchField
                                      ref={plRefs[i]}
                                      fieldData={wp.location}
                                      wayPointFields={[...wayPointFields]}
                                      variant="outlined"
                                      setLocationHandler={(
                                        name,
                                        value,
                                        lat,
                                        lng,
                                        val
                                      ) => {
                                        setWayPointLocationHandler(
                                          name,
                                          value,
                                          lat,
                                          lng,
                                          val,
                                          i
                                        );
                                      }}
                                      emptyLocHandler={(value, name) => {
                                        wpLocHandler(value, name, i);
                                      }}
                                    />
                                  </Grid>
                                </Grid>
                                <Grid container spacing={4}>
                                  <Grid item xs={12}>
                                    <Fields.AntMultiSelectField
                                      fieldData={wp.groups}
                                      options={groupOptions}
                                      selectChangeHandler={(value, name) => {
                                        wayPointGroupChangeHandler(
                                          value,
                                          name,
                                          i
                                        );
                                      }}
                                    />
                                  </Grid>
                                </Grid>
                                <Grid container spacing={4}>
                                  <Grid item xs={6}>
                                    <Fields.InputField
                                      fieldData={wp.transitHours}
                                      variant="outlined"
                                      inputChangeHandler={(value, name) => {
                                        waypointInputChangeHandler(
                                          value,
                                          name,
                                          i
                                        );
                                      }}
                                      endAdornment={
                                        <InputAdornment position="end">
                                          Hours
                                        </InputAdornment>
                                      }
                                    />
                                  </Grid>
                                  <Grid item xs={6}>
                                    <Fields.InputField
                                      fieldData={wp.transitMinutes}
                                      variant="outlined"
                                      inputChangeHandler={(value, name) => {
                                        waypointInputChangeHandler(
                                          value,
                                          name,
                                          i
                                        );
                                      }}
                                      endAdornment={
                                        <InputAdornment position="end">
                                          Mins
                                        </InputAdornment>
                                      }
                                    />
                                  </Grid>
                                </Grid>
                                <Grid container spacing={4}>
                                  <Grid item xs={6}>
                                    <Fields.InputField
                                      fieldData={wp.holdingHours}
                                      variant="outlined"
                                      inputChangeHandler={(value, name) => {
                                        waypointInputChangeHandler(
                                          value,
                                          name,
                                          i
                                        );
                                      }}
                                      endAdornment={
                                        <InputAdornment position="end">
                                          Hours
                                        </InputAdornment>
                                      }
                                    />
                                  </Grid>
                                  <Grid item xs={6}>
                                    <Fields.InputField
                                      fieldData={wp.holdingMinutes}
                                      variant="outlined"
                                      inputChangeHandler={(value, name) => {
                                        waypointInputChangeHandler(
                                          value,
                                          name,
                                          i
                                        );
                                      }}
                                      endAdornment={
                                        <InputAdornment position="end">
                                          Mins
                                        </InputAdornment>
                                      }
                                    />
                                  </Grid>
                                </Grid>
                                <Grid container spacing={4}>
                                  <Grid item xs={12}>
                                    <Fields.InputField
                                      fieldData={wp.mobileNumber1}
                                      variant="outlined"
                                      inputChangeHandler={(value, name) => {
                                        waypointInputChangeHandler(
                                          value,
                                          name,
                                          i
                                        );
                                      }}
                                    />
                                  </Grid>
                                </Grid>
                                <Grid container spacing={4}>
                                  <Grid item xs={12}>
                                    <Fields.InputField
                                      fieldData={wp.mobileNumber2}
                                      variant="outlined"
                                      inputChangeHandler={(value, name) => {
                                        waypointInputChangeHandler(
                                          value,
                                          name,
                                          i
                                        );
                                      }}
                                    />
                                  </Grid>
                                </Grid>
                                <Grid container spacing={4}>
                                  <Grid item xs={12}>
                                    <Fields.InputField
                                      fieldData={wp.mobileNumber3}
                                      variant="outlined"
                                      inputChangeHandler={(value, name) => {
                                        waypointInputChangeHandler(
                                          value,
                                          name,
                                          i
                                        );
                                      }}
                                    />
                                  </Grid>
                                </Grid>
                                <Grid container spacing={4}>
                                  {wayPointFields.length === i + 1 ? (
                                    <Grid item xs={3}>
                                      <Button
                                        variant="contained"
                                        color="primary"
                                        disableElevation
                                        style={{
                                          width: "100%",
                                          textTransform: "none",
                                        }}
                                        onClick={() => {
                                          const dataStatus =
                                            validateFields.validateFieldsData(
                                              wp
                                            );
                                          if (dataStatus.status) {
                                            addWayPointHandler(i);
                                          } else {
                                            let erroredFields = Object.keys(
                                              dataStatus.data
                                            ).filter(
                                              (key) =>
                                                wayPointFields[i][key]
                                                  .isValid === false
                                            );
                                            let newWayPointFields =
                                              _.cloneDeep(wayPointFields);
                                            newWayPointFields.splice(
                                              i,
                                              1,
                                              dataStatus.data
                                            );
                                            setWayPointFields(
                                              newWayPointFields
                                            );
                                            if (erroredFields.length > 0) {
                                              formContainerRef.current.scrollTop =
                                                wayPointFields[i][
                                                  erroredFields[0]
                                                ].ref.current.offsetTop - 150;
                                            }
                                          }
                                        }}
                                      >
                                        <AddIcon />
                                      </Button>
                                    </Grid>
                                  ) : (
                                    <Grid item xs={3}>
                                      <Button
                                        variant="contained"
                                        color="secondary"
                                        disableElevation
                                        style={{
                                          width: "100%",
                                          textTransform: "none",
                                          marginBottom: "12px",
                                        }}
                                        onClick={() => {
                                          wayPointRemoveHandler(i);
                                        }}
                                      >
                                        <RemoveIcon />
                                      </Button>
                                    </Grid>
                                  )}
                                </Grid>
                              </div>
                            );
                          })}
                        </div>
                      ) : (
                        <Button
                          variant="outlined"
                          onClick={() => {
                            let newWayPointFields = _.cloneDeep(wayPointFields);
                            newWayPointFields.push(wayPoints[0]);
                            setWayPointFields(newWayPointFields);
                          }}
                        >
                          Add Waypoints
                        </Button>
                      )}
                    </Grid>
                  </Grid>
                </fieldset>
                <fieldset
                  style={{ border: "1px solid black", borderRadius: "4px" }}
                >
                  <legend
                    style={{
                      fontSize: "15px",
                      width: "auto",
                      fontWeight: "bold",
                    }}
                  >
                    Drop Off
                  </legend>
                  <Grid container spacing={2}>
                    <Grid item xs={12} ref={locationFields.dropoffName.ref}>
                      <Fields.InputField
                        variant="outlined"
                        fieldData={locationFields.dropoffName}
                        inputChangeHandler={locInputChangeHandler}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={2}>
                    <Grid item xs={12} ref={locationFields.dropoffLocation.ref}>
                      <Fields.PlaceSearchField
                        fieldData={locationFields.dropoffLocation}
                        variant="outlined"
                        setLocationHandler={setLocationHandler}
                        emptyLocHandler={locFieldsHandler}
                        ref={placeRef3}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <Fields.AntMultiSelectField
                        options={groupOptions}
                        fieldData={locationFields.dropoffLocationGroups}
                        selectChangeHandler={locGroupChangeHandler}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={2}>
                    <Grid item xs={6}>
                      <Fields.InputField
                        fieldData={locationFields.dropHoldingTimeHours}
                        variant="outlined"
                        inputChangeHandler={locInputChangeHandler}
                        endAdornment={
                          <InputAdornment position="end">Hours</InputAdornment>
                        }
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Fields.InputField
                        fieldData={locationFields.dropHoldingTimeMinutes}
                        variant="outlined"
                        inputChangeHandler={locInputChangeHandler}
                        endAdornment={
                          <InputAdornment position="end">Mins</InputAdornment>
                        }
                      />
                    </Grid>
                  </Grid>
                </fieldset>
                <Grid
                  container
                  spacing={2}
                  className="details_container_content"
                >
                  <Grid item xs={12}>
                    <Button
                      className={classes.waypoint_button}
                      onClick={() => {
                        setMoreInfoOpen(true);
                      }}
                    >
                      Add Fixed Route Details
                    </Button>
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={2}
                  className="details_container_content"
                >
                  <Grid item xs={6}></Grid>
                  <Grid item xs={6}>
                    <Grid container spacing={2}>
                      <Grid item xs={6}>
                        <Button
                          className="cancel_button"
                          onClick={() => {
                            history.push({
                              pathname: "./contractRoutes",
                              state: {
                                clientId: props.location.state.clientId,
                                clientName: props.location.state.clientName,
                                contractId: props.location.state.contractId,
                                contractName: props.location.state.contractName
                              },
                            });
                          }}
                        >
                          Cancel
                        </Button>
                      </Grid>
                      <Grid item xs={6}>
                        <Button
                          className="save_button"
                          onClick={() => {
                            addRouteHandler();
                          }}
                          startIcon={
                            spinner ? (
                              <CircularProgress size={20} color={"#fff"} />
                            ) : null
                          }
                        >
                          Submit
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Grid>
            <Grid
              item
              xs={8}
              style={{ height: "calc(100vh - 178px)", overflowY: "auto" }}
            >
              <div className={classes.map_header}>
                <span>
                  Pickup Location :{" "}
                  <img src={blueMarkerImage} width={20} height={20} />
                </span>
                <span>
                  Waypoint Location :{" "}
                  <img src={orangeMarkerImage} width={20} height={20} />
                </span>
                <span>
                  Dropoff Location :{" "}
                  <img src={redMarkerImage} width={20} height={20} />
                </span>
              </div>
              <Map
                ref={mapRef}
                pickupLocation={locationFields.pickupLocation}
                dropoffLocation={locationFields.dropoffLocation}
                zoom={zoom}
                waypoints={[...wayPointFields]}
                polyLine={[...polyLine]}
                setPickup={setPickup}
                setDropoff={setDropoff}
                setWp={setWp}
                height={"calc(100vh - 252px)"}
              />
            </Grid>
          </Grid>
        </div>
      </div>
    </div>
  );
};

export default AddRoute;
