import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import * as Components from "../../../../sharedComponents";
const useStyles = makeStyles(() => ({
  map: {
    height: "calc(100vh - 80px)",
    border: "1.5px solid #000",
  },
}));
const RouteMap = (props) => {
  const classes = useStyles();
  return (
    <div className={classes.map}>
      <Components.Map
        pickupLocation={props.pickupLocation}
        dropoffLocation={props.dropoffLocation}
        zoom={props.zoom}
      />
    </div>
  );
};
export default React.memo(RouteMap);
