import React, { useState } from "react";
import {
  Dialog,
  DialogTitle,
  makeStyles,
  IconButton,
  Grid,
  Button,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import _ from "lodash";
import PropTypes from "prop-types";
import * as Fields from "../../../../sharedComponents";

const useStyles = makeStyles((theme) => ({
  dialog: {
    display: "flex",
    flexgrow: "1",
    fontWeight: "bold",
    justifyContent: "center",
    margin: "0px 100px ",
  },
  cancel_button: {
    backgroundColor: "#707070",
    textTransform: "none",
    margin: "0px 0px 10px 10px",
  },
  dialog_container: {
    padding: "0px 20px",
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  form_item: {
    marginBottom: "15px",
  },
  center_align: {
    textAlign: "center",
  },
}));

function SimpleDialog(props) {
  const classes = useStyles();
  const formFields = {
    transitTime: {
      name: "transitTime",
      label: "Transit Time",
      value: "",
      topLabel: true,
      type: "number",
      min: 1
    },
    holdingTime: {
      name: "holdingTime",
      label: "Holding Time",
      value: "",
      topLabel: true,
      type: "number",
      min: 1
    },
  };
  const { onClose, open, onOk, wayPointIndex, waypointInputChangeHandler } = props;
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const handleClose = () => {
    onClose();
  };
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    setFields(newFields);
    waypointInputChangeHandler(value, name, wayPointIndex)
  };
  return (
    <div>
      <Dialog
        aria-labelledby="simple-dialog-title"
        open={open}
        onClose={handleClose}
      >
        <div className={classes.dialog_container}>
          <div className={classes.dialog}>
            <DialogTitle id="simple-dialog-title">Add Waypoint</DialogTitle>
            {onClose ? (
              <IconButton
                aria-label="close"
                className={classes.closeButton}
                onClick={onClose}
              >
                <CloseIcon />
              </IconButton>
            ) : null}
          </div>
          <Grid container spacing={3} className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.transitTime}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.form_item}>
            <Grid item xs={12}>
              <Fields.InputField
                fieldData={fields.holdingTime}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.form_item}>
            <Grid item xs={12} className={classes.center_align}>
              <Button
                variant="contained"
                color="primary"
                disableElevation
                style={{ marginRight: "10px" }}
                onClick={onOk}
              >
                Ok
              </Button>
              <Button
                variant="contained"
                color="secondary"
                disableElevation
                onClick={onClose}
              >
                cancel
              </Button>
            </Grid>
          </Grid>
        </div>
      </Dialog>
    </div>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  onOk: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  wayPointIndex: PropTypes.number.isRequired,
  waypointInputChangeHandler: PropTypes.func.isRequired
};

const AddWaypoint = (props) => {
  return (
    <SimpleDialog
      open={props.open}
      onClose={props.handleClose}
      onOk={props.addWayPointHandler}
      wayPointIndex={props.wayPointIndex}
      waypointInputChangeHandler={props.waypointInputChangeHandler}
    />
  );
};

export default AddWaypoint;
