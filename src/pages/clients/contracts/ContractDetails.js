import React, { useState, useEffect, useRef } from "react";
import { Typography, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import Header from "../../../components/header";
import CircularLoading from "../../../components/loader/circularLoading";
import AlertMessage from "../../../components/alertmessage/AlertMessage";
import * as CONFIG from "../../../config/GlobalConstants";
import * as Components from "../../../sharedComponents";
import "../../../common.css";
import _ from "lodash";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Util from "../../../utils";

const useStyles = makeStyles((theme) => ({
  alertBox: {
    padding: "10px 0px",
  },
}));

var Service;
const ContractDetails = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const [loader, setLoader] = useState(false);
  const [contractData, setContractData] = useState();
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  const [percent, setPercent] = useState();
  const uploadRef = useRef();
  Service = global.service;
  let util = new Util();
  useEffect(() => {
    window.scrollTo({ top: 0 });
    if (props.location.state) {
      setLoader(true);
      Service.getContract({ contractId: props.location.state.contractId })
        .then((res) => {
          let contractData = { ...res, attachments: res.attachments.attachments };
          setContractData(contractData);
          setLoader(false);
        })
        .catch(() => {
          setLoader(false);
        });
    } else {
      global.session.remove("BearerToken");
      global.session.remove("profile");
      global.session.remove("hideHeader");
      window.location = "/";
    }
  }, []);
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  const uploadHandler = (uploadedFile) => {
    const formData = new FormData();
    formData.append("file", uploadedFile[0]);
    const config = {
      onUploadProgress: function (progressEvent) {
        var percentCompleted = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total
        );
        setPercent(percentCompleted);
      },
    };
    Service.uploadContractDocument(
      formData,
      { id: props.location.state.contractId },
      config
    )
      .then(() => {
        uploadRef.current.addAttachment();
        setAlertData({
          open: true,
          severity: CONFIG.ALERT_SEVERITY.success,
          message: "File Uploaded successfully!",
        });
      })
      .catch((error) => {
        setAlertData({
          open: true,
          severity: CONFIG.ALERT_SEVERITY.error,
          message: error.response && error.response.data ? error.response.data.message : "Something went wrong!",
        });
      });
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.push({
                  pathname: "./clientContracts",
                  state: { clientId: props.location.state.clientId, clientName: props.location.state.clientName },
                });
              }}
            />
            Contract Details
          </Typography>
        </div>
        {loader ? (
          <CircularLoading />
        ) : contractData ? (
          <div>
            <div className="form_container">
              <div className="details_container">
                <Typography className="details_container_heading">
                  Contract Information
                </Typography>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography gutterBottom variant="body2" color="textSecondary">
                          System Generated Code
                        </Typography>
                        <Typography variant="h6" component="div">
                          {contractData.contract.systemGeneratedContractCode}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  {contractData.contract.onReceipt ? null : (
                    <Grid item xs={3}>
                      <Card className="custom_card">
                        <CardContent>
                          <Typography gutterBottom variant="body2" color="textSecondary">
                            Payment Terms In Days
                          </Typography>
                          <Typography variant="h6" component="div">
                            {contractData.contract.paymentTermsInDays}
                          </Typography>
                        </CardContent>
                      </Card>
                    </Grid>
                  )}
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography gutterBottom variant="body2" color="textSecondary">
                          From Date
                        </Typography>
                        <Typography variant="h6" component="div">
                          {util.formatDate(contractData.contract.fromDate)}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography gutterBottom variant="body2" color="textSecondary">
                          To Date
                        </Typography>
                        <Typography variant="h6" component="div">
                          {util.formatDate(contractData.contract.toDate)}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={6}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary">
                          Additional Notes
                        </Typography>
                        <Typography variant="h6" component="div">
                          {contractData.contract.notes ? contractData.contract.notes : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </Grid>

              </div>
            </div>
            <div className="form_container">
              <div className="details_container">
                {alertData.open ? (
                  <div className={classes.alertBox}>
                    <AlertMessage
                      severity={alertData.severity}
                      message={alertData.message}
                      closeAlert={closeAlert}
                    />
                  </div>
                ) : null}
                <Typography className="details_container_heading">
                  Documents
                </Typography>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Components.UploadDocuments
                    ref={uploadRef}
                    uploadHandler={uploadHandler}
                    data={contractData}
                    percentCompleted={percent}
                    label={"Upload max 5 documents"}
                    max={5}
                    viewFile={true}
                    getAttachments={Service.getContractDocuments}
                    item={contractData.contract}
                  />
                </Grid>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default ContractDetails;
