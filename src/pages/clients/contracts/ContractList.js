import React, { useState, useEffect } from "react";
import Header from "../../../components/header";
import { useHistory } from "react-router-dom";
import { ArrowBack } from "@material-ui/icons";
import { Typography } from "@material-ui/core";
import "../../../common.css";
import { Button } from "@material-ui/core";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import { makeStyles } from "@material-ui/core";
import { Grid } from "@material-ui/core";
import _ from "lodash";
import * as Fields from "../../../sharedComponents/index";
import * as Components from "../../../sharedComponents";
import * as CONFIG from "../../../config/GlobalConstants";
import CloseIcon from "@material-ui/icons/Close";
import CircularLoading from "../../../components/loader/circularLoading";
import AlertMessage from "../../../components/alertmessage/AlertMessage";
import Util from "../../../utils";
var Service;
const headRows = [
  {
    id: "	CustomerContractRefNo",
    disablePadding: true,
    label: "CUSTOMER CONTRACT REF #",
  },
  {
    id: "Systemgeneratedcode",
    disablePadding: true,
    label: "SYSTEM GENERATED CODE",
  },
  { id: "FromDate", disablePadding: true, label: "FROM DATE" },
  { id: "ToDate", disablePadding: true, label: "TO DATE" },
  {
    id: "PaymentTermsInDays",
    disablePadding: true,
    label: "PAYMENT TERMS IN DAYS",
  },
  { id: "actions", disablePadding: true, label: "" },
];
const useStyles = makeStyles((theme) => ({
  header_buttons: {
    padding: "0px 15px",
  },
  alertBox: {
    padding: "10px 20px",
  },
  contract_btn: {
    background: "#649B42",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    "&:hover": {
      background: "#649B42",
    },
  },
  filter_buttons: {
    textAlign: "right",
    padding: "0px 15px",
  },
  clear_button: {
    padding: "6px 15px",
    background: "#D3710F0D",
    border: "1px solid #D3710F",
    color: "#D3710F",
  },
}));
const ContractList = (props) => {
  const history = useHistory();
  const classes = useStyles();
  Service = global.service;
  // const filterFields = {
  //   contract: { label: "Search by Name", name: "contract", value: "", options: [] },
  //   date: { label: "Select date", name: "date", value: "" },
  // };
  // const [filters, setFilters] = useState(_.cloneDeep(filterFields));
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  const [total, setTotal] = useState(0);
  const [loader, setLoader] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  // const autoCompleteChangeHandler = () => { };
  // const dateChangeHandler = () => { };
  let util = new Util();
  useEffect(() => {
    window.scrollTo({ top: 0 });
    if (props.location.state) {
      let dataObj = {
        skip: pagination.current * pagination.pageSize,
        limit: pagination.pageSize,
        clientId: props.location.state.clientId,
      };
      renderContracts(dataObj);
    } else {
      global.session.remove("BearerToken");
      global.session.remove("profile");
      global.session.remove("hideHeader");
      window.location = "/";
    }
  }, []);
  const renderContracts = (dataObj) => {
    setLoader(true);
    Service.getContracts(dataObj)
      .then((res) => {
        let data = res.contracts.map((contract) => {
          return {
            CustomerContractRefNo: (
              <div style={{ textAlign: "center" }}>
                {" "}
                {contract.customerContractReferenceNumber
                  ? contract.customerContractReferenceNumber
                  : "--"}
              </div>
            ),
            Systemgeneratedcode: (
              <div style={{ textAlign: "center" }}>
                {contract.systemGeneratedContractCode}
              </div>
            ),
            FromDate: (
              <div style={{ textAlign: "center" }}>
                {util.formatDate(contract.fromDate)}
              </div>
            ),
            ToDate: (
              <div style={{ textAlign: "center" }}>
                {util.formatDate(contract.toDate)}{" "}
              </div>
            ),
            PaymentTermsInDays: (
              <div style={{ textAlign: "center" }}>
                {contract.paymentTermsInDays
                  ? contract.paymentTermsInDays
                  : "--"}
              </div>
            ),
            actions: (
              <div style={{ textAlign: "center" }}>
                <Components.DropDownMenu
                  options={CONFIG.CONTRACT_MORE_OPTIONS}
                  data={{
                    contractId: contract.id,
                    clientId: props.location.state.clientId,
                    clientName: props.location.state.clientName,
                    contractName: contract.customerContractReferenceNumber
                  }}
                />
              </div>
            ),
          };
        });
        setTableData(data);
        setTotal(res.totalCount);
        setLoader(false);
      })
      .catch((error) => {
        setLoader(false);
        setAlertData({
          open: true,
          severity: CONFIG.ALERT_SEVERITY.error,
          message:
            error.response && error.response.data
              ? error.response.data.message
              : "Something went wrong!",
        });
      });
  };
  const pageChangeHandler = (page) => {
    let dataObj = {
      skip: page * pagination.pageSize,
      limit: pagination.pageSize,
      clientId: props.location.state.clientId,
    };
    renderContracts(dataObj);
    setPagination({ ...pagination, current: page });
  };
  const rowsPerPageChangeHandler = (rowsPerPage) => {
    let dataObj = {
      skip: pagination.current * rowsPerPage,
      limit: rowsPerPage,
      clientId: props.location.state.clientId,
    };
    renderContracts(dataObj);
    setPagination({ ...pagination, pageSize: rowsPerPage });
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.push("./clients");
              }}
            />
            Contracts ( Client Name: {props.location.state.clientName} )
          </Typography>
          <div className={classes.header_buttons}>
            <Button
              variant="contained"
              color="primary"
              startIcon={<AddCircleOutlinedIcon />}
              className={classes.contract_btn}
              onClick={() =>
                history.push({
                  pathname: "./addContract",
                  state: { clientId: props.location.state.clientId, clientName: props.location.state.clientName },
                })
              }
            >
              Add Contract
            </Button>
          </div>
        </div>
        {/* <div className="filter_box">
          <Grid container spacing={3}>
            <Grid item xs={9}>
              <Grid container spacing={2}>
                <Grid item xs={3}>
                  <Fields.AutoCompleteField
                    fieldData={filters.contract}
                    autoCompleteChangeHandler={autoCompleteChangeHandler}
                    className="filter_field"
                  />
                </Grid>
                <Grid item xs={3}>
                  <Fields.DateField
                    fieldData={filters.date}
                    dateChangeHandler={dateChangeHandler}
                    variant="outlined"
                    className="filter_field"
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={3}>
              <div className={classes.filter_buttons}>
                <Button
                  className={classes.clear_button}
                  startIcon={<CloseIcon />}
                >
                  Clear
                </Button>
              </div>
            </Grid>
          </Grid>
        </div> */}
        <div>
          {alertData.open ? (
            <div className={classes.alertBox}>
              <AlertMessage
                severity={alertData.severity}
                message={alertData.message}
                closeAlert={closeAlert}
              />
            </div>
          ) : null}
          {loader ? (
            <CircularLoading />
          ) : (
            <Components.DataTable
              headRows={headRows}
              tableData={tableData}
              pagination={pagination}
              pageChangeHandler={pageChangeHandler}
              rowsPerPageChangeHandler={rowsPerPageChangeHandler}
              total={total}
            />
          )}
        </div>
      </div>
    </div>
  );
};
export default ContractList;
