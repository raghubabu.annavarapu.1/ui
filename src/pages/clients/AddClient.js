import React, { useState, useEffect, useRef } from "react";
import Header from "../../components/header";
import { Typography, Grid, Button, CircularProgress } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import ValidateFields from "../../validations/validateFields";
import AlertMessage from "../../components/alertmessage/AlertMessage";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import * as Fields from "../../sharedComponents";
import * as CONFIG from "../../config/GlobalConstants";
import CircularLoading from "../../components/loader/circularLoading";
import rawCountries from "../../config/rawCountries";
import _ from "lodash";
import "../../common.css";
import Snackbar from "@mui/material/Snackbar";
const useStyles = makeStyles((theme) => ({
  alertBox: {
    padding: "10px 12px",
  },
  input_adorment: { padding: "0px 15px 0px 2px" },
}));

var Service;
const AddClient = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const customDetailFields = [
    {
      detailType: {
        name: "detailType",
        label: "Detail",
        value: { label: "", value: "" },
        topLabel: true,
        options: CONFIG.CUSTOM_CLIENT_DETAILS,
        validationRequired: false,
        isValid: true,
        errorMsg: "Please select detail type",
        allowClear: true,
        ref: useRef(),
      },
      detailValue: {
        name: "detailValue",
        label: "Value",
        value: "",
        validationRequired: false,
        isValid: true,
        topLabel: true,
        errorMsg: "Please enter detail value",
        ref: useRef(),
      },
    },
  ];
  const formFields = {
    clientAliasCode: {
      name: "clientAliasCode",
      label: "Client Alias Code",
      value: "",
      topLabel: true,
      validationRequired: false,
      maxLength: 30,
      validPattern: "SPECIAL_CHARS_DESC",
      isValid: true,
      ref: useRef(),
    },
    contactFirstName: {
      name: "contactFirstName",
      label: "First Name",
      value: "",
      topLabel: true,
      maxLength: 60,
      validationRequired: true,
      validPattern: "SPECIAL_CHARS_DESC",
      isValid: true,
      errorMsg: "Please enter contact first name",
      ref: useRef(),
    },
    contactLastName: {
      name: "contactLastName",
      label: "Last Name",
      value: "",
      topLabel: true,
      maxLength: 60,
      validationRequired: true,
      validPattern: "SPECIAL_CHARS_DESC",
      isValid: true,
      errorMsg: "Please enter contact last name",
      ref: useRef(),
    },
    contactMobile: {
      name: "contactMobile",
      label: "Mobile Number",
      value: "",
      topLabel: true,
      validationRequired: true,
      validPattern: "MOBILE_NUMBER",
      isValid: true,
      errorMsg: "Please enter contact mobile number",
      ref: useRef(),
    },
    contactEmailAddress: {
      name: "contactEmailAddress",
      label: "Email ID",
      value: "",
      topLabel: true,
      validationRequired: true,
      validPattern: "EMAIL",
      isValid: true,
      errorMsg: "Please enter contact email id",
      ref: useRef(),
    },
    companyName: {
      name: "companyName",
      label: "Company Name",
      value: "",
      topLabel: true,
      maxLength: 120,
      validationRequired: true,
      validPattern: "SPECIAL_CHARS_DESC",
      isValid: true,
      errorMsg: "Please enter company name",
      ref: useRef(),
    },
    clientType: {
      name: "clientType",
      label: "Client Type",
      value: { value: "", label: "" },
      options: CONFIG.CLIENT_TYPE_OPTIONS,
      topLabel: true,
      validationRequired: true,
      validPattern: "SPECIAL_CHARS_DESC",
      isValid: true,
      errorMsg: "Please enter client type",
      ref: useRef(),
    },
    gstType: {
      name: "gstType",
      label: "GST Type",
      value: { value: "", label: "" },
      options: CONFIG.GST_TYPES,
      topLabel: true,
      validationRequired: false,
      isValid: true,
      allowClear: true,
      ref: useRef(),
      errorMsg: "Please select gst type",
    },
    gst: {
      name: "gst",
      label: "GST Number",
      value: "",
      topLabel: true,
      validationRequired: false,
      validPattern: "GST",
      isValid: true,
      maxLength: 15,
      errorMsg: "Please enter gst",
      ref: useRef(),
    },
    openingBalance: {
      name: "openingBalance",
      label: "Opening Balance",
      value: "",
      topLabel: true,
      type: "number",
      validationRequired: false,
      validPattern: "NUMERIC",
      isValid: true,
      min: 0,
      ref: useRef(),
    },
    status: {
      name: "status",
      label: "Status",
      value: CONFIG.STATUSES[0],
      options: CONFIG.STATUSES,
      validationRequired: true,
      isValid: true,
      ref: useRef(),
      topLabel: true,
      validPattern: "SPECIAL_CHARS_DESC",
      errorMsg: "Please select status",
    },
    notes: {
      name: "notes",
      label: "Notes",
      value: "",
      topLabel: true,
      maxLength: 300,
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: false,
      isValid: true,
      ref: useRef(),
    },
    addressLine1: {
      name: "addressLine1",
      label: "Address Line 1",
      value: "",
      topLabel: true,
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: true,
      isValid: true,
      errorMsg: "Please enter addressline1",
      ref: useRef(),
    },
    addressLine2: {
      name: "addressLine2",
      label: "Address Line 2",
      value: "",
      topLabel: true,
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: false,
      isValid: true,
      ref: useRef(),
    },
    addressLine3: {
      name: "addressLine3",
      label: "Address Line 3",
      value: "",
      topLabel: true,
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: false,
      isValid: true,
      ref: useRef(),
    },
    faxNo: {
      name: "faxNo",
      label: "Fax No",
      value: "",
      topLabel: true,
      validationRequired: false,
      isValid: true,
      ref: useRef(),
    },
    city: {
      name: "city",
      label: "City",
      value: "",
      topLabel: true,
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: true,
      isValid: true,
      errorMsg: "Please enter city",
      ref: useRef(),
    },
    state: {
      name: "state",
      label: "State",
      value: "",
      topLabel: true,
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: true,
      isValid: true,
      errorMsg: "Please enter state",
      ref: useRef(),
    },
    country: {
      name: "country",
      label: "Country",
      value: "India",
      topLabel: true,
      validPattern: "SPECIAL_CHARS_DESC",
      validationRequired: true,
      isValid: true,
      errorMsg: "Please enter country",
      ref: useRef(),
    },
    postalCode: {
      name: "postalCode",
      label: "Postal Code",
      value: "",
      topLabel: true,
      validPattern: "POSTAL_CODE",
      validationRequired: true,
      isValid: true,
      errorMsg: "Please enter postal code",
      ref: useRef(),
    },
  };
  Service = global.service;
  let validateFields = new ValidateFields();
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const [customFields, setCustomFields] = useState(
    _.cloneDeep(customDetailFields)
  );
  const [customTypeFields, setCustomTypeFields] = useState(
    _.cloneDeep(customDetailFields)
  );
  const [spinner, setSpinner] = useState(false);
  const [loader, setLoader] = useState(false);
  const [clientData, setClientData] = useState();
  const [countryCode, setCountryCode] = useState({
    countryCode: "in",
    dialCode: "91",
  });
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  const [percent, setPercent] = useState();
  const [addAddress, setAddAddress] = useState(false);
  const [snack, setSnack] = useState({ open: false, message: "" });
  const uploadRef = useRef();
  useEffect(() => {
    window.scrollTo({ top: 0 });
    if (props.location.state) {
      setLoader(true);
      Service.getClient({ clientId: props.location.state.clientId })
        .then((res) => {
          let country = rawCountries.filter(
            (cntry) => cntry[2] === res.clientDetails.countryCode.toLowerCase()
          );
          setClientData(res);
          let newFields = _.cloneDeep(fields);
          let newCustomFields = _.cloneDeep(customFields);
          let customDetails = Object.entries(
            res.clientDetails.customDetails
          ).filter((item) => item[1]);
          newCustomFields =
            customDetails.length > 0
              ? customDetails.map((item) => {
                  return {
                    detailType: {
                      name: "detailType",
                      label: "Detail",
                      topLabel: true,
                      options: CONFIG.CUSTOM_CLIENT_DETAILS,
                      value: CONFIG.CUSTOM_CLIENT_DETAILS.filter(
                        (rec) => rec.value === item[0]
                      )[0],
                    },
                    detailValue: {
                      name: "detailValue",
                      label: "Value",
                      topLabel: true,
                      value: item[1],
                      maxLength: 16,
                    },
                  };
                })
              : _.cloneDeep(customDetailFields);
          if (country.length > 0) {
            setCountryCode({
              countryCode: country[0][2],
              dialCode: country[0][3],
            });
            newFields["contactMobile"][
              "value"
            ] = `${country[0][3]}${res.clientDetails.contactMobile}`;
          }
          newFields["contactFirstName"]["value"] =
            res.clientDetails.contactFirstName;
          newFields["contactLastName"]["value"] =
            res.clientDetails.contactLastName;
          newFields["contactEmailAddress"]["value"] =
            res.clientDetails.contactEmailAddress;
          newFields["companyName"]["value"] = res.clientDetails.companyName;
          newFields["clientAliasCode"]["value"] =
            res.clientDetails.clientAliasCode;
          newFields["clientType"]["value"] = CONFIG.CLIENT_TYPE_OPTIONS.filter(
            (item) => item.value === res.clientDetails.clientType
          )[0];
          newFields["openingBalance"]["value"] =
            res.clientDetails.openingBalance;
          newFields["gstType"]["value"] = res.clientDetails.gstType
            ? CONFIG.GST_TYPES.filter(
                (item) => item.value === res.clientDetails.gstType
              )[0]
            : { label: "", value: "" };
          newFields["notes"]["value"] = res.clientDetails.notes;
          newFields["status"]["value"] = CONFIG.STATUSES.filter(
            (item) => Number(item.value) === Number(res.clientDetails.status)
          )[0];
          newFields["gst"]["value"] = res.clientDetails.gst
            ? res.clientDetails.gst
            : "";
          if (res.addresses.length > 0) {
            newFields["addressLine1"]["value"] = res.addresses[0].addressLine1;
            newFields["addressLine2"]["value"] = res.addresses[0].addressLine2;
            newFields["addressLine3"]["value"] = res.addresses[0].addressLine3;
            newFields["faxNo"]["value"] = res.addresses[0].faxNo;
            newFields["postalCode"]["value"] = res.addresses[0].postalCode;
            newFields["city"]["value"] = res.addresses[0].city;
            newFields["state"]["value"] = res.addresses[0].state;
            newFields["country"]["value"] = res.addresses[0].country;
          }
          setFields(newFields);
          setCustomFields(newCustomFields);
          setLoader(false);
        })
        .catch((error) => {
          setLoader(false);
        });
    }
  }, []);
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    newFields[name]["isValid"] = true;
    if (value.length === 0 && newFields[name]["validationRequired"]) {
      newFields[name]["isValid"] = false;
    }
    setFields(newFields);
  };
  const phoneChangeHandler = (country, value, name) => {
    let newFields = _.cloneDeep(fields);
    let newCountryCode = _.cloneDeep(countryCode);
    newCountryCode = {
      countryCode: country.countryCode,
      dialCode: country.dialCode,
    };
    newFields[name]["value"] = value;
    newFields[name]["isValid"] = true;
    if (value.length === 0 && newFields[name]["validationRequired"]) {
      newFields[name]["isValid"] = false;
    }
    setCountryCode(newCountryCode);
    setFields(newFields);
  };
  const autoCompleteChangeHandler = (value, name) => {
    if (value) {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = value;
      newFields[name]["isValid"] = true;
      setFields(newFields);
    } else {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = { value: "", label: "" };
      if (newFields[name]["validationRequired"]) {
        newFields[name]["isValid"] = false;
      }
      setFields(newFields);
    }
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  const validateGST = () => {
    var status = true;
    const newFields = _.cloneDeep(fields);
    if (
      newFields["gst"]["value"].length > 0 &&
      newFields["gstType"]["value"].value
    ) {
      let re = /^[A-Z0-9]{15}$/;
      newFields["gstType"]["isValid"] = true;
      if (re.test(newFields["gst"]["value"])) {
        newFields["gst"]["isValid"] = true;
        newFields["gst"]["errorMsg"] = "Please enter gst";
        status = true;
      } else {
        newFields["gst"]["isValid"] = false;
        newFields["gst"]["errorMsg"] = "Please enter valid gst";
        status = false;
      }
    } else if (
      newFields["gst"]["value"].length === 0 &&
      !newFields["gstType"]["value"].value
    ) {
      newFields["gstType"]["isValid"] = true;
      newFields["gst"]["isValid"] = true;
      status = true;
    } else {
      if (newFields["gst"]["value"].length === 0) {
        newFields["gst"]["isValid"] = false;
        newFields["gst"]["errorMsg"] = "Please enter gst";
      } else if (!newFields["gstType"]["value"].value) {
        newFields["gstType"]["isValid"] = false;
      }
      status = false;
    }
    setFields(newFields);
    return { status: status, data: _.cloneDeep(newFields) };
  };
  const validateCustomDetails = () => {
    let newCustomFields = _.cloneDeep(customFields);
    let status = [];
    newCustomFields.forEach((field, i) => {
      if (
        newCustomFields[i]["detailValue"]["value"].length > 0 &&
        newCustomFields[i]["detailType"]["value"].value
      ) {
        let customDetail = CONFIG.CUSTOM_CLIENT_DETAILS.filter(
          (item) =>
            item.value === newCustomFields[i]["detailType"]["value"].value
        );
        newCustomFields[i]["detailType"]["isValid"] = true;
        if (
          customDetail[0].re.test(newCustomFields[i]["detailValue"]["value"])
        ) {
          newCustomFields[i]["detailValue"]["isValid"] = true;
          newCustomFields[i]["detailValue"]["errorMsg"] = "Please enter value";
          status.push(true);
        } else {
          newCustomFields[i]["detailValue"]["isValid"] = false;
          newCustomFields[i]["detailValue"]["errorMsg"] =
            "Please enter valid value";
          status.push(false);
        }
      } else if (
        newCustomFields[i]["detailValue"]["value"].length === 0 &&
        !newCustomFields[i]["detailType"]["value"].value
      ) {
        newCustomFields[i]["detailType"]["isValid"] = true;
        newCustomFields[i]["detailValue"]["isValid"] = true;
        status.push(true);
      } else {
        if (newCustomFields[i]["detailValue"]["value"].length === 0) {
          newCustomFields[i]["detailValue"]["isValid"] = false;
          newCustomFields[i]["detailValue"]["errorMsg"] = "Please enter value";
        } else if (!newCustomFields[i]["detailType"]["value"].value) {
          newCustomFields[i]["detailType"]["isValid"] = false;
        }
        status.push(false);
      }
    });
    return {
      status: status.filter((item) => !item).length > 0 ? false : true,
      data: _.cloneDeep(newCustomFields),
    };
  };
  const uploadHandler = (uploadedFile) => {
    const formData = new FormData();
    formData.append("file", uploadedFile[0]);
    const config = {
      onUploadProgress: function (progressEvent) {
        var percentCompleted = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total
        );
        setPercent(percentCompleted);
      },
    };
    Service.uploadClientDocument(
      formData,
      { id: props.location.state.clientId },
      config
    )
      .then((res) => {
        uploadRef.current.addAttachment();
        // setAlertData({
        //   open: true,
        //   severity: CONFIG.ALERT_SEVERITY.success,
        //   message: "File Uploaded successfully!",
        // });
        let newSnack = _.cloneDeep(snack);
        newSnack.open = true;
        newSnack.message = "File Uploaded successfully!";
        setSnack(newSnack);
      })
      .catch((error) => {
        setAlertData({
          open: true,
          severity: CONFIG.ALERT_SEVERITY.error,
          message:
            error.response && error.response.data
              ? error.response.data.message
              : "Something went wrong!",
        });
      });
  };
  const addClientHandler = () => {
    let dataObj = _.cloneDeep({
      ...fields,
      clientType: {
        ...fields.clientType,
        value:
          fields.clientType.value.value === undefined
            ? fields.clientType.value
            : fields.clientType.value.value,
      },
      gstType: {
        ...fields.gstType,
        value:
          fields.gstType.value.value === undefined
            ? fields.gstType.value
            : fields.gstType.value.value,
      },
      status: {
        ...fields.status,
        value:
          fields.status.value.value === undefined
            ? fields.status.value
            : fields.status.value.value,
      },
      contactMobile: {
        ...fields.contactMobile,
        value: fields.contactMobile.value.replace(countryCode.dialCode, ""),
      },
    });
    let dataStatus;
    if (!addAddress) {
      var { addressLine1, city, state, country, postalCode, ...clientRest } =
        dataObj;
      dataStatus = validateFields.validateFieldsData(clientRest);
    } else {
      dataStatus = validateFields.validateFieldsData(dataObj);
    }
    // const validPanGst = validatePanGst();
    const validGST = validateGST();
    const validCustomDetails = validateCustomDetails();
    if (dataStatus.status && validGST.status && validCustomDetails.status) {
      setSpinner(true);
      dataObj = {
        ...dataObj,
        countryCode: countryCode.countryCode.toUpperCase(),
        contactMobile: fields.contactMobile.value.replace(
          countryCode.dialCode,
          ""
        ),
        customDetails: [...customFields],
      };
      Service.createClient(dataObj, addAddress)
        .then((res) => {
          let newFields = _.cloneDeep(formFields);
          setFields(newFields);
          setCustomFields(_.cloneDeep(customDetailFields));
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.success,
            message: "Client created successfully!",
          });
          setSpinner(false);
          window.scrollTo({ top: 0, behavior: "smooth" });
        })
        .catch((error) => {
          setSpinner(false);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
          window.scrollTo({ top: 0, behavior: "smooth" });
        });
    } else {
      let newFields = _.cloneDeep(fields);
      let newCustomFields = _.cloneDeep(customFields);
      let customFieldErrors = [];
      let erroredFields = Object.keys(dataStatus.data).filter(
        (key, i) => dataStatus.data[key].isValid === false
      );
      if (erroredFields.length === 0) {
        erroredFields = Object.keys(validGST.data).filter(
          (key, i) => validGST.data[key].isValid === false
        );
      }
      if (erroredFields.length === 0) {
        customFieldErrors = validCustomDetails.data.map((item, i) => {
          let errField = Object.keys(item).filter(
            (key, j) => item[key].isValid === false
          );
          if (errField.length > 0) {
            return { key: errField[0], index: i };
          }
        });
        customFieldErrors = customFieldErrors.filter((item) => item);
      }
      newCustomFields = [...validCustomDetails.data];
      newFields = {
        ...fields,
        ...dataStatus.data,
        clientType: {
          ...dataStatus.data.clientType,
          value: dataStatus.data.clientType.value
            ? newFields.clientType.options.filter(
                (opt) => opt.value === dataStatus.data.clientType.value
              )[0]
            : { label: "", value: "" },
        },
        gstType: {
          ...(validGST.status
            ? dataStatus.data.gstType
            : validGST.data.gstType),
          value: dataStatus.data.gstType.value
            ? newFields.gstType.options.filter(
                (opt) => opt.value === dataStatus.data.gstType.value
              )[0]
            : { label: "", value: "" },
        },
        status: {
          ...dataStatus.data.status,
          value: dataStatus.data.status.value
            ? CONFIG.STATUSES.filter(
                (opt) => opt.value === dataStatus.data.status.value
              )[0]
            : { label: "", value: "" },
        },
        contactMobile: {
          ...dataStatus.data.contactMobile,
          value: countryCode.dialCode + dataStatus.data.contactMobile.value,
        },
      };
      newFields = {
        ...newFields,
        gst: validGST.data.gst,
      };
      setFields(newFields);
      setCustomFields(newCustomFields);
      if (erroredFields.length > 0) {
        window.scrollTo(0, fields[erroredFields[0]].ref.current.offsetTop);
      }
      if (erroredFields.length === 0 && customFieldErrors.length > 0) {
        window.scrollTo(0, customDetailsRef.current.offsetTop - 450);
      }
    }
  };
  const updateClientHandler = () => {
    let dataObj = _.cloneDeep({
      ...fields,
      clientType: {
        ...fields.clientType,
        value:
          fields.clientType.value.value === undefined
            ? fields.clientType.value
            : fields.clientType.value.value,
      },
      gstType: {
        ...fields.gstType,
        value:
          fields.gstType.value.value === undefined
            ? fields.gstType.value
            : fields.gstType.value.value,
      },
      status: {
        ...fields.status,
        value:
          fields.status.value.value === undefined
            ? fields.status.value
            : fields.status.value.value,
      },
      contactMobile: {
        ...fields.contactMobile,
        value: fields.contactMobile.value.replace(countryCode.dialCode, ""),
      },
    });
    let dataStatus;
    if (clientData && clientData.addresses.length > 0) {
      dataStatus = validateFields.validateFieldsData(dataObj);
    } else {
      var { addressLine1, city, state, country, postalCode, ...clientRest } =
        dataObj;
      dataStatus = validateFields.validateFieldsData(clientRest);
    }
    const validGST = validateGST();
    const validCustomDetails = validateCustomDetails();
    if (dataStatus.status && validGST.status && validCustomDetails.status) {
      setSpinner(true);
      dataObj = {
        ...dataObj,
        countryCode: countryCode.countryCode.toUpperCase(),
        contactMobile: fields.contactMobile.value.replace(
          countryCode.dialCode,
          ""
        ),
        customDetails: [...customFields],
      };
      Service.updateClient(
        dataObj,
        {
          clientId: props.location.state.clientId,
          ...(clientData && clientData.addresses.length > 0
            ? { addressId: clientData.addresses[0].id }
            : {}),
        },
        clientData && clientData.addresses.length > 0 ? true : false
      )
        .then((res) => {
          let country = rawCountries.filter(
            (cntry) => cntry[2] === res.client.countryCode.toLowerCase()
          );
          let newFields = _.cloneDeep(formFields);
          let newCustomFields = _.cloneDeep(customFields);
          let customDetails = Object.entries(res.client.customDetails).filter(
            (item) => item[1]
          );
          newCustomFields =
            customDetails.length > 0
              ? customDetails.map((item) => {
                  return {
                    detailType: {
                      name: "detailType",
                      label: "Detail",
                      topLabel: true,
                      options: CONFIG.CUSTOM_CLIENT_DETAILS,
                      value: CONFIG.CUSTOM_CLIENT_DETAILS.filter(
                        (rec) => rec.value === item[0]
                      )[0],
                    },
                    detailValue: {
                      name: "detailValue",
                      label: "Value",
                      topLabel: true,
                      value: item[1],
                      isValid: true,
                      maxLength: 16,
                    },
                  };
                })
              : _.cloneDeep(customDetailFields);
          newFields["contactFirstName"]["value"] = res.client.contactFirstName;
          newFields["contactLastName"]["value"] = res.client.contactLastName;
          newFields["contactEmailAddress"]["value"] =
            res.client.contactEmailAddress;
          newFields["companyName"]["value"] = res.client.companyName;
          newFields["clientAliasCode"]["value"] = res.client.clientAliasCode;
          newFields["clientType"]["value"] = CONFIG.CLIENT_TYPE_OPTIONS.filter(
            (item) => item.value === res.client.clientType
          )[0];
          newFields["openingBalance"]["value"] = res.client.openingBalance;
          newFields["gst"]["value"] = res.client.gst ? res.client.gst : "";
          newFields["gstType"]["value"] = res.client.gstType
            ? CONFIG.GST_TYPES.filter(
                (item) => item.value === res.client.gstType
              )[0]
            : { label: "", value: "" };
          newFields["notes"]["value"] = res.client.notes;
          newFields["status"]["value"] = CONFIG.STATUSES.filter(
            (item) => Number(item.value) === Number(res.client.status)
          )[0];
          newFields["gst"]["value"] = res.client.gst ? res.client.gst : "";
          if (res.address && Object.keys(res.address).length > 0) {
            newFields["addressLine1"]["value"] = res.address.addressLine1;
            newFields["addressLine2"]["value"] = res.address.addressLine2;
            newFields["addressLine3"]["value"] = res.address.addressLine3;
            newFields["faxNo"]["value"] = res.address.faxNo;
            newFields["postalCode"]["value"] = res.address.postalCode;
            newFields["city"]["value"] = res.address.city;
            newFields["state"]["value"] = res.address.state;
            newFields["country"]["value"] = res.address.country;
          }
          if (country.length > 0) {
            setCountryCode({
              countryCode: country[0][2],
              dialCode: country[0][3],
            });
            newFields["contactMobile"][
              "value"
            ] = `${country[0][3]}${res.client.contactMobile}`;
          }
          setCustomFields(newCustomFields);
          setFields(newFields);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.success,
            message: "Client updated successfully!",
          });
          setSpinner(false);
          window.scrollTo({ top: 0, behavior: "smooth" });
        })
        .catch((error) => {
          setSpinner(false);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
          window.scrollTo({ top: 0, behavior: "smooth" });
        });
    } else {
      let newFields = _.cloneDeep(fields);
      let newCustomFields = [...validCustomDetails.data];
      let customFieldErrors = [];
      let erroredFields = Object.keys(dataStatus.data).filter(
        (key, i) => dataStatus.data[key].isValid === false
      );
      if (erroredFields.length === 0) {
        erroredFields = Object.keys(validGST.data).filter(
          (key, i) => validGST.data[key].isValid === false
        );
      }
      if (erroredFields.length === 0) {
        customFieldErrors = validCustomDetails.data.map((item, i) => {
          let errField = Object.keys(item).filter(
            (key, j) => item[key].isValid === false
          );
          if (errField.length > 0) {
            return { key: errField[0], index: i };
          }
        });
        customFieldErrors = customFieldErrors.filter((item) => item);
      }
      newCustomFields = [...validCustomDetails.data];
      newFields = {
        ...fields,
        ...dataStatus.data,
        clientType: {
          ...dataStatus.data.clientType,
          value: dataStatus.data.clientType.value
            ? newFields.clientType.options.filter(
                (opt) => opt.value === dataStatus.data.clientType.value
              )[0]
            : { label: "", value: "" },
        },
        gstType: {
          ...(validGST.status
            ? dataStatus.data.gstType
            : validGST.data.gstType),
          value: dataStatus.data.gstType.value
            ? newFields.gstType.options.filter(
                (opt) => opt.value === dataStatus.data.gstType.value
              )[0]
            : { label: "", value: "" },
        },
        status: {
          ...dataStatus.data.status,
          value: dataStatus.data.status.value
            ? CONFIG.STATUSES.filter(
                (opt) => opt.value === dataStatus.data.status.value
              )[0]
            : { label: "", value: "" },
        },
        contactMobile: {
          ...dataStatus.data.contactMobile,
          value: countryCode.dialCode + dataStatus.data.contactMobile.value,
        },
      };
      newFields = {
        ...newFields,
        gst: validGST.data.gst,
      };
      setFields(newFields);
      setCustomFields(newCustomFields);
      if (erroredFields.length > 0) {
        window.scrollTo(0, fields[erroredFields[0]].ref.current.offsetTop);
      }
      if (erroredFields.length === 0 && customFieldErrors.length > 0) {
        window.scrollTo(0, customDetailsRef.current.offsetTop - 450);
      }
    }
  };
  const snackClose = () => {
    let newSnack = _.cloneDeep(snack);
    newSnack.open = false;
    newSnack.message = "";
    setSnack(newSnack);
  };
  const customAutocompleteChangeHandler = (value, name, i) => {
    if (value) {
      let newCustomFields = _.cloneDeep(customFields);
      newCustomFields[i][name]["value"] = value;
      newCustomFields[i][name]["isValid"] = true;
      let newCustomTypeFields = _.cloneDeep(customTypeFields);
      let selectedTypes = newCustomFields.map(
        (item) => item.detailType.value.value
      );
      newCustomTypeFields[0]["detailType"]["options"] =
        CONFIG.CUSTOM_CLIENT_DETAILS.filter(
          (item) => !selectedTypes.includes(item.value)
        );
      setCustomTypeFields(newCustomTypeFields);
      setCustomFields(newCustomFields);
    } else {
      let newCustomFields = _.cloneDeep(customFields);
      newCustomFields[i][name]["value"] = { value: "", label: "" };
      if (newCustomFields[i][name]["validationRequired"]) {
        newCustomFields[i][name]["isValid"] = false;
      }
      setFields(newCustomFields);
    }
  };
  const customAutocompleteFocusHandler = (value, name, i) => {
    let newCustomFields = _.cloneDeep(customFields);
    let selectedTypes = [];
    if (value.value.length > 0) {
      selectedTypes = newCustomFields
        .map((item) => item.detailType.value.value)
        .filter((item) => item !== value.value);
    } else {
      selectedTypes = newCustomFields.map(
        (item) => item.detailType.value.value
      );
    }
    newCustomFields[i][name]["options"] = CONFIG.CUSTOM_CLIENT_DETAILS.filter(
      (item) => !selectedTypes.includes(item.value)
    );
    setCustomFields(newCustomFields);
  };
  const customInputChangeHandler = (value, name, i) => {
    let newCustomFields = _.cloneDeep(customFields);
    newCustomFields[i][name]["value"] = value;
    newCustomFields[i][name]["isValid"] = true;
    setCustomFields(newCustomFields);
  };
  let customDetailsRef = useRef();
  return (
    <div>
      <Header />
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={snack.open}
        onClose={snackClose}
        message={snack.message}
        key={"top" + "center"}
      />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.push("./clients");
              }}
            />
            {props.location.state ? "Update Client" : "Add Client"}
          </Typography>
        </div>
        {alertData.open ? (
          <div className={classes.alertBox}>
            <AlertMessage
              severity={alertData.severity}
              message={alertData.message}
              closeAlert={closeAlert}
            />
          </div>
        ) : null}
        {loader ? (
          <CircularLoading />
        ) : (
          <div className="form_container">
            <div className={"details_container border_bottom"}>
              <Typography className="details_container_heading">
                Client Details
              </Typography>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} ref={fields.companyName.ref}>
                  <Fields.InputField
                    fieldData={fields.companyName}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                    disabled={props.location.state ? true : false}
                  />
                </Grid>
                <Grid item xs={4} ref={fields.clientAliasCode.ref}>
                  <Fields.InputField
                    fieldData={fields.clientAliasCode}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                    disabled={props.location.state ? true : false}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} ref={fields.contactFirstName.ref}>
                  <Fields.InputField
                    fieldData={fields.contactFirstName}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={4} ref={fields.contactLastName.ref}>
                  <Fields.InputField
                    fieldData={fields.contactLastName}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} ref={fields.contactMobile.ref}>
                  <Fields.PhoneNumberField
                    fieldData={fields.contactMobile}
                    variant="outlined"
                    phoneChangeHandler={phoneChangeHandler}
                    defaultCountry={countryCode}
                  />
                </Grid>
                <Grid item xs={4} ref={fields.contactEmailAddress.ref}>
                  <Fields.InputField
                    fieldData={fields.contactEmailAddress}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid
                  item
                  xs={4}
                  className="custom_select"
                  ref={fields.clientType.ref}
                >
                  <Fields.AntSelectableSearchField
                    fieldData={fields.clientType}
                    autoCompleteChangeHandler={autoCompleteChangeHandler}
                  />
                </Grid>
                <Grid
                  item
                  xs={4}
                  className="start_endorment_field"
                  ref={fields.openingBalance.ref}
                >
                  <Fields.InputField
                    fieldData={fields.openingBalance}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                    startAdornment={
                      <span className={classes.input_adorment}>₹</span>
                    }
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid
                  item
                  xs={4}
                  ref={fields.status.ref}
                  className="custom_select"
                >
                  <Fields.AntSelectableSearchField
                    fieldData={fields.status}
                    autoCompleteChangeHandler={autoCompleteChangeHandler}
                  />
                </Grid>
              </Grid>
            </div>
            <div
              className={
                props.location.state || addAddress
                  ? "details_container border_bottom"
                  : "details_container"
              }
            >
              <Typography className="details_container_heading">
                Business Details
              </Typography>
              <Grid container spacing={3} className="details_container_content">
                <Grid
                  item
                  xs={4}
                  ref={fields.gstType.ref}
                  className="custom_select"
                >
                  <Fields.AntSelectableSearchField
                    fieldData={fields.gstType}
                    autoCompleteChangeHandler={autoCompleteChangeHandler}
                  />
                </Grid>
                <Grid item xs={4} ref={fields.gst.ref}>
                  <Fields.InputField
                    fieldData={fields.gst}
                    inputChangeHandler={inputChangeHandler}
                    variant="outlined"
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={8}>
                  <fieldset
                    style={{
                      border: "1px solid black",
                      borderRadius: "4px",
                      paddingBottom: "20px",
                    }}
                  >
                    <legend
                      style={{
                        fontSize: "15px",
                        width: "auto",
                        fontWeight: "bold",
                      }}
                    >
                      Custom Details
                    </legend>
                    {customFields.map((field, i) => {
                      return (
                        <Grid
                          container
                          spacing={4}
                          key={i}
                          ref={customDetailsRef}
                        >
                          <Grid item xs={5} className="custom_select">
                            <Fields.AntSelectableSearchField
                              fieldData={field.detailType}
                              autoCompleteFocusHandler={(value, name) => {
                                customAutocompleteFocusHandler(value, name, i);
                              }}
                              autoCompleteChangeHandler={(value, name) => {
                                customAutocompleteChangeHandler(value, name, i);
                              }}
                            />
                          </Grid>
                          <Grid item xs={5}>
                            <Fields.InputField
                              fieldData={field.detailValue}
                              variant="outlined"
                              inputChangeHandler={(value, name) => {
                                customInputChangeHandler(value, name, i);
                              }}
                            />
                          </Grid>
                          <Grid
                            item
                            xs={2}
                            style={{
                              alignSelf: "center",
                              textAlign: "center",
                            }}
                          >
                            {i === customFields.length - 1 ? (
                              <Button
                                color="primary"
                                variant="contained"
                                disableElevation
                                style={{
                                  marginTop: "25px",
                                  padding: "8px 0px",
                                }}
                                onClick={() => {
                                  if (
                                    customFields.length ===
                                    CONFIG.CUSTOM_CLIENT_DETAILS.length
                                  ) {
                                    return;
                                  } else {
                                    let newCustomFields =
                                      _.cloneDeep(customFields);
                                    newCustomFields.push(customTypeFields[0]);
                                    setCustomFields(newCustomFields);
                                  }
                                }}
                              >
                                <AddIcon />
                              </Button>
                            ) : (
                              <Button
                                color="secondary"
                                variant="contained"
                                disableElevation
                                style={{
                                  marginTop: "25px",
                                  padding: "8px 0px",
                                }}
                                onClick={() => {
                                  let newCustomFields =
                                    _.cloneDeep(customFields);
                                  newCustomFields.splice(i, 1);
                                  setCustomFields(newCustomFields);
                                }}
                              >
                                <RemoveIcon />
                              </Button>
                            )}
                          </Grid>
                        </Grid>
                      );
                    })}
                  </fieldset>
                </Grid>
              </Grid>
              <Grid
                container
                spacing={3}
                className="details_container_content"
                ref={fields.notes.ref}
              >
                <Grid item xs={8}>
                  <Fields.TextAreaField
                    fieldData={fields.notes}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
              </Grid>
            </div>
            {props.location.state && clientData ? (
              <div
                className={
                  clientData.addresses.length > 0
                    ? "details_container border_bottom"
                    : "details_container"
                }
              >
                <Typography className="details_container_heading">
                  Documents
                </Typography>
                <Grid container spacing={3}>
                  <Grid item xs={8}>
                    <Fields.UploadDocuments
                      ref={uploadRef}
                      uploadHandler={uploadHandler}
                      data={clientData}
                      percentCompleted={percent}
                      label={"Upload max 5 documents"}
                      max={5}
                      viewFile={true}
                      getAttachments={Service.getClientDocuments}
                      item={clientData.clientDetails}
                    />
                  </Grid>
                </Grid>
              </div>
            ) : null}
            {(clientData && clientData.addresses.length > 0) || addAddress ? (
              <div className="details_container">
                <Typography className="details_container_heading">
                  Address Details
                </Typography>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={4} ref={fields.addressLine1.ref}>
                    <Fields.InputField
                      fieldData={fields.addressLine1}
                      inputChangeHandler={inputChangeHandler}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item xs={4} ref={fields.addressLine2.ref}>
                    <Fields.InputField
                      fieldData={fields.addressLine2}
                      inputChangeHandler={inputChangeHandler}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={4} ref={fields.addressLine3.ref}>
                    <Fields.InputField
                      fieldData={fields.addressLine3}
                      inputChangeHandler={inputChangeHandler}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item xs={4} ref={fields.faxNo.ref}>
                    <Fields.InputField
                      fieldData={fields.faxNo}
                      inputChangeHandler={inputChangeHandler}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={4} ref={fields.postalCode.ref}>
                    <Fields.InputField
                      fieldData={fields.postalCode}
                      inputChangeHandler={inputChangeHandler}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item xs={4} ref={fields.city.ref}>
                    <Fields.InputField
                      fieldData={fields.city}
                      inputChangeHandler={inputChangeHandler}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={4} ref={fields.state.ref}>
                    <Fields.InputField
                      fieldData={fields.state}
                      inputChangeHandler={inputChangeHandler}
                      variant="outlined"
                    />
                  </Grid>
                  <Grid item xs={4} ref={fields.country.ref}>
                    <Fields.InputField
                      fieldData={fields.country}
                      inputChangeHandler={inputChangeHandler}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
              </div>
            ) : null}
            {props.location.state ? null : (
              <div className="details_container">
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={8}>
                    <Grid container spacing={2}>
                      <Grid item xs={8}></Grid>
                      <Grid item xs={4} style={{ textAlign: "right" }}>
                        <Button
                          variant="outlined"
                          color="primary"
                          startIcon={addAddress ? <RemoveIcon /> : <AddIcon />}
                          onClick={() => {
                            setAddAddress(!addAddress);
                          }}
                        >
                          {addAddress
                            ? "Remove Client Address"
                            : "Add Client Address"}
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            )}
            <div className="details_container">
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={8}>
                  <Grid container spacing={2}>
                    <Grid item xs={8}></Grid>
                    <Grid item xs={2}>
                      <Button
                        variant="outlined"
                        onClick={() => {
                          history.push("./clients");
                        }}
                        className="cancel_button"
                      >
                        Cancel
                      </Button>
                    </Grid>
                    <Grid item xs={2}>
                      <Button
                        variant="outlined"
                        className="save_button"
                        onClick={() => {
                          if (props.location.state) {
                            updateClientHandler();
                          } else {
                            addClientHandler();
                          }
                        }}
                        startIcon={
                          spinner ? (
                            <CircularProgress size={20} color={"#fff"} />
                          ) : null
                        }
                      >
                        {props.location.state ? "Update" : "Save"}
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default AddClient;
