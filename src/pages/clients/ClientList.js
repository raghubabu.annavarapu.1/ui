import React, { useState, useEffect } from "react";
import Header from "../../components/header";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Button, Grid } from "@material-ui/core";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import CloseIcon from "@mui/icons-material/Close";
import * as Fields from "../../sharedComponents";
import * as Components from "../../sharedComponents";
import * as CONFIG from "../../config/GlobalConstants";
import _ from "lodash";
import { useHistory } from "react-router-dom";
import "../../common.css";
import CircularLoading from "../../components/loader/circularLoading";
import AlertMessage from "../../components/alertmessage/AlertMessage";
import Chip from "@mui/material/Chip";
var Service;
const headRows = [
  { id: "company_name", disablePadding: true, label: "COMPANY NAME" },
  { id: "client_name", disablePadding: true, label: "CLIENT NAME" },
  { id: "mobile_no", disablePadding: true, label: "MOBILE#" },
  { id: "email", disablePadding: true, label: "EMAIL ID" },
  { id: "type", disablePadding: true, label: "TYPE" },
  {
    id: "opening_balance",
    disablePadding: true,
    label: "OPENING BALANCE",
  },
  { id: "status", disablePadding: true, label: "STATUS" },
  { id: "actions", disablePadding: true, label: "" },
];
const useStyles = makeStyles((theme) => ({
  header_buttons: {
    padding: "0px 15px",
  },
  alertBox: {
    padding: "10px 20px",
  },
  client_button: {
    background: "#649B42",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    "&:hover": {
      background: "#649B42",
    },
  },
  history_button: {
    background: "#649B42",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    "&:hover": {
      background: "#649B42",
    },
  },
  filter_buttons: {
    textAlign: "right",
    padding: "0px 15px",
  },
  search_button: {
    padding: "6px 15px",
    marginRight: "12px",
  },
  clear_button: {
    padding: "6px 15px",
    background: "#D3710F0D",
    border: "1px solid #D3710F",
    color: "#D3710F",
    "&:hover": {
      background: "#D3710F0D",
    },
  },
  contract_button: {
    background: "#D08B1D",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    marginRight: theme.spacing(2),
    "&:hover": {
      background: "#D08B1D",
    },
  },
  route_btn: {
    background: "#F23838",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    marginRight: theme.spacing(2),
    "&:hover": {
      background: "#F23838",
    },
  },
}));
const ClientList = (props) => {
  const classes = useStyles();
  const history = useHistory();
  Service = global.service;
  const filterFields = {
    client: { label: "Search by Client Name", name: "client", value: "" },
    // date: { label: "Select date", name: "date", value: "" },
  };
  const [filters, setFilters] = useState(_.cloneDeep(filterFields));
  const [spinner, setSpinner] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  const [total, setTotal] = useState(0);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  useEffect(() => {
    window.scrollTo({ top: 0 });
    let dataObj = {
      skip: pagination.current * pagination.pageSize,
      limit: pagination.pageSize,
    };
    renderClients(dataObj);
  }, []);
  const renderClients = (dataObj) => {
    setSpinner(true);
    Service.getClients(dataObj)
      .then((res) => {
        let data = res.clients.map((client) => {
          return {
            company_name: (
              <div style={{ textAlign: "center" }}>{client.companyName}</div>
            ),
            client_name: (
              <div style={{ textAlign: "center" }}>
                {client.contactFirstName} {client.contactLastName}
              </div>
            ),
            mobile_no: (
              <div style={{ textAlign: "center" }}>
                {client.countryCode} {client.contactMobile}{" "}
              </div>
            ),
            email: (
              <div style={{ textAlign: "center" }}>
                {client.contactEmailAddress}
              </div>
            ),
            type: (
              <div style={{ textAlign: "center" }}>{client.clientType}</div>
            ),
            opening_balance: (
              <div style={{ textAlign: "center" }}>{client.openingBalance}</div>
            ),
            status: (
              <Chip
                style={{
                  background: `${
                    CONFIG.STATUSES.filter(
                      (tr) => parseInt(tr.value) === client.status
                    )[0].color
                  }`,
                  color: "#fff",
                }}
                label={
                  CONFIG.STATUSES.filter(
                    (tr) => parseInt(tr.value) === client.status
                  )[0].label
                }
              />
            ),
            actions: (
              <Components.DropDownMenu
                options={CONFIG.CLIENT_MORE_OPTIONS}
                data={{
                  clientId: client.id,
                  clientName:
                    client.contactFirstName || client.contactLastName
                      ? client.contactFirstName + " " + client.contactLastName
                      : "",
                }}
              />
            ),
          };
        });
        setTotal(res.totalCount);
        setTableData(data);
        setSpinner(false);
      })
      .catch((error) => {
        setSpinner(false);
        setAlertData({
          open: true,
          severity: CONFIG.ALERT_SEVERITY.error,
          message:
            error.response && error.response.data
              ? error.response.data.message
              : "Something went wrong!",
        });
      });
  };
  const inputChangeHandler = (value, name) => {
    let newFilters = _.cloneDeep(filters);
    newFilters[name]["value"] = value;
    setFilters(newFilters);
    setPagination({ ...pagination, current: 0 });
    let dataObj = value
      ? {
          searchText: value,
        }
      : {
          skip: pagination.current * pagination.pageSize,
          limit: pagination.pageSize,
        };
    renderClients(dataObj);
  };
  // const dateChangeHandler = () => {};
  const pageChangeHandler = (page) => {
    let fil = Object.values(filters).filter((fil) => fil.value);
    let dataObj;
    if (fil.length > 0) {
      let filters = fil.map((fil) => {
        switch (fil.name) {
          case "client":
            return { searchText: fil.value };
          default:
            return;
        }
      });
      filters = Object.assign({}, ...filters);
      dataObj = {
        skip: page * pagination.pageSize,
        limit: pagination.pageSize,
        ...filters,
      };
    } else {
      dataObj = {
        skip: page * pagination.pageSize,
        limit: pagination.pageSize,
      };
    }
    renderClients(dataObj);
    setPagination({ ...pagination, current: page });
  };
  const rowsPerPageChangeHandler = (rowsPerPage) => {
    renderClients({
      skip: pagination.current * rowsPerPage,
      limit: rowsPerPage,
    });
    setPagination({ ...pagination, pageSize: rowsPerPage });
  };
  const searchHandler = () => {};
  const clearHandler = () => {
    let newFilters = _.cloneDeep(filterFields);
    setFilters(newFilters);
    renderClients({
      skip: pagination.current * pagination.pageSize,
      limit: pagination.pageSize,
    });
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">Clients</Typography>
          <div className={classes.header_buttons}>
            <Button
              className={classes.client_button}
              startIcon={<AddCircleOutlinedIcon />}
              onClick={() => {
                history.push("./addClient");
              }}
            >
              Add Client
            </Button>
          </div>
        </div>
        <div className="filter_box">
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Grid item xs={2}>
                  <Fields.InputField
                    fieldData={filters.client}
                    inputChangeHandler={inputChangeHandler}
                    className="filter_field"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={2}>
                  <Button
                    className={classes.clear_button}
                    startIcon={<CloseIcon />}
                    onClick={() => {
                      clearHandler();
                    }}
                  >
                    Clear
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>
        <div>
          {alertData.open ? (
            <div className={classes.alertBox}>
              <AlertMessage
                severity={alertData.severity}
                message={alertData.message}
                closeAlert={closeAlert}
              />
            </div>
          ) : null}
          {spinner ? (
            <CircularLoading />
          ) : (
            <Components.DataTable
              headRows={headRows}
              tableData={tableData}
              pagination={pagination}
              pageChangeHandler={pageChangeHandler}
              rowsPerPageChangeHandler={rowsPerPageChangeHandler}
              total={total}
            />
          )}
        </div>
      </div>
    </div>
  );
};
export default ClientList;
