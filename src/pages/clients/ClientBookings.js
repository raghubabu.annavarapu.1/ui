import React, { useState } from "react";
import Header from "../../components/header";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Grid } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import * as Fields from "../../sharedComponents";
import * as Components from "../../sharedComponents";
import _ from "lodash";
import ViewHeadlineIcon from "@mui/icons-material/ViewHeadline";
import '../../common.css';
import * as CONFIG from '../../config/GlobalConstants';
const headRows = [
  { id: "date", disablePadding: true, label: "DATE" },
  { id: "particular", disablePadding: true, label: "PARTICULAR" },
  { id: "gl_account", disablePadding: true, label: "GL ACCOUNT#" },
  { id: "total_amount", disablePadding: true, label: "TOTAL AMOUNT" },
  { id: "actions", disablePadding: true, label: "" },
];
const tableData = [
  {
    date: "22-09-2021",
    particular: "Advance received against LR #",
    gl_account: "123456789",
    total_amount: "Rs. 50000.00",
    actions: (
      <div style={{ color: "#366E93", cursor: "pointer" }}>
        <ViewHeadlineIcon style={{ verticalAlign: "middle" }} /> View Receipt
      </div>
    ),
  },
  {
    date: "22-09-2021",
    particular: "Advance received against LR #",
    gl_account: "123456789",
    total_amount: "Rs. 50000.00",
    actions: (
      <div style={{ color: "#366E93", cursor: "pointer" }}>
        <ViewHeadlineIcon style={{ verticalAlign: "middle" }} /> View Receipt
      </div>
    ),
  },
  {
    date: "22-09-2021",
    particular: "Advance received against LR #",
    gl_account: "123456789",
    total_amount: "Rs. 50000.00",
    actions: (
      <div style={{ color: "#366E93", cursor: "pointer" }}>
        <ViewHeadlineIcon style={{ verticalAlign: "middle" }} /> View Receipt
      </div>
    ),
  },
  {
    date: "22-09-2021",
    particular: "Advance received against LR #",
    gl_account: "123456789",
    total_amount: "Rs. 50000.00",
    actions: (
      <div style={{ color: "#366E93", cursor: "pointer" }}>
        <ViewHeadlineIcon style={{ verticalAlign: "middle" }} /> View Receipt
      </div>
    ),
  }
];
const useStyles = makeStyles((theme) => ({
  header_text: {
    fontSize: "20px",
    fontWeight: "bold",
    lineHeight: "42px",
    paddingRight: "12px",
  },
  client_info: {
    display: "flex",
  },
  client_details: {
    alignSelf: "center",
    display: "flex",
    flexDirection: "column",
    padding: "0px 20px",
    borderLeft: "3px solid #E6E6E6",
    fontSize: "14px",
    color: "#484848",
  },
  view_details: {
    alignSelf: "center",
    color: "#366E93",
    padding: "0px 17px",
    fontWeight: 600,
    cursor: "pointer",
  },
  filter_box: {
    padding: "20px 24px",
    display: "flex",
    justifyContent: "space-between",
  },
  filter_field: {
    background: "#FFFFFF",
  },
}));

const ClientBookings = () => {
  const classes = useStyles();
  const history = useHistory();
  const filterFields = {
    booking: { name: "Booking", label: "Booking", value: "", options: [] },
    date: { name: "date", label: "Date", value: "" },
  };
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  const [total, setTotal] = useState(100);
  const [filters, setFilters] = useState(_.cloneDeep(filterFields));
  const autoCompleteChangeHandler = () => { };
  const dateChangeHandler = () => { };
  const pageChangeHandler = () => { };
  const rowsPerPageChangeHandler = () => { };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <div className={classes.client_info}>
            <Typography className={classes.header_text}>
              <ArrowBack
                className="arrow"
                onClick={() => {
                  history.push("./clients");
                }}
              />
              Prasanna(SS Transports)
            </Typography>
            <div className={classes.client_details}>
              <span>GST# 123456</span>
              <span>9876543210</span>
            </div>
          </div>
          <div className={classes.view_details}>View Details</div>
        </div>
        <div className={classes.filter_box}>
          <Grid container spacing={3}>
            <Grid item xs={2}>
              <Fields.AutoCompleteField
                fieldData={filters.booking}
                autoCompleteChangeHandler={autoCompleteChangeHandler}
                className="filter_field"
              />
            </Grid>
            <Grid item xs={3}>
              <Fields.DateField
                fieldData={filters.date}
                dateChangeHandler={dateChangeHandler}
                variant="outlined"
                className="filter_field"
              />
            </Grid>
          </Grid>
        </div>
        <div>
          <Components.DataTable
            headRows={headRows}
            tableData={tableData}
            pagination={pagination}
            pageChangeHandler={pageChangeHandler}
            rowsPerPageChangeHandler={rowsPerPageChangeHandler}
            total={total}
          />
        </div>
      </div>
    </div>
  );
};

export default ClientBookings;
