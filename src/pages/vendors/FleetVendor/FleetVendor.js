import React, { useState } from "react";
import Header from "../../../components/header";
import "../../../common.css";
import { Button, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import * as Components from "../../../sharedComponents";
import * as CONFIG from "../../../config/GlobalConstants";
import { useHistory } from "react-router-dom";
const headRows = [
    { id: "id", disablePadding: "true", label: " VENDOR_ID" },
    { id: "companyName", disablePadding: true, label: "COMPANY_NAME" },
    { id: "vendorCode", disablePadding: "true", label: "VENDOR CODE" },
    { id: "contactnumber", disablePadding: "true", label: "CONTACT NUMBER" },
    { id: "email_id", disablePadding: "true", label: "EMAIL_ID" },
    { id: "gst", disablePadding: true, label: "GST#" },
    { id: "glcode", disablePadding: true, label: "GL CODE" },
    { id: "openingBalance", disablePadding: true, label: "OPENING_BALANCE" },
    { id: "no_vehicles", disablePadding: true, label: "NO OF VEHICLES" },
    { id: "no_trips", disablePadding: true, label: "NO OF TRIPS" },
    { id: "action", disablePadding: true, label: "ACTIONS" },
];
const tableData = [
    // { name: "1", email_id: "yaminiyadavalli@gmail.com", contactnumber: "9845612356" }
]
const useStyles = makeStyles((theme) => ({
    fleet_btn: {
        textTransform: "none",
        backgroundColor: "#649B42",
        "&:hover": {
            backgroundColor: "#649B42",
        },
    }
}));
const FleetVendor = () => {
    const classes = useStyles();
    const history = useHistory();
    const [pagination, setPagination] = useState({
        current: CONFIG.PAGINATION.current,
        pageSize: CONFIG.PAGINATION.pageSize,
    });
    const [total, setTotal] = useState(0);
    const pageChangeHandler = (page) => {
        let dataObj = {
            skip: page * pagination.pageSize,
            limit: pagination.pageSize,
        };
        tableData(dataObj);
        setPagination({ ...pagination, current: page });
    };
    const rowsPerPageChangeHandler = (rowsPerPage) => {
        let dataObj = {
            skip: pagination.current * rowsPerPage,
            limit: rowsPerPage,
        };
        tableData(dataObj);
        setPagination({ ...pagination, pageSize: rowsPerPage });
    };
    return (
        <div>
            <Header />
            <div className="main_container">
                <div className="header_box">
                    <Typography className="header_text">
                        Fleet Vendor
                    </Typography>
                    <div>
                        <Button variant="contained" color="primary" className={classes.fleet_btn} startIcon={<AddCircleOutlinedIcon />}
                            onClick={() => { history.push('/addFleet') }}>
                            Add FleetVendor
                        </Button>
                    </div>
                </div>
                <div>
                    <Components.DataTable
                        headRows={headRows}
                        tableData={tableData}
                        pagination={pagination}
                        pageChangeHandler={pageChangeHandler}
                        rowsPerPageChangeHandler={rowsPerPageChangeHandler}
                        total={total} />
                </div>
            </div>
        </div>
    )
}
export default FleetVendor;   