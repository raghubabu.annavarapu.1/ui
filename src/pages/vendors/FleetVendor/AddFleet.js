import { Typography, Grid, Button } from '@material-ui/core';
import React, { useRef, useState } from 'react';
import Header from '../../../components/header';
import { ArrowBack } from "@material-ui/icons";
import * as Fields from "../../../sharedComponents";
import "../../../common.css";
import _ from "lodash";
import { useHistory } from 'react-router-dom';
const AddFleet = () => {
    const formFields = {
        firstName: {
            name: "firstName",
            label: "First Name",
            value: "",
            isValid: true,
            validationRequired: true,
            topLabel: true,
            validPattern: "SPECIAL_CHARS_DESC",
            errorMsg: "Please enter first name",
            ref: useRef(),
        },
        lastName: {
            name: "lastName",
            label: "Last Name",
            value: "",
            isValid: true,
            validationRequired: true,
            topLabel: true,
            validPattern: "SPECIAL_CHARS_DESC",
            errorMsg: "Please enter last name",
            ref: useRef(),
        },
        companyName: {
            name: "companyName",
            label: "Company Name",
            value: "",
            isValid: true,
            topLabel: true,
            isValid: true,
            validationRequired: true,
            validPattern: "SPECIAL_CHARS_DESC",
            ref: useRef(),
        },
        mobileNumber: {
            name: "mobileNumber",
            label: "Mobile Number",
            value: "",
            isValid: true,
            validationRequired: true,
            topLabel: true,
            validPattern: "MOBILE_NUMBER",
            errorMsg: "Please enter mobile number",
            ref: useRef(),
            maxLength: 10,
        },
        emailAddress: {
            name: "emailAddress",
            label: "Email ID",
            value: "",
            isValid: true,
            validationRequired: true,
            topLabel: true,
            validPattern: "EMAIL",
            errorMsg: "Please enter email",
            ref: useRef(),
        },
        gstNumber: {
            name: "gstNumber",
            label: "GST Number",
            value: "",
            isValid: true,
            topLabel: true,
            maxLength: 15,
            ref: useRef(),
            errorMsg: "Please enter valid gst",
        },
        vehiclesno: {
            name: "vehiclesno",
            label: "No Of Vehicles",
            value: "",
            isValid: true,
            topLabel: true,
            ref: useRef(),
        },
        glNumber: {
            name: "glNumber",
            label: "GL Number",
            value: "",
            isValid: true,
            topLabel: true,
            ref: useRef(),
        },
        openingBalance: {
            name: "openingBalance",
            label: "Opening Balance",
            value: "",
            isValid: true,
            topLabel: true,
            type: "number",
            min: 0,
            ref: useRef(),
        },
        areaServiceId: {
            name: "areaServiceId",
            label: "Area Service ID",
            value: { label: "", value: "" },
            isValid: true,
            validationRequired: true,
            validPattern: "NUMERIC",
            topLabel: true,
            errorMsg: "Please enter area service id",
            ref: useRef(),
            options: [],
        },
        companyPhoto: {
            name: "companyPhoto",
            label: "Company Photo",
            value: "",
            isValid: true,
            validationRequired: true,
            topLabel: true,
            validPattern: "SPECIAL_CHARS_DESC",
            errorMsg: "Please enter last name",
            ref: useRef(),
        },
        addressLine1: {
            name: "addressLine1",
            label: "Address Line 1",
            value: "",
            isValid: true,
            validationRequired: true,
            topLabel: true,
            errorMsg: "Please enter addressline1",
            validPattern: "SPECIAL_CHARS_DESC",
            ref: useRef(),
        },
        addressLine2: {
            name: "addressLine2",
            label: "Address Line 2",
            value: "",
            isValid: true,
            topLabel: true,
            ref: useRef(),
        },
        addressLine3: {
            name: "addressLine3",
            label: "Address Line 3",
            value: "",
            isValid: true,
            topLabel: true,
            ref: useRef(),
        },
        city: {
            name: "city",
            label: "City",
            value: "",
            isValid: true,
            validationRequired: true,
            topLabel: true,
            errorMsg: "Please enter city",
            validPattern: "SPECIAL_CHARS_DESC",
            ref: useRef(),
        },
        state: {
            name: "state",
            label: "State",
            value: "",
            isValid: true,
            validationRequired: true,
            topLabel: true,
            errorMsg: "Please enter state",
            validPattern: "SPECIAL_CHARS_DESC",
            ref: useRef(),
        },
        country: {
            name: "country",
            label: "Country",
            value: "",
            isValid: true,
            validationRequired: true,
            topLabel: true,
            errorMsg: "Please enter country",
            validPattern: "SPECIAL_CHARS_DESC",
            ref: useRef(),
        },
        postalCode: {
            name: "postalCode",
            label: "Postal Code",
            value: "",
            isValid: true,
            validationRequired: true,
            topLabel: true,
            errorMsg: "Please enter postal code",
            validPattern: "SPECIAL_CHARS_DESC",
            ref: useRef(),
        },
        vendorCode: {
            name: "VendorCode",
            label: "VendorCode",
            value: "",
            isValid: true,
            validationRequired: true,
            topLabel: true,
            validPattern: "SPECIAL_CHARS_DESC",
            errorMsg: "Please enter vendor code",
            ref: useRef(),
        }
    };
    const [fields, setFields] = useState(_.cloneDeep(formFields));
    const history = useHistory();
    const [spinner, setSpinner] = useState(false);
    const inputChangeHandler = () => { };
    const autoCompleteChangeHandler = () => { };
    return (
        <div>
            <Header />
            <div className='main_container'>
                <div className='header_box'>
                    <Typography className='header_text'>
                        <ArrowBack className="arrow"
                            onClick={() => history.push('./fleetVendor')} />
                        Add FleetVendor
                    </Typography>
                </div>
                <div className="form_container">
                    <div className="details_container">
                        <Typography className="details_container_heading">
                            General Information
                        </Typography>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.firstName}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.lastName}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.emailAddress}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.mobileNumber}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.vendorCode}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                        </Grid>
                    </div>
                    <div className="details_container">
                        <Typography className="details_container_heading">
                            Address Details
                        </Typography>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.addressLine1}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}

                                />
                            </Grid>
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.addressLine2}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.addressLine3}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}

                                />
                            </Grid>
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.postalCode}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}

                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.city}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}

                                />
                            </Grid>
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.state}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}

                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.country}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}

                                />
                            </Grid>
                        </Grid>
                    </div>
                    <div className="details_container">
                        <Typography className="details_container_heading">
                            Company Information
                        </Typography>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.companyName}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.vehiclesno}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.gstNumber}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}

                                />
                            </Grid>
                            <Grid
                                item
                                xs={4}
                            >
                                <Fields.InputField
                                    fieldData={fields.companyPhoto}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.glNumber}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}
                                />
                            </Grid>
                            <Grid item xs={4} >
                                <Fields.InputField
                                    fieldData={fields.openingBalance}
                                    variant="outlined"
                                    inputChangeHandler={inputChangeHandler}

                                />
                            </Grid>
                        </Grid>
                    </div>
                    <div className="details_container">
                        <Grid container spacing={3} className="details_container_content">
                            <Grid item xs={8}>
                                <Grid container spacing={2}>
                                    <Grid item xs={8}></Grid>
                                    <Grid item xs={2}>
                                        <Button variant="outlined" className="cancel_button">
                                            Cancel
                                        </Button>
                                    </Grid>
                                    <Grid item xs={2}>
                                        <Button
                                            variant="outlined"
                                            className="save_button"

                                        // startIcon={
                                        //     spinner ? (
                                        //         <CircularProgress size={20} color={"#fff"} />
                                        //     ) : null
                                        // }
                                        >
                                            Save
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </div>

        </div>
    )
}
export default AddFleet;