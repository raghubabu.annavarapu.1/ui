import { Typography, Grid } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import React, { useRef, useEffect, useState } from "react";
import Header from "../../../components/header";
import "../../../common.css";
import { useHistory } from "react-router-dom";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Chip from "@mui/material/Chip";
import * as CONFIG from "../../../config/GlobalConstants";
import CircularLoading from "../../../components/loader/circularLoading";
var Service;
const ServiceDetails = (props) => {
  const history = useHistory();
  const uploadRef = useRef();
  const [loader, setLoader] = useState(false);
  const [vendorData, setVendorData] = useState();
  Service = global.service;
  useEffect(() => {
    window.scrollTo({ top: 0 });
    if (props.location.state) {
      setLoader(true);
      Service.getVendor({ vendorId: props.location.state.vendorId })
        .then((res) => {
          setVendorData(res);
          setLoader(false);
        })
        .catch(() => {
          setLoader(false);
        });
    } else {
      global.session.remove("BearerToken");
      global.session.remove("profile");
      global.session.remove("hideHeader");
      window.location = "/";
    }
  }, []);
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => history.push("./serviceVendor")}
            />
            Service Vendor Details(Vendor Company:{props.location.state.vendorName}
            )
            {vendorData ? (
              <Chip
                style={{
                  background: `${CONFIG.STATUSES.filter(
                    (tr) => parseInt(tr.value) === vendorData.status
                  )[0].color
                    }`,
                  color: "#fff",
                  marginLeft: "10px",
                }}
                label={
                  CONFIG.STATUSES.filter(
                    (tr) => parseInt(tr.value) === vendorData.status
                  )[0].label
                }
              />
            ) : null}
          </Typography>
        </div>
        {loader ? (
          <CircularLoading />
        ) : vendorData ? (
          <div>
            <div className="form_container">
              <div className="details_container">
                <Typography className="details_container_heading">
                  Vendor Information
                </Typography>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          variant="body2"
                          color="textSecondary"
                          gutterBottom
                        >
                          Vendor Company
                        </Typography>
                        <Typography variant="h6" component="div">
                          {vendorData.firstName} {vendorData.lastName}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Mobile Number
                        </Typography>
                        <Typography variant="h6" component="div">
                          {vendorData.mobileNumber}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Contact Email Address
                        </Typography>
                        <Typography variant="h6" component="div">
                          {vendorData.emailAddress}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </Grid>
              </div>
            </div>
            <div className="form_container">
              <div className="details_container">
                <Typography className="details_container_heading">
                  Business Details
                </Typography>
                <Grid
                  container
                  spacing={3}
                  className="details_container_content"
                >
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Company Name
                        </Typography>
                        <Typography variant="h6" component="div">
                          {vendorData.companyName}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          Business Id
                        </Typography>
                        <Typography variant="h6" component="div">
                          {vendorData.businessId}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        >
                          GST Number
                        </Typography>
                        <Typography variant="h6" component="div">
                          {vendorData.gstNumber ? vendorData.gstNumber : "NA"}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                  <Grid item xs={3}>
                    <Card className="custom_card">
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="body2"
                          color="textSecondary"
                        // variant="h6"
                        // component="div"
                        >
                          Service Area
                        </Typography>
                        <Typography variant="h6" component="div">
                          {vendorData.servicearea}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Grid>
                </Grid>
              </div>
            </div>
            {vendorData.length === 0 ? null : (
              <div className="form_container">
                <div className="details_container">
                  <Typography className="details_container_heading">
                    Address Details
                  </Typography>
                  <Grid
                    container
                    spacing={3}
                    className="details_container_content"
                  >
                    <Grid item xs={12}>
                      <Typography
                        gutterBottom
                        variant="body2"
                        color="textSecondary"
                      >
                        Vendor Address
                      </Typography>
                      <Typography variant="h6" component="div">
                        {vendorData.addressLine1}
                        {vendorData.addressLine2
                          ? `, ${vendorData.addressLine2}`
                          : ""}
                        {vendorData.addressLine3
                          ? `, ${vendorData.addressLine3}`
                          : ""}
                        , {vendorData.city}, {vendorData.postalCode},{" "}
                        {vendorData.state}, {vendorData.country}
                      </Typography>
                    </Grid>
                  </Grid>
                </div>
              </div>
            )}
          </div>
        ) : null}
      </div>
    </div>
  );
};
export default ServiceDetails;
