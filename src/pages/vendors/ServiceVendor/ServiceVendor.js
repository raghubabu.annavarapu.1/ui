import React, { useState, useEffect } from "react";
import Header from "../../../components/header";
import "../../../common.css";
import { useHistory } from "react-router-dom";
import { Typography, Button, Chip } from "@material-ui/core";
import * as Components from "../../../sharedComponents";
import * as CONFIG from "../../../config/GlobalConstants";
import { makeStyles } from "@material-ui/core";
import AddCircleOutlined from "@mui/icons-material/AddCircleOutlined";
import CircularLoading from "../../../components/loader/circularLoading";

var Service;
const useStyles = makeStyles((theme) => ({
  header_buttons: {
    padding: "0px 15px",
  },
  service_btn: {
    textTransform: "none",
    padding: "8px 15px",
    backgroundColor: "#649B42",
    "&:hover": {
      backgroundColor: "#649B42",
      padding: "8px 15px",
    },
  },
}));
const headRows = [
  { id: "id", disablePadding: "true", label: " VENDOR ID" },
  { id: "companyName", disablePadding: true, label: "COMPANY NAME" },
  { id: "vendorName", disablePadding: "true", label: "VENDOR NAME" },
  { id: "contactnumber", disablePadding: "true", label: "CONTACT NUMBER" },
  { id: "email_id", disablePadding: "true", label: "EMAIL ID" },
  { id: "gst", disablePadding: true, label: "GST#" },
  { id: "openingBalance", disablePadding: true, label: "OPENING BALANCE" },
  // { id: "dueAmount", disablePadding: true, label: "DUE_AMOUNT" },
  { id: "status", disablePadding: true, label: "STATUS" },
  { id: "action", disablePadding: true, label: "ACTIONS" },
];
const ServiceVendor = () => {
  const classes = useStyles();
  const history = useHistory();
  const [total, setTotal] = useState(0);
  const [spinner, setSpinner] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  Service = global.service;
  useEffect(() => {
    window.scrollTo({ top: 0 });
    let dataObj = {
      skip: pagination.current * pagination.pageSize,
      limit: pagination.pageSize,
    };
    renderVendors(dataObj);
  }, []);
  const renderVendors = (dataObj) => {
    setSpinner(true);
    Service.getVendors(dataObj)
      .then((res) => {
        const data = res.vendors.map((vendor) => {
          return {
            id: vendor.id ? vendor.id : "--",
            companyName: vendor.companyName ? vendor.companyName : "--",
            vendorName:
              vendor.firstName || vendor.lastName
                ? vendor.firstName + " " + vendor.lastName
                : "",
            contactnumber: vendor.mobileNumber ? vendor.mobileNumber : "--",
            email_id: vendor.emailAddress ? vendor.emailAddress : "--",
            gst: vendor.gstNumber ? vendor.gstNumber : "--",
            openingBalance: vendor.openingBalance
              ? vendor.openingBalance
              : "--",
              status: (
                <Chip
                  style={{
                    background: `${
                      CONFIG.STATUSES.filter(
                        (tr) => Number(tr.value) === Number(vendor.status)
                      )[0].color
                    }`,
                    color: "#fff",
                  }}
                  label={
                    CONFIG.STATUSES.filter(
                      (tr) => Number(tr.value) === Number(vendor.status)
                    )[0].label
                  }
                />
              ),
            action: (
              <div>
                {" "}
                <Components.DropDownMenu
                  options={CONFIG.VENDORS_MORE_OPTIONS}
                  data={{
                    vendorId: vendor.id,
                    vendorName:
                      vendor.companyName ? vendor.companyName: "" 
                  }}
                />
              </div>
            ),
          };
        });
        setTotal(res.totalCount);
        setTableData(data);
        setSpinner(false);
      })
      .catch(() => {
        setSpinner(false);
      });
  };
  const pageChangeHandler = (page) => {
    let dataObj = {
      skip: page * pagination.pageSize,
      limit: pagination.pageSize,
    };
    renderVendors(dataObj);
    setPagination({ ...pagination, current: page });
  };
  const rowsPerPageChangeHandler = (rowsPerPage) => {
    let dataObj = {
      skip: pagination.current * rowsPerPage,
      limit: rowsPerPage,
    };
    renderVendors(dataObj);
    setPagination({ ...pagination, pageSize: rowsPerPage });
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">Service Vendor</Typography>
          <div className={classes.header_buttons}>
            <Button
              variant="contained"
              color="primary"
              disableElevation={true}
              className={classes.service_btn}
              startIcon={<AddCircleOutlined />}
              onClick={() => {
                history.push("./addService");
              }}
            >
              Add Service Vendor
            </Button>
          </div>
        </div>
        <div>
          {spinner ? (
            <CircularLoading />
          ) : (
            <Components.DataTable
              headRows={headRows}
              tableData={tableData}
              pagination={pagination}
              pageChangeHandler={pageChangeHandler}
              rowsPerPageChangeHandler={rowsPerPageChangeHandler}
              total={total}
            />
          )}
        </div>
      </div>
    </div>
  );
};
export default ServiceVendor;
