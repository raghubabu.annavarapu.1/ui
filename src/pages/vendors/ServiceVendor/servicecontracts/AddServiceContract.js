import {
  Typography,
  Grid,
  Button,
  FormControlLabel,
  CircularProgress,
} from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import React, { useState, useEffect } from "react";
import Header from "../../../../components/header";
import * as CONFIG from "../../../../config/GlobalConstants";
import _ from "lodash";
import * as Fields from "../../../../sharedComponents";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import RadioGroup, { useRadioGroup } from "@mui/material/RadioGroup";
import Radio from "@mui/material/Radio";
import PropTypes from "prop-types";
import { styled } from "@mui/material/styles";
import InputAdornment from "@mui/material/InputAdornment";
import ValidateFields from "../../../../validations/validateFields";
import AlertMessage from "../../../../components/alertmessage/AlertMessage";
import CircularLoading from "../../../../components/loader/circularLoading";
import Util from "../../../../utils";
const useStyles = makeStyles((theme) => ({
  alertBox: {
    padding: "10px 12px",
  },
}));
const StyledFormControlLabel = styled((props) => (
  <FormControlLabel {...props} />
))(({ theme, checked }) => ({
  ".MuiFormControlLabel-label": checked && {
    color: theme.palette.primary.main,
  },
}));
function MyFormControlLabel(props) {
  const radioGroup = useRadioGroup();
  let checked = false;
  if (radioGroup) {
    checked = radioGroup.value === props.value;
  }
  return <StyledFormControlLabel checked={checked} {...props} />;
}
MyFormControlLabel.propTypes = {
  value: PropTypes.any,
};
var Service;
const AddServiceContract = (props) => {
  const formFields = {
    customerContractReferenceNumber: {
      name: "customerContractReferenceNumber",
      label: "Customer Contract Reference #",
      value: "",
      isValid: true,
      topLabel: true,
      validationRequired: true,
      validPattern: "SPECIAL_CHARS_DESC",
      errorMsg: "Please enter reference number",
    },
    fromDate: {
      name: "fromDate",
      label: "From Date",
      value: null,
      isValid: true,
      topLabel: true,
      validationRequired: true,
      validPattern: "DATE",
      errorMsg: "Please enter from date",
    },
    toDate: {
      name: "toDate",
      label: "To Date",
      value: null,
      topLabel: true,
      isValid: true,
      validationRequired: true,
      validPattern: "DATE",
      errorMsg: "Please enter to date",
    },
    paymentTermsInDays: {
      name: "paymentTermsInDays",
      label: "Payment Terms",
      value: "",
      type: "number",
      isValid: true,
      topLabel: true,
      validationRequired: true,
      validPattern: "NUMERIC",
      min: 0,
      errorMsg: "Please enter payment terms in days",
      onlyInt: true,
    },
    notes: {
      name: "notes",
      label: "Notes",
      value: "",
      topLabel: true,
      isValid: true,
      validationRequired: false,
      maxLength: "300",
    },
  };
  const classes = useStyles();
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const [onReceipt, setOnReceipt] = React.useState("payTerms");
  const [spinner, setSpinner] = useState(false);
  const [loader, setLoader] = useState(false);
  const [contractData, setContractData] = useState();
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  const buttonHandler = (e) => {
    setOnReceipt(e.target.value);
  };
  useEffect(() => {
    window.scrollTo({ top: 0 });
    if (props.location.state && props.location.state.contractId) {
      Service.getContract({ contractId: props.location.state.contractId })
        .then((res) => {
          let newFields = _.cloneDeep(fields);
          let contractData = {
            ...res,
            attachments: res.attachments.attachments,
          };
          newFields["customerContractReferenceNumber"]["value"] =
            res.contract.customerContractReferenceNumber;
          newFields["paymentTermsInDays"]["value"] =
            res.contract.paymentTermsInDays;
          newFields["fromDate"]["value"] = new Date(res.contract.fromDate);
          newFields["toDate"]["value"] = new Date(res.contract.toDate);
          newFields["notes"]["value"] = res.contract.notes;
          if (res.contract.onReceipt) {
            setOnReceipt("payTerms");
          }
          setContractData(contractData);
          setFields(newFields);
          setLoader(false);
        })
        .catch(() => {
          setLoader(false);
        });
    }
  }, []);
  const timeStampChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    newFields[name]["isValid"] = true;
    if (name === "fromDate") {
      newFields["toDate"]["minDate"] = value;
    }
    if (name === "toDate") {
      newFields["fromDate"]["maxDate"] = value;
    }
    setFields(newFields);
  };
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    newFields[name]["isValid"] = true;
    if (value.length === 0 && newFields[name]["validationRequired"]) {
      newFields[name]["isValid"] = false;
    }
    setFields(newFields);
  };
  const onInputBlurHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    if (
      newFields[name]["min"] !== undefined &&
      newFields[name]["min"] > parseInt(value)
    ) {
      newFields[name]["value"] = newFields[name]["min"];
      setFields(newFields);
    }
    if (
      newFields[name]["max"] !== undefined &&
      newFields[name]["max"] < parseInt(value)
    ) {
      newFields[name]["value"] = newFields[name]["max"];
      setFields(newFields);
    }
  };
  const history = useHistory();
  Service = global.service;
  let validateFields = new ValidateFields();
  let util = new Util();
  const addContractHandler = () => {
    let dataObj = {
      ...fields,
    };
    let dataStatus;
    if (onReceipt === "receipt") {
      var { paymentTermsInDays, ...contractRest } = dataObj;
      dataStatus = validateFields.validateFieldsData(contractRest);
    } else {
      dataStatus = validateFields.validateFieldsData(dataObj);
    }
    if (dataStatus.status) {
      setSpinner(true);
      let dataObj = {
        ...fields,
        vendorId: props.location.state.vendorId,
        fromDate: util.formatDate(fields.fromDate.value),
        toDate: util.formatDate(fields.toDate.value),
        onReceipt: onReceipt === "receipt" ? true : false,
      };
      Service.createVendorContract(dataObj)
        .then(() => {
          let newFields = _.cloneDeep(formFields);
          setFields(newFields);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.success,
            message: "Contract created successfully!",
          });
          setSpinner(false);
          window.scroll({ top: 0, behavior: "smooth" });
        })
        .catch((error) => {
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
          setSpinner(false);
        });
    } else {
      let newFields = _.cloneDeep(fields);
      setFields(newFields);
    }
  };
  const updateContractHandler = () => {
    let dataObj = {
      ...fields,
    };
    let dataStatus;
    if (onReceipt === "receipt") {
      var { paymentTermsInDays, ...contractRest } = dataObj;
      dataStatus = validateFields.validateFieldsData(contractRest);
    } else {
      dataStatus = validateFields.validateFieldsData(dataObj);
    }
    if (dataStatus.status) {
      setSpinner(true);
      let dataObj = {
        ...fields,
        vendorId: props.location.state.vendorId,
        contractId: props.location.state.contractId,
        fromDate: util.formatDate(fields.fromDate.value),
        toDate: util.formatDate(fields.toDate.value),
        onReceipt: onReceipt === "receipt" ? true : false,
      };
      Service.updateVendorContract(dataObj)
        .then((res) => {
          let newFields = _.cloneDeep(fields);
          newFields["customerContractReferenceNumber"]["value"] =
            res.contract.customerContractReferenceNumber;
          newFields["paymentTermsInDays"]["value"] =
            res.contract.paymentTermsInDays;
          newFields["fromDate"]["value"] = new Date(res.contract.fromDate);
          newFields["toDate"]["value"] = new Date(res.contract.toDate);
          newFields["notes"]["value"] = res.contract.notes;
          setFields(newFields);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.success,
            message: "Contract updated successfully!",
          });
          setSpinner(false);
          window.scroll({ top: 0, behavior: "smooth" });
        })
        .catch((error) => {
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
          setSpinner(false);
        });
    } else {
      let newFields = _.cloneDeep(fields);
      setFields(newFields);
    }
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  return (
    <div className="main_container">
      <Header />
      <div className="header_box">
        <Typography className="header_text">
          <ArrowBack
            className="arrow"
            onClick={() => {
              // history.push({
              //   pathname: "./servicecontractList",
              //   state: { vendorId: props.location.state.vendorId },
              // });
              history.goBack();
            }}
          />
          {props.location.state && props.location.state.contractId
            ? "Update Contract"
            : "Add Contract"}
        </Typography>
      </div>
      {alertData.open ? (
        <div className={classes.alertBox}>
          <AlertMessage
            severity={alertData.severity}
            message={alertData.message}
            closeAlert={closeAlert}
          />
        </div>
      ) : null}
      {loader ? (
        <CircularLoading />
      ) : (
        <div className="form_container">
          <div className="details_container">
            <Typography className="details_container_heading">
              Contract Details
            </Typography>
            <Grid container spacing={3} className="details_container_content">
              <Grid item xs={4}>
                <Fields.InputField
                  fieldData={fields.customerContractReferenceNumber}
                  variant="outlined"
                  inputChangeHandler={inputChangeHandler}
                  disabled={
                    props.location.state && props.location.state.contractId
                      ? true
                      : false
                  }
                />
              </Grid>
            </Grid>
            <Grid container spacing={3} className="details_container_content">
              <Grid item xs={4}>
                <Fields.DatePickerField
                  fieldData={fields.fromDate}
                  timeStampChangeHandler={timeStampChangeHandler}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <Fields.DatePickerField
                  fieldData={fields.toDate}
                  timeStampChangeHandler={timeStampChangeHandler}
                  variant="outlined"
                />
              </Grid>
            </Grid>
          </div>
          <div style={{ display: "flex" }}>
            <RadioGroup
              name="use-radio-group"
              defaultValue={"payTerms"}
              style={{ margin: "20px" }}
              onChange={(value) => {
                buttonHandler(value);
              }}
            >
              <MyFormControlLabel
                value="payTerms"
                label="Payment Terms In Days"
                control={<Radio />}
              />
              <MyFormControlLabel
                value="receipt"
                label="On Receipt "
                control={<Radio />}
              />
            </RadioGroup>
            <Grid
              item
              xs={3}
              style={{ margin: "20px" }}
              className="end_endorment_field"
            >
              {onReceipt === "payTerms" ? (
                <Fields.InputField
                  fieldData={fields.paymentTermsInDays}
                  inputChangeHandler={inputChangeHandler}
                  onInputBlurHandler={onInputBlurHandler}
                  endAdornment={
                    <InputAdornment position="end">Days</InputAdornment>
                  }
                  variant="outlined"
                />
              ) : null}
            </Grid>
          </div>
          <Grid container spacing={2} className="details_container_content">
            <Grid item xs={8}>
              <Fields.TextAreaField
                fieldData={fields.notes}
                variant="outlined"
                inputChangeHandler={inputChangeHandler}
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} className="details_container_content">
            <Grid item xs={8}>
              <Grid container spacing={2}>
                <Grid item xs={8}></Grid>
                <Grid item xs={2}>
                  <Button
                    variant="outlined"
                    onClick={() => {
                      // history.push({
                      //   pathname: "./servicecontractList",
                      //   state: { vendorId: props.location.state.vendorId },
                      // });
                      history.goBack();
                    }}
                    className="cancel_button"
                  >
                    Cancel
                  </Button>
                </Grid>
                <Grid item xs={2}>
                  <Button
                    variant="outlined"
                    className="save_button"
                    onClick={() => {
                      if (
                        props.location.state &&
                        props.location.state.contractId
                      ) {
                        updateContractHandler();
                      } else {
                        addContractHandler();
                      }
                    }}
                    startIcon={
                      spinner ? (
                        <CircularProgress size={20} color={"#fff"} />
                      ) : null
                    }
                  >
                    {props.location.state && props.location.state.contractId
                      ? "Update"
                      : "Save"}
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>
      )}
    </div>
  );
};
export default AddServiceContract;
