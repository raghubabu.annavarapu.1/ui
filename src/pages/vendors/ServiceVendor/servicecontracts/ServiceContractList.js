import React, { useState, useEffect } from "react";
import Header from "../../../../components/header";
import { useHistory } from "react-router-dom";
import { ArrowBack } from "@material-ui/icons";
import { Typography } from "@material-ui/core";
import "../../../../common.css";
import { Button } from "@material-ui/core";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import { makeStyles } from "@material-ui/core";
import { Grid } from "@material-ui/core";
import _ from "lodash";
import * as CONFIG from "../../../../config/GlobalConstants";
import * as Components from "../../../../sharedComponents";
import CloseIcon from "@material-ui/icons/Close";
import CircularLoading from "../../../../components/loader/circularLoading";
import AlertMessage from "../../../../components/alertmessage/AlertMessage";
import Util from "../../../../utils";
var Service;
const headRows = [
  {
    id: "	CustomerContractRefNo",
    disablePadding: true,
    label: "CUSTOMER CONTRACT REF #",
  },
  {
    id: "Systemgeneratedcode",
    disablePadding: true,
    label: "GL CODE",
  },
  { id: "FromDate", disablePadding: true, label: "FROM DATE" },
  { id: "ToDate", disablePadding: true, label: "TO DATE" },
  {
    id: "PaymentTermsInDays",
    disablePadding: true,
    label: "PAYMENT TERMS IN DAYS",
  },
  {
    id: "Actions",
    disablePadding: true,
    label: "ACTIONS"
  }
];
const useStyles = makeStyles((theme) => ({
  header_buttons: {
    padding: "0px 15px",
  },
  alertBox: {
    padding: "10px 20px",
  },
  contract_btn: {
    background: "#649B42",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    "&:hover": {
      background: "#649B42",
    },
  },
  filter_buttons: {
    textAlign: "right",
    padding: "0px 15px",
  },
  clear_button: {
    padding: "6px 15px",
    background: "#D3710F0D",
    border: "1px solid #D3710F",
    color: "#D3710F",
  },
}));
const ServiceContractList = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const [spinner, setSpinner] = useState(false);
  Service = global.service;
  // const filterFields = {
  //   contract: { label: "Search by Name", name: "contract", value: "", options: [] },
  //   date: { label: "Select date", name: "date", value: "" },
  // };
  // const [filters, setFilters] = useState(_.cloneDeep(filterFields));
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  const [total, setTotal] = useState(0);
  const [tableData, setTableData] = useState([]);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  // const autoCompleteChangeHandler = () => { };
  // const dateChangeHandler = () => { };
  let util = new Util();
  useEffect(() => {
    window.scrollTo({ top: 0 });
    if (props.location.state) {
      let dataObj = {
        skip: pagination.current * pagination.pageSize,
        limit: pagination.pageSize,
        vendorId: props.location.state.vendorId,
      };
      renderContracts(dataObj);
    } else {
      global.session.remove("BearerToken");
      global.session.remove("profile");
      global.session.remove("hideHeader");
      window.location = "/";
    }
  }, []);
  const renderContracts = (dataObj) => {
    setSpinner(true);
    Service.getContracts(dataObj)
      .then((res) => {
        let data = res.contracts.map((contract) => {
          return {
            CustomerContractRefNo: (
              <div style={{ textAlign: "center" }}>
                {" "}
                {contract.customerContractReferenceNumber
                  ? contract.customerContractReferenceNumber
                  : "--"}
              </div>
            ),
            Systemgeneratedcode: (
              <div style={{ textAlign: "center" }}>
                {contract.systemGeneratedContractCode}
              </div>
            ),
            FromDate: (
              <div style={{ textAlign: "center" }}>
                {util.formatDate(contract.fromDate)}
              </div>
            ),
            ToDate: (
              <div style={{ textAlign: "center" }}>
                {util.formatDate(contract.toDate)}{" "}
              </div>
            ),
            PaymentTermsInDays: (
              <div style={{ textAlign: "center" }}>
                {contract.paymentTermsInDays
                  ? contract.paymentTermsInDays
                  : "--"}
              </div>
            ),
            actions: (
              <div>
                <Components.DropDownMenu
                  options={CONFIG.SERVICECONTRACT_MORE_OPTIONS}
                  data={{
                    contractId: contract.id,
                    vendorId: props.location.state.vendorId,
                    contractName: contract.customerContractReferenceNumber
                  }}
                />
              </div>
            )
          };
        });
        setTableData(data);
        setTotal(res.totalCount);
        setSpinner(false);
      })
      .catch((error) => {
        setSpinner(false);
        setAlertData({
          open: true,
          severity: CONFIG.ALERT_SEVERITY.error,
          message:
            error.response && error.response.data
              ? error.response.data.message
              : "Something went wrong!",
        });
      });
  };
  const pageChangeHandler = (page) => {
    let dataObj = {
      skip: page * pagination.pageSize,
      limit: pagination.pageSize,
      vendorId: props.location.state.vendorId,
    };
    renderContracts(dataObj);
    setPagination({ ...pagination, current: page });
  };
  const rowsPerPageChangeHandler = (rowsPerPage) => {
    let dataObj = {
      skip: pagination.current * rowsPerPage,
      limit: rowsPerPage,
      vendorId: props.location.state.vendorId,
    };
    renderContracts(dataObj);
    setPagination({ ...pagination, pageSize: rowsPerPage });
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.push("./serviceVendor");
              }}
            />
            Contracts(Vendor Company: {props.location.state.vendorName})
          </Typography>
          <div className={classes.header_buttons}>
            <Button
              variant="contained"
              color="primary"
              startIcon={<AddCircleOutlinedIcon />}
              className={classes.contract_btn}
              onClick={() =>
                history.push({
                  pathname: "./addserviceContract",
                  state: { vendorId: props.location.state.vendorId },
                })
              }
            >
              Add Contract
            </Button>
          </div>
        </div>
        <div>
          {spinner ? (
            <CircularLoading />
          ) : (
            <Components.DataTable
              headRows={headRows}
              tableData={tableData}
              pagination={pagination}
              pageChangeHandler={pageChangeHandler}
              rowsPerPageChangeHandler={rowsPerPageChangeHandler}
              total={total}
            />
          )}
        </div>
      </div>
    </div>
  );
};
export default ServiceContractList;
