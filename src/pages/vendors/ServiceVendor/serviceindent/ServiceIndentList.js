import React, { useState, useEffect } from "react";
import Header from "../../../../components/header";
import { useHistory } from "react-router-dom";
import { ArrowBack, IndeterminateCheckBox } from "@material-ui/icons";
import { Typography } from "@material-ui/core";
import "../../../../common.css";
import { Button } from "@material-ui/core";
import AddCircleRoundedIcon from "@mui/icons-material/AddCircleRounded";
import { makeStyles } from "@material-ui/core";
import Chip from "@mui/material/Chip";
import _ from "lodash";
import * as CONFIG from "../../../../config/GlobalConstants";
import * as Components from "../../../../sharedComponents";
import CloseIcon from "@material-ui/icons/Close";
import CircularLoading from "../../../../components/loader/circularLoading";
import AlertMessage from "../../../../components/alertmessage/AlertMessage";
import Util from "../../../../utils";
import IndentReciept from "../../../indent/IndentReciept";
var Service;
const headRows = [
  // { id: "S_NO", disablePadding: true, label: "S.NO" },
  { id: "INDENT_ID", disablePadding: true, label: "INDENT#" },
  { id: "VEHICLE_NUMBER", disablePadding: true, label: "VEHICLE#" },
  { id: "LR_NUMBER", disablePadding: true, label: "LR#" },
  { id: "TRIP_ID", disablePadding: true, label: "TRIP ID" },
  {
    id: "VENDOR_INFORMATION",
    disablePadding: true,
    label: "VENDOR INFORMATION",
  },
  { id: "CREATEDBY", disablePadding: true, label: "CREATED BY" },
  { id: "CREATEDDATE", disablePadding: true, label: "CREATED DATE" },
  { id: "STATUS", disablePadding: true, label: "STATUS" },
  { id: "AMOUNT", disablePadding: true, label: "AMOUNT(RS)" },
  { id: "ACTIONS", disablePadding: true, label: "ACTIONS" },
];
const useStyles = makeStyles((theme) => ({
  header_buttons: {
    padding: "0px 15px",
  },
  addindent_btn: {
    textTransform: "none",
    padding: "8px 15px",
    backgroundColor: "#649B42",
    "&:hover": {
      backgroundColor: "#649B42",
      padding: "8px 15px",
    },
  },
  alertBox: {
    padding: "10px 20px",
  },
  header_box: {
    background: "#FFFFFF",
    padding: "20px 15px",
    display: "flex",
    justifyContent: "space-between",
  },
  contract_btn: {
    background: "#649B42",
    textTransform: "none",
    color: "#FFFFFF",
    padding: "8px 15px",
    "&:hover": {
      background: "#649B42",
    },
  },
  filter_buttons: {
    textAlign: "right",
    padding: "0px 15px",
  },
  clear_button: {
    padding: "6px 15px",
    background: "#D3710F0D",
    border: "1px solid #D3710F",
    color: "#D3710F",
  },
}));
const ServiceIndentList = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const [spinner, setSpinner] = useState(false);
  Service = global.service;
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  const [total, setTotal] = useState(0);
  const [tableData, setTableData] = useState([]);
  const [indentIsOpen, setIndentIsOpen] = useState(false);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  let util = new Util();
  useEffect(() => {
    window.scrollTo({ top: 0 });
    if (props.location.state) {
      let dataObj = {
        skip: pagination.current * pagination.pageSize,
        limit: pagination.pageSize,
        vendorId: props.location.state.vendorId,
      };
      renderIndents(dataObj);
    } else {
      global.session.remove("BearerToken");
      global.session.remove("profile");
      global.session.remove("hideHeader");
      window.location = "/";
    }
  }, []);
  function handleClickOpen() {
    setIndentIsOpen(false);
  }
  const renderIndents = (dataObj) => {
    setSpinner(true);
    Service.getIndents(dataObj)
      .then((res) => {
        let data = res.indents.map((indent) => {
          return {
            INDENT_ID: <div style={{ textAlign: "center" }}>{indent.id}</div>,
            VEHICLE_NUMBER: <div>{indent.truck.registrationNumber}</div>,
            LR_NUMBER: (
              <div>{indent.trip && indent.trip.lr ? indent.trip.lr : "--"}</div>
            ),
            TRIP_ID: indent.trip && indent.trip.id ? indent.trip.id : "--",
            VENDOR_INFORMATION: (
              // <div>{indent.vendor ? indent.vendor.firstName + " " + indent.vendor.lastName : "--"}</div>,
              <div className={classes.table_data}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <span>
                    {indent.vendor
                      ? indent.vendor.firstName + " " + indent.vendor.lastName
                      : "--"}
                  </span>
                  <span style={{ fontWeight: "bold" }}>
                    {indent.vendor && indent.vendor.mobileNumber
                      ? indent.vendor.mobileNumber
                      : "--"}
                  </span>
                </div>
              </div>
            ),
            CREATEDBY: (
              <div className={classes.table_data}>
                <div
                  style={{
                    display: "flex",

                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <span>{indent.createdBy ? indent.createdBy.name : "--"}</span>
                  <span style={{ fontWeight: "bold" }}>
                    {indent.createdBy ? indent.createdBy.roleName : "--"}
                  </span>
                </div>
              </div>
            ),
            CREATEDDATE: <div>{indent.createdAtRead}</div>,
            STATUS: (
              <div>
                <Chip
                  style={{
                    background: `${
                      CONFIG.INDENT_STATUS.filter(
                        (tr) => parseInt(tr.value) === indent.status
                      )[0].color
                    }`,
                    color: "#ffff",
                  }}
                  label={
                    CONFIG.INDENT_STATUS.filter(
                      (tr) => parseInt(tr.value) === indent.status
                    )[0].label
                  }
                />
              </div>
            ),
            AMOUNT: (
              <div className={classes.table_data}>
                {indent.indentTotal ? indent.indentTotal.toFixed(2) : "--"}
              </div>
            ),
            ACTIONS: (
              <div className={classes.table_data}>
                {/* <RemoveRedEyeOutlinedIcon style={{ color: "#1269A8", cursor: "pointer" }} /> */}
                <IndentReciept
                  indentId={indent.id}
                  handleClickOpen={handleClickOpen}
                />
              </div>
            ),
          };
        });
        setTableData(data);
        setTotal(res.totalCount);
        setSpinner(false);
      })
      .catch((error) => {
        setSpinner(false);
        setAlertData({
          open: true,
          severity: CONFIG.ALERT_SEVERITY.error,
          message:
            error.response && error.response.data
              ? error.response.data.message
              : "Something went wrong!",
        });
      });
  };
  const pageChangeHandler = (page) => {
    let dataObj = {
      skip: page * pagination.pageSize,
      limit: pagination.pageSize,
      vendorId: props.location.state.vendorId,
    };
    renderIndents(dataObj);
    setPagination({ ...pagination, current: page });
  };
  const rowsPerPageChangeHandler = (rowsPerPage) => {
    let dataObj = {
      skip: pagination.current * rowsPerPage,
      limit: rowsPerPage,
      vendorId: props.location.state.vendorId,
    };
    renderIndents(dataObj);
    setPagination({ ...pagination, pageSize: rowsPerPage });
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.push("./serviceVendor");
              }}
            />
            Indents(Vendor Company: {props.location.state.vendorName})
          </Typography>
          <div className={classes.header_buttons}>
            <Button
              variant="contained"
              className={classes.addindent_btn}
              color="primary"
              disableElevation={true}
              startIcon={<AddCircleRoundedIcon />}
              onClick={() => {
                history.push({
                  pathname: "./addIndent",
                  state: { vendorId: props.location.state.vendorId },
                });
              }}
            >
              Add Indents
            </Button>
          </div>
        </div>
        <div>
          {spinner ? (
            <CircularLoading />
          ) : (
            <Components.DataTable
              headRows={headRows}
              tableData={tableData}
              pagination={pagination}
              pageChangeHandler={pageChangeHandler}
              rowsPerPageChangeHandler={rowsPerPageChangeHandler}
              total={total}
            />
          )}
        </div>
      </div>
    </div>
  );
};
export default ServiceIndentList;
