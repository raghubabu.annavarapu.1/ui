import React, { useState, useRef, useEffect } from "react";
import Header from "../../../components/header";
import { ArrowBack } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import { Typography, Grid, Button, CircularProgress } from "@material-ui/core";
import _ from "lodash";
import * as Fields from "../../../sharedComponents";
import * as CONFIG from "../../../config/GlobalConstants";
import ValidateFields from "../../../validations/validateFields";
import AlertMessage from "../../../components/alertmessage/AlertMessage";
import CircularLoading from "../../../components/loader/circularLoading";

var Service;
const AddService = (props) => {
  const formFields = {
    firstName: {
      name: "firstName",
      label: "First Name",
      value: "",
      isValid: true,
      validationRequired: true,
      topLabel: true,
      validPattern: "SPECIAL_CHARS_DESC",
      errorMsg: "Please enter first name",
      ref: useRef(),
    },
    lastName: {
      name: "lastName",
      label: "Last Name",
      value: "",
      isValid: true,
      validationRequired: true,
      topLabel: true,
      validPattern: "SPECIAL_CHARS_DESC",
      errorMsg: "Please enter last name",
      ref: useRef(),
    },
    status: {
      name: "status",
      label: "Status",
      value: CONFIG.STATUSES[0],
      options: CONFIG.STATUSES,
      topLabel: true,
      validationRequired: true,
      isValid: true,
      errorMsg: "Please select status",
      validPattern: "SPECIAL_CHARS_DESC",
      ref: useRef(),
    },
    companyName: {
      name: "companyName",
      label: "Company Name",
      value: "",
      isValid: true,
      topLabel: true,
      isValid: true,
      validationRequired: true,
      validPattern: "SPECIAL_CHARS_DESC",
      ref: useRef(),
    },
    mobileNumber: {
      name: "mobileNumber",
      label: "Mobile Number",
      value: "",
      isValid: true,
      validationRequired: true,
      topLabel: true,
      validPattern: "MOBILE_NUMBER",
      errorMsg: "Please enter mobile number",
      ref: useRef(),
      maxLength: 10,
    },
    emailAddress: {
      name: "emailAddress",
      label: "Email ID",
      value: "",
      isValid: true,
      validationRequired: true,
      topLabel: true,
      validPattern: "EMAIL",
      errorMsg: "Please enter email",
      ref: useRef(),
    },
    gstNumber: {
      name: "gstNumber",
      label: "GST Number",
      value: "",
      isValid: true,
      topLabel: true,
      maxLength: 15,
      ref: useRef(),
      errorMsg: "Please enter valid gst",
    },
    businessId: {
      name: "businessId",
      label: "Business ID",
      value: "",
      isValid: true,
      topLabel: true,
      ref: useRef(),
    },
    // glNumber: {
    //   name: "glNumber",
    //   label: "GL Number",
    //   value: "",
    //   isValid: true,
    //   topLabel: true,
    //   ref: useRef(),
    // },
    openingBalance: {
      name: "openingBalance",
      label: "Opening Balance",
      value: "",
      isValid: true,
      topLabel: true,
      type: "number",
      min: 0,
      ref: useRef(),
    },
    areaServiceId: {
      name: "areaServiceId",
      label: "Area Service ID",
      value: { label: "", value: "" },
      isValid: true,
      validationRequired: true,
      validPattern: "NUMERIC",
      topLabel: true,
      errorMsg: "Please enter area service id",
      ref: useRef(),
      options: [],
    },
    addressLine1: {
      name: "addressLine1",
      label: "Address Line 1",
      value: "",
      isValid: true,
      validationRequired: true,
      topLabel: true,
      errorMsg: "Please enter addressline1",
      validPattern: "SPECIAL_CHARS_DESC",
      ref: useRef(),
    },
    addressLine2: {
      name: "addressLine2",
      label: "Address Line 2",
      value: "",
      isValid: true,
      topLabel: true,
      ref: useRef(),
    },
    addressLine3: {
      name: "addressLine3",
      label: "Address Line 3",
      value: "",
      isValid: true,
      topLabel: true,
      ref: useRef(),
    },
    city: {
      name: "city",
      label: "City",
      value: "",
      isValid: true,
      validationRequired: true,
      topLabel: true,
      errorMsg: "Please enter city",
      validPattern: "SPECIAL_CHARS_DESC",
      ref: useRef(),
    },
    state: {
      name: "state",
      label: "State",
      value: "",
      isValid: true,
      validationRequired: true,
      topLabel: true,
      errorMsg: "Please enter state",
      validPattern: "SPECIAL_CHARS_DESC",
      ref: useRef(),
    },
    country: {
      name: "country",
      label: "Country",
      value: "",
      isValid: true,
      validationRequired: true,
      topLabel: true,
      errorMsg: "Please enter country",
      validPattern: "SPECIAL_CHARS_DESC",
      ref: useRef(),
    },
    postalCode: {
      name: "postalCode",
      label: "Postal Code",
      value: "",
      isValid: true,
      validationRequired: true,
      topLabel: true,
      errorMsg: "Please enter postal code",
      validPattern: "SPECIAL_CHARS_DESC",
      ref: useRef(),
    },
  };
  const [fields, setFields] = useState(_.cloneDeep(formFields));
  const [spinner, setSpinner] = useState(false);
  const [alertData, setAlertData] = useState({
    open: false,
    severity: "",
    message: "",
  });
  const [loader, setLoader] = useState(false);
  const [vendorData, setVendorData] = useState();
  const history = useHistory();
  Service = global.service;
  let validateFields = new ValidateFields();
  useEffect(() => {
    if (props.location.state) {
      setLoader(true);
      let promise1 = Service.getServiceAreas();
      let promise2 = Service.getVendor({
        vendorId: props.location.state.vendorId,
      });
      Promise.all([promise1, promise2])
        .then((res) => {
          setLoader(false);
          let newFields = _.cloneDeep(fields);
          let serviceArea = res[0].filter(
            (item) => Number(item.id) === Number(res[1].areaServiceId)
          )[0];
          newFields["firstName"]["value"] = res[1].firstName;
          newFields["lastName"]["value"] = res[1].lastName;
          newFields["mobileNumber"]["value"] = res[1].mobileNumber;
          newFields["emailAddress"]["value"] = res[1].emailAddress;
          newFields["companyName"]["value"] = res[1].companyName;
          newFields["gstNumber"]["value"] = res[1].gstNumber
            ? res[1].gstNumber
            : "";
          // newFields["glNumber"]["value"] = res[1].glNumber ? res[1].glNumber : "";
          newFields["businessId"]["value"] = res[1].businessId;
          newFields["addressLine1"]["value"] = res[1].addressLine1;
          newFields["addressLine2"]["value"] = res[1].addressLine2;
          newFields["addressLine3"]["value"] = res[1].addressLine3;
          newFields["openingBalance"]["value"] = res[1].openingBalance;
          newFields["areaServiceId"]["options"] = res[0].map((opt) => {
            return { label: opt.serviceArea, value: opt.id };
          });
          newFields["status"]["value"] = CONFIG.STATUSES.filter(
            (item) => Number(item.value) === Number(res[1].status)
          )[0];
          newFields["areaServiceId"]["value"] = {
            label:
              serviceArea && serviceArea.serviceArea
                ? serviceArea.serviceArea
                : "",
            value: serviceArea && serviceArea.id ? serviceArea.id : "",
          };
          newFields["postalCode"]["value"] = res[1].postalCode;
          newFields["city"]["value"] = res[1].city;
          newFields["state"]["value"] = res[1].state;
          newFields["country"]["value"] = res[1].country;
          setFields(newFields);
          setVendorData(res[1]);
          setLoader(false);
        })
        .catch((error) => {
          setLoader(false);
        });
    } else {
      Service.getServiceAreas()
        .then((res) => {
          let newFields = _.cloneDeep(fields);
          newFields["areaServiceId"]["options"] = res.map((opt) => {
            return { label: opt.serviceArea, value: opt.id };
          });
          setFields(newFields);
        })
        .catch(() => {});
    }
  }, []);
  const inputChangeHandler = (value, name) => {
    let newFields = _.cloneDeep(fields);
    newFields[name]["value"] = value;
    newFields[name]["isValid"] = true;
    if (value.length === 0 && newFields[name]["validationRequired"]) {
      newFields[name]["isValid"] = false;
    }
    setFields(newFields);
  };
  const validateGst = () => {
    const dataFields = _.cloneDeep(fields);
    let valid = true;
    for (var key in dataFields) {
      if (dataFields.hasOwnProperty(key)) {
        var val = dataFields[key];
        if (val.name === "gstNumber" && val.value !== "") {
          let re = /^[A-Z0-9]{15}$/;
          if (re.test(val.value)) {
            valid = true;
            dataFields[val.name]["isValid"] = true;
          } else {
            valid = false;
            dataFields[val.name]["isValid"] = false;
          }
        }
      }
    }
    return { status: valid, data: dataFields };
  };
  const addServiceHandler = () => {
    let dataObj = _.cloneDeep({
      ...fields,
      areaServiceId: {
        ...fields.areaServiceId,
        value:
          fields.areaServiceId.value.value === undefined
            ? fields.areaServiceId.value
            : fields.areaServiceId.value.value,
      },
      status: {
        ...fields.status,
        value:
          fields.status.value.value === undefined
            ? fields.status.value
            : fields.status.value.value,
      },
    });
    let dataStatus = validateFields.validateFieldsData(dataObj);
    const validGst = validateGst();
    if (dataStatus.status && validGst.status) {
      setSpinner(true);
      Service.createVendor(dataObj)
        .then(() => {
          setSpinner(false);
          let newFields = _.cloneDeep(formFields);
          newFields["areaServiceId"]["options"] = [
            ...fields.areaServiceId.options,
          ];
          setFields(newFields);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.success,
            message: "Vendor created successfully!",
          });
          window.scrollTo({ top: 0, behavior: "smooth" });
        })
        .catch((error) => {
          setSpinner(false);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
          window.scrollTo({ top: 0, behavior: "smooth" });
        });
    } else {
      let newFields = _.cloneDeep(fields);
      let erroredFields = Object.keys(dataStatus.data).filter(
        (key, i) => dataStatus.data[key].isValid === false
      );
      if (erroredFields.length === 0) {
        erroredFields = Object.keys(validGst.data).filter(
          (key, i) => validGst.data[key].isValid === false
        );
      }
      newFields = dataStatus.data;
      newFields = {
        ...newFields,
        gstNumber: validGst.data.gstNumber,
        areaServiceId: {
          ...dataStatus.data.areaServiceId,
          value: dataStatus.data.areaServiceId.value
            ? newFields.areaServiceId.options.filter(
                (opt) => opt.value === dataStatus.data.areaServiceId.value
              )[0]
            : { label: "", value: "" },
        },
        status: {
          ...dataStatus.data.status,
          value: dataStatus.data.status.value
            ? CONFIG.STATUSES.filter(
              (opt) => opt.value === dataStatus.data.status.value
            )[0]
            : { label: "", value: "" },
        },
      };
      setFields(newFields);
      if (erroredFields.length > 0) {
        window.scrollTo(0, fields[erroredFields[0]].ref.current.offsetTop);
      }
    }
  };
  const updateVendorHandler = () => {
    let dataObj = _.cloneDeep({
      ...fields,
      areaServiceId: {
        ...fields.areaServiceId,
        value:
          fields.areaServiceId.value.value === undefined
            ? fields.areaServiceId.value
            : fields.areaServiceId.value.value,
      },
      status: {
        ...fields.status,
        value:
          fields.status.value.value === undefined
            ? fields.status.value
            : fields.status.value.value,
      },
    });
    let dataStatus = validateFields.validateFieldsData(dataObj);
    const validGst = validateGst();
    if (dataStatus.status && validGst.status) {
      setSpinner(true);
      Service.updateServiceVendor(dataObj, {
        vendorId: props.location.state.vendorId,
      })
        .then((res) => {
          setSpinner(false);
          let newFields = _.cloneDeep(fields);
          newFields["firstName"]["value"] = res.firstName;
          newFields["lastName"]["value"] = res.lastName;
          newFields["emailAddress"]["value"] = res.emailAddress;
          newFields["gstNumber"]["value"] = res.gstNumber
            ? res.gstNumber
            : "";
          newFields["addressLine1"]["value"] = res.addressLine1;
          newFields["addressLine2"]["value"] = res.addressLine2;
          newFields["addressLine3"]["value"] = res.addressLine3;
          newFields["status"]["value"] = CONFIG.STATUSES.filter(
            (item) => Number(item.value) === Number(res.status)
          )[0];
          newFields["postalCode"]["value"] = res.postalCode;
          newFields["city"]["value"] = res.city;
          newFields["state"]["value"] = res.state;
          newFields["country"]["value"] = res.country;
          setFields(newFields);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.success,
            message: "Vendor Updated Successfully!",
          });
          window.scrollTo({ top: 0, behavior: "smooth" });
        })
        .catch((error) => {
          setSpinner(false);
          setAlertData({
            open: true,
            severity: CONFIG.ALERT_SEVERITY.error,
            message:
              error.response && error.response.data
                ? error.response.data.message
                : "Something went wrong!",
          });
          window.scrollTo({ top: 0, behavior: "smooth" });
        });
    } else {
      let newFields = _.cloneDeep(fields);
      let erroredFields = Object.keys(dataStatus.data).filter(
        (key, i) => dataStatus.data[key].isValid === false
      );
      if (erroredFields.length === 0) {
        erroredFields = Object.keys(validGst.data).filter(
          (key, i) => validGst.data[key].isValid === false
        );
      }
      newFields = dataStatus.data;
      newFields = {
        ...newFields,
        gstNumber: validGst.data.gstNumber,
        areaServiceId: {
          ...dataStatus.data.areaServiceId,
          value: dataStatus.data.areaServiceId.value
            ? newFields.areaServiceId.options.filter(
                (opt) => opt.value === dataStatus.data.areaServiceId.value
              )[0]
            : { label: "", value: "" },
        },
        status: {
          ...dataStatus.data.status,
          value: dataStatus.data.status.value
            ? CONFIG.STATUSES.filter(
              (opt) => opt.value === dataStatus.data.status.value
            )[0]
            : { label: "", value: "" },
        },
      };
      setFields(newFields);
      if (erroredFields.length > 0) {
        window.scrollTo(0, fields[erroredFields[0]].ref.current.offsetTop);
      }
    }
  };
  const closeAlert = () => {
    let alert = _.cloneDeep(alertData);
    alert.open = false;
    alert.severity = "";
    alert.message = "";
    setAlertData(alert);
  };
  const autoCompleteChangeHandler = (value, name) => {
    if (value) {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = value;
      newFields[name]["isValid"] = true;
      setFields(newFields);
    } else {
      let newFields = _.cloneDeep(fields);
      newFields[name]["value"] = { value: "", label: "" };
      if (newFields[name]["validationRequired"]) {
        newFields[name]["isValid"] = false;
      }
      setFields(newFields);
    }
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">
            <ArrowBack
              className="arrow"
              onClick={() => {
                history.push("./serviceVendor");
              }}
            />
            {props.location.state
              ? "Update Service Vendor"
              : "Add Service Vendor"}
          </Typography>
        </div>
        {alertData.open ? (
          <div style={{ padding: "10px 12px" }}>
            <AlertMessage
              severity={alertData.severity}
              message={alertData.message}
              closeAlert={closeAlert}
            />
          </div>
        ) : null}
        {loader ? (
          <CircularLoading />
        ) : (
          <div className="form_container">
            <div className="details_container border_bottom">
              <Typography className="details_container_heading">
                User Details
              </Typography>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} ref={fields.firstName.ref}>
                  <Fields.InputField
                    fieldData={fields.firstName}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
                <Grid item xs={4} ref={fields.lastName.ref}>
                  <Fields.InputField
                    fieldData={fields.lastName}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} ref={fields.mobileNumber.ref}>
                  <Fields.InputField
                    fieldData={fields.mobileNumber}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                    disabled={props.location.state ? true : false}
                  />
                </Grid>
                <Grid item xs={4} ref={fields.emailAddress.ref}>
                  <Fields.InputField
                    fieldData={fields.emailAddress}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} ref={fields.status.ref} className="custom_select">
                  <Fields.AntSelectableSearchField
                    fieldData={fields.status}
                    variant="outlined"
                    autoCompleteChangeHandler={autoCompleteChangeHandler}
                  />
                </Grid>
              </Grid>
            </div>
            <div className="details_container border_bottom">
              <Typography className="details_container_heading">
                Business Details
              </Typography>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} ref={fields.companyName.ref}>
                  <Fields.InputField
                    fieldData={fields.companyName}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                    disabled={props.location.state ? true : false}
                  />
                </Grid>
                <Grid item xs={4} ref={fields.businessId.ref}>
                  <Fields.InputField
                    fieldData={fields.businessId}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                    disabled={props.location.state ? true : false}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} ref={fields.gstNumber.ref}>
                  <Fields.InputField
                    fieldData={fields.gstNumber}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
                <Grid
                  item
                  xs={4}
                  ref={fields.areaServiceId.ref}
                  className={"custom_select"}
                >
                  <Fields.AntSelectableSearchField
                    fieldData={fields.areaServiceId}
                    autoCompleteChangeHandler={autoCompleteChangeHandler}
                    disabled={props.location.state ? true : false}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                {/* <Grid item xs={4} ref={fields.glNumber.ref}>
                  <Fields.InputField
                    fieldData={fields.glNumber}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                    disabled={props.location.state ? true : false}
                  />
                </Grid> */}
                <Grid item xs={4} ref={fields.openingBalance.ref}>
                  <Fields.InputField
                    fieldData={fields.openingBalance}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                    disabled={props.location.state ? true : false}
                  />
                </Grid>
              </Grid>
            </div>
            <div className="details_container">
              <Typography className="details_container_heading">
                Address Details
              </Typography>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} ref={fields.addressLine1.ref}>
                  <Fields.InputField
                    fieldData={fields.addressLine1}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
                <Grid item xs={4} ref={fields.addressLine2.ref}>
                  <Fields.InputField
                    fieldData={fields.addressLine2}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} ref={fields.addressLine3.ref}>
                  <Fields.InputField
                    fieldData={fields.addressLine3}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
                <Grid item xs={4} ref={fields.postalCode.ref}>
                  <Fields.InputField
                    fieldData={fields.postalCode}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} ref={fields.city.ref}>
                  <Fields.InputField
                    fieldData={fields.city}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
                <Grid item xs={4} ref={fields.state.ref}>
                  <Fields.InputField
                    fieldData={fields.state}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={4} ref={fields.country.ref}>
                  <Fields.InputField
                    fieldData={fields.country}
                    variant="outlined"
                    inputChangeHandler={inputChangeHandler}
                  />
                </Grid>
              </Grid>
            </div>
            <div className="details_container">
              <Grid container spacing={3} className="details_container_content">
                <Grid item xs={8}>
                  <Grid container spacing={2}>
                    <Grid item xs={8}></Grid>
                    <Grid item xs={2}>
                      <Button
                        variant="outlined"
                        className="cancel_button"
                        onClick={() => {
                          history.push("./serviceVendor");
                        }}
                      >
                        Cancel
                      </Button>
                    </Grid>
                    <Grid item xs={2}>
                      <Button
                        variant="outlined"
                        className="save_button"
                        onClick={() => {
                          if (props.location.state) {
                            updateVendorHandler();
                          } else {
                            addServiceHandler();
                          }
                        }}
                        startIcon={
                          spinner ? (
                            <CircularProgress size={20} color={"#fff"} />
                          ) : null
                        }
                      >
                        {props.location.state ? "Update" : "Save"}
                      </Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default AddService;
