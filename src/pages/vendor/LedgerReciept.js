import * as React from 'react';
import PropTypes from 'prop-types';
import { DialogContent } from '@material-ui/core';
import Dialog from '@mui/material/Dialog';
import ViewHeadlineIcon from '@material-ui/icons/ViewHeadline';
import { makeStyles } from '@material-ui/core';
import { Typography, Divider, Button } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
    receipt_icon: {
        verticalAlign: 'middle'
    },
    zigzag: {
        height: '57vh',
        width: '20vw',
    },
    details: {
        display: 'flex',
    },
    indent_id: {
        marginRight: theme.spacing(1),
        fontSize: '14px'
    },
    indent_value: {
        flexGrow: '1',
        fontSize: '14px',
        fontWeight: 'bold'
    },
    date: {
        fontSize: '12px',
        fontWeight: 'bold',
    },
    ledger_cheq: {
        display: 'flex',
        marginTop: '20px'
    },
    particular_details: {
        display: 'flex',
        marginTop: '20px'
    },
    particular: {
        flexGrow: '1',
        fontSize: '12px',
        color: '#8E8E8E'
    },
    vehicle: {
        fontSize: '12px',
    },
    diesel_details: {
        display: 'flex'
    },
    diesel_vehicle: {
        flexGrow: '1',
        fontSize: '12px'
    },
    total_amt: {
        fontSize: '12px',
        color: '#8E8E8E'
    },
    diesel_amt: {
        fontSize: '12px',
    },
    download_btn: {
        textTransform: 'none',
        backgroundColor: '#366E93',
        marginTop: '180px',
        "&:hover": {
            background: "#366E93",
        },
    }
}));
function SimpleDialog(props) {
    const { onClose, selectedValue, open } = props;
    const classes = useStyles();
    const handleClose = () => {
        onClose(selectedValue);
    };
    return (
        <Dialog onClose={handleClose} open={open}>
            <DialogContent className={classes.zigzag}>
                <div>
                    <div>
                        <div className={classes.details}>
                            <Typography className={classes.indent_id}>
                                Indent# id :
                            </Typography>
                            <Typography className={classes.indent_value}>
                                12345
                            </Typography>
                            <Typography className={classes.date}>
                                22-09-2021
                            </Typography>
                        </div>
                        <div className={classes.ledger_cheq}>
                            <Typography className={classes.indent_id}>
                                Cheque# :
                            </Typography>
                            <Typography className={classes.indent_value}>
                                AQssqas2312sASCWer
                            </Typography>
                        </div>
                        <Divider style={{ marginTop: '20px' }} />
                    </div>
                    <div>
                        <div className={classes.particular_details}>
                            <Typography className={classes.particular}>
                                PARTICULAR
                            </Typography>
                            <Typography className={classes.total_amt}>
                                TOTAL AMT
                            </Typography>
                        </div>
                        <div className={classes.diesel_details}>
                            <Typography className={classes.diesel_vehicle}>
                                Payments
                            </Typography>
                            <Typography className={classes.diesel_amt}>
                                RS 5000.00
                            </Typography>
                        </div>
                    </div>
                    <Typography align='center'>
                        <Button
                            color='primary'
                            variant='contained' className={classes.download_btn}
                            onClose={handleClose}

                        >
                            Download Reciept
                        </Button>
                    </Typography>
                </div>
            </DialogContent>

        </Dialog>
    );
}

SimpleDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    selectedValue: PropTypes.string.isRequired,
};

export default function SimpleDialogDemo() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [selectedValue, setSelectedValue] = React.useState(0);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = (value) => {
        setOpen(false);
        setSelectedValue(value);
    };
    return (
        <div>
            <div onClick={handleClickOpen}>
                <ViewHeadlineIcon className={classes.receipt_icon} /> View Receipt
            </div>
            <SimpleDialog
                selectedValue={selectedValue}
                open={open}
                onClose={handleClose}
            />
        </div>
    );
}