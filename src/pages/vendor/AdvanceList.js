import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import _ from "lodash";
import ViewHeadlineIcon from '@material-ui/icons/ViewHeadline';
import TablePagination from "@mui/material/TablePagination";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Checkbox from "@mui/material/Checkbox";
import PropTypes from "prop-types";
import { Table, Button } from "@material-ui/core";
import TableBody from "@material-ui/core/TableBody";
import Divider from '@mui/material/Divider';
import { useHistory } from "react-router-dom";
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
const headRows = [
    { id: "DATE", disablePadding: true, label: "DATE" },
    { id: "PARTICULAR", disablePadding: true, label: "PARTICULAR" },
    { id: "REFERENCE ID", disablePadding: true, label: "REFERENCE ID" },
    { id: "ADVANCE AMT", disablePadding: true, label: "ADVANCE AMT" },
    { id: "View Reciept", disablePadding: true, label: "" },
];
const useStyles = makeStyles((theme) => ({
    main_container: {
        background: "#F5F6F7",
        height: "100vh"
    },
    pay_btn: {
        textTransform: "none",
        backgroundColor: "#D08B1D",
        "&:hover": {
            background: "#D08B1D",
        },
        margin: '20px'
    },
    filter_box: {
        padding: "20px 24px",
        display: "flex",
        justifyContent: "space-between",
        border: "2px solid #DBE0E7",
    },
    filter_field: {
        background: "#FFFFFF",
    },
    filter_buttons: {
        textAlign: "right",
        padding: "0px 15px",
    },
    clear_button: {
        padding: "6px 15px",
        background: "#D3710F0D",
        border: "1px solid #D3710F",
        color: "#D3710F",
    },
    table_head: {
        padding: "10px 24px 15px 24px",
        display: "flex",
        justifyContent: "space-between",
    },
    selected_text: {
        color: "#929DAF",
        fontSize: "14px",
        letterSpacing: "0.5px",
        alignSelf: "center"
    },
    row_paper: {
        background: "#FFFFFF",
        boxShadow: "rgba(0, 0, 0, 0.05) 0px 0px 0px 1px",
        borderRadius: "4px"
    },
    divider: {
        height: "12px"
    },
    icon: {
        cursor: "pointer"
    },
    receipt_icon: {
        verticalAlign: "middle"
    },
    receipt: {
        color: "#366E93",
        cursor: "pointer"
    },
}));

function EnhancedTableHead(props) {
    const { classes, onSelectAllClick, numSelected, rowCount } = props;
    return (
        <TableHead className={classes.tableHead}>
            <TableRow>
                <TableCell padding="checkbox">
                    <Checkbox
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{ "aria-label": "select all desserts" }}
                        className={classes.whiteColor}
                    />
                </TableCell>
                {headRows.map((row) => (
                    <TableCell
                        key={row.id}
                        align={"center"}
                        padding={row.disablePadding ? "none" : "default"}
                        className={classes.whiteColor}
                    >
                        {row.label}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}
EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    rowCount: PropTypes.number.isRequired,
};
const AdvanceList = () => {
    const classes = useStyles();
    const history = useHistory();
    const handlePageChange = () => { };
    const handleChangeRowsPerPage = () => { };
    return (
        <div>
            <div>
                <div className={classes.table_head}>
                    <span className={classes.selected_text}>0 SELECTED</span>
                    <div className="custom_pagination">
                        <TablePagination
                            component="div"
                            count={100}
                            page={4}
                            onPageChange={handlePageChange}
                            rowsPerPage={10}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </div>
                </div>
                <div className="custom_table">
                    <Table>
                        <EnhancedTableHead
                            classes={classes}
                            numSelected={0}
                            rowCount={20}
                            onSelectAllClick={() => { }}
                        />
                        <TableBody>
                            <TableRow className={classes.row_paper}>
                                <TableCell padding="checkbox">
                                    <Checkbox />
                                </TableCell>
                                <TableCell align="center" className={classes.cell_content}>22-09-2021</TableCell>
                                <TableCell align="center" className={classes.cell_content}>Advance</TableCell>
                                <TableCell align="center" className={classes.cell_content}>123445</TableCell>
                                <TableCell align="center" className={classes.cell_content}>RS 50000.00</TableCell>
                                <TableCell align="center" className={classes.receipt}>
                                    <ViewHeadlineIcon className={classes.receipt_icon} /> View Receipt
                                </TableCell>
                            </TableRow>
                            <Divider className={classes.divider} />
                            <TableRow className={classes.row_paper}>
                                <TableCell padding="checkbox">
                                    <Checkbox />
                                </TableCell>
                                <TableCell align="center" className={classes.cell_content}>22-09-2021</TableCell>
                                <TableCell align="center" className={classes.cell_content}>Advance</TableCell>
                                <TableCell align="center" className={classes.cell_content}>123445</TableCell>
                                <TableCell align="center" className={classes.cell_content}>RS 50000.00</TableCell>
                                <TableCell align="center" className={classes.receipt}>
                                    <ViewHeadlineIcon className={classes.receipt_icon} /> View Receipt
                                </TableCell>
                            </TableRow>
                            <Divider className={classes.divider} />
                        </TableBody>
                    </Table>
                </div>
            </div>
        </div>
    );
};
export default AdvanceList;
