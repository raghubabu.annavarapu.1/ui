import * as React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@mui/material/Dialog';
import ViewHeadlineIcon from '@mui/icons-material/ViewHeadline';
import { makeStyles } from '@material-ui/core';
import { DialogContent } from '@material-ui/core';
import { Divider, Typography, Button } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
    receipt_icon: {
        verticalAlign: "middle"
    },
    details: {
        display: 'flex',
    },
    indent_id: {
        marginRight: theme.spacing(1),
        fontSize: '14px'
    },
    indent_value: {
        flexGrow: '1',
        fontSize: '14px',
        fontWeight: 'bold'
    },
    date: {
        fontSize: '12px',
        fontWeight: 'bold'
    },
    trip: {
        marginRight: theme.spacing(1),
        fontSize: '14px',

    },
    sub_details: {
        marginTop: '10px'
    },
    trip_details: {
        fontSize: '14px',
        fontWeight: 'bold',
        display: 'flex',
    },
    trip_place: {
        fontSize: '14px',
        fontWeight: 'bold'
    },
    divider: {
        padding: '10px',
        padding: '0.2px'
    },
    particular_details: {
        display: 'flex',
        marginTop: '10px'
    },
    particular: {
        flexGrow: '1',
        fontSize: '12px',
        color: '#8E8E8E'
    },
    vehicle: {
        fontSize: '12px',
    },
    diesel_details: {
        display: 'flex'
    },
    diesel_vehicle: {
        flexGrow: '1',
        fontSize: '12px'
    },
    total_amt: {
        fontSize: '12px',
        color: '#8E8E8E'
    },
    diesel_amt: {
        fontSize: '12px'
    },
    paid_amt: {
        textAlign: "right",
        fontSize: '12px',
        margin: '10px 0px 10px 0px'
    },
    due_amt: {
        textAlign: "right",
        fontSize: '12px',
        margin: '10px 0px 10px 0px'
    },
    due_created: {
        fontSize: '12px'
    },
    createrecieve: {
        display: 'flex',
        marginTop: '20px'
    },
    due_createdname: {
        fontSize: '12px',
        color: '#8E8E8E'
    },
    due_reciever: {
        fontSize: '12px'
    },
    due_recievername: {
        fontSize: '12px',
        color: '#8E8E8E'
    },
    creator: {
        display: 'flex',
        flexGrow: '1',
        color: '#8E8E8E'
    },
    reciever: {
        display: 'flex',
        color: '#8E8E8E'
    },
    download_btn: {
        textTransform: 'none',
        backgroundColor: '#366E93',
        align: 'center',
        marginTop: '25px',
        "&:hover": {
            background: "#366E93",
        },
    },
    zigzag: {
        top: '50%',
        height: '57vh',
        width: '20vw',
        backgroundColor: '#fff',
        "&::before": {
            contet: '',
            position: 'absolute',
            width: '100%',
            height: '10px',
            display: 'block',
            backgroundSize: ' 20px 40px'
        },

    }

}));
function SimpleDialog(props) {
    const classes = useStyles()
    const { onClose, selectedValue, open } = props;
    const handleClose = () => {
        onClose(selectedValue);
    };
    return (
        <Dialog
            open={open}
            onClose={handleClose}
        >
            <DialogContent className={classes.zigzag}>
                <div >
                    <div className={classes.details}>
                        <Typography className={classes.indent_id}>
                            Indent# id :
                        </Typography>
                        <Typography className={classes.indent_value}>
                            12345
                        </Typography>
                        <Typography className={classes.date}>
                            22-09-2021
                        </Typography>
                    </div>
                    <div className={classes.sub_details}>
                        <div className={classes.trip_details}>
                            <Typography className={classes.trip}>
                                Trip :
                            </Typography>
                            <Typography className={classes.trip_place}>
                                Vijayawada - Vizag
                            </Typography>
                        </div>
                        <div className={classes.trip_details}>
                            <Typography className={classes.trip}>
                                Vehicle# :
                            </Typography>
                            <Typography className={classes.trip_place}>
                                AP16TE6436
                            </Typography>
                        </div>
                        <div className={classes.trip_details}>
                            <Typography className={classes.trip}>
                                LR/THC# :
                            </Typography>
                            <Typography className={classes.trip_place}>
                                1234566
                            </Typography>
                        </div>
                    </div>
                </div>
                <Divider style={{ marginTop: '10px' }} />
                <div>
                    <div className={classes.particular_details}>
                        <Typography className={classes.particular}>
                            PARTICULAR
                        </Typography>
                        <Typography className={classes.total_amt}>
                            TOTAL AMT
                        </Typography>
                    </div>
                    <div className={classes.diesel_details}>
                        <Typography className={classes.diesel_vehicle}>
                            Diesel for Vehicle
                        </Typography>
                        <Typography className={classes.diesel_amt}>
                            RS 5000.00
                        </Typography>
                    </div>
                </div>
                <Divider style={{ marginTop: '30px' }} />
                <Typography className={classes.paid_amt}>
                    Paid Amt : RS 3000.00
                </Typography>
                <Divider />
                <Typography className={classes.due_amt}>
                    Due Amt : RS 3000.00
                </Typography>
                <Divider />
                <div className={classes.createrecieve}>
                    <div className={classes.creator}>
                        <Typography className={classes.due_created}>Createdby :</Typography>
                        <Typography className={classes.due_createdname}>Mahesh</Typography>
                    </div>
                    <div className={classes.reciever}>
                        <Typography className={classes.due_reciever}>recievedby :</Typography>
                        <Typography className={classes.due_recievername}>John</Typography>
                    </div>
                </div>
                <Typography align='center'>
                    <Button
                        color='primary'
                        variant='contained' className={classes.download_btn}
                        onClose={handleClose}
                    >
                        Download Reciept
                    </Button>
                </Typography>
            </DialogContent>
        </Dialog >
    );
}
SimpleDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    selectedValue: PropTypes.string.isRequired,
};
export default function SimpleDialogDemo() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [selectedValue, setSelectedValue] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = (value) => {
        setOpen(false);
        setSelectedValue(value);
    };
    return (
        <div>
            <div onClick={handleClickOpen}>
                <ViewHeadlineIcon className={classes.receipt_icon} /> View Receipt
            </div>
            <SimpleDialog
                selectedValue={selectedValue}
                open={open}
                onClose={handleClose}
            />
        </div>
    );
}