import React, { useState } from "react";
import Header from "../../components/header";
import { Typography, Grid, Button } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import * as Fields from "../../sharedComponents";
import _ from "lodash";
import '../../common.css';

const AddVendor = () => {
    const history = useHistory();
    const formFields = {
        firstName: {
            name: "firstName",
            label: "First Name",
            value: "",
            topLabel: true,
        },
        lastName: {
            name: "lastName",
            label: "Last Name",
            value: "",
            topLabel: true,
        },
        mobileNumber: {
            name: "mobileNumber",
            label: "Mobile Number",
            value: "",
            topLabel: true,
        },
        emailId: { name: "emailId", label: "Email ID", value: "", topLabel: true },
        companyName: {
            name: "companyName",
            label: "Company Name",
            value: "",
            topLabel: true,
        },
        areaOfService: {
            name: "areaOfService",
            label: "Area of Service",
            value: "",
            topLabel: true,
        },
        bussinessId: {
            name: "bussinessId",
            label: "Bussiness ID",
            value: "",
            topLabel: true,
        },
        gstNumber: {
            name: "gstNumber",
            label: "GST Number",
            value: "",
            topLabel: true,
        },
        paymentTerms: {
            name: "paymentTerms",
            label: "Payment Terms",
            value: "",
            options: [],
            topLabel: true,
        },
        openingBalance: {
            name: "openingBalance",
            label: "Opening Balance",
            value: "",
            topLabel: true,
        },
        glAccount: {
            name: "glAccount",
            label: "GL ACCOUNT",
            value: "",
            topLabel: true,
        },
        attachments: {
            name: "attachments",
            label: "Attachments",
            value: "",
            topLabel: true,
        },
        flatNo: {
            name: "flatNo",
            label: "Flat/Suite No",
            value: "",
            topLabel: true,
        },
        streetName: {
            name: "streetName",
            label: "Street Name",
            value: "",
            topLabel: true,
        },
        area: { name: "area", label: "Area", value: "", topLabel: true },
        state: { name: "state", label: "State", value: "", topLabel: true },
        country: { name: "country", label: "Country", value: "", topLabel: true },
        pincode: { name: "pincode", label: "Pincode", value: "", topLabel: true },
    };
    const [fields, setFields] = useState(_.cloneDeep(formFields));
    const inputChangeHandler = () => { };
    const selectChangeHandler = () => { };
    return (
        <div>
            <Header />
            <div className="main_container">
                <div className="header_box">
                    <Typography className="header_text">
                        <ArrowBack
                            className="arrow"
                            onClick={() => {
                                history.push("./vendor");
                            }}
                        />
                        Add Vendor
                    </Typography>
                </div>
                <div className="form_container">
                    <div
                        className="details_container border_bottom"
                    >
                        <Typography className="details_container_heading">
                            User Details
                        </Typography>
                        <Grid
                            container
                            spacing={3}
                            className="details_container_content"
                        >
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.firstName}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.lastName}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            spacing={3}
                            className="details_container_content"
                        >
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.mobileNumber}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.emailId}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                    </div>
                    <div
                        className="details_container border_bottom"
                    >
                        <Typography className="details_container_heading">
                            Business Details
                        </Typography>
                        <Grid
                            container
                            spacing={3}
                            className="details_container_content"
                        >
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.companyName}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.areaOfService}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            spacing={3}
                            className="details_container_content"
                        >
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.bussinessId}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.gstNumber}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            spacing={3}
                            className="details_container_content"
                        >
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.paymentTerms}
                                    selectChangeHandler={selectChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.openingBalance}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            spacing={3}
                            className="details_container_content"
                        >
                            {/* <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.glAccount}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid> */}
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.attachments}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                    </div>
                    <div className="details_container">
                        <Typography className="details_container_heading">
                            Address Details
                        </Typography>
                        <Grid
                            container
                            spacing={3}
                            className="details_container_content"
                        >
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.flatNo}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.streetName}
                                    selectChangeHandler={selectChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            spacing={3}
                            className="details_container_content"
                        >
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.area}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.state}
                                    selectChangeHandler={selectChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            spacing={3}
                            className="details_container_content"
                        >
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.country}
                                    inputChangeHandler={inputChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <Fields.InputField
                                    fieldData={fields.pincode}
                                    selectChangeHandler={selectChangeHandler}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid
                            container
                            spacing={3}
                            className="details_container_content"
                        >
                            <Grid item xs={8}>
                                <Grid container spacing={2}>
                                    <Grid item xs={8}></Grid>
                                    <Grid item xs={2}>
                                        <Button
                                            variant="outlined"
                                            onClick={() => {
                                                history.push("./clients");
                                            }}
                                            className="cancel_button"
                                        >
                                            Cancel
                                        </Button>
                                    </Grid>
                                    <Grid item xs={2}>
                                        <Button
                                            variant="outlined"
                                            className="save_button"
                                        >
                                            Save
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AddVendor;
