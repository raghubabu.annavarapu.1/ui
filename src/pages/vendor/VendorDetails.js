import React, { useState } from "react";
import Header from "../../components/header";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Grid } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import _ from "lodash";
import Box from '@mui/material/Box';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import LedgerList from "./LedgerList";
import DueList from "./DueList";
import AdvanceList from "./AdvanceList";
import * as Fields from '../../sharedComponents/index';
const useStyles = makeStyles((theme) => ({
    main_container: {
        background: "#F5F6F7",
        height: "100vh"
    },
    header_box: {
        background: "#FFFFFF",
        padding: "20px 24px",
        display: "flex",
        justifyContent: "space-between",
    },
    header_text: {
        fontSize: "20px",
        fontWeight: "bold",
        lineHeight: "42px",
        paddingRight: "12px",
    },
    vendor_info: {
        display: "flex",
    },
    vendor_details: {
        alignSelf: "center",
        display: "flex",
        flexDirection: "column",
        padding: "0px 20px",
        borderLeft: "3px solid #E6E6E6",
        fontSize: "14px",
        color: "#484848",
    },
    arrow: {
        cursor: "pointer",
        verticalAlign: "middle",
        margin: "0px 10px 4px 0px",
    },
    view_details: {
        alignSelf: "center",
        color: "#366E93",
        padding: "0px 17px",
        fontWeight: 600,
        cursor: "pointer",
    },
    filter_field: {
        background: "#FFFFFF",
    },
    indicator: {
        background: 'linear-gradient(85deg, #F3644F 70%, #F3644F 90%)',
    },
}));

const VendorDetails = () => {
    const classes = useStyles();
    const history = useHistory();
    const filterFields = {
        booking: { name: "Booking", label: "Booking", value: "", options: [] },
        date: { name: "date", label: "Date", value: "" },
    };
    const [filters, setFilters] = useState(_.cloneDeep(filterFields));
    const [value, setValue] = React.useState(0);
    const [open, setOpen] = useState(false);
    const dateChangeHandler = () => { };
    const handleTabs = (e, val) => {
        console.warn(val)
        setValue(val)
    }
    function TabPanel(props) {
        const { children, value, index } = props;
        return (<div>
            {
                value === index && (
                    <div>{children}</div>
                )
            }
        </div>)
    }
    const handleClickOpen = () => {
        setOpen(true);
    };
    return (
        <div>
            <Header />
            <div className={classes.main_container}>
                <div className={classes.header_box}>
                    <div className={classes.vendor_info}>
                        <Typography className={classes.header_text}>
                            <ArrowBack
                                className={classes.arrow}
                                onClick={() => {
                                    history.push("./vendor");
                                }}
                            />
                            Rajesh Kumar(SS Transports)
                        </Typography>
                        <div className={classes.vendor_details}>
                            <span>rajesh@gmail.com</span>
                            <span>9876543210</span>
                        </div>
                    </div>
                    <div className={classes.view_details}>View Details</div>
                </div>
                <div>
                    <div style={{ display: 'flex', margin: '20PX' }}>
                        <div style={{ marginRight: '30px', flexGrow: '1' }}>
                            <Box sx={{ width: '30%', backgroundColor: 'white' }}>
                                <Tabs value={value} onChange={handleTabs} centered
                                    classes={{ indicator: classes.indicator }}>
                                    <Tab label="Due(s)" />
                                    <Tab label="Ledger" />
                                    <Tab label="Advance" />
                                </Tabs>
                            </Box>
                        </div>
                        {/* <div>
                            <Button>pay advance</Button>
                        </div> */}
                        <div style={{ margin: '20px' }}>
                            <Grid item xs={12}>
                                <Fields.DateField
                                    fieldData={filters.date}
                                    dateChangeHandler={dateChangeHandler}
                                    variant="outlined"
                                    className={classes.filter_field}
                                />
                            </Grid>
                        </div>
                    </div>
                    <TabPanel value={value} index={0}>
                        <DueList handleClickOpen={handleClickOpen} />
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <LedgerList handleClickOpen={handleClickOpen}
                        />
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                        <AdvanceList handleClickOpen={handleClickOpen}
                        />
                    </TabPanel>
                </div>
            </div>
        </div >
    );
};

export default VendorDetails;
