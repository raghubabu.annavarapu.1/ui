import React, { useState } from 'react';
import * as L from 'leaflet';
import { useMapEvents, Polyline } from 'react-leaflet';
import './map.css';
import 'leaflet/dist/leaflet.css';

export default function DrawPolyline(props) {
    const coordinates = props.coordinates;
    const map = useMapEvents({
    });
    L.polyline(coordinates)
        .addTo(map);
    map.flyTo(new L.LatLng(coordinates[0][0], coordinates[0][1]), map.getZoom())
    return (
        <Polyline positions={props.coordinates} />
    );
}
