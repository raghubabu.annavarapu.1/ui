import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { MapContainer, TileLayer, Marker, Tooltip, Popup, Polyline } from 'react-leaflet';
import './custom.css';
import 'leaflet/dist/leaflet.css';
import LocationMarker from './marker';
import * as L from 'leaflet';
import { redMarkerImage } from "../../../assets/index";
import { skyblueMarkerImage } from "../../../assets/index";
import { orangeMarkerImage } from "../../../assets/index";
import { yellowMarkerImage } from "../../../assets/index";

import { speedIcon } from "../../../assets/index";
import { accelerationIcon } from "../../../assets/index";
import { cornerIcon } from "../../../assets/index";
import { brakeIcon } from "../../../assets/index";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    width: '100%',
    margin: 'auto',
    backgroundColor: '#000',
    alignItems: 'center',
    height: '100%'
  },
  leafletContainer: {
    margin: 'auto',
    width: '100%',
    height: (props) => props.height
  },
  leafletMarkerIcon: {
    width: '20px !important',
    height: '35px !important'
  },
  icon: {
    width: 40,
    height: 40,
    margin: 5,
    color: '#FF0000'
  },
}));

export default function Map(props) {
  // const position = [51.505, -0.09];
  const classes = useStyles(props);
  const [showOverSpeedMarkers, setShowOverSpeedMarkers] = useState(true);
  const [showCrashMarkers, setShowCrashMarkers] = useState(false);
  const [showHarshAccelerationMarkers, setShowHarshAccelerationMarkers] = useState(false);
  const [showHarshBrakingMarkers, setShowHarshBrakingMarkers] = useState(false);
  const [showHarshCorneringMarkers, setShowHarshCorneringMarkers] = useState(false);

  const [overSpeedingData, setOverSpeedingData] = useState([]);
  const [crashData, setCrashData] = useState([]);
  const [harshAccelerationData, setHarshAccelerationData] = useState([]);
  const [harshBrakingData, setHarshBrakingData] = useState([]);
  const [harshCorneringData, setHarshCorneringData] = useState([]);

  const getDriverBehaviorData = (trackingInput) => {
    let vehicleFlespiId = null;
    // console.log("Selected Vehicle data: ", props.SelectedVehicle);
    if (props.SelectedVehicle && props.SelectedVehicle.flespi_id) {
      vehicleFlespiId = props.SelectedVehicle.flespi_id;
    }
    setOverSpeedingData([]);
    setCrashData([]);
    setHarshAccelerationData([]);
    setHarshBrakingData([]);
    setHarshCorneringData([]);
    if (vehicleFlespiId) {
      let input = {
        flespiId: vehicleFlespiId,
        fromUnixTimestamp: parseInt((new Date(props.fromDate).getTime()) / 1000),
        toUnixTimestamp: parseInt((new Date(props.toDate).getTime()) / 1000)
      }
      let Service = global.service;
      const promise1 = Service.getOverSpeedingEvents(input);
      const promise2 = Service.getHarshAccelerationEvents(input);
      const promise3 = Service.getHarshBrakingEvents(input);
      const promise4 = Service.getHarshCorneringEvents(input);
      const promise5 = Service.getRunningHours(input);
      const promise6 = Service.getTraveledDistance(input);
      //const promise5 = Service.getCrashEvents(input);
      Promise.all([promise1, promise2, promise3, promise4, promise5, promise6])
        .then((response) => {
          // console.log("Response: ", response);
          if (response.length > 0) {
            let data1 = response[0].data ? response[0].data.map((ce, index) => ({ value: index, label: index, position: [ce["position.latitude"], ce["position.longitude"]], timestamp: ce["timestamp"], overSpeed: ce["overspeeding.speed"], overSpeedingStatus: ce["overspeeding.status"] })) : [];
            if (data1.length > 0) {
              setOverSpeedingData(data1);
            }
            let data2 = response[1].data ? response[1].data.map((ce, index) => ({ value: index, label: index, position: [ce["position.latitude"], ce["position.longitude"]], timestamp: ce["timestamp"], harshAccelerationEvent: ce["harsh.acceleration.event"] })) : [];
            if (data2.length > 0) {
              setHarshAccelerationData(data2);
            }
            let data3 = response[2].data ? response[2].data.map((ce, index) => ({ value: index, label: index, position: [ce["position.latitude"], ce["position.longitude"]], timestamp: ce["timestamp"], harshBrakingEvent: ce["harsh.braking.event"] })) : [];
            if (data3.length > 0) {
              setHarshBrakingData(data3);
            }
            let data4 = response[3].data ? response[3].data.map((ce, index) => ({ value: index, label: index, position: [ce["position.latitude"], ce["position.longitude"]], timestamp: ce["timestamp"], harshCorneringEvent: ce["harsh.cornering.event"], harshCorneringAngle: ce["harsh.cornering.angle"] })) : [];
            if (data4.length > 0) {
              setHarshCorneringData(data4);
            }
            if (response[4].data && response[4].data) {
              // console.log(response[4].data, "seconds");
              let minutes = parseInt(response[4].data / 60);
              let hours = parseInt(minutes / 60);
              minutes = parseInt(minutes % 60);
              props.getRunningHours(hours + (minutes > 0 ? (":" + minutes) : ''))
            }
            if (response[5].data && response[5].data) {
              let mileageData = response[5].data;
              if (mileageData.length > 1) {
                let distance = mileageData[mileageData.length - 1]["vehicle.mileage"] - mileageData[0]["vehicle.mileage"];
                props.getDistanceTravelled(distance);
              }
            }
            handleDriverBehaviorEvents();
          }
        })
        .catch((error) => {
          // console.log(error);
        });
    }
  }

  useEffect(() => {
    getDriverBehaviorData(props);
  }, [props.key]);

  const handleDriverBehaviorEvents = () => {
    if (props.DriverBehaviorEventType.indexOf("overSpeedingEvents") > -1) {
      setShowOverSpeedMarkers(true);
    } else {
      setShowOverSpeedMarkers(false);
    }

    if (props.DriverBehaviorEventType.value == "crashEvents") {
      setShowCrashMarkers(true);
    } else {
      setShowCrashMarkers(false);
    }

    if (props.DriverBehaviorEventType.indexOf("harshAccelerationEvents") > -1) {
      setShowHarshAccelerationMarkers(true);
    } else {
      setShowHarshAccelerationMarkers(false);
    }

    if (props.DriverBehaviorEventType.indexOf("harshBrakingEvents") > -1) {
      setShowHarshBrakingMarkers(true);
    } else {
      setShowHarshBrakingMarkers(false);
    }

    if (props.DriverBehaviorEventType.indexOf("harshCorneringEvents") > -1) {
      setShowHarshCorneringMarkers(true);
    } else {
      setShowHarshCorneringMarkers(false);
    }
  }
  useEffect(() => {
    handleDriverBehaviorEvents();
  }, [props.DriverBehaviorEventType]);

  return (
    <div>
      <MapContainer
        key={props.key}
        className={classes.leafletContainer}
        // style={{position: 'relative'}}
        center={props.coordinates[0].position}
        zoom={12}
        scrollWheelZoom={true}
        dragging={true}
        bounds={L.latLngBounds(props.coordinates[0].position, props.coordinates[props.coordinates.length - 1].position)}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {
          showOverSpeedMarkers ?
            overSpeedingData.map((x, index) => {
              return (
                <Marker
                  key={index}
                  position={x.position}
                  icon={L.divIcon(SetMarkerDivIcon("os"))}
                >
                  <Tooltip>{"Over Speed(" + x.position[0] + ", " + x.position[1] + "): " + x.overSpeed + " KMPH at " + ConvertUnixTimeStampToISTTime(x.timestamp)}</Tooltip>
                </Marker>
              )
            }
            ) : null
        }
        {/* {
          showCrashMarkers ?
          crashData.map((x, index) => {
            return (
              <Marker
                key={index}
                position={x.position}
                icon={L.divIcon(SetMarkerDivIcon("cr"))}
                >
                <Popup>{x}</Popup>
              </Marker>
            )
          }
          ): null
        } */}
        {
          showHarshAccelerationMarkers ?
            harshAccelerationData.map((x, index) => {
              return (
                <Marker
                  key={index}
                  position={x.position}
                  icon={L.divIcon(SetMarkerDivIcon("ha"))}
                >
                  <Tooltip>{"Harsh Accelerated (" + x.position[0] + ", " + x.position[1] + "): at " + ConvertUnixTimeStampToISTTime(x.timestamp)}</Tooltip>
                </Marker>
              )
            }
            ) : null
        }
        {
          showHarshBrakingMarkers ?
            harshBrakingData.map((x, index) => {
              return (
                <Marker
                  key={index}
                  position={x.position}
                  icon={L.divIcon(SetMarkerDivIcon("hb"))}
                >
                  <Tooltip>{"Harsh Braking: (" + x.position[0] + ", " + x.position[1] + ") at " + ConvertUnixTimeStampToISTTime(x.timestamp)}</Tooltip>
                </Marker>
              )
            }
            ) : null
        }
        {
          showHarshCorneringMarkers ?
            harshCorneringData.map((x, index) => {
              return (
                <Marker
                  key={index}
                  position={x.position}
                  icon={L.divIcon(SetMarkerDivIcon("hc"))}
                >
                  <Tooltip>{"Harsh Cornering Angle(" + x.position[0] + ", " + x.position[1] + "): " + x.harshCorneringAngle.toFixed(2) + " degree at " + ConvertUnixTimeStampToISTTime(x.timestamp)}</Tooltip>
                </Marker>
              )
            }
            ) : null
        }
        <LocationMarker currentPosition={props.currentPosition} coordinates={props.coordinates} className={classes.leafletMarkerIcon} />
      </MapContainer>
    </div>
  )
}

function SetMarkerDivIcon(type) {
  let imagePath = orangeMarkerImage;
  if (type == "os") {
    imagePath = speedIcon;
  } else if (type == "ha") {
    imagePath = accelerationIcon;
  } else if (type == "hb") {
    imagePath = brakeIcon;
  } else if (type == "hc") {
    imagePath = cornerIcon;
  }

  return {

    html: `<div style='background: none; border: none; width: 100%; height: 100%; margin-top: -15px;'>
            <img src='${imagePath}' style='width: 100%; height: 100%;' />
      </div>`,
    className: '',
  }
}

function ConvertUnixTimeStampToISTTime(unixTimeStamp) {
  return new Date(unixTimeStamp * 1000).toLocaleString();
}