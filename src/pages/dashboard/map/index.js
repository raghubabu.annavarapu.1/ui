import React, { useEffect, useState } from 'react';
import Map from './map';
import PlayerPanel from './playerPanel';
import { Typography } from 'antd';
import CircularProgress from '@material-ui/core/CircularProgress';

function TrackingMap(props) {
    const [PolyLinePoints, setPolyLinePoints] = useState([]);
    const [positionFromPanel, getPositionFromPanel] = useState({ position: [], direction: 0 });
    const [selectedVehicle, setSelectedVehicle] = useState(props.SelectedVehicle);
    const [ErrorMessage, setErrorMessage] = useState('Loading...');
    const [loading, setLoading] = useState(false);
    const setPositionInMap = (pos) => {
        getPositionFromPanel(pos);
    };

    const getTrackingData = (trackingInput) => {
        let vehicleFlespiId = null;
        // console.log("Selected Vehicle data: ", props.SelectedVehicle);
        if (props.SelectedVehicle && props.SelectedVehicle.flespi_id) {
            vehicleFlespiId = props.SelectedVehicle.flespi_id;
        }
        if (vehicleFlespiId) {
            setLoading(true);
            props.getDistanceTravelled(null);
            props.getRunningHours(null);
            setPolyLinePoints([]); 
            global.service.getTrackingMessages(
                {
                    flespiId: vehicleFlespiId,
                    fromUnixTimestamp: Math.floor((new Date(props.fromDate).getTime()) / 1000),
                    toUnixTimestamp: Math.floor((new Date(props.toDate).getTime()) / 1000)
                }
            ).then((response) => {
                // console.log("Response: ", response);
                // console.log("Response: ", response.status);
                if (response.status) {
                    var points = response.data.map((ce, index) => ({ value: index, label: index, position: [ce["position.latitude"], ce["position.longitude"]], direction: ce["position.direction"], timestamp: ce["timestamp"] }))
                    getPositionFromPanel({ position: points[0].position, direction: points[0].direction });
                    setPolyLinePoints(points);
                    // console.log(points);
                    setLoading(false);
                    if(points.length==0){
                        setErrorMessage("No Data Available");
                    }
                }else{
                    setLoading(false);
                    setErrorMessage("No Data Available");
                }
            }).catch((err) => {
                // console.log(err);
                // this.setState({ successMsg: err.toString() });
                setLoading(false);
                setErrorMessage("Unable to fetch data from API");
            });
        }
    }
    useEffect(() => {
        getTrackingData(props);
    }, [props.key]);
    return (
        <>
            {PolyLinePoints.length > 0
                ?
                <>
                    <Map key={props.key}
                        currentPosition={positionFromPanel}
                        coordinates={PolyLinePoints}
                        height={props.height}
                        SelectedVehicle={props.SelectedVehicle}
                        fromDate={props.fromDate}
                        toDate={props.toDate}
                        getDistanceTravelled={props.getDistanceTravelled}
                        getRunningHours={props.getRunningHours}
                        DriverBehaviorEventType={props.DriverBehaviorEventType}
                    />
                    <PlayerPanel key={props.key} setPositionInMap={setPositionInMap} coordinates={PolyLinePoints}/>
                </>
                :
                loading ?
                    <CircularProgress disableShrink style={{position: 'fixed', top: '50%', left: '50%'}}/> :
                    <div style={{position: 'fixed', top: '50%', width: '100%', textAlign: 'center', fontSize: 25, fontFamily: 'sans-serif', color: '#366E93'}}>
                        <Typography >{ErrorMessage} </Typography>
                    </div>
            }

        </>
    );
}

export default TrackingMap;