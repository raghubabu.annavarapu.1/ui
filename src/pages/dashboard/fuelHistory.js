import React, { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
import moment from 'moment';
import { AutoSelect, DateRangePicker, AntdSelect } from '../../components/index';
import { Typography, IconButton, Paper, TextField, Divider, Button } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import  'chartjs-adapter-moment';

import {
    Chart, CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    TimeScale
} from 'chart.js';

Chart.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    TimeScale
)

export default function FuelHistoryContent(props) {
    const [ErrorMessage, setErrorMessage] = useState(null);
    const [maxYValue, setMaxYValue] = useState(200);
    const [stepValue, setStepValue] = useState(50);
    const [loading, setLoading] = useState(false);
    const [selectedVehicle, setSelectedVehicle] = React.useState(null);
    const [timestamps, setTimestamps] = useState(null);
    const [fuelReadings, setFuelReadings] = useState(null);
    const [fuelRefilled, setFuelRefilled] = useState(null);
    const [distanceTravelledByFuel, setDistanceTravelledByFuel] = useState(null);
    const [vehicleNum, setDefaultVehicle] = useState(undefined);
    const [fromDateToHist, setFromDateToHist] = React.useState(new Date(new Date().setHours(0, 0, 0, 0)));
    const [toDateToHist, setToDateToHist] = React.useState(new Date(new Date().setHours(23, 59, 59, 0)));
    const [dateRangeValue, setDateRangeValue] = React.useState(
        [moment().startOf('day'),
        moment().endOf('day')]);
    const [vehicleSelectIndex, setVehicleSelectIndex] = React.useState(0);
    const getFuelData = (selectedTruck) => {
        let vehicleFlespiId = null;
        if (selectedVehicle && selectedVehicle.flespi_id) {
            vehicleFlespiId = selectedVehicle.flespi_id;
        } else if (selectedTruck && selectedTruck.flespi_id) {
            vehicleFlespiId = selectedTruck.flespi_id;
        }
        if (vehicleFlespiId) {
            setLoading(true);
            setDistanceTravelledByFuel(null);
            setFuelRefilled(null);
            setErrorMessage(null);
            // props.getRunningHours(null);
            const promise1 = global.service.getTraveledDistance({
                flespiId: vehicleFlespiId,
                fromUnixTimestamp: parseInt((new Date(fromDateToHist).getTime()) / 1000),
                toUnixTimestamp: parseInt((new Date(toDateToHist).getTime()) / 1000)
            });
            const promise2 = global.service.getFuelHistory(
                {
                    flespiId: vehicleFlespiId,
                    from: Math.floor((new Date(fromDateToHist).getTime()) / 1000),
                    to: Math.floor((new Date(toDateToHist).getTime()) / 1000)
                }
            );
            Promise.all([promise1, promise2])
                .then((response) => {
                    if (response[0].data) {
                        let mileageData = response[0].data;
                        if (mileageData.length > 1) {
                            let distance = mileageData[mileageData.length - 1]["vehicle.mileage"] - mileageData[0]["vehicle.mileage"];
                            setDistanceTravelledByFuel(distance ? distance.toFixed(2) + ' KM' : '');
                        }
                    }
                    if (response[1].status) {
                        // setLoading(false);
                        if (response[1].data.length == 0) {
                            setErrorMessage("No Data Available");
                        }
                        else {
                            setFuelRefilled(response[1].fuelRefilled ? (response[1].fuelRefilled + ' L') : 0);
                            let graphData = []; let prevTimeValue; let maxValue = 0;
                            for (let record of response[1].data) {
                                const timeValue = moment(record.timestamp * 1000).format('yyyy-MM-DD HH:mm');
                                if (maxValue < parseInt(record.fuel)) {
                                    maxValue = parseInt(record.fuel);
                                }
                                if (prevTimeValue) {
                                    if (prevTimeValue != timeValue) {
                                        graphData.push(record);
                                        prevTimeValue = timeValue;
                                    }
                                } else {
                                    graphData.push(record);
                                    prevTimeValue = timeValue;
                                }
                            }
                            // setting value near to hundreds
                            maxValue = maxValue < 200 ? 200 : (maxValue < 300 ? 300 : 400);
                            let stepValue = 50;
                            setTimestamps(graphData.map(m => moment(m.timestamp * 1000).format('yyyy-MM-DD HH:mm')));
                            // setTimestamps(graphData.map(m => m.timestamp));
                            setFuelReadings(graphData.map(m => m.fuel));
                            setMaxYValue(maxValue);
                            setStepValue(stepValue);
                        }
                    } else {
                        // setLoading(false);
                        setErrorMessage("No Data Available");
                    }
                    setLoading(false);
                }).catch((err) => {
                    console.log(err);
                    // this.setState({ successMsg: err.toString() });
                    setLoading(false);
                    setErrorMessage("Unable to fetch data from API");
                });

        }
    }
    useEffect(() => {
        if (props.fuelVehicles) {
            setDefaultVehicle(props.fuelVehicles[0]);
            setSelectedVehicle(props.fuelVehicles[0]);
            onSelectFuelVehicle(props.fuelVehicles[0])
        }
    }, [props.fuelVehicles]);

    useEffect(() => {
        getFuelData();
    }, [selectedVehicle, fromDateToHist, toDateToHist])

    const onSelectFuelVehicle = (value) => {
        console.log(value);
        if (value && value.length > 0) {
            let vehicleNo = value;
            let vehiclesInfo = props.trucksInfo.filter(x => x.regi_no.toLowerCase().trim() == vehicleNo.toLowerCase().trim());
            if (vehiclesInfo && vehiclesInfo.length > 0) {
                setSelectedVehicle(vehiclesInfo[0]);
                setDefaultVehicle(value);
            }
        }
    }

    const handleDateRangeControl = (dateArray) => {
        console.log(dateArray);
        if (moment(dateArray[1]).diff(dateArray[0], 'm') > 0) {
            setFromDateToHist(dateArray[0]._d);
            if (moment(dateArray[1]).diff(dateArray[0], 'd') > 7) {
                dateArray[1] = moment(dateArray[0]).add(7, 'd');
            }
            setToDateToHist(dateArray[1]._d);
            setDateRangeValue(dateArray);
        }
    }

    const state = {
        labels: timestamps ? timestamps : [],
        datasets: [
            {
                label: 'Fuel quantity(L)',
                fill: false,
                lineTension: 0,
                backgroundColor: '#FFFFFF',
                borderColor: '#068ae4',
                borderWidth: 1,
                data: fuelReadings ? fuelReadings : [],
                pointStyle: 'circle',
                radius: 0.5,
                pointBackgroundColor: '#068ae4',
                hoverBackgroundColor: '#FF0000'
            }
        ]
    }
    return (
        <>

            <div
                style={{ padding: 10, height: window.innerHeight - 150 }}
            >
                <div style={{ display: 'flex', padding: 5 }}>
                    <AntdSelect key="fuel-vehicle-dropdown" placeholder="Select Vehicle" items={props.fuelVehicles} onChange={onSelectFuelVehicle} value={vehicleNum} />

                    <div style={{ padding: 5 }} ><DateRangePicker onDateSelect={handleDateRangeControl} value={dateRangeValue} /></div>
                    <div style={{ padding: 5, paddingLeft: 10, alignItems: 'center', float: 'left', breakInside: 'avoid', width: 250 }}>
                        <Typography style={{ color: '#366E93', fontSize: 12, fontWeight: 600 }}> Fuel Refilled: {fuelRefilled}</Typography>
                        <Typography style={{ color: '#366E93', fontSize: 12, fontWeight: 600 }}> Distance Travelled: {distanceTravelledByFuel}</Typography>
                    </div>
                </div>
                {loading ?
                    <CircularProgress disableShrink style={{ position: 'fixed', top: '50%', left: '50%' }} />
                    :
                    (
                        ErrorMessage
                            ?
                            <div style={{ position: 'fixed', top: '50%', width: '100%', textAlign: 'center', fontSize: 25, fontFamily: 'sans-serif', color: '#366E93' }}>
                                <Typography >{ErrorMessage} </Typography>
                            </div>
                            :
                            <div
                                key={selectedVehicle + "-" + new Date(fromDateToHist).getTime() + "-" + new Date(toDateToHist).getTime()}
                                style={{ padding: 10, paddingBottom: 20 }}
                            >
                                <Line
                                    style={{ backgroundColor: '#FFFFFF', height: window.innerHeight - 250 }}
                                    data={state}
                                    options={{
                                        maintainAspectRatio: false,
                                        plugins: {
                                            legend: {
                                                display: false
                                            },
                                            tooltip: {
                                                displayColors: false,
                                                backgroundColor: '#000',
                                                bodyColor: '#FFF',
                                                titleColor: '#FFF'
                                            }
                                        },
                                        scales: {
                                            y: {
                                                display: true,
                                                min: 0,
                                                max: maxYValue,
                                                title: {
                                                    display: true,
                                                    text: 'Fuel Quantity(L)',
                                                    font: {
                                                        family: 'sans-serif',
                                                        size: 12,
                                                        weight: 500
                                                    }
                                                },
                                                // gridLines: {
                                                //     color: '#03204F',
                                                //     drawBorder: true,
                                                //     tickMarkLength: 1,
                                                //     display: true,
                                                //     lineWidth: 0.10
                                                // },
                                                ticks: {
                                                    // beginAtZero: true,
                                                    stepSize: stepValue,
                                                    // display: true,
                                                    // fontStyle: 'normal',
                                                    // fontSize: 12,
                                                    // fontColor: '#222222',
                                                    // fontFamily: 'Arial',
                                                    // padding: 10
                                                },
                                            },
                                            x: {
                                                type: 'time',
                                                time: {
                                                    displayFormats: {
                                                        hour: 'yyyy-MM-DD HH:mm'
                                                    }
                                                }
                                            }
                                        },
                                    }}
                                />
                            </div>
                    )
                }
            </div>
        </>
    );
}
