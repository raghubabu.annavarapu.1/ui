import { Typography, Grid, Button, FormControlLabel, } from "@material-ui/core";
import { ArrowBack } from "@material-ui/icons";
import React, { useState } from "react";
import Header from "../../components/header";
import { useHistory } from "react-router-dom";
import * as Fields from '../../sharedComponents';
import "../../common.css";
import _ from "lodash";
import PropTypes from "prop-types";
import { styled } from "@mui/material/styles";
import RadioGroup, { useRadioGroup } from "@mui/material/RadioGroup";
import Radio from "@mui/material/Radio";
import { style } from "@mui/system";
const AddUser = () => {
    const history = useHistory();
    const formFields = {
        firstName: {
            name: "firstName",
            label: "First Name",
            value: "",
            topLabel: true,
        },
        lastName: {
            name: "lastName",
            label: "Last Name",
            value: "",
            topLabel: true,
        },
        userName: {
            name: "userName",
            label: "User Name",
            value: "",
            topLabel: true,
        },
        email: {
            name: "email",
            label: "Email",
            value: "",
            topLabel: true
        },
        companyName: {
            name: "companyName",
            label: "Company Name",
            value: "",
            topLabel: true
        },
        contactNumber: {
            name: "contactNumber",
            label: "Contact Number",
            value: "",
            topLabel: true
        },
        employee: {
            name: "employee",
            label: "Employee Role/Designation",
            value: { label: "", value: "" },
            options: [],
            validationRequired: true,
            isValid: true,
            topLabel: true,
        },
        empid_userid: {
            name: "empid_userid",
            label: "Emp ID/User ID",
            value: "",
            topLabel: true
        }
    }
    const StyledFormControlLabel = styled((props) => (
        <FormControlLabel {...props} />
    ))(({ theme, checked }) => ({
        ".MuiFormControlLabel-label": checked && {
            color: theme.palette.primary.main,
        },
    }));
    function MyFormControlLabel(props) {
        const radioGroup = useRadioGroup();
        let checked = false;
        if (radioGroup) {
            checked = radioGroup.value === props.value;
        }
        return <StyledFormControlLabel checked={checked} {...props} />;
    }
    MyFormControlLabel.propTypes = {
        value: PropTypes.any,
    };
    const [fields, setFields] = useState(_.cloneDeep(formFields));
    const inputChangeHandler = () => { };
    const autoCompleteChangeHandler = () => { };
    const [onReceipt, setOnReceipt] = React.useState("payTerms");
    const [checked, setChecked] = useState(false)
    const handleClick = () =>
        setChecked(!checked)
    const buttonHandler = (e) => {
        setOnReceipt(e.target.value);
    };
    return (
        <div>
            <Header />
            <div className="main_container">
                <div className="header_box">
                    <Typography className="header_text">
                        <ArrowBack
                            className="arrow"
                            onClick={() => {
                                history.push("./Users");
                            }}
                        />Add User
                    </Typography>
                </div>
                <div className="form_container">
                    <div>
                        <div
                            style={{ padding: "20px" }}
                        >
                            <Typography className="details_container_heading">
                                User Details
                            </Typography>
                            <Grid
                                container
                                spacing={3}
                                className="details_container_content"
                            >
                                <Grid item xs={4}>
                                    <Fields.InputField
                                        fieldData={fields.firstName}
                                        inputChangeHandler={inputChangeHandler}
                                        variant="outlined"
                                    />
                                </Grid>
                                <Grid item xs={4}>
                                    <Fields.InputField
                                        fieldData={fields.lastName}
                                        inputChangeHandler={inputChangeHandler}
                                        variant="outlined"
                                    />
                                </Grid>
                            </Grid>
                            <Grid
                                container
                                spacing={3}
                                className="details_container_content"
                            >
                                <Grid item xs={4}>
                                    <Fields.InputField
                                        fieldData={fields.userName}
                                        inputChangeHandler={inputChangeHandler}
                                        variant="outlined"
                                    />
                                </Grid>
                                <Grid item xs={4}>
                                    <Fields.InputField
                                        fieldData={fields.email}
                                        inputChangeHandler={inputChangeHandler}
                                        variant="outlined"
                                    />
                                </Grid>
                            </Grid>
                            <Grid
                                container
                                spacing={3}
                                className="details_container_content"
                            >
                                <Grid item xs={4}>
                                    <Fields.InputField
                                        fieldData={fields.contactNumber}
                                        inputChangeHandler={inputChangeHandler}
                                        variant="outlined"
                                    />
                                </Grid>
                                <Grid item xs={4}>
                                    <Fields.InputField
                                        fieldData={fields.companyName}
                                        inputChangeHandler={inputChangeHandler}
                                        variant="outlined"
                                    />
                                </Grid>

                            </Grid>
                            <Grid
                                container
                                spacing={3}
                                className="details_container_content">
                                <Grid item xs={4}>
                                    <Fields.AutoCompleteField
                                        fieldData={fields.employee}
                                        autoCompleteChangeHandler={autoCompleteChangeHandler} />
                                </Grid>
                                <Grid item xs={4}>
                                    <Fields.InputField
                                        fieldData={fields.empid_userid}
                                        inputChangeHandler={inputChangeHandler}
                                        variant="outlined"
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs={4}>
                                <RadioGroup
                                    // name="use-radio-group"
                                    row aria-label="gender" name="row-radio-buttons-group"
                                    defaultValue={"payTerms"}
                                    onChange={(value) => {
                                        buttonHandler(value);
                                    }}
                                    style={{ marginTop: "25px" }}

                                >
                                    <MyFormControlLabel
                                        value="payTerms"
                                        label="User Active"
                                        control={<Radio />}
                                    />
                                    <MyFormControlLabel
                                        value="receipt"
                                        label="User InActive "
                                        control={<Radio />}
                                    />
                                </RadioGroup>
                            </Grid>
                            <Grid container spacing={3} className="details_container_content">
                                <Grid item xs={8}>
                                    <Grid container spacing={2}>
                                        <Grid item xs={8}></Grid>
                                        <Grid item xs={2}>
                                            <Button
                                                variant="outlined"
                                                className="cancel_button"
                                            >
                                                Cancel
                                            </Button>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <Button
                                                variant="outlined"
                                                className="save_button"
                                            >
                                                Save
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default AddUser;