import React, { useState, useEffect } from "react";
import Header from "../../components/header";
import "../../common.css";
import { Typography, Button, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import * as Fields from "../../sharedComponents";
import AddCircleRoundedIcon from "@mui/icons-material/AddCircleRounded";
import _ from "lodash";
import CloseIcon from "@mui/icons-material/Close";
import * as Components from "../../sharedComponents";
import * as CONFIG from "../../config/GlobalConstants";
import { useHistory } from "react-router-dom";
import CircularLoading from "../../components/loader/circularLoading";
import PhoneAndroid from "@mui/icons-material/PhoneAndroid";
const headRows = [
  { id: "name", disablePadding: true, label: "NAME" },
  { id: "email_id", disablePadding: true, label: "EMAIL ID" },
  { id: "contactnumber", disablePadding: true, label: "CONTACT NUMBER" },
  { id: "empid", disablePadding: true, label: "EMP ID" },
  {
    id: "userrole_designation",
    disablePadding: true,
    label: "USER ROLE/DESIGNATION",
  },
  { id: "groups", disablePadding: true, label: "GROUPS" },
  //   { id: "actions", disablePadding: true, label: "" },
];
const useStyles = makeStyles(() => ({
  addUser_btn: {
    textTransform: "none",
    backgroundColor: "#649B42",
    "&:hover": {
      backgroundColor: "#649B42",
    },
    padding: "8px 15px",
    marginRight: "12px",
  },
  header_buttons: {
    padding: "0px 15px",
  },
  clear_button: {
    padding: "6px 15px",
    background: "#D3710F0D",
    border: "1px solid #D3710F",
    color: "#D3710F",
    "&:hover": {
      background: "#D3710F0D",
    },
  },
  filter_buttons: {
    textAlign: "right",
    padding: "0px 15px",
  },
}));
const filterFields = {
  user: { label: "Search by UserName", name: "user", value: "" },
};
var Service;
const FleetUsers = () => {
  const classes = useStyles();
  const [filters, setFilters] = useState(_.cloneDeep(filterFields));
  const [pagination, setPagination] = useState({
    current: CONFIG.PAGINATION.current,
    pageSize: CONFIG.PAGINATION.pageSize,
  });
  const history = useHistory();
  const [total, setTotal] = useState(0);
  const [tableData, setTableData] = useState([]);
  const [spinner, setSpinner] = useState(false);
  Service = global.service;
  useEffect(() => {
    window.scrollTo({ top: 0 });
    let dataObj = {
      skip: pagination.current * pagination.pageSize,
      limit: pagination.pageSize,
    };
    renderFleetusers(dataObj);
  }, []);
  const renderFleetusers = (dataObj) => {
    setSpinner(true);
    Service.getFleetUsers(dataObj)
      .then((res) => {
        const data = res.feetUsers.map((user) => {
          return {
            name: user.name ? user.name : "--",
            email_id: user.email ? user.email : "--",
            contactnumber: (
              <stack>
                {user.mobile ? (
                  <>
                    <PhoneAndroid
                      color="red"
                      style={{
                        width: "16px",
                        height: "18px",
                        margin: "-2px 5px 0px 0px",
                        color: "#3D7396",
                        verticalAlign: "middle",
                      }}
                    />
                    {user.mobile}
                  </>
                ) : (
                  "--"
                )}
              </stack>
            ),
            empid: user.employeeId ? user.employeeId : "--",
            userrole_designation: user.roleId ? (
              <div>
                {
                  CONFIG.USER_ROLES.filter(
                    (tr) => parseInt(tr.value) === user.roleId
                  )[0].label
                }
              </div>
            ) : (
              "--"
            ),
            groups: user.groupCount ? user.groupCount : "--"
          };
        });
        setTableData(data);
        setTotal(res.totalCount);
        setSpinner(false);
      })
      .catch(() => {
        setSpinner(false);
      });
  };
  const inputChangeHandler = () => { };
  const pageChangeHandler = (page) => {
    let dataObj = {
      skip: page * pagination.pageSize,
      limit: pagination.pageSize,
    };
    renderFleetusers(dataObj);
    setPagination({ ...pagination, current: page });
  };
  const rowsPerPageChangeHandler = (rowsPerPage) => {
    let dataObj = {
      skip: pagination.current * rowsPerPage,
      limit: rowsPerPage,
    };
    renderFleetusers(dataObj);
    setPagination({ ...pagination, pageSize: rowsPerPage });
  };
  return (
    <div>
      <Header />
      <div className="main_container">
        <div className="header_box">
          <Typography className="header_text">Fleet Users</Typography>
        </div>
        {/* <div className="filter_box">
          <Grid container spacing={3}>
            <Grid item xs={9}>
              <Grid container spacing={2}>
                <Grid item xs={3}>
                  <Fields.InputField
                    fieldData={filters.user}
                    inputChangeHandler={inputChangeHandler}
                    className="filter_field"
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={3}>
              <div className={classes.filter_buttons}>
                <Button
                  className={classes.clear_button}
                  startIcon={<CloseIcon />}
                >
                  Clear
                </Button>
              </div>
            </Grid>
          </Grid>
        </div> */}
        <div>
          {spinner ? (
            <CircularLoading />
          ) : (
            <Components.DataTable
              headRows={headRows}
              tableData={tableData}
              pagination={pagination}
              pageChangeHandler={pageChangeHandler}
              rowsPerPageChangeHandler={rowsPerPageChangeHandler}
              total={total}
            />
          )}
        </div>
      </div>
    </div>
  );
};
export default FleetUsers;
