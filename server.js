const express = require('express');
const path = require('path');
const app = express();
const port = 4000;

boot = () => {
    registerRoutes();
    server();
}
registerRoutes = () => {
    app.use(express.static(path.join(__dirname, 'build')));
    app.get('*', function(req, res) {
      res.sendFile(path.join(__dirname, 'build', 'index.html'));
    });
}

server = () => {
    app.listen(port, () => {
        console.log('Listening on Port: ' + port);
    })
}

boot();
